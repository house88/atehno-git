перечислим несколько редиректов простой заменой адресов (не в формате регулярных выражений)
1|1|0|/novosti/kak_oplatit_webmoney|/news/kak-oplatit-webmoney|301
1|1|0|/novosti/novogonie-skidki|/news/novogodnie-skidki|301
1|0|0|/katalog?id=100|/catalog/siemens|301
1|1|0|/articles.htm|/articles|301
1|1|0|/delivery-terms.htm|/section/uslovija-dostavki|301
1|1|0|/misc/bad-products||410
страницы вида .../brendy/некое_слово.html переносим на .../brands/некое_слово через регулярные выражения
1|1|1|'/brendy/([a-z]+)\.html$'i|/brands/$1|301
страниц вида .../manuals/eng/нечто и вида .../docs/pdf/нечто уже не будет на сайте
1|0|1|'/manuals/eng/.+$'i||410
1|0|1|'/docs/pdf/.+$'i||410
страницы вида /secret/некое_число... или ...?secret=нечто закрываем для доступа
1|0|1|'^/secret/[0-9]+'i||403
1|0|1|'\?secret=.+$'i||403
