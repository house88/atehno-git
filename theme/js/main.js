function systemMessages(mess_array, ul_class){
	typeM = typeof mess_array;
	var good_array = [];
	switch(typeM){
		case 'string':
			good_array = [mess_array];
		break;
		case 'array':
			good_array = mess_array;
		break;
		case 'object':
			for(var i in mess_array){
				good_array.push(mess_array[i]);
			}
		break;

	}

	if (!$('.system-text-messages-b').is(':visible')){
		$( '.system-text-messages-b' ).fadeIn('fast');
	}
	var $systMessUl = $('.system-text-messages-b ul');
	$systMessUl.html('');
	for (var li in good_array ) {
		$systMessUl.prepend('<li  class="message-' + ul_class + '">'+ good_array[li] +' <i class="ca-icon ca-icon_remove"></i></li>');
		$systMessUl.children('li').first().addClass('zoomIn').show().delay( 30000 ).slideUp('slow', function(){
			if($systMessUl.children('li').length == 1){
				$systMessUl.closest('.system-text-messages-b').slideUp();
				$(this).remove();
			}else
				$(this).remove();
		});
	}
}

$('body').on("click", '.system-text-messages-b li .fa-times', function(){
	var $li = $(this).closest('li');
	$li.clearQueue();
	$li.slideUp('slow',function(){
		if($('.system-text-messages-b li').length == 1){
			$('.system-text-messages-b').slideUp();
			$li.remove();
		}else
			$li.remove();
	});
});

function showLoader(parentId){
    var $this = $(parentId).children('.ajax-loader');
    if ($this.length > 0)
        $this.show();
    else {
        $(parentId).prepend('<div class="ajax-loader"><div class="wrapper"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>');
    }
}
function hideLoader(parentId){
    var $this = $(parentId).children('.ajax-loader');
    if ($this.length > 0)
        $this.hide();
}

jQuery(function($) {
	
});
