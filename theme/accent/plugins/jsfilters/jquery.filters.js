(function( $ ) { //jquery filters

	var jsFilters = $.fn.jsFilters = function(options) {
		var returnObj = {};

		var _jsFilters = {};

		var _selector = this.selector;

		var _settings = $.extend( {
                'filterContainer' : '.jsfilters-container',
				'container': '.selected-filters-list',
				'ulClass' : 'jsfilter-list',
				'liClass' : 'jsfilter-element',
				'debug' : false,
				'autoApply' : true,
				callBack: function(){},
				onSet: function(callerObj, filterObj){
					//callerObj - object which called the setting operation
					//filterObj - object which describes the activated filter
				},
				onDelete: function(filterObj, callerObj){
					//filterObj - object which describes the deactivated filter
				},
				onApply: function(callerObj){
					//callerObj - object which called the setting operation
				},
				onReset: function(callerObj){
					//callerObj - object which called the setting operation
				}
		});

		var methods = {
			init: function(){
				methods.createUl();

				this.each(function(){
					if (typeof _jsFilters[this.name]  != 'object' ){
						methods.addFilter(this);
					}
				})

				methods.controlButtons();

				methods.addFilterListeners();
			},
            reInit: function(){
                delete _jsFilters;
                _jsFilters = {};
				methods.createUl();

                $(_selector).each(function(){

					if (typeof _jsFilters[this.name]  != 'object'){
						methods.addFilter(this);
					}
				});
            },

			//(private) Create UL as list for filters
			createUl: function(){
				if($(_settings.container).size()){
					$container = $(_settings.container);
					$container.html("<ul class='" + _settings.ulClass + "'></ul>");
				} else{
					// throw new Error('Container should be set in settings!');
				}

			},

			//(private) Add a item(li) on filter list(ul)
			activateFilter: function(filter){
				if(filter.currentValue.value == filter.defaultValue.value)
					return false;

				$ulFilters = $("." + _settings.ulClass);
				switch(filter.type){
					default:
						if(!$ulFilters.find('li.af-'+filter.name).size()){
							// liHTML = '<li class="af-' + filter.name + '">';
							// liHTML += '<div class="btn-group" role="group">';
							// liHTML += '<button type="button" class="btn btn-default btn-xs">' + filter.filterLabel + ': <strong>' + filter.currentValue.text + '</strong></button>';
							// liHTML += '<button type="button" class="btn btn-danger btn-xs dt-filter-delete" data-parent="' + filter.name + '"><span class="ca-icon ca-icon_remove"></span></button>';
							// liHTML += '</div>';
							// liHTML += '</li>';

							liHTML = '<li class="af-' + filter.name + '">\
										<div class="input-group fs-10" role="group">\
											<span class="input-group-addon filter-text">' + filter.filterLabel + ': <strong>' + filter.currentValue.text + '</strong></span>\
											<span type="button" class="input-group-addon btn btn-danger dt-filter-delete" data-parent="' + filter.name + '">\
												<span class="ca-icon ca-icon_remove"></span>\
											</span>\
										</div>\
									</li>';

							$ulFilters.append(liHTML);
						}else{
							$('li.af-'+filter.name + " strong").text(filter.currentValue.text);
						}
					break;
					case 'checkbox':
						var values_text = filter.currentValue.text.split(', ');
						$('li.af-'+filter.name).remove();
						if(values_text.length > 0){
							$.each(values_text, function( index, value_text ) {
								// liHTML = '<li class="af-' + filter.name + '" data-ftext="' + value_text + '">';
								// liHTML += '<div class="btn-group" role="group">';
								// liHTML += '<button type="button" class="btn btn-default btn-xs">' + filter.filterLabel + ': <strong>' + value_text + '</strong></button>';
								// liHTML += '<button type="button" class="btn btn-danger btn-xs dt-filter-delete" data-parent="' + filter.name + '" data-ftext="' + value_text + '"><span class="ca-icon ca-icon_remove"></span></button>';
								// liHTML += '</div>';
								// liHTML += '</li>';
								
								liHTML = '<li class="af-' + filter.name + '" data-ftext="' + value_text + '">\
											<div class="input-group fs-10" role="group">\
												<span class="input-group-addon filter-text">' + filter.filterLabel + ': <strong>' + value_text + '</strong></span>\
												<span type="button" class="input-group-addon btn btn-danger dt-filter-delete" data-parent="' + filter.name + '" data-ftext="' + value_text + '">\
													<span class="ca-icon ca-icon_remove"></span>\
												</span>\
											</div>\
										</li>';

								$ulFilters.append(liHTML);
							});
						}
					break;
				}
			},

			//(private) Remove an item(li) from filter list(ul)
			deactivateFilter: function(filter, filterText){
				if($('li.af-'+filter.name+((filterText != undefined)?'[data-ftext="'+filterText+'"]':'')).size())
					$('li.af-'+filter.name+((filterText != undefined)?'[data-ftext="'+filterText+'"]':'')).remove();
			},

			//(private) Buttons 'Reset' and 'Apply'
			controlButtons: function(){
				$ul = $("." + _settings.ulClass);
				if(!$ul.children('li').size()){
					$ul.siblings().remove();
					return false;
				}

//				if(!$ul.siblings('.dt-filter-reset').size()){
//					aReset = '<a class="dt-filter-reset dt-filter-reset-buttons">Reset</a>';
//					$ul.parent().prepend(aReset);
//				}

				if(!_settings.autoApply && !$ul.siblings('.dt-filter-apply').size()){
					aApply = '<a class="dt-filter-apply dt-filter-apply-buttons">Apply</a>';
					$ul.parent().prepend(aApply);
				}
			},

			//(private) Append class to buttons
			addClassToButtons: function(buttonClass, className){
				$('.' + buttonClass).addClass(className);
			},

			//(private) Append class to buttons
			removeClassFromButtons: function(buttonClass, className){
				$('.' + buttonClass).removeClass(className);
			},
			//(private) Add a filter to the filter collector
			addFilter: function(obj){
				var idFilter;
				if(obj.name != "" && obj.name != undefined)
					idFilter = obj.name;
				else if($(obj).attr('data-name') != "")
					idFilter = $(obj).attr('data-name');
				else
					throw new Error('Object should have "name" or "data-name" attribute');

				if(typeof _jsFilters[idFilter] == 'object')
					return false;

				_jsFilters[idFilter] = {};
				_jsFilters[idFilter].jqObj = $(obj);

				if(obj.name)
					_jsFilters[idFilter].name = obj.name;
				else
					_jsFilters[idFilter].name = _jsFilters[idFilter].jqObj.attr('data-name');

				_jsFilters[idFilter].filterLabel = _jsFilters[idFilter].jqObj.attr('data-title');
				_jsFilters[idFilter].tagName = obj.tagName;

				switch(obj.tagName){
					case 'INPUT':
						_jsFilters[idFilter].type = _jsFilters[idFilter].jqObj.prop('type');
					break;
					case 'SELECT':
				        _jsFilters[idFilter].type = 'select';
					break;
					case 'A':
						_jsFilters[idFilter].type = 'button';
					break;
				}

				_jsFilters[idFilter].defaultValue =  methods.getFilterDefaultVal(_jsFilters[idFilter]);;
				_jsFilters[idFilter].currentValue =  methods.getFilterVal(_jsFilters[idFilter]);
				methods.activateFilter(_jsFilters[idFilter]);
			},

			//(public) Remove an active filter
			removeFilter: function(name){
				methods.toDefault(_jsFilters[name]);
				methods.deactivateFilter(_jsFilters[name]);
				methods.callCallback();
			},

			//(private) Remove one or all filters from
			clearActiveFilters: function(filterName, filterText){
					var activeFilters = {};

					filterName ? activeFilters[filterName] = _jsFilters[filterName] : activeFilters = methods.getActiveFilters()

					for(var obj in activeFilters){
						methods.deactivateFilter(activeFilters[obj], filterText);
						methods.toDefault(activeFilters[obj], filterText);

						_settings.onDelete(methods.getFilter(activeFilters[obj]), _jsFilters[filterName]);

					}

					(_settings.autoApply || !$("." + _settings.ulClass).children('li').size()) && methods.callCallback(true);

					methods.removeClassFromButtons('dt-filter-apply-buttons','active');

					methods.controlButtons();
			},

			//(private) Handler with value of the filter on setting operation
			proccessingFilter: function(obj, new_value, value_text){

				if(obj.name != "" && obj.name != undefined)
					idFilter = obj.name;
				else if($(obj).attr('data-name') != "")
					idFilter = $(obj).attr('data-name');
				else
					throw new Error('Object should have "name" or "data-name" attribute');

				if(_jsFilters[idFilter]){

					var filter = _jsFilters[idFilter];

					if(filter.currentValue.value == new_value)
						return false;

					methods.updateFilter(filter, new_value, value_text);

					filter.currentValue.text = value_text;
					filter.currentValue.value = new_value;

					if(filter.currentValue.value == filter.defaultValue.value/* || filter.currentValue.value == ""*/){
						methods.deactivateFilter(filter);

						(!$("." + _settings.ulClass).children('li').size()) && methods.callCallback(true);
					}else
						methods.activateFilter(filter);

				}else{
					methods.addFilter(obj);
					_jsFilters[idFilter].independent = true;
				}

				methods.callCallback();

				methods.removeClassFromButtons('dt-filter-apply-buttons','active');

				_settings.onSet(obj, methods.getFilter(_jsFilters[idFilter]));

				methods.controlButtons();
			},

			//(private) Handler with value of the filter on updating operation
			updateFilter: function(filter, new_value, value_text){
				if(filter.currentValue.value == new_value)
					return false;

				switch(filter.type){
					case 'select':
                        filter.currentValue.value = new_value;

                        if(filter.jqObj.attr('multiple') != undefined){
                            values = new_value.split('-');
                            var texts = [];

                            for(var v in values){
                                filter.jqObj.children('option[value="' + values[v] +'"]').prop('selected', true);
                                texts.push(filter.jqObj.children('option[value="' + values[v] +'"]').text());
                            }

                            if(texts.length)
                                filter.currentValue.text = texts.join('-');
                            else
                                filter.currentValue.value = new_text;
                        }else{
							var $selectedOption = filter.jqObj.children('option[value="' + filter.currentValue.value +'"]');
							$selectedOption.prop('selected', true);
							filter.currentValue.text = $selectedOption.text();
                        }
					break;
					case 'radio':
                        filter.currentValue.value = new_value;
						var $checkedRadio = $('input[name="' + filter.name + '"][type="radio"][value="' + filter.currentValue.value + '"]' + _selector);
						$checkedRadio.prop('checked',true);

                        if($checkedRadio.data('value-text') == undefined)
							throw new Error('Radio should have attribute "data-value-text" ');

						filter.currentValue.text = $checkedRadio.data('value-text');

					break;
					case 'text':
                        filter.currentValue.value = new_value;
						filter.jqObj.val(filter.currentValue.value);
						filter.currentValue.text = new_value;
					break;
					case 'checkbox':
                        var values = new_value.split('-');
                        var texts = [];

                        for(var v in values){
                            $('input[name="' + filter.name + '"][type="checkbox"][value="' + values[v] +'"]').prop('checked', true);
                            texts.push($('input[name="' + filter.name + '"][type="checkbox"][value="' + values[v] +'"]').data('value-text'));
                        }

						filter.currentValue.value = new_value;

                        if(texts.length)
                            filter.currentValue.text = texts.join('-');
                        else
                            filter.currentValue.value = new_text;
					break;
					case 'button':
                        filter.currentValue.value = new_value;
						if(value_text == undefined)
							throw new Error('Button should have attribute "data-value-text" ');

						filter.currentValue.text = value_text;
						filter.currentValue.value = new_value;
					break;
				}
				methods.activateFilter(filter);
			},

			//(private) Set a filter to default value
			toDefault: function(filter, filterText){
				switch(filter.type){
					case 'select':
						filter.jqObj.children('option[value="' + filter.defaultValue.value +'"]').prop('selected',true).trigger('change');
						filter.currentValue = $.extend({},filter.defaultValue);
					break;
					case 'radio':
						$('input[name="' + filter.name + '"][type="radio"][value="' + filter.defaultValue.value + '"]' + _selector).prop('checked',true).trigger('change');
						filter.currentValue = $.extend({},filter.defaultValue);
					break;
					case 'text':
						filter.jqObj.val(filter.defaultValue.value);
						filter.currentValue = $.extend({},filter.defaultValue);
					break;
					case 'checkbox':					
						var values_text = filter.currentValue.text.split(', ');
						if(values_text.length > 1){
							var filter_values = filter.currentValue.value.split('-');
							var new_values = [];
							var new_texts = [];
							$.each(values_text, function( index, value_text ) {
								if(value_text == filterText){
									$('input[name="' + filter.name + '"][type="checkbox"][data-value-text="'+filterText+'"]' + _selector).prop('checked', false).trigger('change');
								} else{
									new_values.push(filter_values[index]);
									new_texts.push(value_text);
								}
							});
							filter.currentValue = {'value':new_values.join('-'), 'text': new_texts.join(', ')};
						} else{
							$('input[name="' + filter.name + '"][type="checkbox"]' + _selector).prop('checked', false).trigger('change');
							filter.currentValue = $.extend({},filter.defaultValue);
						}
					break;
					case 'button':
						$('a[data-name="' + filter.name + '"].is-active' + _selector).removeClass('is-active');

						if(_jsFilters[filter.name].independent)
							delete _jsFilters[filter.name];
						else
							filter.currentValue = $.extend({},filter.defaultValue);
					break;
				}
			},

			//(private) Add listeners for all elements
			addFilterListeners: function(){

				methods.addListnersRadio();

				methods.addListnersText();

				methods.addListnersCheckbox();

				methods.addListnersA();

				methods.addListnersX();

				methods.addListnersReset();

				methods.addListnersApply();

			},

			//(private) Add listener for RADIO
			addListnersRadio: function(){
				$('input[type="radio"]'+_selector).on('change', function(){
					var jqObj = $(this);

					if(jqObj.data('value-text') == undefined)
						throw new Error('Radio should have attribute "data-value-text"');
						
					new_text = jqObj.data('value-text');
					new_value = jqObj.val();

					methods.proccessingFilter(this, new_value, new_text);
				})
			},

			//(private) Add listener for ETXT
			addListnersText: function(){
				$('input[type=text]'+_selector).on('change', function(e){
					var new_value = $(this).val();
					var new_text = $(this).val();
					methods.proccessingFilter(this, new_value, new_text);
				})
			},

			//(private) Add listener for CHECKBOX
			addListnersCheckbox: function(){
				$('input[type="checkbox"]'+_selector).on('change', function(){
					var new_value, new_text;
					var jqObj = $(this);
                    var texts = [];
                    var values = [];
                    $('input[type="checkbox"][name="' + this.name + '"]:checked').each(function(){
                        $this = $(this);

                        if($this.data('value-text') == undefined)
                            throw new Error('Checkbox should have attribute "data-value-text"');
						
                        texts.push($this.data('value-text'));
                        values.push($this.attr('value'));
                    });
					new_text = texts.join(', ');
					new_value = values.join('-');

					methods.proccessingFilter(this, new_value, new_text);

				})
			},

			//(private) Add listener for A(ADD BUTTONS)
			addListnersA: function(){
				$("body").on('click', "a"+_selector, function(e){
					e.preventDefault();
					var new_value, new_text;
					var jqObj = $(this);
                    var texts = [];
                    var values = [];
					jqObj.toggleClass('is-active');
                    $('a[data-name="' + jqObj.data('name') + '"].is-active').each(function(){
                        $this = $(this);

                        if($this.data('name') == undefined)
							throw new Error('A should have "data-name" attribute');

						if($this.data('value') == undefined)
							throw new Error('A should have "data-value" attribute');

                        texts.push($this.data('value-text'));
                        values.push($this.data('value'));
                    });

					new_text = texts.join(', ');
					new_value = values.join('-');
					methods.proccessingFilter(this, new_value, new_text);
				});
			},

			//(private) Add listener for X(REMOVE BUTTONS)
			addListnersX: function(){
				$("body").on('click', '.dt-filter-delete', function(){
					var filterId = $(this).data('parent');
					var filterText = $(this).data('ftext');
					methods.clearActiveFilters(filterId, filterText);
					//methods.callCallback();
				});
			},

			//(private) Add listener for RESET BUTTON
			addListnersReset: function(){
				$("body").on('click', '.dt-filter-reset-buttons', function(){
					methods.clearActiveFilters();
					//methods.callCallback(true);
					_settings.onReset();
				});
			},

			//(private) Add listener for APPLY BUTTON
			addListnersApply: function(){
				$("body").on('click', '.dt-filter-apply-buttons', function(){
					if($(this).hasClass('active'))
						return false;

					methods.callCallback(true);
					_settings.onApply();
					methods.addClassToButtons('dt-filter-apply-buttons','active');
				});
			},

			//(private) Get current value of the filter
			getFilterVal: function(filter){
				var filterValue = {};

				switch(filter.type){
					case 'select':

                        if(filter.jqObj.attr('multiple') != undefined){
                            var texts = [];
                            var values = [];
                            filter.jqObj.children(_selector + ' option:selected').each(function(){
                                var val_text = $(this).data('value-text');

                                if(val_text == undefined)
                                    val_text = this.text;

                                texts.push(val_text);
                                values.push(this.value);
                            });
                            new_text = texts.join(', ');
                            new_value = values.join('-');
                        }else{
                            var $optionSelected = filter.jqObj.children(_selector + ' option:selected').first();
                            var val_text = $optionSelected.data('value-text');

                            if(val_text == undefined)
                                val_text = $optionSelected.text();

                            new_text = val_text;
                            new_value = $optionSelected.val();
                        }
						filterValue.text = new_text;
						filterValue.value = new_value;

					break;
					case 'radio':
						var $radioChecked = $('input[type="radio"][name="' + filter.name + '"][data-current="true"]' + _selector);

						if(!$radioChecked.size())
							$radioChecked = $('input[type="radio"][name="' + filter.name + '"]:checked' + _selector);

						if(!$radioChecked.size())
							$radioChecked = $('input[type="radio"][name="' + filter.name + '"][data-default="true"]' + _selector);

						if(!$radioChecked.size())
							$radioChecked = $('input[type="radio"][name="' + filter.name + '"][value=""]' + _selector);

						if($radioChecked.size())
							$radioChecked.prop('checked', true);


						filterValue.text = $radioChecked.attr('data-value-text');
						filterValue.value = $radioChecked.val();
					break;
					case 'text':
						filterValue.text = filterValue.value = filter.jqObj.val();
					break;
					case 'checkbox':
                        var texts = [];
                        var values = [];
                        $('input[name="' + filter.name + '"]:checked' + _selector).each(function(){
                            texts.push($(this).data('value-text'));
                            values.push($(this).val());
                        });

                        filterValue.text = texts.length ? texts.join(', ') : "";
                        filterValue.value = values.length ? values.join('-') : "";
					break;
					case 'button':
						var $activeButton = $('a[data-name=' + filter.name + '][data-current=true]' + _selector);

						if(!$activeButton.size())
							$activeButton = filter.jqObj;

						filterValue.text = $activeButton.attr('data-value-text');
						filterValue.value = $activeButton.attr('data-value');
					break;
				}
				return filterValue;
			},

			//(private) Get default value of the filter
			getFilterDefaultVal: function(filter){
				var filterValue = {};

				switch(filter.type){
					case 'select':
                        var $optionsSelected = filter.jqObj.children('option[data-default=true]');

                        if(filter.jqObj.attr('multiple') != undefined){
                            if(!$optionsSelected.size()){
                                filterValue.text = "";
                                filterValue.value = "";
                            }else{
                                var texts = [];
                                var values = [];
                                $optionsSelected.each(function(){
                                    var val_text = $(this).data('value-text');

                                    if(val_text == undefined)
                                        val_text = this.text;

                                    texts.push(val_text);
                                    values.push(this.value);
                                });
                                filterValue.text = texts.join(', ');
                                filterValue.value = values.join('-');
                            }
                        }else{
                            var $optionSelected = $optionsSelected.first();

                            if(!$optionSelected.size())
                                $optionSelected = filter.jqObj.children('option[value=""]').first();

                            if(!$optionSelected.size()){
                                filterValue.text = "";
                                filterValue.value = "";
                            }else{
                                var val_text = $optionSelected.data('value-text');

                                if(val_text == undefined)
                                    val_text = $optionSelected.text();

                                filterValue.text = val_text;
                                filterValue.value = $optionSelected.val();
                            }
                        }
						break;
					case 'radio':
						var $radioChecked = $('input[type="radio"][name="' + filter.name + '"][data-default="true"]' + _selector);

						if(!$radioChecked.size())
							$radioChecked = $('input[type="radio"][name="' + filter.name + '"][value=""]' + _selector).first();

						if(!$radioChecked.size())
							$radioChecked = $('input[type="radio"][name="' + filter.name + '"]' + _selector).first();


						filterValue.text = $radioChecked.attr('data-value-text');
						filterValue.value = $radioChecked.val();
						break;
					case 'text':
						filterValue.text = filterValue.value = "";
						break;
					case 'checkbox':
						filterValue.text = "";
						filterValue.value = "";
						break;
					case 'button':
						var $button = $('a[data-name=' + filter.name + '][data-default=true]' + _selector);

						if(!$button.size())
							$button = $('a[data-name=' + filter.name + '][data-value=""]' + _selector);

						if($button.size()){
							filterValue.text = $button.attr('data-value-text');
							filterValue.value = $button.attr('data-value');
						}else{
							filterValue.text = "";
							filterValue.value = "";
						}
						break;
				}
				return filterValue;
			},

			//(private) Get a public info for the filter
			getFilter: function(filter){
				if(filter == undefined)
					return {};

				return {
						'name'	: filter.name,
						'label'	: filter.filterLabel,
						'tag' 	: filter.tagName,
						'value'	: filter.currentValue.value,
						'default': filter.defaultValue.value
				};
			},

			//(private) Get list of the active filters
			getActiveFilters: function(){
				var returnObj = {};
				for(var obj in _jsFilters){
					if(_jsFilters[obj].currentValue.value != _jsFilters[obj].defaultValue.value)
						returnObj[_jsFilters[obj].name] = _jsFilters[obj];
				}
				return returnObj;
			},

			//(public) Get list of the active filters in format for DataTables
			getFilterDTFormat: function(){
				var returnArray = [];
				var activeFilters = methods.getActiveFilters();

				for(var key in activeFilters)
					returnArray.push({"name":activeFilters[key].name, "value":activeFilters[key].currentValue.value});

				if(!_settings.debug){
					console.log(_jsFilters);
					console.log(returnArray);
				}

				return returnArray;
			},

			//(public) Get list of the active filters in format for DataTables
			getFilterLinkFormat: function(){
				var returnArray = [];
				var activeFilters = methods.getActiveFilters();

				for(var key in activeFilters)
					returnArray.push(activeFilters[key].name+"_"+activeFilters[key].currentValue.value);

				return returnArray.join(':');
			},

			//(private) Caller of main callback
			callCallback: function(forced){
				(forced || _settings.autoApply) && _settings.callBack();
			}

		};

		//Begin
		_settings = $.extend(_settings, options);

		methods.init.apply( this, arguments );

		returnObj.getFilters	= methods.getFilterLinkFormat;
		returnObj.removeFilter 	= methods.removeFilter;
		returnObj.reInit 	    = methods.reInit;
		return returnObj;
	}


})(jQuery);
