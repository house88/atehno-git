var rellax = new Rellax('.rellax', {
	center: true
});

var tradein_callme = function(btn){
	var $this = $(btn);
	var $form = $this.closest('form');
	$.ajax({
		type: "POST",
		dataType:'json',
		url: base_url+'notify/ajax_operations/tradein_callme',
		data: $form.serialize(),
		beforeSend: function(){
			showLoader('body');
			$this.addClass("disabled");
		},
		success: function(resp){
			systemMessages(resp.message, resp.mess_type);
			hideLoader('body');
			if(resp.mess_type == 'success'){
				$form[0].reset();
			} else{
				$this.removeClass("disabled");
			}
		}
	});

	return false;
}

var scrollToTradeInForm  = function(element){
	var $this = $(element);
	var scrollTarget = $this.attr('href');
	var offset = isMobile ? -20 : 70;
	$('html, body').animate({scrollTop: $( scrollTarget ).offset().top - offset}, 500);
	$this.trigger('blur');
}