function systemMessages(mess_array, ul_class){
	typeM = typeof mess_array;
	var good_array = [];
	switch(typeM){
		case 'string':
			good_array = [mess_array];
		break;
		case 'array':
			good_array = mess_array;
		break;
		case 'object':
			for(var i in mess_array){
				good_array.push(mess_array[i]);
			}
		break;

	}

	if (!$('.system-text-messages-b').is(':visible')){
		$( '.system-text-messages-b' ).fadeIn('fast');
	}
	var $systMessUl = $('.system-text-messages-b ul');
	$systMessUl.html('');
	for (var li in good_array ) {
		$systMessUl.prepend('<li  class="message-' + ul_class + '">'+ good_array[li] +' <i class="ca-icon ca-icon_remove"></i></li>');
		$systMessUl.children('li').first().addClass('zoomIn').show().delay( 30000 ).slideUp('slow', function(){
			if($systMessUl.children('li').length == 1){
				$systMessUl.closest('.system-text-messages-b').slideUp();
				$(this).remove();
			}else
				$(this).remove();
		});
	}
}


function showLoader(parentId){
    var $this = $(parentId).children('.ajax-loader');
    if ($this.length > 0)
        $this.show();
    else {
        $(parentId).prepend('<div class="ajax-loader"><div class="wrapper"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>');
    }
}

function showAbsoluteLoader(parentId){
    var $this = $(parentId).children('.ajax-loader.absolute-loader');
    if ($this.length > 0)
        $this.show();
    else {
        $(parentId).prepend('<div class="ajax-loader absolute-loader"><div class="wrapper"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>');
    }
}
function hideLoader(parentId){
    var $this = $(parentId).children('.ajax-loader');
    if ($this.length > 0)
        $this.hide();
}

var productsFilter;
$(function(){
	'use strict';

	$("body").on('click', '.confirm-dialog',function(e){
		var $thisBtn = $(this);
		e.preventDefault();

		var dtype = $thisBtn.data('dtype');
		var bdtype = BootstrapDialog.TYPE_PRIMARY;
		switch (dtype) {
			default:
			case 'default':
				bdtype = BootstrapDialog.TYPE_DEFAULT;
			break;
			case 'info':
				bdtype = BootstrapDialog.TYPE_INFO;
			break;
			case 'primary':
				bdtype = BootstrapDialog.TYPE_PRIMARY;
			break;
			case 'success':
				bdtype = BootstrapDialog.TYPE_SUCCESS;
			break;
			case 'warning':
				bdtype = BootstrapDialog.TYPE_WARNING;
			break;
			case 'danger':
				bdtype = BootstrapDialog.TYPE_DANGER;
			break;
		}
		BootstrapDialog.show({
			title: $thisBtn.data('title'),
			message: $thisBtn.data('message'),
			closable: false,
			draggable: true,
			type: bdtype,
			buttons: [{
				label: 'Да',
				cssClass: 'btn-success',
				action: function(dialogRef){
					var callBack = $thisBtn.data('callback');
					var $button = this;
                    $button.disable();

					window[callBack]($thisBtn);
					dialogRef.close();
				}
			},
			{
				label: 'Отменить',
				cssClass: 'btn-danger',
				action: function(dialogRef){
					dialogRef.close();
				}
			}]
		});
	});

	productsFilter = $('.js_filter').jsFilters({
		'container': '.jsfilters-container',
		callBack: function(){
			var filters = productsFilter.getFilters();
			if(filters != ''){
				filters = '/filter/'+filters;
			}
			var url = filter_url+filters+filter_get_url;
			if(window.history.pushState){
				history.pushState({}, filter_title, url);
			} else{
				window.location.href = url;
			}
			$.ajax({
				type: 'POST',
				url: url,
				dataType: 'JSON',
				data:{},
				success: function(resp){
					if(resp.mess_type == 'success'){						
						$('#catalog-wr').html(resp.catalog_content);

						var filters_available = JSON.parse(resp.filters_available);
						$('#catalog-filters-list .filter[data-property-type="select"]').hide();
						$('#catalog-filters-list .filter[data-property-type="multiselect"]').hide();
						$('#catalog-filters-list .filter[data-property-type="stock"]').hide();
						$('#catalog-filters-list .filter[data-property-type="suppliers"]').hide();
						$('#catalog-filters-list .filter[data-property-type="brands"]').hide();
						$('#catalog-filters-list .filter .filter-list [data-property-value]').hide();
						
						$.each(filters_available, function(id_property_available, property_values_available){
							var $_filter_wrapper = $('#catalog-filters-list .filter[data-property="'+id_property_available+'"]');
							$_filter_wrapper.show();
							$_filter_wrapper.find('.show_more_filters').hide();
							$_filter_wrapper.find('.show_less_filters').hide();

							var visible_values = 0;
							$.each(property_values_available, function(id_value_available, id_value_counter){
								if(intval(id_value_counter) > 0){
									$_filter_wrapper.find('.filter-list [data-property-value="'+id_value_available+'"] .filter-counter').text('('+id_value_counter+')');
									visible_values++;
									if (id_value_counter > 0 && (visible_values <= 6 || id_property_available == 'fb')) {
										$_filter_wrapper.find('.filter-list [data-property-value="'+id_value_available+'"]').show();
									} else{
										$_filter_wrapper.find('.show_more_filters').show();
									}
								}
							});
						});
					}
				}
			});
		},
		onSet: function(callerObj, filterObj){
		},
		onDelete: function(filterObj, callerObj){
			switch(callerObj.type){
				case 'checkbox':
				case 'radio':
					$('input').icheck('update');
				break;
				case 'text':
					var $this=callerObj.jqObj;
					if($this.data('jsfilter-type') == 'range'){
						var min = $this.data('min');
						var max = $this.data('max');
						var $range_slider_input = $this.closest('.jsfilter-range').find('.range-slider');
						$.removeData($range_slider_input, 'fvalue');
						$range_slider_input.removeAttr('data-fvalue');
						$range_slider_input.bootstrapSlider('setValue', [$range_slider_input.data('min'), $range_slider_input.data('max')]);
					}
				break;
			}
		}
	});

	$('input').icheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red'
	});

	$(".range-slider").each(function(){
		var $this = $(this); 
		var min = $this.data('min');
		var max = $this.data('max');
		if($this.data('fvalue') != undefined){
			var value = $this.data('fvalue');
		} else{
			var value = [min, max];
		}
		$this.bootstrapSlider({ 
			min: min, 
			max: max, 
			value: value, 
			focus: true 
		}).on('slideStop', function(ev){
			$this.closest('.jsfilter-range').find('input.js_filter').val(ev.value.join('-')).change();
		});
	});

	$('body').on("click", '.system-text-messages-b li .ca-icon_remove', function(){
		var $li = $(this).closest('li');
		$li.clearQueue();
		$li.slideUp('slow',function(){
			if($('.system-text-messages-b li').length == 1){
				$('.system-text-messages-b').slideUp();
				$li.remove();
			}else
				$li.remove();
		});
	});

	$(document).on('click', '.header-search_form-shadow', function(e){
		reset_search();
	});

	$('.nav-catalog-wr').on('click', 'a', function(e){
		if($(this).attr('href') === '#'){
			e.preventDefault();
		}
	});

	$('.catalog-menu-link').on('mouseover', function(e){
		$('.nav-catalog_categories').removeClass('fadeOut').addClass('fadeIn');
	}).on('mouseleave', function(e){
		$('.nav-catalog_categories').removeClass('fadeIn').addClass('fadeOut');
	});

	$('.nav-catalog_categories > ul > li').on('mouseover', function(e){
		$(this).find('.nav-catalog_categories-right').addClass('animated').removeClass('fadeOut').addClass('fadeIn');
	}).on('mouseleave', function(e){
		$(this).find('.nav-catalog_categories-right').removeClass('animated').removeClass('fadeIn').addClass('fadeOut');
	});

	$('.nav-catalog_categories > ul > li > .nav-catalog_categories-right > ul').slimscroll({
		width: '200px',
		height: '100%',
		railVisible: true,
    	disableFadeOut: false
	});

	$('#header-nav-mobile ul.navbar-nav > li > a').on('click', function(e){
		if($(this).attr('href') == '#'){
			e.preventDefault();
		}

		e.stopPropagation();

		$(this).parent('li').siblings().removeClass('active');
		$(this).parent('li').toggleClass('active');
	});

	$('#header-nav-mobile ul.navbar-nav > li > ul > li.nav-catalog_submenu-toggle').on('click', function(e){
		e.stopPropagation();
		$(this).siblings().removeClass('active');
		$(this).toggleClass('active');
	});

	$('#header-nav-mobile .navbar').on('show.bs.collapse', function () {
		var actives = $(this).find('.collapse.in'),
			hasData;

		if (actives && actives.length) {
			hasData = actives.data('collapse')
			if (hasData && hasData.transitioning) return
			actives.collapse('hide')
			hasData || actives.data('collapse', null)
		}
	});

	$(document).on('show.bs.tab', '.nav-tabs-responsive [data-toggle="tab"]', function(e) {
		var $target = $(e.target);
		var $tabs = $target.closest('.nav-tabs-responsive');
		var $current = $target.closest('li');
		var $next = $current.next();
		var $prev = $current.prev();

		$tabs.find('>li').removeClass('next prev');
		$prev.addClass('prev');
		$next.addClass('next');
	});

	$('.nav-pills, .nav-tabs').tabdrop({
		'text' : '<i class="ca-icon ca-icon_more"></i>'
	});

	/*------------------------------
		GRID/LIST TOGGLE
	------------------------------*/
	$('.toggle-product_view').click(function(e) {
		$('.toggle-product_view').removeClass('active');
        $(this).addClass('active');
		var view_type = $(this).data('type');
		$('#products').fadeOut(300, function() {
			$(this).removeClass('grid list small-list').addClass(view_type).fadeIn(300);
		});
		$.cookie('_product_view', view_type, { expires: 7 , path: '/'});
    });

	/*------------------------------
		SCROLL FUNCTION
	------------------------------*/
	function scrollToObj(target, offset, time) {
		$('html, body').animate({scrollTop: $( target ).offset().top - offset}, time);
	}

	$("a.scroll[href^='#']").click(function(){
		scrollToObj($.attr(this, 'href'), 80, 1000);
		return false;
	});

	$("#scrolltop").click(function() {
		scrollToObj('body',0, 1000);
    });

	$('.modal-form-container .login-form-link').click(function(e) {
		$(".modal-form-container #login-form").delay(100).fadeIn(100);
		$(".modal-form-container #register-form").fadeOut(100);
		$(".modal-form-container #forgot-form").fadeOut(100);
		e.preventDefault();
	});
	$('.modal-form-container .register-form-link').click(function(e) {
		$(".modal-form-container #register-form").delay(100).fadeIn(100);
		$(".modal-form-container #login-form").fadeOut(100);
		$(".modal-form-container #forgot-form").fadeOut(100);
		e.preventDefault();
	});
	$('.modal-form-container .forgot-form-link').click(function(e) {
		$(".modal-form-container #forgot-form").delay(100).fadeIn(100);
		$(".modal-form-container #register-form").fadeOut(100);
		$(".modal-form-container #login-form").fadeOut(100);
		e.preventDefault();
	});

	$('body').on('click touchend', ".call-systmess", function(e){
		e.preventDefault();
		var $thisBtn = $(this);
		var mess = $thisBtn.data('message');
		var type = $thisBtn.data('type');
		systemMessages(mess, type);

		return false;
	});

	$('body').on('click touchend', ".call-function", function(e){
		e.preventDefault();
		var $thisBtn = $(this);
		if($thisBtn.hasClass('disabled')){
			return false;
		}
		
		var callBack = $thisBtn.data('callback');
		window[callBack]($thisBtn);
		return false;
	});
	

	$('body').on('click touchend', ".call-popup", function(e){
		e.preventDefault();
		var $this = $(this);
		if($this.hasClass('disabled')){
			return false;
		}

		var popup_url = $this.data('href');
		var $popup_parent = $($this.data('popup'));
		var formdata = {};
		if($this.data('formdata-callback') != undefined){
			formdata = window[$this.data('formdata-callback')]($this);
		}
		$.ajax({
			type: 'POST',
			url: popup_url,
			data: formdata,
			dataType: 'JSON',
			beforeSend: function(){},
			success: function(resp){
				if(resp.mess_type == 'success'){
					$popup_parent.html(resp.popup_content).modal('show');
				} else{
					systemMessages( resp.message, resp.mess_type );
				}
			}
		});
		return false;
	});

	set_user_city();

	$('#cities-modal span.city-wr').on('click', function(){
		var id_city = intval($(this).data('city'));
		if(existCookie('_at_city') && $.cookie('_at_city') == id_city){
			id_city = 0;
		}

		$.ajax({
			type: 'POST',
			url: base_url + 'users/ajax_operations/update_city',
			data: {id_city:id_city},
			dataType: 'JSON',
			beforeSend: function(){
				showLoader('body');
				$('#cities-modal').modal('hide');
			},
			success: function(resp){
				if(resp.mess_type == 'success'){
					location.reload(true);
				}
			}
		});
	});

	var _product_view_type = 'grid';
	if(existCookie('_product_view')){
		_product_view_type = $.cookie('_product_view');
	} else{		
		$.cookie('_product_view', 'grid', { expires: 7 , path: '/'});
	}
	$('.toggle-display_wr > .toggle_item').removeClass('active');
	$('.toggle-display_wr > .toggle_item[data-type="'+_product_view_type+'"]').addClass('active');
	$('.products-list').fadeOut(300, function() {
		$(this).removeClass('grid list small-list').addClass(_product_view_type).fadeIn(300);
	});

	update_cart();

	// Tooltips
	$('.h-tooltip').each(function () {
		$(this).tooltip({
			html: true,
			title: $('#' + $(this).data('tip')).html()
		});
	});
	$('.h-tooltip-click').click(function () {
		$(this).tooltip({
			html: true,
			title: $('#' + $(this).data('tip')).html()
		}).tooltip('toggle');
	});

	$('body').on('mouseover mouseenter', '*[title]:not(iframe)', function(){
		if(!$(this).hasClass('tooltipstered')){
			$(this).tooltipster();
		}
		$(this).tooltipster('show');
    });
	
	$('.header-search_form input[name="search"]').on("input", function(ev) { 
		var $this = $(this);
		clearTimeout(typingTimer);
		typingTimer = setTimeout(function(){
			doLiveSearch(ev, $this.val());
		}, doneTypingInterval);
	}).on('focus', function(ev) {
		if (ev.keyCode != 13) {
			$('body').addClass('search-active');
			$(this).addClass('active');
			$('.header-search_form-shadow').fadeIn();
			doLiveSearch(ev, this.value);
		}
	});
	
	$('.mheader-search_form input[name="search"]').on("input change", function(ev) { 
		var $this = $(this);
		clearTimeout(typingTimer);
		typingTimer = setTimeout(function(){
			doLiveSearch(ev, $this.val());
		}, doneTypingInterval);
	}).on('focus', function(ev) {
		if (ev.keyCode != 13) {
			$('body').addClass('search-active');
			$(this).addClass('active');
			$('.mheader-search_form-shadow').fadeIn();
			doLiveSearch(ev, this.value);
		}
	});
});

var show_more_filters = function(element){
	var $this = $(element);
	var $_filter_wrapper = $this.closest('.filter');
	
	$this.hide();
	$_filter_wrapper.find('.show_less_filters').hide();

	$_property_values = $_filter_wrapper.find('.filter-list li');
	$_property_values.each(function(index){
		var value_counter = $(this).find('.filter-counter').text();
		if(value_counter != '(0)'){
			$(this).show();
		}
	});
	$_filter_wrapper.find('.show_less_filters').show();
}

var show_less_filters = function(element){
	var $this = $(element);
	var $_filter_wrapper = $this.closest('.filter');
	var $_property_values = $_filter_wrapper.find('.filter-list li');
	
	$this.hide();
	$_filter_wrapper.find('.show_more_filters').hide();
	$_property_values.hide();
	
	var visible_values = 0;
	$_property_values.each(function(index){
		var value_counter = $(this).find('.filter-counter').text();
		if(value_counter != '(0)'){
			visible_values++;
			if (visible_values <= 6) {
				$(this).show();
			}
		}
	});
	
	if (visible_values >= 6) {
		$_filter_wrapper.find('.show_more_filters').show();
	}
}

var isMobile = $(window).width() <= 1030;
var typingTimer;
var doneTypingInterval = 300;

function dropdown(obj, link, after_link){
	//alert(after_link)
	if(after_link == undefined || after_link == null)
		after_link = '';

	if(after_link != '')
		after_link = after_link + '&' + obj.name+'='+obj.value
	else
		after_link = '?' + obj.name+'='+obj.value

	window.location.replace(link+after_link);
}

var toggle_product_view = function(btn){
	var $this = $(btn);		
	var view_type = $this.data('type');
	$this.addClass('active').siblings('.toggle_item').removeClass('active');
	$('#products').fadeOut(300, function() {
		$(this).removeClass('grid list small-list').addClass(view_type).fadeIn(300);
	});
	$.cookie('_product_view', view_type, { expires: 7 , path: '/'});
}

/*------------------------------
	PRODUCT QUANTITY
------------------------------*/
function change_qty(btn){
	var $this = $(btn);
	var action = $this.data('action');
	var $parent_b = $this.closest('.product-quantity');
	var $input_qty = $parent_b.find('input.item_quantity');
	var temp = parseInt($input_qty.val());

	switch (action) {
		case 'plus':
			new_temp = temp + 1;
		break;
		case 'minus':
			if(temp > 1) {
				new_temp = temp - 1;
			} else{
				new_temp = temp;
			}
		break;
	}

	if($this.data('type') == 'basket' && new_temp != temp){
		var item = $this.data('item');
		$.ajax({
			type: 'POST',
			url: base_url+'cart/ajax_operations/change_quantity',
			dataType: 'JSON',
			data:{item:item, quantity:new_temp},
			beforeSend: function(){
				showLoader('body');
			},
			success: function(resp){
				if(resp.mess_type == 'success'){
					location.reload(true);
				}
			}
		});
	}

	$input_qty.val(new_temp);
}

function change_qty_input(input){
	var $this = $(input);
	var quantity = intval($this.val());
	var item = $this.data('item');
	$.ajax({
		type: 'POST',
		url: base_url+'cart/ajax_operations/change_quantity',
		dataType: 'JSON',
		data:{item:item, quantity:quantity},
		beforeSend: function(){
			showLoader('body');
		},
		success: function(resp){
			if(resp.mess_type == 'success'){
				location.reload(true);
			} else{
				hideLoader('body');
				systemMessages(resp.message, resp.mess_type);
			}
		}
	});
}

var add_to_cart_credit = function(btn){
	var $this = $(btn);
	var $form = $this.closest('form');
	var item = $form.find('input[name="item"]').val();
	var months = $form.find('input[name="credit_months"]').val();
	$.ajax({
		type: 'POST',
		url: base_url+'cart/ajax_operations/add',
		dataType: 'JSON',
		data:{item:item, quantity:1, months:months},
		beforeSend: function(){
			showLoader('body');
		},
		success: function(resp){
			if(resp.mess_type == 'success'){
				window.location.href = base_url+'cart';
			} else{
				hideLoader('body');
				systemMessages(resp.message, resp.mess_type);
			}
		}
	});
}

var add_to_cart = function(btn){
	var $this = $(btn);
	var item = $this.data('item');
	var quantity = 1;
	var $small_cart_parent = $('#collapse-cart');
	var small_cart_open = false;
	if($small_cart_parent.find('.cart-item').length == 0){
		small_cart_open = true;
	}

	if($('input[name="item_'+item+'_quantity"]').length){
		quantity = $('input[name="item_'+item+'_quantity"]').val();
	}

	$.ajax({
		type: 'POST',
		url: base_url+'cart/ajax_operations/add',
		dataType: 'JSON',
		data:{item:item, quantity:quantity},
		beforeSend: function(){
			showLoader('body');
		},
		success: function(resp){
			if(resp.mess_type == 'success'){
				update_cart(small_cart_open);
			} else{
				systemMessages(resp.message, resp.mess_type);
			}
			hideLoader('body');
		}
	});
}

var update_big_cart = false;
var remove_from_cart = function(btn){
	var $this = $(btn);
	var item = $this.data('item');
	var small_cart_open = false;
	$.ajax({
		type: 'POST',
		url: base_url+'cart/ajax_operations/delete',
		dataType: 'JSON',
		data:{item:item},
		beforeSend: function(){
			showLoader('body');
		},
		success: function(resp){
			if(resp.mess_type == 'success'){
				if(update_big_cart){
					location.reload(true);
				} else{
					update_cart(small_cart_open);
				}
			}
			hideLoader('body');
		}
	});
}

var remove_from_big_cart = function(btn){
	var $this = $(btn);
	var item = $this.data('item');
	$.ajax({
		type: 'POST',
		url: base_url+'cart/ajax_operations/delete',
		dataType: 'JSON',
		data:{item:item},
		beforeSend: function(){
			showLoader('body');
		},
		success: function(resp){
			if(resp.mess_type == 'success'){
				location.reload(true);
			}
		}
	});
}

var create_order = function(btn){
	var $this = $(btn);
	var $form = $this.closest('form.basket_form');

	$.ajax({
		type: 'POST',
		url: base_url+'orders/ajax_operations/add',
		dataType: 'JSON',
		data:$form.serialize(),
		beforeSend: function(){
			showLoader('body');
		},
		success: function(resp){
			if(resp.mess_type == 'success'){
				window.location.href = resp.redirect_to;
			} else{
				systemMessages(resp.message, resp.mess_type);
			}
			hideLoader('body');
		}
	});
}

var order_card_pay = function(btn){
	var $this = $(btn);
	var $form_container = $('#js-payment-form-container');

	$.ajax({
		type: 'POST',
		url: base_url+'payment/ajax_operations/get_form',
		dataType: 'JSON',
		data:{token:$this.data('order')},
		beforeSend: function(){
			showLoader('body');
		},
		success: function(resp){
			if(resp.mess_type == 'success'){
				$form_container.html(resp.form);
				$form_container.find('form').submit();
			} else{
				systemMessages(resp.message, resp.mess_type);
				hideLoader('body');
			}
		}
	});
}

function update_cart(small_cart_open){
	$.ajax({
		type: 'POST',
		url: base_url+'cart/ajax_operations/update',
		dataType: 'JSON',
		success: function(resp){
			if(resp.mess_type == 'success'){
				$('.wr-cart-items').slimScroll({destroy: true});
				$('.wr-cart-items').html(resp.cart_content);
				$('.cart_count').text(resp.cart_count);
				if(resp.cart_count > 0){
					$('.cart_count').show();
					$('.cart-description').html('<div class="cart-full">Товаров: '+resp.cart_count+'</div><div class="cart-price">'+resp.cart_price+'</div>');	
					$('.wr-cart-items').slimscroll({
						position: 'left',
						height: '100%',
						railVisible: true,
						disableFadeOut: false
					});
					if(small_cart_open == true){
						$('#collapse-cart').collapse('show');
					}
				} else{
					$('.cart-description').html('<div class="cart-empty">Пусто :(</div>');					
				}
			}
		}
	});
}

var cart_apply_pcode = function(btn){
	var $this = $(btn);
	var $pcode_input = $this.closest('.input-group').find('input[name="pcode"]');
	var pcode = $pcode_input.val();
	if(pcode == ''){
		return false;
	}

	$.ajax({
		type: 'POST',
		url: base_url+'cart/ajax_operations/apply_pcode',
		dataType: 'JSON',
		data: {pcode:pcode},
		success: function(resp){
			if(resp.mess_type == 'success'){
				location.reload(true);
			} else{
				systemMessages(resp.message, resp.mess_type);
				$pcode_input.val('');
			}
		}
	});
}

function set_user_city(){
	if(existCookie('_at_city')){
		var $city_el = $('#cities-modal').find('span.city-wr[data-city="'+$.cookie('_at_city')+'"]');
		$city_el.addClass('active');
		$('.header-info_phone').find('.city_name').text($city_el.text());
		$('#cities-modal').modal('hide');
	}
}

var update_personal_settings = function(btn){
	var $this = $(btn);
	var $form = $this.closest('form');
	$.ajax({
		type: 'POST',
		url: base_url+'users/ajax_operations/update',
		dataType: 'JSON',
		data: $form.serialize(),
		beforeSend: function(){
			showLoader('body');
		},
		success: function(resp){
			systemMessages(resp.message, resp.mess_type);
			set_user_city();
			hideLoader('body');
		}
	});
}

var update_personal_password = function(btn){
	var $this = $(btn);
	var $form = $this.closest('form');
	$.ajax({
		type: 'POST',
		url: base_url+'users/ajax_operations/change_password',
		dataType: 'JSON',
		data: $form.serialize(),
		beforeSend: function(){
			showLoader('body');
		},
		success: function(resp){
			systemMessages(resp.message, resp.mess_type);
			if(resp.mess_type == 'success'){
				$form[0].reset();
			}
			hideLoader('body');
		}
	});
}

/*------------------------------
	LIVE SEARCH
------------------------------*/
function doLiveSearch(ev, keywords) {
    if (ev.keyCode == 38 || ev.keyCode == 40) {
        return false;
    }

    if (keywords == '' || keywords.length < 2) {
        return false;
    }
	
    $.ajax({
        url: base_url+'search/catalog',
		type: 'GET',
        dataType: 'JSON',
		data: {keywords:keywords,mode:'simple'},
		beforeSend: function(){
			if(!isMobile){
				showAbsoluteLoader('.header-search_form .search-results');
			} else{
				showAbsoluteLoader('.mheader-search_form .msearch-results');
			}
		},
        success: function(resp) {
			if(!isMobile){
				hideLoader('.header-search_form .search-results');
				if(resp.mess_type == 'success'){
					$('.search-results').fadeIn();
					$('.header-search_form .search-icon').attr('href', base_url+'search/catalog?keywords='+encodeURI(keywords));
					$('.header-search_form #search_result_products').html(resp.products);
					$('.header-search_form #search_result_categories').html(resp.categories);
					if(resp.products_found == 0){
						$('.header-search_form .col-delimiter').hide();
					} else{
						$('.header-search_form .col-delimiter').show();
					}
					
					$('.header-search_form #search_result_categories > ul').slimscroll({
						height: '315px',
						railVisible: true,
						disableFadeOut: true
					});
				}
			} else{
				hideLoader('.mheader-search_form .msearch-results');
				if(resp.mess_type == 'success'){
					$('.msearch-results').fadeIn();
					$('.mheader-search_form .msearch-direct').attr('href', base_url+'search/catalog?keywords='+encodeURI(keywords));
					$('.mheader-search_form #msearch_result_products').html(resp.products);
					$('.mheader-search_form #msearch_result_categories').html(resp.categories);
				}
			}
        }
    });
    return true;
}

/*------------------------------
	WINDOW RESIZE
------------------------------*/
$(window).resize(function(){
	if($(window).width() > 1013){
		$('.filters-wr').show();
		$('.toggle-filter_wr').hide();
		isMobile = false;
	} else{
		$('.filters-wr').hide();
		$('.toggle-filter_wr').show();
		isMobile = true;
	}
	$('body').removeClass('no-scroll');
	// reset_search();
});


var show_auth_popup = function(btn){
	var $this = $(btn);
	var $target = $($this.data('target'));
	var action = $($this.data('action'));
	$(action).trigger('click');
	$target.modal('show');
}

var show_dimmer = function(btn){
	var $this = $(btn);
	$('.filters-wr').fadeIn();
	$('body').addClass('no-scroll');
}

var close_dimmer = function(btn){
	var $this = $(btn);
	$('.filters-wr').fadeOut();
	$('body').removeClass('no-scroll');
}
var show_search_dimmer = function(btn){
	var $this = $(btn);
	$('.mobile-search-wr').fadeIn();
	$('body').addClass('no-scroll');
}

var close_search_dimmer = function(btn){
	var $this = $(btn);
	$('.mobile-search-wr').fadeOut();
	$('.mobile-search-wr input[name="search"]').removeClass('active').val('');
	$('.mobile-search-wr .msearch-results').fadeOut();		
	$('.mobile-search-wr #msearch_result_products').html('');
	$('.mobile-search-wr #msearch_result_categories').html('');
	$('body').removeClass('no-scroll');
}

function reset_search(){
	// RESET MOBILE
	$('.mobile-search-wr').hide();
	$('.mobile-search-wr input[name="search"]').removeClass('active').val('');
	$('.mobile-search-wr .msearch-results').fadeOut();		
	$('.mobile-search-wr #msearch_result_products').html('');
	$('.mobile-search-wr #msearch_result_categories').html('');
	
	// RESET DESKTOP
	$('.header-search_form input[name="search"]').removeClass('active').val('');
	$('.search-results').fadeOut();		
	$('.header-search_form #search_result_products').html('');
	$('.header-search_form #search_result_categories').html('');
	$('.header-search_form-shadow').fadeOut();	
	
	$('body').removeClass('search-active');
	$('body').removeClass('no-scroll');
}

var msearch_init = function(btn){
	var $this = $(btn);
	if($('.navbar-collapse-search').hasClass('in')){
		$('.mheader-search_form-shadow').fadeOut();
	} else{
		$('.mheader-search_form-shadow').fadeIn();
	}	
	$('.navbar-collapse-search').collapse('toggle');
}

var sort_products = function(btn){
	var $this = $(btn);
	var sort_by = $this.data('sortby');
	window.location.search = '?sort_by=' + sort_by;
}

/*------------------------------
	WINDOW SCROLL
------------------------------*/
$(window).scroll(function(){
	/*------------------------------
		FIXED NAVBAR
	------------------------------*/
	if($(window).scrollTop() > 160) {
		if($('#header-navigation').hasClass("nav-static-top")) {
			$('#header-navigation').removeClass('nav-static-top').addClass('nav-fixed-top');
			$('#header-navigation').addClass('fadeIn');
			$('#header .header-logo').hide();
			$('#header-navigation .nav-header-logo').show();
			$('body').css("padding-top","70px");
			$('#header-navigation').css("z-index","132");
		}
	}else {
		$('#header-navigation').removeClass('nav-fixed-top').addClass('nav-static-top');
		$('#header-navigation').removeClass('fadeIn');
		$('#header .header-logo').show();
		$('#header-navigation .nav-header-logo').hide();
		$('body').css("padding-top","0px");
		$('#header-navigation').css("z-index","100");
	}
	/*------------------------------
		SCROLL TOP
	------------------------------*/
	if($(window).scrollTop() > 300) {
		$("#scrolltop").addClass("in");
	}
	else {
		$("#scrolltop").removeClass("in");
	}
});

function existCookie(cookie_name){
	if($.cookie(cookie_name) == undefined){
        return false;
    } else{
        return true;
    }
}

function number_format(num_value, view_decimals){
	if(view_decimals){		
		return (round_up(num_value, 2)).toFixed(2);
	} else{
		return Math.ceil(num_value).toFixed();
	}
}

function round_up(num, precision) {
	precision = Math.pow(10, precision);
	return Math.ceil(num * precision) / precision;
}

function intval(num){
	if (typeof num == 'number' || typeof num == 'string'){
		num = num.toString();
		var dotLocation = num.indexOf('.');
		if (dotLocation > 0){
			num = num.substr(0, dotLocation);
		}

		if (isNaN(Number(num))){
			num = parseInt(num);
		}

		if (isNaN(num)){
			return 0;
		}

		return Number(num);
	} else if (typeof num == 'object' && num.length != null && num.length > 0){
		return 1;
	} else if (typeof num == 'boolean' && num === true){
		return 1;
	}

	return 0;
}

function floatval(mixed_var) {
	return (parseFloat(mixed_var) || 0);
}


var popup_share = function(obj){
	var $this = $(obj);
	var popupUrl = '';
	var popupTitle = $this.data('title');
	var popupOptions = 'width=550,height=400,0,status=0';
	var socialUrl = $this.data('url');
    var socialTitle = cleanInput($this.data('title'));
    switch ($this.data('social')) {
        case 'facebook':
            popupUrl += 'http://www.facebook.com/share.php?u=' + socialUrl + '&title=' + socialTitle;
        break;
		case 'vk':
			popupUrl += 'https://vk.com/share.php?url=' + socialUrl;
		break;
		case 'ok':
			popupUrl += 'http://www.ok.ru/dk?st.cmd=addShare&st.s=1&st._surl=' + socialUrl + '&st.comments=' + socialTitle;
		break;
	}

	winPopup(popupUrl, popupTitle, popupOptions);
}

function winPopup(mylink,title,options) {
	var mywin = window.open(mylink,title,options);
	mywin.focus();

	return mywin;
}

function cleanInput(str){
	if(str != undefined && str != '' && str!= null){
		str = str.replace(/&/g, "&amp;");
		str = str.replace(/>/g, "&gt;");
		str = str.replace(/</g, "&lt;");
		str = str.replace(/"/g, "&quot;");
		str = str.replace(/'/g, "&#039;");
		return str;
	} else{
		return false;
	}
}