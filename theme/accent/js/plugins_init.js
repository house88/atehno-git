$(function(){
    $('.home_top-slider').bxSlider({
        adaptiveHeight: true,
        auto: true,
        autoStart: true,
        autoDirection: 'next',
        autoHover: true,
        pause: 5000,
        pager: true,
        mode: 'fade',
        nextText: '<i class="ca-icon ca-icon_arrow-right "></i>',
        prevText: '<i class="ca-icon ca-icon_arrow-left "></i>',
        touchEnabled:false
    });
    
    $('.newest_products-slider').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        prevArrow:'<a class="slick-slide-prev" href=""><i class="ca-icon ca-icon_arrow-left "></i></a>',
        nextArrow:'<a class="slick-slide-next" href=""><i class="ca-icon ca-icon_arrow-right "></i></a>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 425,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('.popular_products-slider').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        prevArrow:'<a class="slick-slide-prev" href=""><i class="ca-icon ca-icon_arrow-left "></i></a>',
        nextArrow:'<a class="slick-slide-next" href=""><i class="ca-icon ca-icon_arrow-right "></i></a>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 425,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
});