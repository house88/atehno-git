/*------------------------------
	CONCURS ACTIONS
------------------------------*/
$(function(){
	'use strict';

	if(id_concurs){
		$('#select_concurs_photo').fileupload({
			url: base_url+'concurs/ajax_operations/upload_file',
			dataType: 'json',
			formData:{concurs:id_concurs},
			beforeSend: function(){
				showAbsoluteLoader('.concurs_btn_upload');
			},
			done: function (e, data) {
				if(data.result.mess_type != 'success'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
					var template = '<div class="concurs_uploaded_photo-item" style="background-image:url('+base_url+data.result.file.path+data.result.file.name+');"></div>\
									<div class="btn-group">\
										<a href="#" class="btn btn-danger btn-xs pull-right call-function" data-callback="concurs_remove_file" title="Удалить" data-title="Удалить" data-file="'+data.result.file.name+'" data-concurs="'+id_concurs+'">\
											<span class="ca-icon ca-icon_remove"></span>\
										</a>\
										<a href="#" class="btn btn-success btn-xs pull-right call-function" data-callback="concurs_save_file" title="Сохранить" data-title="Сохранить" data-file="'+data.result.file.name+'" data-concurs="'+id_concurs+'">\
											<span class="ca-icon ca-icon_ok"></span>\
										</a>\
									</div>';
					$('.concurs_btn_upload').hide();
					$('.concurs_uploaded_photo-wr').html(template).fadeIn('slow');
				}
				hideLoader('.concurs_btn_upload');
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');
	}
});

var concurs_remove_file = function(obj){
	var $this = $(obj);
	var temp_file = $this.data('file');
	$.ajax({
		type: 'POST',
		url: base_url+'concurs/ajax_operations/remove_file',
		data: {temp_file:temp_file,concurs:id_concurs},
		dataType: 'JSON',
		success: function(resp){
			if(resp.mess_type == 'success'){
				$('.concurs_uploaded_photo-wr').hide().html('');
				$('.concurs_btn_upload').fadeIn('slow');
			} else{
				systemMessages(resp.message, resp.mess_type);
			}
		}
	});
	return false;
}
		
var concurs_save_file = function(obj){
	var $this = $(obj);
	var temp_file = $this.data('file');
	$.ajax({
		type: 'POST',
		url: base_url+'concurs/ajax_operations/save_file',
		data: {temp_file:temp_file,concurs:id_concurs},
		dataType: 'JSON',
		beforeSend: function(){
			showAbsoluteLoader('.concurs_btn_upload');
		},
		success: function(resp){
			systemMessages(resp.message, resp.mess_type);
			if(resp.mess_type == 'success'){
				$('.concurs_uploaded_photo-wr').closest('.square-b').parent().remove();
			}
			hideLoader('.concurs_btn_upload');
		}
	});
	return false;
}
		
var concurs_vote = function(obj){
	var $this = $(obj);
	var vote = $this.data('vote');
	$.ajax({
		type: 'POST',
		url: base_url+'concurs/ajax_operations/concurs_vote',
		data: {vote:vote},
		dataType: 'JSON',
		success: function(resp){
			systemMessages(resp.message, resp.mess_type);
			if(resp.mess_type == 'success'){
				location.reload(true);
			}
		}
	});
	return false;
}