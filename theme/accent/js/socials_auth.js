var ok_login = function(obj){
    var $this = $(obj);
    var link = $this.data('link');
    showLoader('body');
    window.location.href = link;
}

var vk_login = function(obj){
    var $this = $(obj);
    var link = $this.data('link');
    showLoader('body');
    window.location.href = link;
}

var fb_login = function(obj){
    var $this = $(obj);

    FB.login(function(response) {
		if (response.authResponse) {
			statusChangeCallback(response);
		}
	}, {scope: 'email,public_profile', return_scopes: true});
}

function statusChangeCallback(response) {
    if (response.status === 'connected') {
        $.ajax({
            type: 'POST',
            url: base_url+'auth/ajax_operations/fb_signin',
            data: response,
            dataType: 'JSON',
            beforeSend: function(){
                showLoader('body');
            },
            success: function(resp){
                if(resp.mess_type == 'success'){
                    location.reload(true);
                } else{
                    systemMessages(resp.message, resp.mess_type);
                    hideLoader('body');
                }
            }
        });
    } else{
        hideLoader('body');
    }
}

function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}

window.fbAsyncInit = function() {
    FB.init({
        appId      : '176649066443991',
        autoLogAppEvents : false,
        cookie     : true,  // enable cookies to allow the server to access the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v3.0' // use graph api version 2.12
    });
};

(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = 'https://connect.facebook.net/ru_RU/sdk.js';
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));