$(function(){    
    $('body').on('click', ".call-function:not(.disabled)", function(e){
		e.preventDefault();
        
        var $thisBtn = $(this);
        var callBack = $thisBtn.data('callback');
        
		callFunction(callBack, $thisBtn);
        
        return false;
    });
    
    $(".bs-select-element").selectpicker();

    $('body').on('click', ".call-popup", function(e){
        e.preventDefault();

        var $this = $(this);
        if($this.hasClass('disabled')){
            return false;
        }
    
        var popup_url = $this.data('href');
        var $popup_parent = $($this.data('popup'));
        $.ajax({
            type: 'GET',
            url: popup_url,
            data: {},
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
            },
            success: function(resp){
                if(resp.mess_type == 'success'){                    
                    hideDropdown();

                    $popup_parent.html(resp.content).modal('show').on('hidden.bs.modal', function(){
                        $(this).html('');
                    });
                } else{
                    systemMessages( resp.message, resp.mess_type );
                }
            }
        });
        return false;
    });

    $("body").on('click', '.confirm-dialog',function(e){
		var $thisBtn = $(this);
        e.preventDefault();

        BootstrapModalWrapperFactory.createModal({
            title: $thisBtn.data('title'),
            message: $thisBtn.data('message'),
            type: $thisBtn.data('type'),
            closable: false,
            closeByBackdrop: false,
            buttons: [
                {
                    label: "Да",
                    cssClass: "btn btn-success btn-sm btn-flat",
                    action: function (button, buttonData, originalEvent) {
                        var callBack = $thisBtn.data('callback');
                        $(button).prop('disabled', true);

                        window[callBack]($thisBtn);
                        return this.hide();
                    }
                },
                {
                    label: "Отменить",
                    cssClass: "btn btn-danger btn-sm btn-flat",
                    action: function (button, buttonData, originalEvent) {
                        return this.hide();
                    }
                }
            ]
        }).show();
	});
});

function hideDropdown(){
    $('body').trigger('click');
}

var callFunction = function (fn) {
    var args = Array.prototype.slice.call(arguments, 1) || [];

    if (typeof fn === 'string' && fn in window) {
        window[fn].apply(window[fn], args);
    } else if (typeof fn === 'function') {
        fn.apply(fn, args);
    }
};

function systemMessages(message, toastrType){
	typeM = typeof message;
    
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

	switch(typeM){
		case 'string': 
            toastr[toastrType](message);
		break;
		case 'array':
        case 'object': 
            for(var i in message){
                toastr[toastrType](message[i]);
			}
		break;
    }
}

function showLoader(loaderElement,lodaerText){
    var text = undefined === lodaerText ? 'Обработка данных...' : '';

	var $this = $(loaderElement).children('.ajax-loader');

	if ($this.length > 0)
		$this.show();
	else {
		
		$(loaderElement).prepend('<div class="ajax-loader">\
                                    <div class="loader-bouces">'+ text +'</div>\
								</div>');
		$(loaderElement).children('.ajax-loader').show();
	}
}

function hideLoader(parentId){
	var $this = $(parentId).children('.ajax-loader');

	if ($this.length > 0)
		$this.hide();
}

function clearSystemMessages(){
    toastr.clear();
}

function number_format(num_value, view_decimals){
	if(view_decimals){		
		return (round_up(num_value, 2)).toFixed(2);
	} else{
		return Math.ceil(num_value).toFixed();
	}
}

function round_up(num, precision) {
	precision = Math.pow(10, precision);
	return Math.ceil(num * precision) / precision;
}

function intval(num){
	if (typeof num == 'number' || typeof num == 'string'){
		num = num.toString();
		var dotLocation = num.indexOf('.');
		if (dotLocation > 0){
			num = num.substr(0, dotLocation);
		}

		if (isNaN(Number(num))){
			num = parseInt(num);
		}

		if (isNaN(num)){
			return 0;
		}

		return Number(num);
	} else if (typeof num == 'object' && num.length != null && num.length > 0){
		return 1;
	} else if (typeof num == 'boolean' && num === true){
		return 1;
	}

	return 0;
}

function floatval(mixed_var) {
	return (parseFloat(mixed_var) || 0);
}

function fixDataTablePagination(thisDt){
    var dtbottom = $(thisDt.get(0)).parent().find('.dataTables_paginate');
	if (thisDt.api().page.info().pages > 1) {
		dtbottom.show();
	} else{
        dtbottom.hide();
    }
}

var filters_has_datepicker = false;
var initDtFilter = function(cookie_page){
	return $('.dt_filter').dtFilters('.dt_filter',{
		'container': '.dtfilter-list',
		'debug':false,
		'txtResetBtn' : '<i class="fad fa-eraser"></i>',
        'use_cookie': true,
        'cookie_page': cookie_page || null,
        callBack: function(){ 
            $.cookie( 'cookie_page', cookie_page);
            $.cookie( '_dtFilters', JSON.stringify(dtFilter.getDTFilter()));
            dtTable.fnDraw(); 
        },
		beforeSet: function(callerObj){
			if(typeof beforeSetFilters == 'function'){
				beforeSetFilters(callerObj);
			}
		},
		onSet: function(callerObj, filterObj) {
			if(typeof onSetFilters == 'function'){
				onSetFilters(callerObj, filterObj);
			}
		},
		onDelete: function(filter){
			if(typeof onDeleteFilters == 'function'){
				onDeleteFilters(filter);
			}
		},
		onReset: function(){
			if(typeof onResetFilters == 'function'){
				onResetFilters();
			}
		}
	});
}

//region DataTable Mobile
var mobileDataTable = function($table, replaceTitles){
	replaceTitles = typeof replaceTitles !== 'undefined' ? Boolean(~~replaceTitles) : true;

	$table.each(function() {
		var titles = [];
		var $thisTable = $(this);

		$thisTable.find('> thead > tr > th').each(function() {
			titles.push($(this).text());
		});

		if (replaceTitles) {
			$thisTable.find('> tbody > tr > td').each(function() {
				$(this).attr('data-title', titles[$(this).index()]);
			});
		}
    });
};

$(function(){
    if(($('.main-data-table').length > 0) && ($(window).width() < 768)){
		$('.main-data-table').addClass('main-data-table--mobile');
	}
});

$(window).on('resizestop', function () {
	if($('.main-data-table').length > 0){
		if($(this).width() < 768){
			$('.main-data-table').addClass('main-data-table--mobile');
		}else{
			$('.main-data-table').removeClass('main-data-table--mobile');
		}
	}
});
//endregion DataTable Mobile

var changeRecordWeight = function(btn){
	var $this = $(btn);
	var $element_parent = $this.closest($this.data('parent-target'));
	switch($this.data('order')){
		case 'up':
			var $prev_element_row = $element_parent.prev();
			$element_parent.insertBefore($prev_element_row);
		break;
		case 'down':
			var $next_element_row = $element_parent.next();
			$element_parent.insertAfter($next_element_row);
		break;
	}
};

(function () {
	this.uniqid = function (pr, en) {
		var pr = pr || '', en = en || false, result;
  
		this.seed = function (s, w) {
			s = parseInt(s, 10).toString(16);
			return w < s.length ? s.slice(s.length - w) : (w > s.length) ? new Array(1 + (w - s.length)).join('0') + s : s;
		};

		result = pr + this.seed(parseInt(new Date().getTime() / 1000, 10), 8) + this.seed(Math.floor(Math.random() * 0x75bcd15) + 1, 5);
  
		if (en) result += (Math.random() * 10).toFixed(8).toString();

		return result;
	};
})();