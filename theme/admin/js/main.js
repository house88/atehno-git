function systemMessages(mess_array, ul_class){
	typeM = typeof mess_array;
	var good_array = [];
	switch(typeM){
		case 'string':
			good_array = [mess_array];
		break;
		case 'array':
			good_array = mess_array;
		break;
		case 'object':
			for(var i in mess_array){
				good_array.push(mess_array[i]);
			}
		break;

	}

	if (!$('.system-text-messages-b').is(':visible')){
		$( '.system-text-messages-b' ).fadeIn('fast');
	}
	var $systMessUl = $('.system-text-messages-b ul');
	$systMessUl.html('');
	for (var li in good_array ) {
		$systMessUl.prepend('<li  class="message-' + ul_class + '">'+ good_array[li] +' <i class="ca-icon ca-icon_remove call-function" data-callback="closeSystemMessage"></i></li>');
		$systMessUl.children('li').first().addClass('zoomIn').show().delay( 30000 ).fadeOut('slow', function(){
			$(this).remove();
		});
	}
}

var closeSystemMessage = function(element){
	var $element = $(element);
	var $li = $element.closest('li');
	$li.clearQueue();
	$li.fadeOut('slow',function(){		
		$li.remove();
	});
}

function showLoader(parentId){
    var $this = $(parentId).children('.ajax-loader');
    if ($this.length > 0)
        $this.show();
    else {
        $(parentId).prepend('<div class="ajax-loader"><div class="wrapper"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>');
    }
}
function hideLoader(parentId){
    var $this = $(parentId).children('.ajax-loader');
    if ($this.length > 0)
        $this.hide();
}
$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

function intval(num){
	if (typeof num == 'number' || typeof num == 'string'){
		num = num.toString();
		var dotLocation = num.indexOf('.');
		if (dotLocation > 0){
			num = num.substr(0, dotLocation);
		}

		if (isNaN(Number(num))){
			num = parseInt(num);
		}

		if (isNaN(num)){
			return 0;
		}

		return Number(num);
	} else if (typeof num == 'object' && num.length != null && num.length > 0){
		return 1;
	} else if (typeof num == 'boolean' && num === true){
		return 1;
	}

	return 0;
}

function floatval(mixed_var) {
	return (parseFloat(mixed_var) || 0);
}

$(function() {
	$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
            input.val(log);
        }
    });

	$("body").on('click', '.confirm-dialog',function(e){
		var $thisBtn = $(this);
		e.preventDefault();

		BootstrapDialog.show({
			message: $thisBtn.data('message'),
			closable: false,
			draggable: true,
			buttons: [{
				label: 'Ok',
				cssClass: 'btn-success',
				action: function(dialogRef){
					var callBack = $thisBtn.data('callback');
					var $button = this;
                    $button.disable();

					window[callBack]($thisBtn);
					dialogRef.close();
				}
			},
			{
				label: 'Cancel',
				action: function(dialogRef){
					dialogRef.close();
				}
			}]
		});
	});
	
	$('body').on('click', ".call-popup", function(e){
		e.preventDefault();
		var $this = $(this);
		if($this.hasClass('disabled')){
			return false;
		}

		var popup_url = $this.data('href');
		var $popup_parent = $($this.data('popup'));
		$.ajax({
			type: 'GET',
			url: popup_url,
			data: {},
			dataType: 'JSON',
			beforeSend: function(){
				clearSystemMessages();
			},
			success: function(resp){
				if(resp.mess_type == 'success'){
					$popup_parent.html(resp.popup_content).modal('show').on('hidden.bs.modal', function(){
						$(this).html('');
					});
				} else{
					systemMessages( resp.message, resp.mess_type );
				}
			}
		});
		return false;
	});


	$(".modal-fullscreen").on('show.bs.modal', function () {
	  setTimeout( function() {
		$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
	  }, 0);
	});
	$(".modal-fullscreen").on('hidden.bs.modal', function () {
	  $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
	});

	$("body").on("input propertychange", ".floating-label-form-group", function(e) {
		$(this).toggleClass("floating-label-form-group-with-value", !! $(e.target).val());
	}).on("focus", ".floating-label-form-group", function() {
		$(this).addClass("floating-label-form-group-with-focus");
	}).on("blur", ".floating-label-form-group", function() {
		$(this).removeClass("floating-label-form-group-with-focus");
	});
});