function showLoader(loaderElement,lodaerText){
	if(lodaerText==undefined)
		lodaerText = 'Обработка данных...';

	var $this = $(loaderElement).children('.ajax-loader');

	if ($this.length > 0)
		$this.show();
	else {
		
		$(loaderElement).prepend('<div class="ajax-loader">\
									<div class="wrapper">\
										<div class="bounce1"></div>\
										<div class="bounce2"></div>\
										<div class="bounce3"></div>\
									</div>\
								</div>');
		$(loaderElement).children('.ajax-loader').show();
	}
}

function hideLoader(parentId){
	var $this = $(parentId).children('.ajax-loader');

	if ($this.length > 0)
		$this.hide();
}

function systemMessages(mess_array, ul_class){
	typeM = typeof mess_array;
	var good_array = [];
	switch(typeM){
		case 'string': 
			good_array = [mess_array]; 
		break;
		case 'array': 
			good_array = mess_array; 
		break;
		case 'object': 
			for(var i in mess_array){
				good_array.push(mess_array[i]);
			}
		break;
		
	}
	
	if (!$('.system-text-messages-b').is(':visible')){
		$( '.system-text-messages-b' ).fadeIn('fast');
	}
	var $systMessUl = $('.system-text-messages-b ul');
	$systMessUl.html('');
	for (var li in good_array ) {
		$systMessUl.prepend('<li  class="message-' + ul_class + '">'+ good_array[li] +' <i class="ca-icon ca-icon_remove call-function" data-callback="closeSystemMessage"></i></li>');
		$systMessUl.children('li').first().addClass('zoomIn').show().delay( 30000 ).fadeOut('slow', function(){
			$(this).remove();
		});
	}
}

var closeSystemMessage = function(element){
	var $element = $(element);
	var $li = $element.closest('li');
	$li.clearQueue();
	$li.fadeOut('slow',function(){		
		$li.remove();
	});
}

var copy_to_clipboard = function(btn){
	var $this = $(btn);
	var text_element = $('body').find($this.data('clipboard-target')).val();
	var clipboardBtn = new Clipboard('[data-clipboard-btn]', {
		text: function (trigger) {
			return text_element;
		}
	});
	clipboardBtn.on('success',function(e){
		systemMessages('Скопировано!', 'success');
	});
	clipboardBtn.on('error',function(e){
		systemMessages('Не скопировано!', 'error');
	});
}

function intval(num){
	if (typeof num == 'number' || typeof num == 'string'){
		num = num.toString();
		var dotLocation = num.indexOf('.');
		if (dotLocation > 0){
			num = num.substr(0, dotLocation);
		}

		if (isNaN(Number(num))){
			num = parseInt(num);
		}

		if (isNaN(num)){
			return 0;
		}

		return Number(num);
	} else if (typeof num == 'object' && num.length != null && num.length > 0){
		return 1;
	} else if (typeof num == 'boolean' && num === true){
		return 1;
	}

	return 0;
}

function floatval(mixed_var) {
	return (parseFloat(mixed_var) || 0);
}

function number_format(num_value, view_decimals){
	if(view_decimals){		
		return (round_up(num_value, 2)).toFixed(2);
	} else{
		return Math.ceil(num_value).toFixed();
	}
}

function round_up(num, precision) {
	precision = Math.pow(10, precision);
	return Math.ceil(num * precision) / precision;
}

function clearSystemMessages(){	
	if (!$('.system-text-messages-b').is(':visible')){
		$( '.system-text-messages-b' ).fadeIn('fast');
	}
	$('.system-text-messages-b ul').html('');
}

$(function() {
	$('body').on('mouseover mouseenter', '*[title]', function(){
		if(!$(this).hasClass('tooltipstered')){
			$(this).tooltipster();
		}
		$(this).tooltipster('show');
    });
	
	$("body").on('click', '.confirm-dialog',function(e){
		var $thisBtn = $(this);
		e.preventDefault();

		BootstrapDialog.show({
			title: $thisBtn.data('title'),
			message: $thisBtn.data('message'),
			closable: false,
			draggable: true,
			buttons: [{
				label: 'Да',
				cssClass: 'btn-success',
				action: function(dialogRef){
					var callBack = $thisBtn.data('callback');
					var $button = this;
                    $button.disable();

					window[callBack]($thisBtn);
					dialogRef.close();
				}
			},
			{
				label: 'Отменить',
				cssClass: 'btn-danger',
				action: function(dialogRef){
					dialogRef.close();
				}
			}]
		});
	});
	
	$('body').on('click', ".call-popup", function(e){
		e.preventDefault();
		var $this = $(this);
		if($this.hasClass('disabled')){
			return false;
		}

		var popup_url = $this.data('href');
		var $popup_parent = $($this.data('popup'));
		$.ajax({
			type: 'GET',
			url: popup_url,
			data: {},
			dataType: 'JSON',
			beforeSend: function(){
				clearSystemMessages();
			},
			success: function(resp){
				if(resp.mess_type == 'success'){
					$popup_parent.html(resp.popup_content).modal('show').on('hidden.bs.modal', function(){
						$(this).html('');
					});
				} else{
					systemMessages( resp.message, resp.mess_type );
				}
			}
		});
		return false;
	});
	
    $('#side-menu').metisMenu();
	
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
	
	var my_posts = $("[rel=tooltip]");

	var size = $(window).width();
	for(i=0;i<my_posts.length;i++){
		the_post = $(my_posts[i]);

		if(the_post.hasClass('invert') && size >=767 ){
			the_post.tooltip({ placement: 'left'});
			the_post.css("cursor","pointer");
		}else{
			the_post.tooltip({ placement: 'rigth'});
			the_post.css("cursor","pointer");
		}
	}
	
	$(".fancybox").fancybox({
		padding:0
	});

	var widthWrFilter = 350;
	//filter panel
	$('body').on('click', '.wr-filter-admin-panel .btn-display', function(){
		var $thisBtn = $(this);

		if(!$thisBtn.hasClass('hide-btn')){
			$thisBtn.closest('.wr-filter-admin-panel').animate({'right': "-"+widthWrFilter}, 350, function(){
				$thisBtn.addClass('hide-btn').children('span').html('&laquo;');
				$('.wr-filter-admin-panel .wr-hidden').hide();
			});
		}else{
			$thisBtn.closest('.wr-filter-admin-panel').animate({'right': 0}, 350, function(){
				$thisBtn.removeClass('hide-btn').children('span').html('&raquo;');
				$('.wr-filter-admin-panel .wr-hidden').show();
			});
		}
	});

	$('body').on('click', '.wr-filter-admin-panel .wr-hidden', function(){
		var $thisBlock = $(this);

		if($thisBlock.is(':visible')){
			$thisBlock.closest('.wr-filter-admin-panel').animate({'right': "-"+widthWrFilter}, 350, function(){
				$('.wr-filter-admin-panel .btn-display').addClass('hide-btn').children('span').html('&laquo;');
				$thisBlock.hide();
			});
		}
	});

	$('body').on('click', ".call-function", function(e){
		e.preventDefault();
		var $thisBtn = $(this);
		var callBack = $thisBtn.data('callback');
		window[callBack]($thisBtn);
		return false;
	});
});
