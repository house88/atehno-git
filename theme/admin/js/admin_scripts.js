var delete_item = function(btn){
    var $this = $(btn);
    var item = $this.data('item');
    $.ajax({
        type: 'POST',
        url: base_url+'admin/items/ajax_operations/delete',
        data: {item:item},
        dataType: 'JSON',
        success: function(resp){
            systemMessages(resp.message, resp.mess_type);
            if(resp.mess_type == 'success'){
                $this.closest('article.product-item').parent().remove();
            }
        }
    });
    return false;
}
var change_marketing_status = function(btn){
    var $this = $(btn);
    var item = $this.data('item');
    var marketing = $this.data('marketing');
    $.ajax({
        type: 'POST',
        url: base_url+'admin/items/ajax_operations/change_marketing_status',
        data: {item:item,marketing:marketing},
        dataType: 'JSON',
        success: function(resp){
            systemMessages(resp.message, resp.mess_type);
            if(resp.mess_type == 'success'){
                $this.toggleClass('btn-primary btn-default');
            }
        }
    });
    return false;
}