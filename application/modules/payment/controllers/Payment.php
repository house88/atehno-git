<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();

		$this->load->model("Payment_model", "payment");
		$this->load->library('Paynet');
	}

	function index(){
		redirect('spage/delivery-menu', 'auto', 301);
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(3);
		switch($option){
			case 'get_form':
				$this->form_validation->set_rules('token', 'Заказ', 'required|xss_clean|alpha_dash');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }
				
				$order_hash = $this->input->post('token');
				$order_info = Modules::run('orders/_get_by_hash', $order_hash);
				
				if(empty($order_info)){
					jsonResponse('Ошибка: Данные не верны!');
				}

				$paynet_payment = $this->paynet->registerOrder($order_info);
				if(empty($paynet_payment->Data)){
					jsonResponse('Ошибка: Сервис оплаты картой не доступен. Попробуйте позже.');
				}

				Modules::run('orders/_update', (int) $order_info['id_order'], array('order_payment_card_status' => 'processing'));
				jsonResponse('', 'success', array('form' => $paynet_payment->Data));
			break;
		}
	}

	function paynet_callback(){
		$this->payment->handler_insert_log(array(
			'log_status' => 'success',
			'log_context' => json_encode(array(
				'message' => 'Начало процесса оплаты системой Paynet.'
			))
		));

		$paymentInfo = file_get_contents('php://input');
		$paymentObj = json_decode($paymentInfo);

		if (!$paymentObj) {
			$this->payment->handler_insert_log(array(
				'log_status' => 'error',
				'log_context' => json_encode(array(
					'message' => 'Нет информации о транзакции.'
				))
			));


			http_response_code(400);
			return "The returning object has not found !";
		}

		$this->payment->handler_insert_log(array(
			'log_status' => 'processing',
			'log_context' => json_encode(array(
				'message' => 'Информации о транзакции получена.',
				'paymentInfo' => $paymentInfo
			))
		));

		if($paymentObj->EventType !== 'PAID'){
			$this->payment->handler_insert_log(array(
				'log_status' => 'error',
				'log_context' => json_encode(array(
					'message' => 'Оплата не прошла. Статус оплаты в системе Paynet: ' . $paymentObj->EventType
				))
			));

			http_response_code(400);
			return "NOT SUCCESS.";
		}


		$checkObj = $this->paynet->paymentGet($paymentObj->Payment->ExternalId);
		$this->payment->handler_insert_log(array(
			'log_status' => 'processing',
			'log_context' => json_encode(array(
				'message' => 'Получение информации о транзакции.',
				'transaction' => $checkObj
			))
		));
		
		if($checkObj->IsOk())
		{
			$id_order = (int) @$checkObj->Data[0]['Invoice'];
			if ((int) $checkObj->Data[0]['Status'] !== 4) {
				$this->payment->handler_insert_log(array(
					'log_status' => 'processing',
					'log_context' => json_encode(array(
						'message' => 'Транзакция не подтвержденна. Статус транзакции в системе Paynet: ' . $checkObj->Data[0]['Status'],
						'checkObj' => $checkObj
					))
				));

				http_response_code(400);
				return 'The payment status is not complete.';
			}

			$order = Modules::run('orders/_get', $id_order);
			if (empty($order)) {
				$this->payment->handler_insert_log(array(
					'log_status' => 'error',
					'log_context' => json_encode(array(
						'message' => 'Заказ '. $id_order .' не найден.',
						'checkObj' => $checkObj
					))
				));

				http_response_code(400);
				return 'The Order does not exist.';
			}

			Modules::run('orders/_update', $id_order, array(
				'order_payment_card_status' => 'paid',
				'order_payment_card_details' => json_encode(array(
					'status' => 'success',
					'context' => array(
						'message' => 'Оплата заказа прошла успешно.',
						'transaction' => $checkObj
					)
				))
			));

			$this->payment->handler_insert_log(array(
				'log_status' => 'success',
				'log_context' => json_encode(array(
					'message' => 'Оплата заказа прошла успешно.',
					'checkObj' => $checkObj
				))
			));

			http_response_code(200);
			return 'The payment has confirmed.';
		}

		$this->payment->handler_insert_log(array(
			'log_status' => 'error',
			'log_context' => json_encode(array(
				'message' => 'Информация о транзакции в системе Paynet: IS NOT OK'
			))
		));

		http_response_code(400);
		return 'CheckObj IS NOT OK.';
	}

	function _get($id_payment){
		return $this->payment->handler_get($id_payment);
	}

	function _get_all($params = array()){
		return $this->payment->handler_get_all($params);
	}

	function _get_delivery_relations(){
		return $this->payment->handler_get_delivery_relations();
	}

	function _get_paynet_payment($externalId){
		return $this->paynet->paymentGet($externalId);
	}
}
