<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Configs extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		
		$this->data = array();
		
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		return_404($this->data);
	}

	function updatePriceFile(){
		$config = $this->_generatePriceSettings();
		$this->_toFile($config, 'price.php');
	}

	private function _generatePriceSettings(){
		$currencies = Modules::run('currency/_get_all');
		$config['currency'] = [];
		foreach ($currencies as $currency) {
			$config['currency']['id'][$currency['id_currency']] = $currency;

			if(1 === (int) $currency['currency_default']){
				$config['currency']['default'] = $currency;
			}

			if(1 === (int) $currency['currency_admin']){
				$config['currency']['default']['admin'] = $currency;
			}

			if(1 === (int) $currency['currency_user']){
				$config['currency']['default']['user'] = $currency;
			}
		}

		$priceVariants = Modules::run('items/_get_prices_variants_all');
		foreach ($priceVariants as $priceVariant) {
			$priceVariant['price_column'] = "item_price_{$priceVariant['id_price_variant']}";
			$config['staticVariant'][$priceVariant['id_price_variant']] = $priceVariant;
		}

		$dynamicPriceVariants = Modules::run('items/_get_dynamic_prices_variants_all');
		foreach ($dynamicPriceVariants as $dynamicPriceVariant) {
			$dynamicPriceVariant['price_column'] = "item_price_{$dynamicPriceVariant['id_price_variant']}";
			$config['dynamicVariant'][$dynamicPriceVariant['id_dynamic_price']] = $dynamicPriceVariant;
		}

		return $config;
    }

	private function _toFile($configs = [], $fileName){
		if(null === $fileName){
			return false;
		}

		$pathToFile = APPPATH . "config/{$fileName}";
		$file = fopen($pathToFile, "w");
		fwrite($file, '<?php defined(\'BASEPATH\') OR exit(\'No direct script access allowed\');'. PHP_EOL .'$config[\'price\'] = ' . var_export($configs, true) . ';');
		fclose($file);
	}
}
