<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}
		
		$this->view_module_path = 'modules/import/';
		$this->active_menu = 'settings';
		$this->active_submenu = 'import';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Import_model", "import");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		checkPermision('manage_import', '/admin');

		$this->data['page_header'] = 'Варианты импорта';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'import_scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_import');

				$this->data['price_variants'] = Modules::run('items/_get_prices_variants_all', array('active' => 1));

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit':
				checkPermisionAjax('manage_import');

				$id_import = (int)$this->uri->segment(5);
				$this->data['import'] = $this->import->handler_get($id_import);
				if(empty($this->data['import'])){
					jsonResponse('Вариант импорта не существует.');
				}

				$this->data['price_variants'] = Modules::run('items/_get_prices_variants_all', array('active' => 1));

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'xml_example':
				$id_import = (int)$this->uri->segment(5);
				$this->data['import'] = $this->import->handler_get($id_import);
				if(empty($this->data['import'])){
					jsonResponse('Вариант импорта не существует.');
				}

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'import_example_view'), $this->data, true);
				jsonResponse('Сохранено.', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				$import_columns = $this->input->post('import_columns');
				if(empty($import_columns)){
					jsonResponse('Выберите нужные поля для импорта.');
				}

				$this->form_validation->set_rules('import_format', 'Формат данных', 'required');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('before_action', 'Перед импортом', 'required|xss_clean');


				if(isset($import_columns['items'])){
					if(!in_array('item_prog_id', $import_columns['items'])){
						$this->form_validation->set_rules('item_prog_id', 'Поля для товаров - Программный ИД товара', 'required|xss_clean');
					}
					if(!in_array('category_prog_id', $import_columns['items'])){
						$this->form_validation->set_rules('category_prog_id', 'Поля для товаров - Программный ИД категорий', 'required|xss_clean');
					}
					if(!in_array('item_title', $import_columns['items'])){
						$this->form_validation->set_rules('item_title', 'Поля для товаров - Название товара', 'required|xss_clean');
					}
					if(!in_array('item_price', $import_columns['items'])){
						$this->form_validation->set_rules('item_price', 'Поля для товаров - Цена товара', 'required|xss_clean');
					}
				}
				if(isset($import_columns['categories'])){
					if(!in_array('category_prog_id', $import_columns['categories'])){
						$this->form_validation->set_rules('с_category_prog_id', 'Поля для категорий - Программный ИД категорий', 'required|xss_clean');
					}
					if(!in_array('category_prog_parent_id', $import_columns['categories'])){
						$this->form_validation->set_rules('category_prog_parent_id', 'Поля для категорий - Программный ИД родителя категорий', 'required|xss_clean');
					}
					if(!in_array('category_title', $import_columns['categories'])){
						$this->form_validation->set_rules('category_title', 'Поля для категорий - Название категорий', 'required|xss_clean');
					}
				}

				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$insert = array(
					'import_title' => $this->input->post('title'),
					'import_format' => $this->input->post('import_format'),
					'import_before_action' => $this->input->post('before_action'),
					'import_columns' => json_encode($import_columns)
				);

				$this->import->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				checkPermisionAjax('manage_import');

				$import_columns = $this->input->post('import_columns');
				if(empty($import_columns)){
					jsonResponse('Выберите нужные поля для импорта.');
				}

				$this->form_validation->set_rules('id_import', 'Вариант импорта', 'required');
				$this->form_validation->set_rules('import_format', 'Формат данных', 'required');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('before_action', 'Перед импортом', 'required|xss_clean');

				if(isset($import_columns['items'])){
					if(!in_array('item_prog_id', $import_columns['items'])){
						$this->form_validation->set_rules('item_prog_id', 'Поля для товаров - Программный ИД товара', 'required|xss_clean');
					}
					if(!in_array('category_prog_id', $import_columns['items'])){
						$this->form_validation->set_rules('category_prog_id', 'Поля для товаров - Программный ИД категорий', 'required|xss_clean');
					}
					if(!in_array('item_title', $import_columns['items'])){
						$this->form_validation->set_rules('item_title', 'Поля для товаров - Название товара', 'required|xss_clean');
					}
					if(!in_array('item_price', $import_columns['items'])){
						$this->form_validation->set_rules('item_price', 'Поля для товаров - Цена товара', 'required|xss_clean');
					}
				}
				if(isset($import_columns['categories'])){
					if(!in_array('category_prog_id', $import_columns['categories'])){
						$this->form_validation->set_rules('с_category_prog_id', 'Поля для категорий - Программный ИД категорий', 'required|xss_clean');
					}
					if(!in_array('category_prog_parent_id', $import_columns['categories'])){
						$this->form_validation->set_rules('category_prog_parent_id', 'Поля для категорий - Программный ИД родителя категорий', 'required|xss_clean');
					}
					if(!in_array('category_title', $import_columns['categories'])){
						$this->form_validation->set_rules('category_title', 'Поля для категорий - Название категорий', 'required|xss_clean');
					}
				}

				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_import = (int)$this->input->post('id_import');
				$import = $this->import->handler_get($id_import);
				if(empty($import)){
					jsonResponse('Вариант импорта не существует.');
				}

				$update = array(
					'import_title' => $this->input->post('title'),
					'import_format' => $this->input->post('import_format'),
					'import_before_action' => $this->input->post('before_action'),
					'import_columns' => json_encode($import_columns)
				);

				$this->import->handler_update($id_import, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				$this->form_validation->set_rules('id_import', 'Вариант импорта', 'required');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_import = (int)$this->input->post('id_import');
				$import = $this->import->handler_get($id_import);
				if(empty($import)){
					jsonResponse('Вариант импорта не существует.');
				}

				$this->import->handler_delete($id_import);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				checkPermisionAjax('manage_import');

				$this->form_validation->set_rules('id_import', 'Вариант импорта', 'required');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_import = (int)$this->input->post('id_import');
				$import = $this->import->handler_get($id_import);
				if(empty($import)){
					jsonResponse('Вариант импорта не существует.');
				}

				if($import['import_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->import->handler_update($id_import, array('import_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
			case 'change_cron':
				checkPermisionAjax('manage_import');

				$this->form_validation->set_rules('id_import', 'Вариант импорта', 'required');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_import = (int)$this->input->post('id_import');
				$import = $this->import->handler_get($id_import);
				if(empty($import)){
					jsonResponse('Вариант импорта не существует.');
				}

				if($import['import_active'] < 1 && $import['import_cron'] < 1){
					jsonResponse('Вариант импорта должен быть активен.');
				}

				$this->import->handler_update_all(array('import_cron' => 0));

				if($import['import_cron'] < 1){
					$this->import->handler_update($id_import, array('import_cron' => 1));
				}
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_import');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart'),
					'sort_by' => array(	'id_import-asc'	)
				);

				$records = $this->import->handler_get_all($params);
				$records_total = $this->import->handler_get_count($params);
				
				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){
						$columns = json_decode($record['import_columns'], true);
						$columns_by_type = array();
						if(!empty($columns['items'])){
							$columns_by_type[] = '<strong>Поля для товаров</strong>: '.implode(', ', $columns['items']);
						}
						
						if(!empty($columns['categories'])){
							$columns_by_type[] = '<strong>Поля для категорий</strong>: '.implode(', ', $columns['categories']);
						}
						
						if(!empty($columns['stocks'])){
							$columns_by_type[] = '<strong>Поля для складов</strong>: '.implode(', ', $columns['stocks']);
						}
			
						if(!empty($columns['suppliers'])){
							$columns_by_type[] = '<strong>Поля для поставщиков</strong>: '.implode(', ', $columns['suppliers']);
						}
						
						return array(
							'dt_id'			=>  $record['id_import'],
							'dt_name'		=>  $record['import_title'],
							'dt_columns'	=>  implode('<br>', $columns_by_type),
							'dt_active'		=>  '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['import_active'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-import="'.$record['id_import'].'"></a>',
							'dt_cron'		=>  '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['import_cron'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_cron" data-import="'.$record['id_import'].'"></a>',
							'dt_actions'	=> '<div class="dropdown">
													<a data-toggle="dropdown" href="#" aria-expanded="false">
														<i class="fas fa-ellipsis-h"></i>
													</a>
													<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/import/popup/xml_example/' . $record['id_import']).'">
															<i class="fad fa-eye"></i> Пример Xml
														</a>
														<div class="dropdown-divider"></div>
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/import/popup/edit/' . $record['id_import']).'">
															<i class="fad fa-pencil"></i> Редактировать
														</a>
														<div class="dropdown-divider"></div>
														<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите удалить вариант импорта?" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-callback="delete_action" data-import="'.$record['id_import'].'" data-type="bg-danger">
															<i class="fad fa-trash-alt"></i> Удалить
														</a>
													</div>
												</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
		}
	}
}
