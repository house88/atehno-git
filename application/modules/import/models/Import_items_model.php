<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Import_items_model extends CI_Model{
	var $import_items_table = 'import_items';

	function __construct(){
		parent::__construct();
	}

	function handler_update_all(array $data, array $conditions = array()){
		$this->scope_updated($conditions['updated']);

		$this->db->update($this->import_items_table, $data);
	}

	function handler_delete_all(array $conditions = array()){
		$this->scope_created_before_date($conditions['created_before_date']);

		return $this->db->delete($this->import_items_table);
	}

	function handler_get_all(array $conditions = array()){
		$this->scope_updated($conditions['updated']);
        
		return $this->db->get($this->import_items_table)->result_array();
	}

	private function scope_updated(?bool $updated):void
	{
		if(null === $updated){
			return;
		}
		
		$this->db->where('update_date IS ' . ($updated ? 'NOT' : '') . ' NULL');
	}

	private function scope_created_before_date(?string $before_date):void
	{
		if(null === $before_date){
			return;
		}

		$this->db->where('DATE(create_date) <=', $before_date);
	}
}
