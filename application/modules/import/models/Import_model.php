<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Import_model extends CI_Model{
	var $import = "import";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->import, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_import, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_import', $id_import);
		$this->db->update($this->import, $data);
	}

	function handler_update_all($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->update($this->import, $data);
	}

	function handler_delete($id_import){
		$this->db->where('id_import', $id_import);
		$this->db->delete($this->import);
	}

	function handler_get($id_import){
		$this->db->where('id_import', $id_import);
		return $this->db->get($this->import)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_import ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($import_list)){
			$this->db->where_in('id_import', $import_list);
        }

        if(isset($active)){
			$this->db->where('import_active', $active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->import)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($import_list)){
			$this->db->where_in('id_import', $import_list);
        }

        if(isset($active)){
			$this->db->where('import_active', $active);
        }

		return $this->db->count_all_results($this->import);
	}
}
