<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/promo_code/';
		$this->active_menu = 'catalog';
		$this->active_submenu = 'promo_code';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Promo_code_model", "promo_code");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		$this->data['page_header'] = 'Промо коды';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_promo_code');

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit':
				checkPermisionAjax('manage_promo_code');

				$id_pcode = (int) $this->uri->segment(5);
				$this->data['pcode'] = $this->promo_code->handler_get($id_pcode);
				if(empty($this->data['pcode'])){
					jsonResponse('Промо код не найден.');
				}

				$this->data['pcode_hashes'] = $this->promo_code->handler_get_pcode_hashes($id_pcode);

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				checkPermisionAjax('manage_promo_code');

				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[200]');
				$this->form_validation->set_rules('items_type', 'Товары', 'required|xss_clean');
				$this->form_validation->set_rules('pcode_type', 'Тип', 'required|xss_clean');
				$this->form_validation->set_rules('pcode_date', 'Дата', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$items_type = $this->input->post('items_type', true);
				if(!in_array($items_type, array('all', 'not_actional', 'not_newest'))){
					jsonResponse('Данные не верны.');
				}

				$pcode_items = explode(',', $this->input->post('items', true));
				if(!empty($pcode_items)){
					$pcode_items = array_filter($pcode_items);
				}

				$pcode_categories = explode(',', $this->input->post('categories', true));
				if(!empty($pcode_categories)){
					$pcode_categories = array_filter($pcode_categories);
				}

				$pcode_type = $this->input->post('pcode_type', true);
				if(!in_array($pcode_type, array('multiple', 'once'))){
					jsonResponse('Данные не верны.');
				}

				$insert = array(
					'pcode_name' => $this->input->post('title', true),
					'pcode_items_type' => $items_type,
					'pcode_type' => $pcode_type,
					'pcode_items' => implode(',', $pcode_items),
					'pcode_categories' => implode(',', $pcode_categories),
					'pcode_active' => (int) $this->input->post('visible')
				);

				if($this->input->post('pcode_date')){
					list($pcode_date_from, $pcode_date_to) = explode(' - ', $this->input->post('pcode_date'));

					if(validateDate($pcode_date_from, 'd.m.Y')){
						$insert['pcode_date_start'] = getDateFormat($pcode_date_from, 'd.m.Y', 'Y-m-d');
					}

					if(validateDate($pcode_date_to, 'd.m.Y')){
						$insert['pcode_date_end'] = getDateFormat($pcode_date_to, 'd.m.Y', 'Y-m-d');
					}
				}

				$id_pcode = $this->promo_code->handler_insert($insert);

				$pcode_hashes = $this->input->post('pcode_hashes');
				if(!empty($pcode_hashes)){
					$insert_hashes = array();
					foreach ($pcode_hashes as $hash_info) {
						if(!empty($hash_info['hash'])){
							$insert_hashes[] = array(
								'id_pcode' => $id_pcode,
								'pcode_hash' => xss_clean($hash_info['hash']),
								'pcode_discount' => (int) $hash_info['discount'],
								'pcode_once_quantity' => $pcode_type == 'once' ? (int) $hash_info['quantity'] : 0
							);
						}
					}

					if(!empty($insert_hashes)){
						$this->promo_code->handler_insert_hashes($insert_hashes);
					}
				}

				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				checkPermisionAjax('manage_promo_code');

				$this->form_validation->set_rules('id_pcode', 'Промо код', 'required');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[200]');
				$this->form_validation->set_rules('items_type', 'Товары', 'required|xss_clean');
				$this->form_validation->set_rules('pcode_type', 'Тип', 'required|xss_clean');
				$this->form_validation->set_rules('pcode_date', 'Дата', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$items_type = $this->input->post('items_type', true);
				if(!in_array($items_type, array('all', 'not_actional', 'not_newest'))){
					jsonResponse('Данные не верны.');
				}

				$pcode_items = explode(',', $this->input->post('items', true));
				if(!empty($pcode_items)){
					$pcode_items = array_filter($pcode_items);
				}

				$pcode_categories = explode(',', $this->input->post('categories', true));
				if(!empty($pcode_categories)){
					$pcode_categories = array_filter($pcode_categories);
				}

				$pcode_type = $this->input->post('pcode_type', true);
				if(!in_array($pcode_type, array('multiple', 'once'))){
					jsonResponse('Данные не верны.');
				}

				$id_pcode = (int)$this->input->post('id_pcode');
				$pcode = $this->promo_code->handler_get($id_pcode);
				if(empty($pcode)){
					jsonResponse('Данные не верны.');
				}

				$update = array(
					'pcode_name' => $this->input->post('title', true),
					'pcode_items_type' => $items_type,
					'pcode_type' => $pcode_type,
					'pcode_items' => implode(',', $pcode_items),
					'pcode_categories' => implode(',', $pcode_categories),
					'pcode_active' => $this->input->post('visible') ? 1 : 0
				);

				if($this->input->post('pcode_date')){
					list($pcode_date_from, $pcode_date_to) = explode(' - ', $this->input->post('pcode_date'));

					if(validateDate($pcode_date_from, 'd.m.Y')){
						$update['pcode_date_start'] = getDateFormat($pcode_date_from, 'd.m.Y', 'Y-m-d');
					}

					if(validateDate($pcode_date_to, 'd.m.Y')){
						$update['pcode_date_end'] = getDateFormat($pcode_date_to, 'd.m.Y', 'Y-m-d');
					}
				}

				$this->promo_code->handler_update($id_pcode, $update);

				$this->promo_code->handler_delete_pcode_hashes($id_pcode);

				$pcode_hashes = $this->input->post('pcode_hashes');
				if(!empty($pcode_hashes)){
					$insert_hashes = array();
					foreach ($pcode_hashes as $hash_info) {
						if(!empty($hash_info['hash'])){
							$insert_hashes[] = array(
								'id_pcode' => $id_pcode,
								'pcode_hash' => xss_clean($hash_info['hash']),
								'pcode_discount' => (int) $hash_info['discount'],
								'pcode_once_quantity' => $pcode_type == 'once' ? (int) $hash_info['quantity'] : 0
							);
						}
					}

					if(!empty($insert_hashes)){
						$this->promo_code->handler_insert_hashes($insert_hashes);
					}
				}

				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				checkPermisionAjax('manage_promo_code');

				$this->form_validation->set_rules('pcode', 'Промо код', 'required');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_pcode = (int)$this->input->post('pcode');
				$pcode = $this->promo_code->handler_get($id_pcode);
				if(empty($pcode)){
					jsonResponse('Промо код не существует.');
				}

				$this->promo_code->handler_delete($id_pcode);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				checkPermisionAjax('manage_promo_code');

				$this->form_validation->set_rules('pcode', 'Промо код', 'required');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_pcode = (int)$this->input->post('pcode');
				$pcode = $this->promo_code->handler_get($id_pcode);
				if(empty($pcode)){
					jsonResponse('Промо код не существует.');
				}
				
				$this->promo_code->handler_update($id_pcode, array('pcode_active' => (int) $pcode['pcode_active'] === 1 ? 0 : 1));

				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list':
				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength', true),
					'start' => (int) $this->input->post('iDisplayStart', true)
				);
				
				$sort_by = flat_dt_ordering($_POST, array(
					'dt_start'	=> 'pcode_date_start',
					'dt_end' 	=> 'pcode_date_end'
				));
				if(!empty($sort_by)){
					$params['sort_by'] = $sort_by;
				}
		
				$records = $this->promo_code->handler_get_all($params);
				$records_total = $this->promo_code->handler_get_count($params);

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){
						return array(
							'dt_name'		=>  $record['pcode_name'],
							'dt_start'		=>  getDateFormat($record['pcode_date_start'], 'Y-m-d', 'd.m.Y'),
							'dt_end'		=>  getDateFormat($record['pcode_date_end'], 'Y-m-d', 'd.m.Y'),
							'dt_active'		=>  '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['pcode_active'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-pcode="'.$record['id_pcode'].'"></a>',
							'dt_actions'		=> $this->lauth->have_right('manage_promo_code') ? 
													'<div class="dropdown">
														<a data-toggle="dropdown" href="#" aria-expanded="false">
															<i class="fas fa-ellipsis-h"></i>
														</a>
														<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
															<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/promo_code/popup/edit/' . $record['id_pcode']).'">
																<i class="fad fa-pencil"></i> Редактировать
															</a>
															<div class="dropdown-divider"></div>
															<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите удалить промо код?" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-callback="delete_action" data-pcode="'.$record['id_pcode'].'" data-type="bg-danger">
																<i class="fad fa-trash-alt"></i> Удалить
															</a>
														</div>
													</div>' : ''
						);
					}, $records)
				);

				jsonResponse('', 'success', $output);
			break;
		}
	}
}
