<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promo_code extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->data = array();

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Promo_code_model", "promo_code");
        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		show_404();
	}

	function _get($id_pcode = 0){
		return $this->promo_code->handler_get((int) $id_pcode);
	}

	function _get_all($params = array()){
		return $this->promo_code->handler_get_all($params);
	}
	
	function _get_active($params = array()){
		if(!$this->lauth->can_apply_pcode()){
			return array();
		}

		$record = $this->promo_code->handler_get_now($params);
		if(empty($record)){
			return array();
		}

		if($record['pcode_type'] == 'once'){
			if((int)$record['pcode_once_quantity'] == 0){
				return array();
			}

			if((int)$record['pcode_once_quantity'] == (int)$record['pcode_used_quantity']){
				return array();
			}
		}

		$pcode_items = explode(',', $record['pcode_items']);
		$record['pcode_items'] = array_filter($pcode_items);

		$pcode_categories = explode(',', $record['pcode_categories']);
		$pcode_categories = array_filter($pcode_categories);
		if(!empty($pcode_categories)){
			$pcode_categories = Modules::run('categories/_get_children', array('id_category' => $pcode_categories));
			$record['pcode_categories'] = $pcode_categories;
		}

		return $record;
	}
	
	function _get_used($id_pcode){

		$record = $this->promo_code->handler_get((int) $id_pcode);
		if(empty($record)){
			return array();
		}

		$pcode_items = explode(',', $record['pcode_items']);
		$record['pcode_items'] = array_filter($pcode_items);

		$pcode_categories = explode(',', $record['pcode_categories']);
		$pcode_categories = array_filter($pcode_categories);
		if(!empty($pcode_categories)){
			$pcode_categories = Modules::run('categories/_get_children', array('id_category' => $pcode_categories));
			$record['pcode_categories'] = $pcode_categories;
		}

		return $record;
	}

	function _up_hash_used_quantity($id_hash = 0){
		$record = $this->promo_code->handler_get_hash($id_hash);
		if(empty($record)){
			return;
		}

		$update = array(
			'pcode_used_quantity' => (int) $record['pcode_used_quantity'] + 1
		);
		$this->promo_code->handler_update_hash($id_hash, $update);
	}
}
