<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promo_code_model extends CI_Model{
	var $promo_code = "promo_code";
	var $promo_code_hashes = "promo_code_hashes";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->promo_code, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_pcode = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_pcode', $id_pcode);
		$this->db->update($this->promo_code, $data);
	}

	function handler_delete($id_pcode = 0){
		$this->db->where('id_pcode', $id_pcode);
		$this->db->delete($this->promo_code);
	}

	function handler_get($id_pcode = 0){
		$this->db->where('id_pcode', $id_pcode);
		return $this->db->get($this->promo_code)->row_array();
	}

	function handler_get_now($conditions = array()){
		$this->db->select("*");
		$this->db->from("{$this->promo_code} pc");
		$this->db->join("{$this->promo_code_hashes} pch", "pc.id_pcode = pch.id_pcode", "inner");

		extract($conditions);

		if(isset($id_hash)){
			$this->db->where('pch.id_hash', $id_hash);
		}
		
	
		if(isset($pcode_hash)){
			$this->db->where('pch.pcode_hash', $pcode_hash);
		}
		
		if(isset($id_pcode)){
			$this->db->where('pc.id_pcode', $id_pcode);
		}
		
		$this->db->where('pc.pcode_active', 1);
		$this->db->where('pc.pcode_date_start <= DATE(NOW())', null);
		$this->db->where('pc.pcode_date_end >= DATE(NOW())', null);
		$this->db->order_by('pch.pcode_used_quantity ASC');
		$this->db->limit(1);

		return $this->db->get()->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_pcode DESC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->promo_code)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

		return $this->db->count_all_results($this->promo_code);
	}

	// HASHES

	function handler_insert_hashes($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert_batch($this->promo_code_hashes, $data);
		return $this->db->insert_id();
	}
	
	function handler_get_pcode_hashes($id_pcode = 0){
		$this->db->where('id_pcode', (int) $id_pcode);

		return $this->db->get($this->promo_code_hashes)->result_array();
	}
	
	function handler_get_hash($id_hash = 0){
		$this->db->where('id_hash', (int) $id_hash);

		return $this->db->get($this->promo_code_hashes)->row_array();
	}
	
	function handler_update_hash($id_hash = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_hash', $id_hash);
		$this->db->update($this->promo_code_hashes, $data);
	}
	
	function handler_delete_pcode_hashes($id_pcode = 0){
		$this->db->where('id_pcode', (int) $id_pcode);

		$this->db->delete($this->promo_code_hashes);
	}
}
