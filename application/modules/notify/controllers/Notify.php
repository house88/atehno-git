<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Notify extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value'];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value'];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value'];

        $this->load->library('email');
        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		return_404($this->data);
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(3);
		switch($option){
			case 'tradein_callme':
				$this->form_validation->set_rules('name', 'Имя', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('phone', 'Телефон', 'required|xss_clean|max_length[12]');
				$this->form_validation->set_rules('old_item', 'Название старого устройства', 'required|xss_clean|max_length[500]');
				$this->form_validation->set_rules('new_item', 'Желаемое устройство или номер заказа', 'required|xss_clean|max_length[500]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$phone_display = $this->input->post('phone');
				if($this->input->post('phone')){
					$phoneFormatResult = formatPhoneNumber($this->input->post('phone'));
					if(true === $phoneFormatResult['is_valid']){
						$phone_display = $phoneFormatResult['formated'];
					}
				}

				Modules::run('email/send', array(
					'to' => $this->data['settings']['tradein_email']['setting_value'],
					'subject' => 'Клиент нуждается в консультации по Trade-In',
					'email_data' => array(
						'user_name' => $this->input->post('name', true),
						'user_phone' => $phone_display,
						'old_item' => $this->input->post('old_item', true),
						'new_item' => $this->input->post('new_item', true),
						'title' => 'Клиент нуждается в консультации по Trade-In',
						'email_content' => 'trade_in_callme_tpl'
					)
				));

				jsonResponse('Спасибо за Ваше обрашение. Наш специалист свяжеться с вами.', 'success');
			break;
			case 'gps_monitoring_callme':
				$this->form_validation->set_rules('user_name', 'Имя', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_phone', 'Телефон', 'required|xss_clean|max_length[12]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }
				
				$email_data = array(
					'user_name' => $this->input->post('user_name', true),
					'user_phone' => $this->input->post('user_phone', true),
					'title' => 'Клиент нуждается в консультации по GPS Мониторингу',
					'email_content' => 'gps_monitoring_callme_tpl'
				);

				Modules::run('email/send', array(
					'to' => $this->data['settings']['gps_monitoring_email']['setting_value'],
					'subject' => 'Клиент нуждается в консультации по GPS Мониторингу',
					'email_data' => $email_data
				));

				jsonResponse('Спасибо за Ваше обрашение. Наш специалист свяжеться с вами.', 'success');
			break;
			case 'secure_cam_callme':
				$this->form_validation->set_rules('user_name', 'Имя', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_phone', 'Телефон', 'required|xss_clean|max_length[12]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }
				
				$email_data = array(
					'user_name' => $this->input->post('user_name', true),
					'user_phone' => $this->input->post('user_phone', true),
					'title' => 'Консультация по Видеонаблюдению',
					'email_content' => 'secure_cam_callme_tpl'
				);

				Modules::run('email/send', array(
					'to' => $this->data['settings']['video_security_email']['setting_value'],
					'subject' => 'Консультация по Видеонаблюдению',
					'email_data' => $email_data
				));

				jsonResponse('Спасибо за Ваше обрашение. Наш специалист свяжеться с вами.', 'success');
			break;
			case 'callClient':
				$this->form_validation->set_rules('phone', 'Номер телефона', 'required');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}

				$callapi_sip_id = $this->lauth->callapi_sip_id();
				if($callapi_sip_id === 0){
					jsonResponse('SIP ID не установлен для вашего профиля.');
				}

				$phoneFormatResult = formatPhoneNumber($this->input->post('phone'));
				if(false === $phoneFormatResult['is_valid']){
					jsonResponse('Номер телефона не валиден.');
				}

				$phone = $phoneFormatResult['formated'];
		
				$ch = curl_init('http://212.56.202.234:4567/originate');
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
					'cmd' => 'makeCall',
					'from' => $callapi_sip_id,
					'to' => $phone,
					'token' => 'k2tc4BgyWyb9JK'
				)));
		
				$response = curl_exec($ch);
				$response = json_decode($response, true);
				if(isset($response['error'])){
					$error_message = 'Ошибка сервиса: ' . $response['error'] .'. Code: '. curl_errno($ch);
				}

				curl_close($ch);
		
				if (isset($response['error'])) {
					jsonResponse($error_message);
				}

				jsonResponse('Звонок начат ...', 'success', ['response' => $response]);
			break;
		}
	}

	function show_email(){
		$email_data = array(
            'user_name' => 'Andrei',
            'user_phone' => '684958673',
            'title' => 'Консультация по GPS Мониторингу',
            'email_content' => 'gps_monitoring_callme_tpl'
        );
		$this->load->view('email_templates/email_tpl',$email_data);
	}
}
