<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value'];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value'];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value'];

		$this->load->model('Orders_model', 'orders');
		$this->load->model('cart/Cart_model', 'cart');
		$this->load->model('items/Items_Model', 'items');
		$this->load->model("delivery/Delivery_model", "delivery");
		$this->load->model("payment/Payment_model", "payment");
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function update(){
		$records = $this->orders->handler_get_all();
		foreach($records as $record){
			$ordered_items = $this->orders->handler_get_orders_items($record['id_order']);
			$update_order['order_ordered_items'] = json_encode($ordered_items);
			$this->orders->handler_update($record['id_order'], $update_order);
			$this->orders->handler_update_search_info($record['id_order']);
		}		
	}

	function index(){
		return_404($this->data);
	}

	function view(){
		$order_hash = $this->uri->segment(2);
		$this->data['order'] = $this->orders->handler_get_by_hash($order_hash);
		if(empty($this->data['order'])){
			return_404($this->data);
		}

		if($this->data['order']['order_user_viewed'] <= 1){
			$this->orders->handler_update($this->data['order']['id_order'], array('order_user_viewed' => 2));
			$this->data['order']['order_user_viewed'] += 1;
		}

		$this->breadcrumbs[] = array(
			'title' => 'Заказ номер ' . orderNumber($this->data['order']['id_order']),
			'link' => base_url('order/'.$order_hash)
		);

		$status = get_order_status($this->data['order']['order_status']);
		$this->data['show_payment_button'] = false;
		$this->data['status_payment_procesing'] = false;
		if($this->data['order']['order_payment_card'] == 1 && true === $status['is_payable']){
			$this->data['show_payment_button'] = true;
			$payment_info = Modules::run('payment/_get_paynet_payment', $this->data['order']['id_order']);

			if($payment_info->IsOk()){
				$this->data['show_payment_button'] = !in_array($payment_info->Data[0]['Status'], array(2,3));
				$this->data['status_payment_procesing'] = in_array($payment_info->Data[0]['Status'], array(2,3));
			}
		}

		if($this->input->get('payment') && in_array($this->input->get('payment'), array($this->data['order']['order_payment_card_success_token'], $this->data['order']['order_payment_card_cancel_token']))){
			$payment_token = $this->input->get('payment');
			if($payment_token === $this->data['order']['order_payment_card_success_token'] && in_array($this->data['order']['order_payment_card_status'], array('not_paid', 'paid'))){
				$this->session->set_flashdata('system_messages', '<li class="message-success zoomIn">Спасибо за оформление оплаты. <i class="ca-icon ca-icon_remove"></i></li>');
			} else if($payment_token === $this->data['order']['order_payment_card_cancel_token'] && $this->data['order']['order_payment_card_status'] === 'processing'){
				$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Процесс оплаты был отменен. <i class="ca-icon ca-icon_remove"></i></li>');
				$this->orders->handler_update($this->data['order']['id_order'], array('order_payment_card_status' => 'not_paid'));
			}

			redirect('/order/' . $order_hash);
		}

		$ordered_items = json_decode($this->data['order']['order_ordered_items'], true);
		$id_delivery = (int) $this->data['order']['order_delivery_option'];
		$this->data['delivery_option'] = $this->delivery->handler_get($id_delivery);

		$id_payment = (int) $this->data['order']['order_payment_option'];
		$this->data['payment_option'] = $this->payment->handler_get($id_payment);
		if(!empty($this->data['payment_option']) && $this->data['payment_option']['payment_by_credit'] == 1){
			$credit = getItemsCreditOptions($ordered_items, true, $this->data['order']['order_cashback']);
			if($credit){
				$order_credit_data = json_decode($this->data['order']['order_payment_data'], true);
				$credit_months = 0;
				if(!empty($order_credit_data)){
					$credit_months = $order_credit_data['months'];
				}

				$this->data['payment_option']['credit_option'] = $credit['default'];
				foreach($credit['options'] as $key_credit => $credit_option_data){
					if($credit_option_data['months'] == $credit_months){
						$this->data['payment_option']['credit_option'] = $credit_option_data;
					}
				}
				
				$creditMonthlyAmount = niceDisplayPrice(getCreditMonthlyAmount($this->data['payment_option']['credit_option'], $this->data['order']['order_delivery_price']), [
					'useDecimals' => 1 === (int) $this->data['order']['order_price_decimals']
				]);
				$this->data['payment_option']['credit_option']['creditMonthlyAmount'] = $creditMonthlyAmount;
			}
		}

		$this->data['breadcrumbs'] = $this->breadcrumbs;
		
		$this->data['main_content'] = 'orders/details_view';
		$this->load->view($this->theme->public_view('shop_view'), $this->data);
	}

	function ajax_list_dt(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		if(!$this->lauth->logged_in()){
			jsonResponse('Ошибка! Пройдите авторизацию.');
		}

		$params = array(
            'limit' => intVal($_POST['iDisplayLength']),
            'start' => intVal($_POST['iDisplayStart']),
			'id_user' => $this->session->userdata('id_user')
        );
		
		$params['sort_by'] = flat_dt_ordering($_POST, array(
			'dt_name'     => 'id_order',
			'dt_amount'   => 'order_price_final',
			'dt_created'  => 'order_created'
		));
		
        $records = $this->orders->handler_get_all($params);
        $records_total = $this->orders->handler_get_count($params);

        $output = array(
            "sEcho" => intval($_POST['sEcho']),
            "iTotalRecords" => $records_total,
            "iTotalDisplayRecords" => $records_total,
            "aaData" => array()
        );

        foreach ($records as $record) {
			$status = get_order_status($record['order_status']);
			$ordered_items = json_decode($record['order_ordered_items'], true);
			$delivery_option = $this->delivery->handler_get($record['order_delivery_option']);	
			$id_payment = (int) $record['order_payment_option'];
			$payment_option = $this->payment->handler_get($record['order_payment_option']);
			if(!empty($payment_option) && $payment_option['payment_by_credit'] == 1){
				$credit = getItemsCreditOptions($ordered_items, true, $record['order_cashback']);
				if($credit){
					$order_credit_data = json_decode($record['order_payment_data'], true);
					$credit_months = 0;
					if(!empty($order_credit_data)){
						$credit_months = $order_credit_data['months'];
					}

					$payment_option['credit_option'] = $credit['default'];
					foreach($credit['options'] as $key_credit => $credit_option_data){
						if($credit_option_data['months'] == $credit_months){
							$payment_option['credit_option'] = $credit_option_data;
						}
					}
					
					$creditMonthlyAmount = niceDisplayPrice(getCreditMonthlyAmount($payment_option['credit_option'], $record['order_delivery_price']), [
						'useDecimals' => 1 === (int) $record['order_price_decimals']
					]);
					$payment_option['credit_option']['creditMonthlyAmount'] = $creditMonthlyAmount;
				}
			}

			$show_payment_button = false;
			$status_payment_procesing = false;
			if($record['order_payment_card'] == 1 && true === $status['is_payable']){
				$show_payment_button = true;
				if($record['order_payment_card_status'] !== 'paid'){
					$payment_info = Modules::run('payment/_get_paynet_payment', $record['id_order']);
	
					if($payment_info->IsOk()){
						$show_payment_button = !in_array($payment_info->Data[0]['Status'], array(2,3));
						$status_payment_procesing = in_array($payment_info->Data[0]['Status'], array(2,3));
					}
				}
			}

			$order_detail = $this->load->view($this->theme->public_view('orders/details_view'), array(
				'order' => $record, 
				'delivery_option' => $delivery_option, 
				'payment_option' => $payment_option,
				'show_payment_button' => $show_payment_button,
				'status_payment_procesing' => $status_payment_procesing
			), true);

            $output['aaData'][] = array(
                'dt_toggle'		=> '<a class="order_details" href="#"><span class="ca-icon ca-icon_plus"></span></a>',
                'dt_name'		=> orderNumber($record['id_order']),
                'dt_amount'		=> $record['order_price_final'] .' '. $record['order_currency'],
                'dt_created'	=> getDateFormat($record['order_created']),
				'dt_status' 	=> '<div class="label label-'.$status['color_class'].'">'.$status['title'].'</div>',
				'dt_detail'		=> $order_detail
            );
        }
        jsonResponse('', 'success', $output);
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(3);
		switch($option){
			case 'add':
				$this->form_validation->set_rules('user_name', 'Имя', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_email', 'Email', 'xss_clean|valid_email');
				$this->form_validation->set_rules('user_phone', 'Телефон', 'required|xss_clean|max_length[12]');
				$this->form_validation->set_rules('user_address', 'Адрес', 'required|xss_clean|max_length[150]');
				$this->form_validation->set_rules('user_comment', 'Комментарий к заказу', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('delivery_method', 'Способ доставки', 'required|xss_clean|is_natural_no_zero');
				$this->form_validation->set_rules('payment_method', 'Способ оплаты', 'required|xss_clean|is_natural_no_zero');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$basketHash = get_cookie('_cart_products');
				if(empty($basketHash) || empty($basket = $this->cart->handler_get($basketHash))){
					jsonResponse('В корзине нет товаров!');
				}

				$basketProductsDetails = json_decode($basket['basket_data'], true);
				if(
					empty($basketProductsDetails) || 
					empty($products = Modules::run('items/_get_all', [
						'items_list' => array_keys($basketProductsDetails)
					]))
				){
					jsonResponse('В корзине нет товаров!');
				}

				$idDelivery = (int) $this->input->post('delivery_method');
				$deliveryMethod = $this->delivery->handler_get($idDelivery);
				if(empty($deliveryMethod)){
					jsonResponse('Метод доставки не найден.');
				}

				$idPayment = (int) $this->input->post('payment_method');
				$paymentMethod = $this->payment->handler_get($idPayment);
				if(empty($paymentMethod)){
					jsonResponse('Способ оплаты не найден.');
				}

				$paymentDeliveryRelation = $this->_get_payment_delivery_relation($idPayment, $idDelivery);
				if(empty($paymentDeliveryRelation)){
					jsonResponse('Способ оплаты и Метод доставки не доступны.');
				}

				$orderUserPhone = '';
				if($this->input->post('user_phone')){
					$phoneFormatResult = formatPhoneNumber($this->input->post('user_phone'));
					if(false === $phoneFormatResult['is_valid']){
						jsonResponse('Номер телефона не валиден.');
					}

					$orderUserPhone = $phoneFormatResult['formated'];
				}

				$categoriesId = array_unique(array_map('intval', array_column($products, 'id_category')));
				$oversizedCategoriesId = array_column(Modules::run('categories/_get_all', [
					'id_category' => $categoriesId, 
					'is_oversized' => 1]
				), 'category_id');
				
				$promoCodeParams = [];
				if($basket['id_pcode'] > 0){
					$promoCodeParams['id_pcode'] = (int) $basket['id_pcode'];
					$promoCodeParams['id_hash'] = (int) $basket['id_pcode_hash'];
				}

				$promoCode = Modules::run('promo_code/_get_active', $promoCodeParams);
				if($basket['id_pcode'] == 0 || !empty($promoCode) && (int) $basket['id_pcode'] !== (int) $promoCode['id_pcode']){
					$promoCode = [];
				}

				if (!empty($promoCode)) {
					Modules::run('promo_code/_up_hash_used_quantity', $promoCode['id_hash']);
				}

				$userInfo = $this->lauth->getClient(true);
				$useDecimals = 1 === (int) @$userInfo['group_view_decimals'];

				$insertOrder = [
					'id_pcode' => !empty($promoCode) ? $promoCode['id_pcode'] : 0,
					'id_pcode_hash' => !empty($promoCode) ? $promoCode['id_hash'] : 0,
					'order_user_name' => $this->input->post('user_name', true),
					'order_user_email' => $this->input->post('user_email', true),
					'order_user_address' => $this->input->post('user_address', true),
					'order_user_comment' => $this->input->post('user_comment', true),
					'order_user_phone' => $orderUserPhone,
					'order_delivery_option' => $idDelivery,
					'order_payment_option' => $idPayment,
					'order_created' => date('Y-m-d H:i:s'),
					'id_user' => !empty($userInfo) ? (int) $userInfo['id'] : 0
				];

				$idOrder = $this->orders->handler_insert($insertOrder);
				$priceColumn = getPriceColumn();

				$cashbackAmount = 0;
				$applyBonusForAmount = 0;
				$countOversizeItems = 0;
				$orderAmount = 0;
				$orderCurrency = [];
				$orderedProducts = [];
				foreach($products as $productKey => $product){
					$itemQuantity = (int) $basketProductsDetails[$product['id_item']]['quantity'];

					$product['item_cart_qty'] = $itemQuantity;
					if(in_array($product['id_category'], $oversizedCategoriesId)){
						$countOversizeItems += $itemQuantity;
					}

					if(empty($promoCode) && 1 === (int) @$userInfo['group_view_cashback']){
						$cashbackAmount += $product['item_cashback_price'] * $itemQuantity;
					}

					$product['priceObject'] = getPriceObject($product[$priceColumn['db_price_column']], [
						'tempAmount' => $product['item_temp_price'],
						'pcode' => $promoCode
					]);

					$productAmount = $product['priceObject']['display_price'] * $itemQuantity;
					$orderAmount += $productAmount;
					if($product['item_cashback_price'] == 0 && 1 === (int) @$userInfo['group_view_cashback']){
						$applyBonusForAmount += $productAmount;
					}
					
					if(empty($orderCurrency)){
						$orderCurrency = [
							'code' => $product['priceObject']['currency_code'],
							'symbol' => $product['priceObject']['currency_symbol'],
							'number' => $product['priceObject']['currency_number'],
							'rate' => $product['priceObject']['currency_rate'],
						];
					}

					
					$orderedProducts[] = [
						'id_order' => $idOrder,
						'id_item' => $product['id_item'],
						'item_title' => $product['item_title'],
						'item_code' => $product['item_code'],
						'item_prog_id' => $product['item_prog_id'],
						'item_url' => $product['item_url'],
						'item_photo' => $product['item_photo'],
						'item_price' => $product['priceObject']['display_price'],
						'item_price_final' => $productAmount,
						'item_cashback_price' => 1 === (int) @$userInfo['group_view_cashback'] ? 1 * $product['item_cashback_price'] : 0,
						'item_currency' => $orderCurrency['symbol'],
						'item_quantity' => $itemQuantity,
						'item_zero_credit' => !empty($promoCode) ? 0 : (int) $product['item_zero_credit'],
						'item_zero_credit_months' => !empty($promoCode) ? 0 : (int) $product['item_zero_credit_months'],
						'item_has_promo_discount' => ($product['priceObject']['has_promo_discount'] == true) ? 1 : 0,
						'is_oversize' => (int) in_array($product['id_category'], $oversizedCategoriesId)
					];
				}

				$this->orders->handler_insert_orders_items($orderedProducts);
				$orderedProducts = $this->orders->handler_get_orders_items($idOrder);

				$userBonus = 1 === (int) @$userInfo['group_view_cashback'] && (float) @$userInfo['user_cashback_balance'] > 0 ? $userInfo['user_cashback_balance'] * 1: 0;
				$applyBonusAmount = min([($cashbackAmount + $userBonus), $applyBonusForAmount]);
				$nextOrderCashbackAmount = (($cashbackAmount + $userBonus) <= $applyBonusForAmount) ? 0 : (($cashbackAmount + $userBonus) - $applyBonusForAmount);

				$updateOrder = array(
					'order_price' => $orderAmount,
					'order_price_final' => $orderAmount,
					'order_cashback' => $applyBonusAmount,
					'order_price_decimals' => $useDecimals,
					'order_currency' => $orderCurrency['symbol'],
					'order_currency_code' => $orderCurrency['code'],
					'order_currency_number' => $orderCurrency['number'],
					'order_ordered_items' => json_encode($orderedProducts),
					'order_hash' => getHash(uniqid("order-{$idOrder}-") . '-email-' . $insertOrder['order_user_email']),
					'order_payment_data' => json_encode([]),
					'order_delivery_price' => 0
				);

				if(compare_float_numbers(0, (float) $paymentMethod['payment_add_percent'], '<')){
					$paymentAddOptions = json_decode($paymentMethod['payment_add_options'], true);
					if(!empty($paymentAddOptions[$orderCurrency['code']])){
						$updateOrder['order_payment_add_percent'] = (float) $paymentAddOptions[$orderCurrency['code']];
						$updateOrder['order_payment_add_price'] = $orderAmount * (float) $paymentAddOptions[$orderCurrency['code']] / 100;
					}
				}

				if(1 === (int) $paymentMethod['payment_by_credit']){
					$orderCredit = getItemsCreditOptions($orderedProducts, (float) @$userInfo['user_cashback_balance']);
					if(false !== $orderCredit && !empty($orderCredit)){
						$months = (int) $this->input->post('credit_months', true);
						$paymentMethod['credit_option'] = $orderCredit['default'];
						foreach($orderCredit['options'] as $keyCredit => $creditOption){
							if((int) $creditOption['months'] === $months){
								$paymentMethod['credit_option'] = $creditOption;
							}
						}

						$updateOrder['order_payment_data'] = json_encode([
							'months' => $months,
							'creditOption' => $paymentMethod['credit_option']
						]);
					}
				} else if(1 === (int) $paymentMethod['payment_by_card']){
					$updateOrder['order_payment_card'] = 1;
					$updateOrder['order_payment_card_status'] = 'not_paid';
				}

				$deliveryPrice = 0;
				$deliveryPriceOptions = json_decode($deliveryMethod['delivery_price_options'], true);                                            
				if(isset($deliveryPriceOptions[$orderCurrency['code']])){
					$deliveryPriceOption = $deliveryPriceOptions[$orderCurrency['code']];

					$deliveryPrice = compare_float_numbers($orderAmount, (float) $deliveryPriceOption['price_free'], '>=') ? 0 : $deliveryPriceOption['price'];
					if(!empty($deliveryPriceOption['price_for_oversize'])){
						$deliveryPrice += $deliveryPriceOption['price_for_oversize'] * $countOversizeItems;
					}
				}

				$updateOrder['order_delivery_price'] = $deliveryPrice;

				$this->orders->handler_update($idOrder, $updateOrder);
				$this->orders->handler_update_search_info($idOrder);

				$this->cart->handler_delete($basketHash);
				delete_cookie('_cart_products');

				$orderDetails = $this->orders->handler_get($idOrder);

				$subject = 'Вы оформили заказ номер '.orderNumber($idOrder).' на сайте atehno.md';
				$emailData = [
					'title' => $subject,
					'payment' => $paymentMethod,
					'delivery' => $deliveryMethod,
					'order' => $orderDetails,
					'email_content' => 'order_detail_tpl'
				];

				Modules::run('email/send', array(
					'to' => $emailData['order']['order_user_email'],
					'subject' => $subject,
					'email_data' => $emailData
				));

				$subject = 'Клиент оформил заказ номер '.orderNumber($idOrder).' на сайте atehno.md';
				$emailData['title'] = $subject;
				$emailData['email_content'] = 'order_detail_admin_tpl';

				Modules::run('email/send', array(
					'to' => $this->data['settings']['order_admin_email']['setting_value'],
					'subject' => $subject,
					'email_data' => $emailData
				));

				$userGroupID = empty($userInfo) ? -1 : (int) $userInfo['id_group'];
				if(
					isValidMobileNumber($orderDetails['order_user_phone']) 
					&& !empty($sendSmsTemplate = Modules::run('sms/_getOrderPaymentDeliveryTemplate', (int) $orderDetails['order_status'], (int) $paymentDeliveryRelation['id_relation']))
					&& !empty($smsConfigs = Modules::run('sms/_getConfigs'))
					&& in_array($userGroupID, $smsConfigs['groups_of_users']??[])
				){
					Modules::run('sms/_send', $orderDetails['order_user_phone'], [
						'idTemplate' => $sendSmsTemplate['id_template'], 
						'replace' => [
							"[ORDER_ID]" => orderNumber($idOrder),
							"[ORDER_SHORT_URL]" => Modules::run('ushort/_create', site_url("order/{$orderDetails['order_hash']}"))
						]
					]);
				}

				jsonResponse('', 'success', array('redirect_to' => base_url('order/'.$updateOrder['order_hash'])));
			break;
		}
	}

	function show_email(){
		$delivery_option = $this->delivery->handler_get(3);
		$email_data = array(
			'order' => $this->orders->handler_get(8),
			'delivery' => $delivery_option,
			'title' => 'Вы оформили заказ номер '.orderNumber(8),
			'email_content' => 'order_detail_tpl'
		);
		$email_data['email_content'] = 'order_detail_tpl';
		$this->load->view('email_templates/email_tpl',$email_data);
	}

	function _get_by_hash($hash){
		return $this->orders->handler_get_by_hash($hash);
	}

	function _get($id_order){
		return $this->orders->handler_get($id_order);
	}

	function _update($id_order, $update){
		return $this->orders->handler_update($id_order, $update);
	}
	
	function _get_all($params = array()){
		return $this->orders->handler_get_all((array) $params);
	}
	
	function _get_payment_delivery_relation(int $id_payment, int $id_delivery){
		return $this->orders->handler_get_payment_delivery_relation($id_payment, $id_delivery);
	}
	
	function _getPaymentDeliveryRelationsDetails(){
		$paymentOptions = arrayByKey(Modules::run('payment/_get_all'), 'id_payment');
		$paymentDeliveryRelations = arrayByKey(Modules::run('payment/_get_delivery_relations'), 'id_delivery', true);
		$deliveryOptions = Modules::run('delivery/_get_all');

		$results = [];
		foreach ($deliveryOptions as $deliveryOption) {
			if(empty($paymentDeliveryRelations[$deliveryOption['id_delivery']])){
				continue;
			}

			$record = [
				'delivery' => $deliveryOption,
				'paymentRelations' => []
			];

			foreach ($paymentDeliveryRelations[$deliveryOption['id_delivery']] as $relationDetails) {
				if(empty($paymentOptions[$relationDetails['id_payment']])){
					continue;
				}

				$record['paymentRelations'][] = array_merge($relationDetails, [
					'paymentDetail' => $paymentOptions[$relationDetails['id_payment']]
				]);
			}

			$results[] = $record;
		}

		return $results;
	}

	function _getDeliveryPaymentsRelationsOptions($smsTemplateRelationsSelected = []){
		$records = $this->_getPaymentDeliveryRelationsDetails();
		
		$optionsString = '';
		foreach ($records as $record) {
			$optionsString .= '<optgroup label="'.clean_output($record['delivery']['delivery_title']).'">';
			foreach ($record['paymentRelations'] as $paymentRelation) {
				$optionsString .= '<option value="'.$paymentRelation['id_relation'].'" '. get_choice('selected', in_array($paymentRelation['id_relation'], $smsTemplateRelationsSelected)) .'>'.$paymentRelation['paymentDetail']['payment_title'].'</option>';
			}
			$optionsString .= '</optgroup>';
		}

		return $optionsString;
	}

	function _change_status(array $params){		
		$privateToken = hash('sha512', 'changeOrderStatusFromExternalResource');
		$queryToken = $params['token'] ?? null;
		if ($privateToken !== $queryToken) {
			return "Error1";
		}

		$changes = [];
		$adminChanges = [];
		$update = [];
				
		$idOrder = (int) $params['order'] ?? null;
		$orderDetails = $this->orders->handler_get($idOrder);
		if(empty($orderDetails)){
			return "Error2";
		}

		$orderStatuses = get_order_statuses();

		$idStatus = $params['status'];
		if(!array_key_exists($idStatus, $orderStatuses)){
			return "Error3";
		}

		if(array_key_exists($orderDetails['order_status'], get_order_processing_statuses()) && array_key_exists('manager', $params)){
			$idManager = (int) $params['manager'];
			$idManagerOld = (int) $orderDetails['id_manager'];
			
			if($idManagerOld > 0){
				$oldManager = Modules::run('users/_get', $idManagerOld);
			}

			$manager = Modules::run('users/_get', $idManager);
			if($idManagerOld !== $idManager){
				if(!empty($manager)){
					if(!empty($oldManager)){
						$adminChanges['id_manager'] = "1C: Изменился Менеджер: c {$oldManager->user_login} на {$manager->user_login}";
					} else{
						$adminChanges['id_manager'] = "1C: Прикрепился Менеджер: {$manager->user_login}";
					}
				} else if(!empty($oldManager)){
					$adminChanges['id_manager'] = "1C: Открепился Менеджер: {$oldManager->user_login}";
				}
			}

			$update['id_manager'] = $idManager;
		}

		$deliveryDetails = $this->delivery->handler_get($orderDetails['order_delivery_option']);
		$currentStatus = get_order_status($orderDetails['order_status']);
		if(false === $currentStatus['is_final']){
			$newStatus = $orderStatuses[$idStatus];
			if((int) $orderDetails['order_status'] !== $newStatus['id']){
				$update['order_status'] = $idStatus;
				$changes['status'] = "Изменился статус заказа: {$newStatus['title']}.";
				$adminChanges['status'] = "Изменился статус заказа, через 1С, на: {$newStatus['title']}.";

				$adminTimeline = json_decode($orderDetails['order_admin_timeline'], true);
				$adminTimeline[] = array(
					'date' => date('d.m.Y H:i:s'),
					'name' => "1C",
					'notes' => $adminChanges
				);
				$update['order_admin_timeline'] = json_encode($adminTimeline);
			}
		}

		if(empty($update)){
			return;
		}

		// UPDATE USER CASHBACK
		$clientDetails = $this->lauth->getClient((int) $orderDetails['id_user']);
		if(!empty($clientDetails) && 1 === (int) $clientDetails['group_view_cashback']){
			$newStatus = get_order_status($idStatus);
			if(false === $currentStatus['is_final'] && true === $newStatus['is_final']){
				$userBonus =  (float) $clientDetails['user_cashback_balance'];
				$cashbackAmount = 0;
				$applyBonusForAmount = 0;

				$ordered_items = arrayByKey(json_decode($orderDetails['order_ordered_items'], true), 'id_order_item');
				foreach ($ordered_items as $ordered_item) {
					if($ordered_item['item_cashback_price'] == 0 && 1 === (int) $clientDetails['group_view_cashback']){
						$applyBonusForAmount += $ordered_item['item_price_final'];
					}

					if(0 === (int) $orderDetails['id_pcode'] && 1 === (int) $clientDetails['group_view_cashback']){
						$cashbackAmount += $ordered_item['item_cashback_price'] * $ordered_item['item_quantity'];
					}
				}

				$applyBonusAmount = min([($cashbackAmount + $userBonus), $applyBonusForAmount]);
				$nextOrderCashbackAmount = (($cashbackAmount + $userBonus) <= $applyBonusForAmount) ? 0 : (($cashbackAmount + $userBonus) - $applyBonusForAmount);

				$this->auth_model->handler_update($orderDetails['id_user'], array('user_cashback_balance' => $nextOrderCashbackAmount));
				$update['order_cashback'] = $applyBonusAmount;
			}
		}

		$this->orders->handler_update($idOrder, $update);

		$idPayment = (int) $orderDetails['order_payment_option'];
		$paymentOption = $this->payment->handler_get($idPayment);
		if($paymentOption['payment_by_credit'] == 1){
			$orderedItems = arrayByKey(json_decode($orderDetails['order_ordered_items'], true), 'id_order_item');
			$credit = getItemsCreditOptions($orderedItems, $orderDetails['order_cashback']);                        
			if($credit){
				$orderCreditDetails = json_decode($orderDetails['order_payment_data'], true);
				$creditMonths = 0;
				if(!empty($orderCreditDetails)){
					$creditMonths = $orderCreditDetails['months'];
				}

				$paymentOption['credit_option'] = $credit['default'];
				foreach($credit['options'] as $creditOption){
					if($creditOption['months'] == $creditMonths){
						$payment_option['credit_option'] = $creditOption;
					}
				}

				$creditMonthlyAmount = niceDisplayPrice(getCreditMonthlyAmount($paymentOption['credit_option'], $orderDetails['order_delivery_price']), [
					'useDecimals' => 1 === (int) $orderDetails['order_price_decimals']
				]);
				$paymentOption['credit_option']['creditMonthlyAmount'] = $creditMonthlyAmount;
			}
		}
		
		$orderDetails = $this->orders->handler_get($idOrder);
		$paymentDeliveryRelation = $this->_get_payment_delivery_relation($idPayment, (int) $orderDetails['order_delivery_option']);

		$emailData = array(
			'order' => $orderDetails,
			'delivery' => $this->delivery->handler_get($orderDetails['order_delivery_option']),
			'payment' => $paymentOption,
			'title' => 'Изменения в заказе номер '.orderNumber($idOrder).' на сайте atehno.md',
			'changes' => $changes,
			'email_content' => 'order_detail_admin_tpl'
		);
		
		Modules::run('email/send', array(
			'to' => $this->data['settings']['order_admin_email']['setting_value'],
			'subject' => '1C: Изменения в заказе номер '.orderNumber($idOrder).' на сайте atehno.md',
			'email_data' => $emailData
		));

		$emailData['email_content'] = 'order_detail_tpl';
		Modules::run('email/send', array(
			'to' => $emailData['order']['order_user_email'],
			'subject' => 'Изменения в заказе номер '.orderNumber($idOrder).' на сайте atehno.md',
			'email_data' => $emailData
		));

		$userGroupID = empty($clientDetails) ? -1 : (int) $clientDetails['id_group'];
		if(
			!empty($paymentDeliveryRelation)
			&& isValidMobileNumber($orderDetails['order_user_phone']) 
			&& !empty($sendSmsTemplate = Modules::run('sms/_getOrderPaymentDeliveryTemplate', (int) $orderDetails['order_status'], (int) $paymentDeliveryRelation['id_relation']))
			&& !empty($smsConfigs = Modules::run('sms/_getConfigs'))
			&& in_array($userGroupID, $smsConfigs['groups_of_users']??[])
		){
			Modules::run('sms/_send', $orderDetails['order_user_phone'], [
				'idTemplate' => $sendSmsTemplate['id_template'], 
				'replace' => [
					"[ORDER_ID]" => orderNumber($idOrder),
					"[ORDER_SHORT_URL]" => Modules::run('ushort/_create', site_url("order/{$orderDetails['order_hash']}"))
				]
			]);
		}

		return "Success";
	}

	// function _calculate(array $params){
	// 	if(
	// 		empty($params['basketProductsDetails']) || 
	// 		empty($products = Modules::run('items/_get_all', [
	// 			'items_list' => array_keys($params['basketProductsDetails'])
	// 		]))
	// 	){
	// 		jsonResponse('В корзине нет товаров!');
	// 	}

	// 	$idDelivery = (int) $this->input->post('delivery_method');
	// 	$deliveryMethod = $this->delivery->handler_get($idDelivery);
	// 	if(empty($deliveryMethod)){
	// 		jsonResponse('Метод доставки не найден.');
	// 	}

	// 	$idPayment = (int) $this->input->post('payment_method');
	// 	$paymentMethod = $this->payment->handler_get($idPayment);
	// 	if(empty($paymentMethod)){
	// 		jsonResponse('Способ оплаты не найден.');
	// 	}

	// 	$categoriesId = array_unique(array_map('intval', array_column($products, 'id_category')));
	// 	$oversizedCategoriesId = array_column(Modules::run('categories/_get_all', [
	// 		'id_category' => $categoriesId, 
	// 		'is_oversized' => 1]
	// 	), 'category_id');

	// 	$userInfo = $this->lauth->getClient(true);
	// 	$useDecimals = 1 === (int) @$userInfo['group_view_decimals'];
	// 	$priceColumn = getPriceColumn();

	// 	$cashbackAmount = 0;
	// 	$applyBonusForAmount = 0;
	// 	$countOversizeItems = 0;
	// 	$orderAmount = 0;
	// 	$orderCurrency = [];
	// 	$orderedProducts = [];
	// 	foreach($products as $productKey => $product){
	// 		$itemQuantity = (int) $params['basketProductsDetails'][$product['id_item']]['quantity'];

	// 		$product['item_cart_qty'] = $itemQuantity;
	// 		if(in_array($product['id_category'], $oversizedCategoriesId)){
	// 			$countOversizeItems += $itemQuantity;
	// 		}

	// 		if(empty($promoCode) && 1 === (int) @$userInfo['group_view_cashback']){
	// 			$cashbackAmount += $product['item_cashback_price'] * $itemQuantity;
	// 		}

	// 		$product['priceObject'] = getPriceObject($product[$priceColumn['db_price_column']], [
	// 			'tempAmount' => $product['item_temp_price'],
	// 			'pcode' => $promoCode
	// 		]);

	// 		$productAmount = $product['priceObject']['display_price'] * $itemQuantity;
	// 		$orderAmount += $productAmount;
	// 		if($product['item_cashback_price'] == 0 && 1 === (int) @$userInfo['group_view_cashback']){
	// 			$applyBonusForAmount += $productAmount;
	// 		}
			
	// 		if(empty($orderCurrency)){
	// 			$orderCurrency = [
	// 				'code' => $product['priceObject']['currency_code'],
	// 				'symbol' => $product['priceObject']['currency_symbol'],
	// 				'number' => $product['priceObject']['currency_number'],
	// 				'rate' => $product['priceObject']['currency_rate'],
	// 			];
	// 		}

			
	// 		$orderedProducts[] = [
	// 			'id_order' => $idOrder,
	// 			'id_item' => $product['id_item'],
	// 			'item_title' => $product['item_title'],
	// 			'item_code' => $product['item_code'],
	// 			'item_prog_id' => $product['item_prog_id'],
	// 			'item_url' => $product['item_url'],
	// 			'item_photo' => $product['item_photo'],
	// 			'item_price' => $product['priceObject']['display_price'],
	// 			'item_price_final' => $productAmount,
	// 			'item_cashback_price' => 1 === (int) @$userInfo['group_view_cashback'] ? 1 * $product['item_cashback_price'] : 0,
	// 			'item_currency' => $orderCurrency['symbol'],
	// 			'item_quantity' => $itemQuantity,
	// 			'item_zero_credit' => !empty($promoCode) ? 0 : (int) $product['item_zero_credit'],
	// 			'item_zero_credit_months' => !empty($promoCode) ? 0 : (int) $product['item_zero_credit_months'],
	// 			'item_has_promo_discount' => ($product['priceObject']['has_promo_discount'] == true) ? 1 : 0,
	// 			'is_oversize' => (int) in_array($product['id_category'], $oversizedCategoriesId)
	// 		];
	// 	}

	// 	$this->orders->handler_insert_orders_items($orderedProducts);
	// 	$orderedProducts = $this->orders->handler_get_orders_items($idOrder);

	// 	$userBonus = 1 === (int) @$userInfo['group_view_cashback'] && (float) @$userInfo['user_cashback_balance'] > 0 ? $userInfo['user_cashback_balance'] * 1: 0;
	// 	$applyBonusAmount = min([($cashbackAmount + $userBonus), $applyBonusForAmount]);
	// 	$nextOrderCashbackAmount = (($cashbackAmount + $userBonus) <= $applyBonusForAmount) ? 0 : (($cashbackAmount + $userBonus) - $applyBonusForAmount);

	// 	$updateOrder = array(
	// 		'order_price' => $orderAmount,
	// 		'order_price_final' => $orderAmount,
	// 		'order_cashback' => $applyBonusAmount,
	// 		'order_price_decimals' => $useDecimals,
	// 		'order_currency' => $orderCurrency['symbol'],
	// 		'order_currency_code' => $orderCurrency['code'],
	// 		'order_currency_number' => $orderCurrency['number'],
	// 		'order_ordered_items' => json_encode($orderedProducts),
	// 		'order_hash' => getHash(uniqid("order-{$idOrder}-") . '-email-' . $insertOrder['order_user_email']),
	// 		'order_payment_data' => json_encode([]),
	// 		'order_delivery_price' => 0
	// 	);

	// 	if(compare_float_numbers(0, (float) $paymentMethod['payment_add_percent'], '<')){
	// 		$paymentAddOptions = json_decode($paymentMethod['payment_add_options'], true);
	// 		if(!empty($paymentAddOptions[$orderCurrency['code']])){
	// 			$updateOrder['order_payment_add_percent'] = (float) $paymentAddOptions[$orderCurrency['code']];
	// 			$updateOrder['order_payment_add_price'] = $orderAmount * (float) $paymentAddOptions[$orderCurrency['code']] / 100;
	// 		}
	// 	}

	// 	if(1 === (int) $paymentMethod['payment_by_credit']){
	// 		$orderCredit = getItemsCreditOptions($orderedProducts, (float) @$userInfo['user_cashback_balance']);
	// 		if(false !== $orderCredit && !empty($orderCredit)){
	// 			$months = (int) $this->input->post('credit_months', true);
	// 			$paymentMethod['credit_option'] = $orderCredit['default'];
	// 			foreach($orderCredit['options'] as $keyCredit => $creditOption){
	// 				if((int) $creditOption['months'] === $months){
	// 					$paymentMethod['credit_option'] = $creditOption;
	// 				}
	// 			}

	// 			$updateOrder['order_payment_data'] = json_encode([
	// 				'months' => $months,
	// 				'creditOption' => $paymentMethod['credit_option']
	// 			]);
	// 		}
	// 	} else if(1 === (int) $paymentMethod['payment_by_card']){
	// 		$updateOrder['order_payment_card'] = 1;
	// 		$updateOrder['order_payment_card_status'] = 'not_paid';
	// 	}

	// 	$deliveryPrice = 0;
	// 	$deliveryPriceOptions = json_decode($deliveryMethod['delivery_price_options'], true);                                            
	// 	if(isset($deliveryPriceOptions[$orderCurrency['code']])){
	// 		$deliveryPriceOption = $deliveryPriceOptions[$orderCurrency['code']];

	// 		$deliveryPrice = compare_float_numbers($orderAmount, (float) $deliveryPriceOption['price_free'], '>=') ? 0 : $deliveryPriceOption['price'];
	// 		if(!empty($deliveryPriceOption['price_for_oversize'])){
	// 			$deliveryPrice += $deliveryPriceOption['price_for_oversize'] * $countOversizeItems;
	// 		}
	// 	}

	// 	$updateOrder['order_delivery_price'] = $deliveryPrice;

	// 	$this->orders->handler_update($idOrder, $updateOrder);
	// 	$this->orders->handler_update_search_info($idOrder);
	// }
}