<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use App\Services\PhoneCodesService;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\NumberParseException;

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}
		
		$this->view_module_path = 'modules/orders/';
		$this->active_menu = 'orders';
		$this->active_submenu = 'orders';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model('Orders_model', 'orders');
		$this->load->model('items/Items_Model', 'items');
		$this->load->model("delivery/Delivery_model", "delivery");
		$this->load->model("payment/Payment_model", "payment");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		checkPermision('manage_orders', '/admin');

		$order_statuses = get_order_statuses();
		$params = array();
		foreach ($order_statuses as $status_key => $order_status) {
			$params['status'] = $status_key;
			$this->data['statuses_amount'][$status_key] = array(
				'totals_amount' => $this->orders->handler_sum_all($params),
				'status' => $order_status
			);
		}

		$this->data['users'] = $this->orders->handler_get_orders_users();
		$this->data['delivery_options'] = $this->delivery->handler_get_all(array('active' => 1));
		$this->data['payment_options'] = $this->payment->handler_get_all(array('active' => 1));

		$this->data['page_header'] = 'Список заказов';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'orders_scripts_view');
		$this->data['dt_filter'] = $this->theme->apanel_view($this->view_module_path . 'filter_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'change':
				checkPermisionAjax('manage_orders');

				$order = $this->input->post('order');
				if(empty($order)){
					jsonResponse('Данные не верны!');
				}

				$id_order = (int) $order['id_order'];
				$order_info = $this->orders->handler_get($id_order);
				if(empty($order_info)){
					jsonResponse('Данные не верны!');
				}
				
				$status = (int) $order['status'];
				$status_info = get_order_status($status);
				if(
					$status_info['require_1c_id'] === true
				){
					if(empty($order['prog_order_id'])){
						jsonResponse('Для изменения статуса заказа на "'. $status_info['title'] .'" заполните поле Номер реализации в 1С.');
					}

					if(!validateDate($order['pickup_date'], 'd/m/Y H:i')){
						jsonResponse('Для изменения статуса заказа на "'. $status_info['title'] .'" заполните поле Дата получения заказа.');
					}
				}

				$user_info = $this->auth_model->handler_get_by_id($order_info['id_user']);

				$changes = [];
				$admin_changes = [];
				$update_order = [];
				$update_ordered_items = [];
				$cashbackAmount = 0;
				$applyBonusForAmount = 0;

				$ordered_items = arrayByKey(json_decode($order_info['order_ordered_items'], true), 'id_order_item');
				$ordered_items_new = $order['ordered_items'];
				$order_price = 0;
				$order_price_final = 0;
				foreach ($ordered_items_new as $item_key => $ordered_item) {
					if(!array_key_exists($item_key, $ordered_items)){
						continue;
					}

					$update_ordered_items[$item_key]['id_order_item'] = $item_key;
					$update_ordered_items[$item_key]['item_quantity'] = (int) $ordered_item['quantity'];
					if($ordered_items[$item_key]['item_quantity'] != $ordered_item['quantity']){
						$changes['items_quantity'] = 'Изменилось количество товаров.';
						$admin_changes['items_quantity'] = 'Изменилось количество товаров.';
					}

					$item_discount = (float) $ordered_item['discount'];
					$update_ordered_items[$item_key]['item_discount'] = $item_discount;
					if($ordered_items[$item_key]['item_discount'] != $item_discount){
						$changes['item_discount'] = 'Изменилась скидка на товары.';
						$admin_changes['item_discount'] = 'Изменилась скидка на товары.';
					}
					
					$update_ordered_items[$item_key]['item_price_final'] = numberFormat(($ordered_items[$item_key]['item_price'] - ($ordered_items[$item_key]['item_price'] * $item_discount / 100)) * $ordered_item['quantity'], $order_info['order_price_decimals']);

					$order_price += numberFormat($ordered_items[$item_key]['item_price'], $order_info['order_price_decimals']) * $ordered_item['quantity'];

					$order_price_final += $update_ordered_items[$item_key]['item_price_final'];

					if($ordered_items[$item_key]['item_cashback_price'] == 0 && 1 === (int) @$user_info->group_view_cashback){
						$applyBonusForAmount += $update_ordered_items[$item_key]['item_price_final'];
					}

					if(0 === (int)$order_info['id_pcode'] && 1 === (int) @$user_info->group_view_cashback){
						$cashbackAmount += $ordered_items[$item_key]['item_cashback_price'] * $ordered_item['quantity'];
					}
				}
				
				if(!empty($update_ordered_items)){
					$this->orders->handler_update_orders_items($update_ordered_items);
					$ordered_items = $this->orders->handler_get_orders_items($id_order);
					$update_order['order_ordered_items'] = json_encode($ordered_items);
				}

				// UPDATE USER CASHBACK
				if(!empty($user_info) && 1 === (int) $user_info->group_view_cashback){
					if(in_array($order_info['order_status'], array_keys(get_order_processing_statuses())) && array_key_exists($status, get_order_final_status())){
						$userBonus =  (float) $user_info->user_cashback_balance;
						$applyBonusAmount = min([($cashbackAmount + $userBonus), $applyBonusForAmount]);
						$nextOrderCashbackAmount = (($cashbackAmount + $userBonus) <= $applyBonusForAmount) ? 0 : (($cashbackAmount + $userBonus) - $applyBonusForAmount);

						$this->auth_model->handler_update($order_info['id_user'], array('user_cashback_balance' => $nextOrderCashbackAmount));
						$update_order['order_cashback'] = $applyBonusAmount;
					}
				}

				// ORDER PRICE
				$update_order['order_price'] = $order_price;

				// ORDER FINAL PRICE
				$update_order['order_price_final'] = $order_price_final;
				if($order_info['order_price_final'] != $order_price_final){
					$changes['price_final'] = 'Изменилась цена заказа.';
					$admin_changes['price_final'] = 'Изменилась цена заказа: ' . $order_price_final;
				}

				// PRICE DISCOUNT
				$discount_by_currecy = $order_price - $order_price_final;
				$update_order['order_discount_by_currecy'] = $discount_by_currecy;

				// PERCENTS DISCOUNT
				$discount_by_percent = $order_price > 0 ? 100 - ($order_price_final * 100/$order_price) : 0;
				$update_order['order_discount_by_percent'] = $discount_by_percent;
				if($order_info['order_discount_by_percent'] != $discount_by_percent){
					$changes['discount'] = 'Изменилась скидка заказа.';
					$admin_changes['discount'] = 'Изменилась скидка заказа: %' . $discount_by_percent;
				}

				if(!isset($order['order_client_not_responding']) && $order_info['order_client_not_responding'] == 1){
					$update_order['order_client_not_responding'] = 0;
					$changes['order_client_not_responding'] = 'Убран признак: Клиент не отвечает.';
					$admin_changes['order_client_not_responding'] = 'Убран признак: Клиент не отвечает.';
				}

				$ordered_items = arrayByKey($ordered_items, 'id_order_item');

				if(array_key_exists($order_info['order_status'], get_order_processing_statuses()) && (int) $order_info['id_manager'] === 0){
					$update_order['id_manager'] = (int) $this->lauth->id_user();
				}

				foreach ($order as $order_key => $value) {
					switch ($order_key) {
						case 'status':
							$id_manager = isset($order['id_manager']) ? (int) $order['id_manager'] : (int) $order_info['id_manager'];
							if($id_manager > 0 && !empty($manager = Modules::run('users/_get', $id_manager))){
								$current_order_status = get_order_status($order_info['order_status']);
								if($current_order_status['is_final'] === false){
									$status = xss_clean($value);
									$order_status = get_order_status($status);
									if($order_info['order_status'] != $status){
										$update_order['order_status'] = $status;
										$changes['status'] = 'Изменился статус заказа: '.$order_status['title'].'.';
										$admin_changes['status'] = 'Изменился статус заказа: '.$order_status['title'];
									}
								}
							}
						break;
						case 'prog_order_id':
							$prog_order_id = xss_clean($value);
							$update_order['prog_order_id'] = $prog_order_id;
						break;
						case 'id_manager':
							if(array_key_exists($order_info['order_status'], get_order_processing_statuses())){
								$id_manager = (int) $value;
								$id_manager_old = (int) $order_info['id_manager'];
								
								if($id_manager_old > 0){
									$old_manager = Modules::run('users/_get', (int) $order_info['id_manager']);
								}
	
								$manager = Modules::run('users/_get', $id_manager);
								if($order_info['id_manager'] != $id_manager){
									if(!empty($manager)){
										if(!empty($old_manager)){
											$admin_changes['id_manager'] = "Изменился Менеджер: c {$old_manager->user_login} на {$manager->user_login}";
										} else{
											$admin_changes['id_manager'] = "Прикрепился Менеджер: {$manager->user_login}";
										}
									} else if(!empty($old_manager)){
										$admin_changes['id_manager'] = "Открепился Менеджер: {$old_manager->user_login}";
									}
								}
	
								$update_order['id_manager'] = $id_manager;
							}
						break;
						case 'order_client_not_responding':
							if($order_info['order_client_not_responding'] == 0){
								$update_order['order_client_not_responding'] = 1;
								$changes['order_client_not_responding'] = 'Добавлен признак: Клиент не отвечает.';
								$admin_changes['order_client_not_responding'] = 'Добавлен признак: Клиент не отвечает.';
							}
						break;
						case 'pickup_date':
							if(validateDate($value, 'd/m/Y H:i')){
								$pickup_date = getDateFormat($value, 'd/m/Y H:i', 'Y-m-d H:i:s');
								if($order_info['order_pickup_date'] != $pickup_date){
									$update_order['order_pickup_date'] = $pickup_date;
									$changes['order_pickup_date'] = 'Изменилось Обговоренная дата забора заказа.';
									$admin_changes['order_pickup_date'] = 'Изменилось Обговоренная дата забора заказа: ' . $value;
								}
							} else{
								$update_order['order_pickup_date'] = null;
							}
						break;
						case 'user_name':
							$user_name = xss_clean($value);
							if($order_info['order_user_name'] != $user_name){
								$update_order['order_user_name'] = $user_name;
								$changes['user_name'] = 'Изменилось Имя клиента.';
								$admin_changes['user_name'] = 'Изменилось Имя клиента: ' . $user_name;
							}
						break;
						case 'user_email':
							$user_email = xss_clean($value);
							if($order_info['order_user_email'] != $user_email){
								$update_order['order_user_email'] = $user_email;
								$changes['user_email'] = 'Изменился Email клиента.';
								$admin_changes['user_email'] = 'Изменился Email клиента: ' . $user_email;
							}
						break;
						case 'user_phone':
							$user_phone = xss_clean($value);
							$phoneFormatResult = formatPhoneNumber($user_phone);
							if(true === $phoneFormatResult['is_valid']){
								$user_phone = $phoneFormatResult['formated'];
							}

							if($order_info['order_user_phone'] != $user_phone){
								$update_order['order_user_phone'] = $user_phone;
								$changes['user_phone'] = 'Изменился Телефон клиента.';
								$admin_changes['user_phone'] = 'Изменился Телефон клиента: ' . $user_phone;
							}
						break;
						case 'user_address':
							$user_address = xss_clean($value);
							if($order_info['order_user_address'] != $user_address){
								$update_order['order_user_address'] = $user_address;
								$changes['user_address'] = 'Изменился Адрес клиента.';
								$admin_changes['user_address'] = 'Изменился Адрес клиента: ' . $user_address;
							}
						break;
						case 'delivery':
							$delivery = intVal($value);
							$delivery_option = $this->delivery->handler_get($delivery);
							$update_order['order_delivery_option'] = $delivery;
							if($order_info['order_delivery_option'] != $delivery){
								$changes['delivery'] = 'Изменился метод доставки.';
								$admin_changes['delivery'] = 'Изменился метод доставки: ' . $delivery_option['delivery_title'];
							}
						break;
						case 'delivery_price':
							$delivery_price = intVal($value);
							if($order_info['order_delivery_price'] != $delivery_price){
								$update_order['order_delivery_price'] = $delivery_price;
								$changes['delivery_price'] = 'Изменилась цена доставки.';
								$admin_changes['delivery_price'] = 'Изменилась цена доставки: ' . $delivery_price;
							}
						break;
						case 'user_comment':
							$user_comment = xss_clean($value);
							if($order_info['order_user_comment'] != $user_comment){
								$update_order['order_user_comment'] = $user_comment;
								$changes['user_comment'] = 'Изменился коментарий клиента.';
								$admin_changes['user_comment'] = 'Изменился коментарий клиента: ' . nl2br($user_comment);
							}
						break;
						case 'admin_comment':
							$admin_comment = xss_clean($value);
							if($order_info['order_admin_comment'] != $admin_comment){
								$update_order['order_admin_comment'] = $admin_comment;
								$admin_changes['admin_comment'] = 'Изменился коментарий администратора: ' . nl2br($admin_comment);
							}
						break;
					}
				}

				if(!empty($admin_changes)){
					$admin_timeline = json_decode($order_info['order_admin_timeline'], true);
					$admin_info = $this->lauth->user_data();
					$admin_timeline[] = array(
						'date' => date('d.m.Y H:i:s'),
						'name' => $admin_info->user_nicename,
						'notes' => $admin_changes
					);
					$update_order['order_admin_timeline'] = json_encode($admin_timeline);
				}

				if(!empty($update_order)){
					$this->orders->handler_update($id_order, $update_order);
					$this->orders->handler_update_search_info($id_order);
				}

				$updatedOrderDetails = $this->orders->handler_get($id_order);
				$paymentDeliveryRelation = $this->orders->handler_get_payment_delivery_relation((int) $updatedOrderDetails['order_payment_option'], (int) $updatedOrderDetails['order_delivery_option']);

				$id_payment = (int) $updatedOrderDetails['order_payment_option'];
				$payment_option = $this->payment->handler_get($id_payment);
				if($payment_option['payment_by_credit'] == 1){
					$credit = getItemsCreditOptions($ordered_items, $updatedOrderDetails['order_cashback']);                        
					if($credit){
						$order_credit_data = json_decode($updatedOrderDetails['order_payment_data'], true);
						$credit_months = 0;
						if(!empty($order_credit_data)){
							$credit_months = $order_credit_data['months'];
						}

						$payment_option['credit_option'] = $credit['default'];
						foreach($credit['options'] as $key_credit => $credit_option_data){
							if($credit_option_data['months'] == $credit_months){
								$payment_option['credit_option'] = $credit_option_data;
							}
						}

						$creditMonthlyAmount = niceDisplayPrice(getCreditMonthlyAmount($payment_option['credit_option'], $updatedOrderDetails['order_delivery_price']), [
							'useDecimals' => 1 === (int) $updatedOrderDetails['order_price_decimals']
						]);
						$payment_option['credit_option']['creditMonthlyAmount'] = $creditMonthlyAmount;
					}
				}

				if(!empty($changes)){
					$email_data = array(
						'order' => $updatedOrderDetails,
						'delivery' => $this->delivery->handler_get($updatedOrderDetails['order_delivery_option']),
						'payment' => $payment_option,
						'title' => 'Изменения в заказе номер '.orderNumber($id_order).' на сайте atehno.md',
						'changes' => $changes,
						'email_content' => 'order_detail_admin_tpl'
					);
					
					Modules::run('email/send', array(
						'to' => $this->data['settings']['order_admin_email']['setting_value'],
						'subject' => 'Изменения в заказе номер '.orderNumber($id_order).' на сайте atehno.md',
						'email_data' => $email_data
					));

					if($this->input->post('notify_user')){
						$email_data['email_content'] = 'order_detail_tpl';
						Modules::run('email/send', array(
							'to' => $email_data['order']['order_user_email'],
							'subject' => 'Изменения в заказе номер '.orderNumber($id_order).' на сайте atehno.md',
							'email_data' => $email_data
						));
					}

					$userGroupID = empty($user_info) ? -1 : (int) $user_info->id_group;
					if(
						(int) $updatedOrderDetails['order_status'] !== (int) $order_info['order_status']
						&& !empty($paymentDeliveryRelation)
						&& isValidMobileNumber($updatedOrderDetails['order_user_phone']) 
						&& !empty($sendSmsTemplate = Modules::run('sms/_getOrderPaymentDeliveryTemplate', (int) $updatedOrderDetails['order_status'], (int) $paymentDeliveryRelation['id_relation']))
						&& !empty($smsConfigs = Modules::run('sms/_getConfigs'))
						&& in_array($userGroupID, $smsConfigs['groups_of_users']??[])
					){
						Modules::run('sms/_send', $updatedOrderDetails['order_user_phone'], [
							'idTemplate' => $sendSmsTemplate['id_template'], 
							'replace' => [
								"[ORDER_ID]" => orderNumber($id_order),
								"[ORDER_SHORT_URL]" => Modules::run('ushort/_create', site_url("order/{$updatedOrderDetails['order_hash']}"))
							]
						]);
					}
				}

				jsonResponse('Сохраненно.', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_orders');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart'),
					'sort_by' => array(	'showroom_date-desc' )
				);

				$params['sort_by'] = flat_dt_ordering($_POST, array(
					'dt_name'     => 'id_order',
					'dt_amount'   => 'order_price_final',
					'dt_created'  => 'order_created',
					'dt_pickup'   => 'order_pickup_date'
				));
		
				//region Filters
				if($this->input->post('user')){
					$params['id_user'] = (int) $this->input->post('user');
				}
		
				if($this->input->post('id_manager')){
					$params['id_manager'] = (int) $this->input->post('id_manager');
				}
		
				if($this->input->post('status_client')){
					$params['status_client'] = (int) $this->input->post('status_client');
				}
		
				if($this->input->post('status')){
					$params['status'] = (int) $this->input->post('status');
				}
		
				if($this->input->post('delivery')){
					$params['delivery'] = (int) $this->input->post('delivery');
				}
		
				if($this->input->post('payment')){
					$params['payment'] = (int) $this->input->post('payment');
				}
		
				if($this->input->post('prog_order_id')){
					$params['prog_order_id'] = $this->input->post('prog_order_id', true);
				}
				
				if($this->input->post('order_price_final_from')){
					$params['order_price_final_from'] = (int) $this->input->post('order_price_final_from');
				}
		
				if($this->input->post('order_price_final_to')){
					$params['order_price_final_to'] = (int) $this->input->post('order_price_final_to');
				}
		
				if($this->input->post('order_created')){
					list($order_created_from, $order_created_to) = explode(' - ', $this->input->post('order_created'));

					if(validateDate($order_created_from, 'd.m.Y')){
						$params['order_created_from'] = getDateFormat($order_created_from, 'd.m.Y', 'Y-m-d');
					}

					if(validateDate($order_created_to, 'd.m.Y')){
						$params['order_created_to'] = getDateFormat($order_created_to, 'd.m.Y', 'Y-m-d');
					}
				}
		
				if($this->input->post('order_updated')){
					list($order_updated_from, $order_updated_to) = explode(' - ', $this->input->post('order_updated'));

					if(validateDate($order_updated_from, 'd.m.Y')){
						$params['order_last_update_from'] = getDateFormat($order_updated_from, 'd.m.Y', 'Y-m-d');
					}

					if(validateDate($order_updated_to, 'd.m.Y')){
						$params['order_last_update_to'] = getDateFormat($order_updated_to, 'd.m.Y', 'Y-m-d');
					}
				}
		
				if($this->input->post('order_pickup')){
					list($order_pickup_from, $order_pickup_to) = explode(' - ', $this->input->post('order_pickup'));

					if(validateDate($order_pickup_from, 'd.m.Y')){
						$params['order_pickup_date_from'] = getDateFormat($order_pickup_from, 'd.m.Y', 'Y-m-d');
					}

					if(validateDate($order_pickup_to, 'd.m.Y')){
						$params['order_pickup_date_to'] = getDateFormat($order_pickup_to, 'd.m.Y', 'Y-m-d');
					}
				}
		
				if($this->input->post('keywords')){
					$params['keywords'] = $this->input->post('keywords', true);
				}
				//endregion Filters
				
				$records = $this->orders->handler_get_all($params);
				$records_total = $this->orders->handler_get_count($params);
		
				$order_statuses = get_order_statuses();
				$managers = arrayByKey(Modules::run('users/_get_all', array(
					'group_type' => array('admin','moderator','content_menedger')
				)), 'id');

				$payment_options = arrayByKey($this->payment->handler_get_all(), 'id_payment');
				$delivery_options = arrayByKey($this->delivery->handler_get_all(), 'id_delivery');
				
				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record) use ($managers, $payment_options, $delivery_options){
						$id_manager = (int) $record['id_manager'];
						$status = get_order_status($record['order_status']);

						$client_not_responding = '';
						if((int) $record['order_client_not_responding'] === 1){
							$client_not_responding = '<div><span class="badge bg-warning">Клиент не отвечает</span></div>';
						}
			
						$_actions = array();
						if ($this->lauth->have_right('view_order_logs')) {
							$_actions[] = '<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/orders/popup/logs/'.$record['id_order']).'" >
												<i class="fad fa-clipboard-list"></i> Логи заказа
											</a>';
						}
						
						if (false === $status['is_final']) {
							$_actions[] = '<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/orders/popup/cart/'.$record['id_order']).'" >
												<i class="fad fa-credit-card-front"></i> Корзина
											</a>';
						}

						$_additional_actions = '';
						if(!empty($_actions)){
							$_additional_actions = '<div class="btn-group" role="group">
														<button id="js-dropdown-order-'.$record['id_order'].'-actions" type="button" class="btn btn-default btn-flat btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															<i class="fas fa-ellipsis-h"></i>
														</button>
														<div class="dropdown-menu dropdown-menu-right" aria-labelledby="js-dropdown-order-'.$record['id_order'].'-actions">
															'. implode('', $_actions) .'
														</div>
													</div>';
						}

						$paymentDetails = '&mdash;';
						if(!empty($payment_options[$record['order_payment_option']])){
							$paymentDetails = $this->getPaymentOptionInfo($record, $payment_options[$record['order_payment_option']]);
						}

						$deliveryDetails = '&mdash;';
						if(!empty($delivery_options[$record['order_delivery_option']])){
							$deliveryDetails = $delivery_options[$record['order_delivery_option']]['delivery_title'];
						}
			
						return array(
							'dt_name'				=> $client_not_responding . orderNumber($record['id_order']) . '<br><strong>1С: </strong>' . (!empty($record['prog_order_id']) ? $record['prog_order_id'] : '&mdash;'),
							'dt_user'				=> $record['order_user_name'],
							'dt_manager'			=> (!empty($managers[$id_manager])) ? $managers[$id_manager]['user_login'] : '&mdash;',
							'dt_amount'				=> numberFormat($record['order_price_final'] + $record['order_delivery_price'] - $record['order_cashback'], $record['order_price_decimals']) .' '. $record['order_currency'],
							'dt_payment_delivery'	=> '<strong>Оплата: </strong>' . $paymentDetails . '<br>' .
														'<strong>Доставка: </strong>' . $deliveryDetails,
							'dt_created'			=> getDateFormat($record['order_created']),
							'dt_pickup'				=> validateDate($record['order_pickup_date'], 'Y-m-d H:i:s') ? getDateFormat($record['order_pickup_date'], 'Y-m-d H:i:s', 'd/m/Y H:i') : '&mdash;',
							'dt_status' 			=> '<span class="badge bg-'.$status['color_class'].'">'.$status['title'].'</span>',
							'dt_actions'			=> '<div class="btn-group">
															<button type="button" class="btn btn-secondary btn-flat btn-sm call-popup" data-popup="#general_popup_form" data-href="'.base_url('admin/orders/popup/edit/' . $record['id_order']).'">
																<i class="fad fa-eye"></i>
															</button>
															'. $_additional_actions .'
														</div>'
						);
					}, $records)
				);
				
				foreach ($order_statuses as $status_key => $order_status) {
					$params['status'] = $status_key;
					$output['statuses_amount'][$status_key] = array(
						'totals_amount' => $this->orders->handler_sum_all($params),
						'status' => $order_status
					);
				}
				
				jsonResponse('', 'success', $output);
			break;
		}
	}

	private function getPaymentOptionInfo($order, $payment_info = null){
		if(empty($payment_info)){
			$payment_info = Modules::run('payment/_get', (int) $order['order_payment_option']);
		}

		$payment_option_info = $payment_info['payment_title'];
		
		if($payment_info['payment_by_credit'] == 1){
			$ordered_items = json_decode($order['order_ordered_items'], true);
			$credit = getItemsCreditOptions($ordered_items, $order['order_cashback']);
			if($credit){
				$order_payment_data = json_decode($order['order_payment_data'], true);
				$months = 0;
				if(!empty($order_payment_data)){
					$months = $order_payment_data['months'];
				}

				$credit_option = $credit['default'];
				foreach($credit['options'] as $key_credit => $credit_option_data){
					if($credit_option_data['months'] == $months){
						$credit_option = $credit_option_data;
					}
				}

				$creditMonthlyAmount = niceDisplayPrice(getCreditMonthlyAmount($credit_option, $order['order_delivery_price']), [
					'useDecimals' => 1 === (int) $order['order_price_decimals']
				]);

				$payment_option_info .= " на {$months} месяцев, по {$creditMonthlyAmount} {$credit_option['currency']}/мес.";
			} else{
				$payment_option_info .= ' &mdash; <strong>Недоступен!!!</strong>';
			}
		}

		return $payment_option_info;
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			case 'edit':
				checkPermisionAjax('manage_orders');

				$id_order = (int) $this->uri->segment(5);
				if(empty($id_order)){
					jsonResponse('Данные не верны!');
				}

				$this->data['order'] = $this->orders->handler_get($id_order);
				if(empty($this->data['order'])){
					jsonResponse('Данные не верны!');
				}

				if((int) $this->data['order']['id_manager'] === 0){
					$this->data['order']['id_manager'] = $this->lauth->id_user();
				}


				$this->data['status_payment_procesing'] = false;
				if($this->data['order']['order_payment_card'] == 1){
					$payment_info = Modules::run('payment/_get_paynet_payment', $this->data['order']['id_order']);
					if($payment_info->IsOk()){
						$this->data['status_payment_procesing'] = in_array($payment_info->Data[0]['Status'], array(2,3));
					}
				}
				
				$this->data['ordered_items'] = json_decode($this->data['order']['order_ordered_items'], true);
				$items_id = array_column($this->data['ordered_items'], 'id_item');
				$items = arrayByKey(Modules::run('items/_get_all', array(
					'items_list' => $items_id
				)), 'id_item');

				$suppliers = arrayByKey(Modules::run('suppliers/_get_all'), 'supplier_prog_id');
				$stocks = arrayByKey(Modules::run('stocks/_get_all'), 'stock_prog_id');
			
				$this->data['ordered_items'] = array_map(function($ordered_item) use ($items, $suppliers, $stocks){
					$temp_suppliers = array();
					$temp_stocks = array();
					if(isset($items[$ordered_item['id_item']])){
						if((int) $items[$ordered_item['id_item']]['item_suppliers_available'] > 0){
							$item_suppliers = json_decode($items[$ordered_item['id_item']]['item_suppliers'], true);
							foreach ($item_suppliers as $item_supplier) {
								$temp_suppliers[] = "<div>{$suppliers[$item_supplier['supplier_prog_id']]['supplier_name']} {$item_supplier['from']} - {$item_supplier['to']}, <span class=\"badge bg-primary\">{$item_supplier['quantity']}</span></div>";
							}
						}
						
						if((int) $items[$ordered_item['id_item']]['item_stocks_quantity'] > 0){
							$item_stocks = json_decode($items[$ordered_item['id_item']]['item_stocks'], true);
							foreach ($item_stocks as $item_stock) {
								$temp_stocks[] = "<div>{$stocks[$item_stock['stock_prog_id']]['stock_name']} <span class=\"badge bg-success\">{$items[$ordered_item['id_item']]['item_stocks_quantity']}</span></div>";
							}
						}
					}

					$ordered_item['suppliers'] = $temp_suppliers;
					$ordered_item['stocks'] = $temp_stocks;

					return $ordered_item;
				}, $this->data['ordered_items']);

				$this->data['delivery_options'] = arrayByKey($this->delivery->handler_get_all(), 'id_delivery');
				$this->data['payment_options'] = arrayByKey($this->payment->handler_get_all(), 'id_payment');
				$this->data['opened_orders'] = (in_array($this->data['order']['order_status'], array('1','2','3','4','5')) && $this->data['order']['id_user'] > 0)?$this->orders->handler_get_count(array('id_user' => $this->data['order']['id_user'], 'status' => array('1','2','3','4','5'))):0;

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'cart':
				checkPermisionAjax('manage_orders');

				$id_order = (int) $this->uri->segment(5);
				if(empty($id_order)){
					jsonResponse('Данные не верны!');
				}

				$this->data['order'] = $this->orders->handler_get($id_order);
				if(empty($this->data['order'])){
					jsonResponse('Данные не верны!');
				}

				$this->data['ordered_items'] = $this->data['order']['order_ordered_items'] = json_decode($this->data['order']['order_ordered_items'], true);
				$items_id = array_column($this->data['ordered_items'], 'id_item');
				$items = arrayByKey(Modules::run('items/_get_all', array(
					'items_list' => $items_id
				)), 'id_item');

				$suppliers = arrayByKey(Modules::run('suppliers/_get_all'), 'supplier_prog_id');
				$stocks = arrayByKey(Modules::run('stocks/_get_all'), 'stock_prog_id');
			
				$this->data['ordered_items'] = array_map(function($ordered_item) use ($items, $suppliers, $stocks){
					$temp_suppliers = array();
					$temp_stocks = array();
					if(isset($items[$ordered_item['id_item']])){
						if((int) $items[$ordered_item['id_item']]['item_suppliers_available'] > 0){
							$item_suppliers = json_decode($items[$ordered_item['id_item']]['item_suppliers'], true);
							foreach ($item_suppliers as $item_supplier) {
								$temp_suppliers[] = "<div>{$suppliers[$item_supplier['supplier_prog_id']]['supplier_name']} {$item_supplier['from']} - {$item_supplier['to']}, <span class=\"badge bg-primary\">{$item_supplier['quantity']}</span></div>";
							}
						}
						
						if((int) $items[$ordered_item['id_item']]['item_stocks_quantity'] > 0){
							$item_stocks = json_decode($items[$ordered_item['id_item']]['item_stocks'], true);
							foreach ($item_stocks as $item_stock) {
								$temp_stocks[] = "<div>{$stocks[$item_stock['stock_prog_id']]['stock_name']} <span class=\"badge bg-success\">{$items[$ordered_item['id_item']]['item_stocks_quantity']}</span></div>";
							}
						}
					}

					$ordered_item['suppliers'] = $temp_suppliers;
					$ordered_item['stocks'] = $temp_stocks;

					return $ordered_item;
				}, $this->data['ordered_items']);
				
				$this->data['pcode'] = array();
				if((int) $this->data['order']['id_pcode'] > 0){
					$this->data['pcode'] = Modules::run('promo_code/_get_used', (int) $this->data['order']['id_pcode']);
				}

				$this->data['basket_credit'] = false;
				$this->data['count_oversize_items'] = 0;
				
				$total_cashback = 0;
				$total_items_price = 0;
				$total_bonus = 0;

				// USER
				$this->data['user_info'] = $this->auth_model->handler_get_by_id($this->data['order']['id_user']);
				if(!empty($this->data['user_info'])){
					$total_cashback = $this->data['user_info']->user_cashback_balance;
				}

				foreach($this->data['ordered_items'] as $item){
					$item_quantity = (int) $item['item_quantity'];

					if((int) @$item['is_oversize'] === 1){
						$this->data['count_oversize_items'] += $item_quantity;
					}

					if(@$this->data['user_info']->group_view_cashback){
						$total_cashback += $item['item_cashback_price'] * $item_quantity;
						
						if($item['item_cashback_price'] == 0){
							$total_bonus += $item['item_price'] * $item_quantity;
						}
					}
					
					$total_items_price += $item['item_price'] * $item_quantity;
				}

				$total_bonus_price = ($total_cashback <= $total_bonus) ? $total_cashback : $total_bonus;
				$total_items_price -= $total_bonus_price;

				$credit = getCreditOptions($total_items_price);
				if($credit){
					$this->data['credit'] = $credit;
					$this->data['credit_aditional'] = array(
						'price' => $total_items_price,
						'currency' => $this->data['order']['order_currency'],
						'credit_months' => implode(',', $credit['months_list']),
						'default_months' => $credit['default']['months']
					);
					$this->data['credit_available'] = true;
				}

				$this->data['delivery_options'] = arrayByKey($this->delivery->handler_get_all(array('active' => 1)), 'id_delivery');
				$this->data['payment_options'] = arrayByKey($this->payment->handler_get_all(array('active' => 1)), 'id_payment');

				$this->data['payment_delivery_relation'] = array();
				$this->data['payment_delivery_methods'] = array();
				$this->data['delivery_payment_methods'] = array();
				$payment_delivery_relation = Modules::run('payment/_get_delivery_relations');

				if(!empty($payment_delivery_relation)){
					foreach ($payment_delivery_relation as $relation) {
						$relation_key = "{$relation['id_payment']}_{$relation['id_delivery']}";
						$this->data['payment_delivery_relation'][$relation_key] = $relation['relation_text'];
						
						$this->data['payment_delivery_methods'][$relation['id_payment']][] = $relation['id_delivery'];
						$this->data['delivery_payment_methods'][$relation['id_delivery']][] = $relation['id_payment'];
					}
				}
				$this->data['payment_delivery_relation'] = json_encode($this->data['payment_delivery_relation']);
				$this->data['payment_delivery_methods'] = $this->data['payment_delivery_methods'];
				$this->data['delivery_payment_methods'] = $this->data['delivery_payment_methods'];

				// dump($this->data);

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'cart_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'logs':
				checkPermisionAjax('view_order_logs');

				$id_order = (int) $this->uri->segment(5);
				if(empty($id_order)){
					jsonResponse('Данные не верны!');
				}

				$this->data['order'] = $this->orders->handler_get($id_order);
				if(empty($this->data['order'])){
					jsonResponse('Данные не верны!');
				}

				$logs = json_decode($this->data['order']['order_admin_timeline'], true);
				if(!empty($logs)){
					usort($logs, function($log1, $log2){
						return $log1['date'] <= $log2['date'];
					});
				} else{
					$logs = array();
				}

				$this->data['order_logs'] = $logs;

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'logs_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны!');
			break;
		}
	}
}
