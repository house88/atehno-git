<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migrate_model extends CI_Model{
	var $items = "items";
	var $categories = "categories";
	var $categories_old = "categories_old";
	var $products = "products";
	function __construct(){
		parent::__construct();
	}

	function handler_get_products(){
		return $this->db->get($this->products)->result();
	}

	function handler_get_items(){
		return $this->db->get($this->items)->result();
	}

	function handler_get_categories(){
		return $this->db->get($this->categories_old)->result();
	}

	function handler_get_categories_new(){
		return $this->db->get($this->categories)->result();
	}

    function handler_insert_batch($data = array()){
        if(empty($data)){
            return false;
        }
        
		$this->db->insert_batch($this->items, $data);
    }

    function handler_insert_batch_categories($data = array()){
        if(empty($data)){
            return false;
        }
        
		$this->db->insert_batch($this->categories, $data);
    }

	function handler_update_item($id_item, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_item', $id_item);
		$this->db->update($this->items, $data);
	}

	function handler_update_items_batch($data = array(), $key){
        if(empty($data)){
            return;
        }

        $this->db->update_batch($this->items, $data, $key);
	}

	function handler_update_category($id_category, $data = array()){
        if(empty($data)){
            return;
        }
		$this->db->where('category_id', $id_category);
        $this->db->update($this->categories, $data);
	}

	function handler_update_categories_batch($data = array(), $key){
        if(empty($data)){
            return;
        }

        $this->db->update_batch($this->categories, $data, $key);
	}

	function handler_count_category_items($categories_list = ''){
        if(empty($categories_list)){
            return 0;
        }

        $this->db->where_in('id_category', $categories_list);

		return $this->db->count_all_results($this->items);
	}

	function handler_get_users_old(){
		return $this->db->get('old_users')->result();
	}

	function handler_get_users_new(){
		$this->db->where("status", 0);
		return $this->db->get('users')->result();
	}
	
	function handler_insert_batch_users($data = array()){
		if(empty($data)){
			return false;
		}
		
		$this->db->insert_batch('users', $data);
	}
	
	function handler_update_batch_users($data = array(), $column_key = 'id'){
		if(empty($data)){
			return false;
		}
		
		$this->db->update_batch('users', $data, $column_key);
	}
	
	function handler_update_user_by_email($data = array(), $email = ''){
		if(empty($data)){
			return false;
		}

		$this->db->where('user_email', $email);		
		$this->db->update('users', $data);
	}
}
?>
