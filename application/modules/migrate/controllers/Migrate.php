<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Migrate extends MX_Controller{
	function __construct(){
		parent::__construct();
        $this->load->model("migrate_model", 'migrate');
	}

    function index(){
        $this->items();
    }

    function pinfo(){
        echo phpinfo();
    }

    function users(){
        exit('Done!!! Users are in database.');
        $users = $this->migrate->handler_get_users_old();
        $insert = array();
        foreach ($users as $user) {
            $username = array();
            $old_name = explode('|', $user->name);
            if(!empty($old_name[0])){
                if(!$this->form_validation->valid_emails($old_name[0])){
                    $username[] = $old_name[0];
                }
            }
            if(!empty($old_name[2])){
                if(!$this->form_validation->valid_emails($old_name[2])){
                    $username[] = $old_name[2];
                }
            }
            if(!empty($old_name[1])){
                if(!$this->form_validation->valid_emails($old_name[1])){
                    $username[] = $old_name[1];
                }
            }
            $username = implode(' ', $username);
            $username = mb_convert_case($username, MB_CASE_TITLE, "UTF-8"); ;

            $address_old = explode('|', $user->address);
            $address = array();
            foreach ($address_old as $address_values) {
                $value = xss_clean($address_values);
                if(!empty($value)){
                    if(!$this->form_validation->valid_emails($value)){
                        $address[] = $value;
                    }
                }
            }

            if($this->form_validation->valid_emails($user->email)){
                $insert[] = array(
                    'id_group' => 4,
                    'user_login' => $username,
                    'user_pass' => $user->password,
                    'user_nicename' => $username,
                    'user_email' => $user->email,
                    'registered_date' => $user->created,
                    'activation_key' => md5($user->email.uniqid().'@cc@ctivate'),
                    'status' => 1,
                    'user_phone' => $user->phone,
                    'user_address' => implode(', ', $address)
                );
            }
        }
        $this->migrate->handler_insert_batch_users($insert);
    }

    function update_users(){
        exit('Done!!! Users are in database.');
        $users = $this->migrate->handler_get_users_old();
        foreach ($users as $user) {
            $username = array();
            $old_name = explode('|', $user->name);
            if(!empty($old_name[0])){
                if(!$this->form_validation->valid_email($old_name[0])){
                    $username[] = $old_name[0];
                }
            }
            if(!empty($old_name[2])){
                if(!$this->form_validation->valid_email($old_name[2])){
                    $username[] = $old_name[2];
                }
            }
            if(!empty($old_name[1])){
                if(!$this->form_validation->valid_email($old_name[1])){
                    $username[] = $old_name[1];
                }
            }
            $username = implode(' ', $username);
            $username = mb_convert_case($username, MB_CASE_TITLE, "UTF-8"); ;

            if($this->form_validation->valid_email($user->email)){
                $update = array(
                    'user_login' => $username,
                    'user_nicename' => $username
                );
                $this->migrate->handler_update_user_by_email($update, $user->email);
            }
        }
    }
    
    function users_activation_token(){
        exit('Done!!! Users are in database.');
        $users = $this->migrate->handler_get_users_new();
        foreach ($users as $user) {
            $email_data = array(
                'email_content' => 'user_activation_tpl',
                'activation_key' => $user->activation_key
            );

            Modules::run('email/send', array(
                'to' => $user->user_email,
                'subject' => 'ATEHNO - Активируите ваш кабинет.',
                'email_data' => $email_data
            ));
        }
    }

	function items(){
        exit('Done!!! Items are in database.');
        ini_set('max_execution_time', 0);
		$products = $this->migrate->handler_get_products();
        foreach ($products as $product) {
            $old_images = explode('|', $product->images);
            $old_images = array_filter($old_images);
            $images = json_encode($old_images);
            $items[] = array(
                'item_prog_id' => $product->product_id,
                'old_url' => $product->url,
                'id_category_old' => $product->category_id,
                'id_brand_old' => $product->brand_id,
                'item_title' => $product->model,
                'item_description' => $product->body,
                'item_visible' => $product->enabled,
                'item_hit' => $product->hit,
                'item_photo_small' => $product->small_image,
                'item_photo' => $product->large_image,
                'mt' => $product->meta_title,
                'mk' => $product->meta_keywords,
                'md' => $product->meta_description,
                'item_created' => $product->created,
                'item_last_update' => $product->modified,
                'item_commented' => $product->commented,
                'item_price' => $product->price,
                'item_quantity' => $product->quantity,
                'item_code' => $product->pcode,
                'item_photos' => $images,
                'item_viewed' => $product->browsed
            );
        }

        $this->migrate->handler_insert_batch($items);
	}

	function items_old_images(){
        exit('Done!!! Items are in database.');
        ini_set('max_execution_time', 0);
		$products = $this->migrate->handler_get_products();
        foreach ($products as $product) {
            $old_images = explode('|', $product->images);
            $old_images = array_filter($old_images);
            $images = json_encode($old_images);
            $update[] = array(
                'item_prog_id' => $product->product_id,
                'item_photo_old' => $product->large_image,
                'item_photos_old' => $images
            );
        }

        $this->migrate->handler_update_items_batch($update, 'item_prog_id');
	}

    function categories_items_count(){
        // exit('Done!!! Categories are in database.');
        ini_set('max_execution_time', 0);
		$categories = $this->migrate->handler_get_categories_new();
        foreach ($categories as $category) {
            $categories_list = array($category->category_id);
            if(!empty($category->category_children)){
                $childrens = explode(',', $category->category_children);
                $categories_list = array_merge($categories_list,$childrens);
            }

            $counters_categories = array(
                'category_items_count' => $this->migrate->handler_count_category_items($categories_list)
            );
            $this->migrate->handler_update_category($category->category_id, $counters_categories);
        }
        $total = 0;
        foreach ($counters_categories as $value) {
            $total += $value['category_items_count'];
        }
        echo $total;
    }

    function check_items_images(){
        exit('Done!!! Items are in database.');
		$items = $this->migrate->handler_get_items();
        $total_images = 0;
        foreach ($items as $item) {
            if(!empty($item->item_photo)){
                $total_images = $total_images + 2;
            }
            if(!empty($item->item_photos)){
                $photos = json_decode($item->item_photos, true);
                $total_images = $total_images + (count($photos) - 1) * 2;
            }
        }

        echo $total_images;
    }

    function item_images_clean(){
        exit('Done!!! Items are in database.');
        ini_set('max_execution_time', 0);
        $path = 'files/items';
        remove_dir($path);
    }

    function prepare_images_path(){
        exit('Done!!! Items are in database.');
        $new_path = 'files/items/';
        create_dir($new_path);
    }

    function items_images(){
        exit('Done!!! Items are in database.');
        ini_set('max_execution_time', 0);
		$items = $this->migrate->handler_get_items();
        $url = 'http://atehno.md/files/products/';
        $new_path = 'files/items/';
        create_dir($new_path);
		$this->load->library('image_lib');

        foreach ($items as $item) {
            if(!empty($item->item_photo_old)){
                $remote_url = $url.rawurlencode($item->item_photo_old);
                $image = file_get_contents($remote_url);
                if($image){
                    $path_parts = pathinfo($remote_url);
                    $update = array(
                        'id_item' => $item->id_item,
                        'item_photo' => $item->item_url.'.'.$path_parts['extension']
                    );
                    
                    file_put_contents($new_path.$update['item_photo'], $image);
                    $config = array(
                        'source_image'      => $new_path.$update['item_photo'],
                        'create_thumb'      => false,
                        'new_image'         => FCPATH . $new_path,
                        'maintain_ratio'    => true,
                        'width'             => 800,
                        'height'            => 600
                    );
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();

                    $config = array(
                        'source_image'      => $new_path.$update['item_photo'],
                        'create_thumb'      => true,
                        'thumb_marker'      => 'thumb_500x500_',
                        'new_image'         => FCPATH . $new_path,
                        'maintain_ratio'    => true,
                        'width'             => 500,
                        'height'            => 500
                    );

                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();

                    $this->migrate->handler_update_item($item->id_item, $update);
                }

                if(!empty($item->item_photos_old)){
                    $photos = json_decode($item->item_photos_old, true);
                    if(count($photos) > 1){
                        $new_photos = array();
                        foreach($photos as $key => $photo){
                            if($key == 0){
                                continue;
                            }

                            $remote_url = $url.rawurlencode($photo);
                            $image = file_get_contents($remote_url);
                            if($image){
                                $path_parts = pathinfo($remote_url);
                                $new_photo = $item->item_url.'_'.$key.'.'.$path_parts['extension'];
                                $new_photos[] = $new_photo;
                                
                                create_dir($new_path);
                                file_put_contents($new_path.$new_photo, $image);
                                $config = array(
                                    'source_image'      => $new_path.$new_photo,
                                    'create_thumb'      => false,
                                    'new_image'         => FCPATH . $new_path,
                                    'maintain_ratio'    => true,
                                    'width'             => 800,
                                    'height'            => 600
                                );
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                
                                $config = array(
                                    'source_image'      => $new_path.$new_photo,
                                    'create_thumb'      => true,
                                    'thumb_marker'      => 'thumb_500x500_',
                                    'new_image'         => FCPATH . $new_path,
                                    'maintain_ratio'    => true,
                                    'width'             => 500,
                                    'height'            => 500
                                );
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                            }
                        }

                        if(!empty($new_photos)){
                            $this->migrate->handler_update_item($item->id_item, array('item_photos' => json_encode($new_photos)));
                        }
                    }
                }

            }

        }
    }

    function items_images_check_clean(){
        exit('Done!!! Items are in database.');
        ini_set('max_execution_time', 0);
		$items = $this->migrate->handler_get_items();
        $path = 'files/items/';

        foreach ($items as $item) {
            if(!empty($item->item_photo)){
                if(!file_exists($path.$item->item_photo)){
                    $update = array(
                        'id_item' => $item->id_item,
                        'item_photo' => '',
                        'item_photos' => ''
                    );
                    $this->migrate->handler_update_item($item->id_item, $update);
                    if(!empty($item->item_photos)){
                        $photos = json_decode($item->item_photos_old, true);
                        foreach($photos as $key => $photo){
                            @unlink($path.$photo);
                        }
                    }
                }

            }

        }
    }

    function items_aditional_images(){
        exit('Done!!! Images are in database.');
        ini_set('max_execution_time', 0);
		$items = $this->migrate->handler_get_items();
        $url = 'http://atehno.md/files/products/';
        $new_path = 'files/items/';

		$this->load->library('image_lib');

        foreach ($items as $item) {
            if(empty($item->item_photos_old)){
                continue;
            }

            $photos = json_decode($item->item_photos_old, true);
            if(count($photos) > 1){
                $new_photos = array();
                foreach($photos as $key => $photo){
                    if($key == 0){
                        continue;
                    }

                    $remote_url = $url.rawurlencode($photo);
                    $image = file_get_contents($remote_url);
                    if($image){
                        $path_parts = pathinfo($remote_url);
                        $new_photo = $item->item_url.'_'.$key.'.'.$path_parts['extension'];
                        $new_photos[] = $new_photo;
                        
                        create_dir($new_path);
                        file_put_contents($new_path.$new_photo, $image);
                        $config = array(
                            'source_image'      => $new_path.$new_photo,
                            'create_thumb'      => false,
                            'new_image'         => FCPATH . $new_path,
                            'maintain_ratio'    => true,
                            'width'             => 800,
                            'height'            => 600
                        );
                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();
                        
                        $config = array(
                            'source_image'      => $new_path.$new_photo,
                            'create_thumb'      => true,
                            'thumb_marker'      => 'thumb_500x500_',
                            'new_image'         => FCPATH . $new_path,
                            'maintain_ratio'    => true,
                            'width'             => 500,
                            'height'            => 500
                        );
                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();
                    }
                }

                if(!empty($new_photos)){
                    $this->migrate->handler_update_item($item->id_item, array('item_photos' => json_encode($new_photos)));
                }
            }
        }
    }

    function update_url(){
        // exit('Done!!! URL are in database.');
        ini_set('max_execution_time', 0);

        $config = array(
			'table' => 'items',
			'id' => 'id_item',
			'field' => 'item_url',
			'title' => 'item_title',
			'replacement' => 'dash'
		);
		$this->load->library('slug', $config);

		$items = $this->migrate->handler_get_items();
        foreach ($items as $item) {
            $update[$item->id_item] = array('id_item' => $item->id_item);
            if(empty($item->old_url)){
                $update[$item->id_item]['item_url'] = $update[$item->id_item]['old_url'] = $this->slug->create_slug(cut_str($item->item_title, 200).'-'.$item->item_prog_id);
            } else{
                $update[$item->id_item]['item_url'] = $update[$item->id_item]['old_url'] = $item->old_url;
            }
        }

        $this->migrate->handler_update_items_batch($update, 'id_item');
    }

    function update_category_url(){
        exit('Done!!! Items are in database.');
        ini_set('max_execution_time', 0);

        $config = array(
			'table' => 'categories',
			'id' => 'category_id',
			'field' => 'category_url',
			'title' => 'category_title',
			'replacement' => 'dash'
		);
		$this->load->library('slug', $config);

		$categories = $this->migrate->handler_get_categories_new();
        foreach ($categories as $category) {
            $breadcrumbs = json_decode('['.$category->category_breadcrumbs.']', true);
            $last_category = end($breadcrumbs);
            if(count($breadcrumbs) > 1){
                $url = $breadcrumbs[0]['category_title'].'_'.$last_category['url'];
            } else{
                $url = $last_category['url'];
            }
            $update[] = array(
				'category_id' => $category->category_id,
				'category_url' => $this->slug->create_slug($url)
			);
        }
        $this->migrate->handler_update_categories_batch($update, 'category_id');
    }

	function update_actional(){
		$this->load->model("items/Items_model", "items");
		$this->items->handler_update_actionals();
	}
}



?>

