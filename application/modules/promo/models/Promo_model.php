<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promo_model extends CI_Model{
	var $promo = "promo";
	var $promo_items = "promo_items";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->promo, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_promo = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_promo', $id_promo);
		$this->db->update($this->promo, $data);
	}

	function handler_delete($id_promo = 0){
		$this->db->where('id_promo', $id_promo);
		$this->db->delete($this->promo);
	}

	function handler_get_by_url($promo_url = null){
		if(empty($promo_url)){
			return false;
		}

		$this->db->where('promo_url', $promo_url);
		$this->db->limit(1);
		return $this->db->get($this->promo)->row_array();
	}

	function handler_get($id_promo = 0){
		$this->db->where('id_promo', $id_promo);
		return $this->db->get($this->promo)->row_array();
	}

	function handler_get_now($conditions = array()){
		extract($conditions);
		
		if(isset($id_promo)){
			$this->db->where('id_promo', $id_promo);
		}

		$this->db->where('pcode_active', 1);
		$this->db->where('pcode_date_start <=', date('Y-m-d'));
		$this->db->where('pcode_date_end >=', date('Y-m-d'));
		$this->db->limit(1);
		return $this->db->get($this->promo)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_promo DESC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}

			if (!empty($multi_order_by)) {
				$order_by = implode(',', $multi_order_by);
			}
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->promo)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

		return $this->db->count_all_results($this->promo);
	}

	// ITEMS PROMO
	function handler_set_item_promo($data = array()){
		if(empty($data)){
			return false;
		}

		$this->db->insert_batch($this->promo_items, $data);
		return $this->db->insert_id();
	}

	function handler_delete_item_promo($id_item = 0){
		$this->db->where('id_item', $id_item);
		$this->db->delete($this->promo_items);
	}

	function handler_get_item_all($id_item = 0){
		$this->db->where('id_item', $id_item);
		return $this->db->get($this->promo_items)->result_array();
	}
}
