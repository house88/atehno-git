<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promo extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->data = array();

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Promo_model", "promo");
		$this->load->model('items/Items_Model', 'items');
		$this->load->model('categories/Categories_Model', 'categories');
        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		$uri = $this->uri->uri_to_assoc(1);
		checkURI($uri, array('promo', 'category', 'page'), $this->data);

		$promo_url = xss_clean($uri['promo']);
		$this->data['promo'] = $this->promo->handler_get_by_url($promo_url);
		if(empty($this->data['promo'])){
			return_404($this->data);
		}
		
		if ($this->data['promo']['promo_active'] != 1) {
			return_404($this->data);
		}

		$today = date('Y-m-d');
		if ($this->data['promo']['promo_date_start'] > $today || $this->data['promo']['promo_date_end'] < $today) {
			return_404($this->data);
		}

		$products_params = array(
			'id_promo' => $this->data['promo']['id_promo'],
			'item_visible' => 1
		);

		$promo_categories = explode(';', $this->data['promo']['promo_categories']);
		$promo_settings = json_decode($this->data['promo']['promo_settings'], true);
		$this->data['selected_category_breadcrumb'] = array();
		if(!empty($promo_categories)){
			if(isset($uri['category'])){
				$uri_category = decript_uri_hash($uri['category']);
				$uri_category_components = explode(':', $uri_category);
				$category_key = !empty($uri_category_components[0]) ? $uri_category_components[0] : null;
				$sub_category_key = !empty($uri_category_components[1]) ? $uri_category_components[1] : null;
				if(!isset($promo_settings[$category_key])){
					return_404($this->data);
				}

				$this->data['selected_category'] = $category_key;
				$this->data['selected_category_breadcrumb'][] = array(
					'name' => $promo_settings[$category_key]['name'],
					'link' => site_url('promo/'.$this->data['promo']['promo_url'].'/category/'.encript_uri_hash($category_key))
				);
				$id_category = explode(';', $promo_settings[$category_key]['id_categories']);
				if (!empty($promo_settings[$category_key]['children'])) {					
					if (!empty($sub_category_key)) {
						if(!isset($promo_settings[$category_key]['children'][$sub_category_key])){
							return_404($this->data);
						}

						$this->data['selected_category_child'] = $sub_category_key;
						
						$this->data['selected_category_breadcrumb'][] = array(
							'name' => $promo_settings[$category_key]['children'][$sub_category_key]['name'],
							'link' => site_url('promo/'.$this->data['promo']['promo_url'].'/category/'.encript_uri_hash($category_key.':'.$sub_category_key))
						);

						$id_category = explode(';', $promo_settings[$category_key]['children'][$sub_category_key]['id_categories']);
					} else{
						$temp_category = first_array_element($promo_settings[$category_key]['children']);
						$this->data['selected_category_child'] = first_array_key($promo_settings[$category_key]['children']);
						
						$this->data['selected_category_breadcrumb'][] = array(
							'name' => $temp_category['name'],
							'link' => site_url('promo/'.$this->data['promo']['promo_url'].'/category/'.encript_uri_hash($category_key.':'.$this->data['selected_category_child']))
						);

						// $temp_categories = array_column($promo_settings[$category_key]['children'], 'id_categories');
						// $temp_categories = implode(';', $temp_categories);
						// $id_category = explode(';', $temp_categories);

						$id_category = explode(';', $temp_category['id_categories']);
					}
				}
			} else{
				if(!empty($promo_settings)){
					$category = first_array_element($promo_settings);
					$this->data['selected_category'] = first_array_key($promo_settings);
					$this->data['selected_category_breadcrumb'][] = array(
						'name' => $promo_settings[$this->data['selected_category']]['name'],
						'link' => site_url('promo/'.$this->data['promo']['promo_url'].'/category/'.encript_uri_hash($this->data['selected_category']))
					);

					if(!empty($category['children'])){
						$temp_category = first_array_element($category['children']);
						$this->data['selected_category_child'] = first_array_key($category['children']);
						
						$this->data['selected_category_breadcrumb'][] = array(
							'name' => $temp_category['name'],
							'link' => site_url('promo/'.$this->data['promo']['promo_url'].'/category/'.encript_uri_hash($this->data['selected_category'].':'.$this->data['selected_category_child']))
						);

						// $id_category = explode(';', $temp_category['id_categories']);
						// $temp_categories = array_column($category['children'], 'id_categories');
						// $temp_categories = implode(';', $temp_categories);
						// $id_category = explode(';', $temp_categories);
						$id_category = explode(';', $temp_category['id_categories']);
					} else{
						$id_category = explode(';', $category['id_categories']);
					}
				}
			}

			if(!empty($id_category)){				
				// GET CATEGORY
				$categories = $this->categories->handler_get_all(array(
					'id_category' => $id_category
				));

				if(empty($categories)){
					return_404($this->data);
				}
	
				$categories_list = array();
				foreach ($categories as $category) {
					$temp_categories = explode(',', $category['category_children']);
					$temp_categories = array_filter($temp_categories);
					$temp_categories[] = $category['category_id'];

					$categories_list = array_merge($categories_list, $temp_categories);
				}

				if (!empty($categories_list)) {
					$products_params['id_category'] = $categories_list;
				}
			}
		}

		$price_column = getPriceColumn();
		$products_params['item_price_column'] = $price_column['db_price_column'];

		$start = 0;
		$limit = $this->data['settings']['promo_products_per_page']['setting_value'];

		$this->data['page_number'] = 1;
		if(isset($uri['page'])){
			$page_number = (int)$uri['page'];
			$this->data['page_number'] = ($page_number > 0)?$page_number:1;
			$start = ($uri['page']  == 1) ? 0 : ($uri['page'] * $limit) - $limit;
		}
		
		$products_params['limit'] = $limit;
		$products_params['start'] = $start;
		
		// GET PRODUCTS
		$this->data['total_products'] = $total = $this->items->handler_get_count_promo($products_params);
		$this->data['products'] = $this->items->handler_get_all_promo($products_params);

		// PAGINATION
		$settings = $this->pagination_lib->get_settings('products', $total, $limit);
        $this->pagination->initialize($settings);
        $this->data['pagination'] = $this->pagination->create_links();

		$this->load->view($this->theme->public_view('promo_view'), $this->data);
	}

	function email(){
		$this->data['promo'] = $this->promo->handler_get_by_url('dell-promo');

		// GET PRODUCTS
		$products_params = array(
			'id_promo' => $this->data['promo']['id_promo'],
			'item_visible' => 1
		);

		$products_params['item_price_column'] = 'item_price';

		$promo_categories = array_filter(explode(';', $this->data['promo']['promo_categories']));
		if(empty($promo_categories)){
			$this->data['products'] = $this->items->handler_get_all_promo($products_params);
		} else{
			$this->data['categories'] = array();
			$promo_settings = json_decode($this->data['promo']['promo_settings'], true);
			foreach ($promo_settings as $key => $promo_setting) {
				if(empty($promo_setting['items_categories'])){				
					continue;
				}

				// GET CATEGORY
				$temp_categories = $this->categories->handler_get_all(array(
					'id_category' => $promo_setting['items_categories']
				));

				if(empty($temp_categories)){
					continue;
				}
	
				$id_category = array();
				foreach ($temp_categories as $category) {
					$list_categories = explode(',', $category['category_children']);
					$list_categories = array_filter($list_categories);
					$list_categories[] = $category['category_id'];

					$id_category = $list_categories;
				}

				if (empty($id_category)) {
					continue;
				}
				
				$products_params['id_category'] = $id_category;
				$this->data['categories'][$key] = array(
					'name' => $promo_setting['name'],
					'products' => $this->items->handler_get_all_promo($products_params)
				);
			}
		}

		$this->load->view('admin/email', $this->data);
	}

	function _get($id_promo = 0){
		return $this->promo->handler_get((int) $id_promo);
	}

	function _get_all($params = array()){
		return $this->promo->handler_get_all($params);
	}

	function _get_item_all($id_item = 0){
		return $this->promo->handler_get_item_all($id_item);
	}

	function _set_item_promo($insert = array()){
		if(empty($insert)){
			return false;
		}

		return $this->promo->handler_set_item_promo($insert);
	}

	function _delete_item_promo($id_item = 0){
		return $this->promo->handler_delete_item_promo((int) $id_item);
	}
}
