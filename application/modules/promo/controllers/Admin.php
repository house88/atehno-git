<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}
		
		$this->view_module_path = 'modules/promo/';
		$this->active_menu = 'special_pages';
		$this->active_submenu = 'promo';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Promo_model", "promo");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		checkPermision('manage_promo');

		$this->data['page_header'] = 'Промо страницы';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_promo');

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit':
				checkPermisionAjax('manage_promo');

				$id_promo = (int) $this->uri->segment(5);
				$this->data['promo'] = $this->promo->handler_get($id_promo);
				if(empty($this->data['promo'])){
					jsonResponse('Промо страница не найден.');
				}

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'email':
				checkPermisionAjax('manage_promo');

				$id_promo = (int) $this->uri->segment(5);
				$this->data['promo'] = $this->promo->handler_get($id_promo);
				if(empty($this->data['promo'])){
					jsonResponse('Промо страница не найден.');
				}

				$this->load->model('items/Items_Model', 'items');
				$this->load->model('categories/Categories_Model', 'categories');
				
				// GET PRODUCTS
				$products_params = array(
					'id_promo' => $id_promo,
					'item_visible' => 1
				);
		
				$products_params['item_price_column'] = 'item_price';

				$promo_categories = array_filter(explode(';', $this->data['promo']['promo_categories']));
				if(empty($promo_categories)){
					$this->data['products'] = $this->items->handler_get_all_promo($products_params);
				} else{
					$this->data['categories'] = array();
					$promo_settings = json_decode($this->data['promo']['promo_settings'], true);
					foreach ($promo_settings as $key => $promo_setting) {
						if(empty($promo_setting['items_categories'])){				
							continue;
						}

						// GET CATEGORY
						$temp_categories = $this->categories->handler_get_all(array(
							'id_category' => $promo_setting['items_categories']
						));

						if(empty($temp_categories)){
							continue;
						}
			
						$id_category = array();
						foreach ($temp_categories as $category) {
							$list_categories = explode(',', $category['category_children']);
							$list_categories = array_filter($list_categories);
							$list_categories[] = $category['category_id'];

							$id_category = $list_categories;
						}

						if (empty($id_category)) {
							continue;
						}
						
						$products_params['id_category'] = $id_category;
						$this->data['categories'][$key] = array(
							'name' => $promo_setting['name'],
							'products' => $this->items->handler_get_all_promo($products_params)
						);
					}
				}

				$this->data['email_code'] = $this->load->view($this->theme->apanel_view($this->view_module_path . 'email_code'), $this->data, true);
				
				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'email_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				checkPermisionAjax('manage_promo');

				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[200]');
				$this->form_validation->set_rules('promo_date', 'Дата активности', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				if($this->input->post('promo_date')){
					list($date_start, $date_end) = explode(' - ', $this->input->post('promo_date'));

					if(!(validateDate($date_start, 'd.m.Y') && validateDate($date_end, 'd.m.Y')) ){
						jsonResponse('Дата активности неверное.');
					}
				}

				$id_banner = (int) $this->input->post('promo_banner');
				if($id_banner > 0){
					$banner = Modules::run('banners/_get', $id_banner);
					if(empty($banner)){
						jsonResponse('Баннер не найден.');
					}
				}

				$promo_settings = (array) $this->input->post('categories');
				$promo_categories = array();
				if(!empty($promo_settings)){
					foreach ($promo_settings as $key => $category) {
						$temp_id_categories = array();
						if(!empty($category['id_categories'])){
							$temp = explode(';', $category['id_categories']);
							$temp = array_filter($temp);
							if(!empty($temp)){
								$temp_id_categories = array_merge($temp_id_categories, $temp);
							}

							$promo_settings[$key]['id_categories'] = implode(';', $temp);
						}

						if(!empty($category['children'])){
							foreach ($category['children'] as $child_key => $child) {
								if(!empty($child['id_categories'])){
									$temp = explode(';', $child['id_categories']);
									$temp = array_filter($temp);
									if(!empty($temp)){
										$temp_id_categories = array_merge($temp_id_categories, $temp);
									}

									$promo_settings[$key]['children'][$child_key]['id_categories'] = implode(';', $temp);
								}
							}
						}

						$promo_settings[$key]['items_categories'] = array_keys(array_flip($temp_id_categories));

						$promo_categories = array_merge($promo_categories, $temp_id_categories);
					}
				}

				$promo_categories = array_keys(array_flip($promo_categories));

				$promo_email = array();
				if($this->input->post('promo_email_image')){
					$promo_email['image'] = $this->input->post('promo_email_image', true);
					$promo_email['text'] = $this->input->post('promo_email_text');
				}

				$insert = array(
					'promo_name' => $this->input->post('title', true),
					'promo_url' => strForURL($this->input->post('title', true)),
					'promo_banner' => $id_banner,
					'promo_date_start' => getDateFormat($date_start, 'd.m.Y', 'Y-m-d'),
					'promo_date_end' => getDateFormat($date_end, 'd.m.Y', 'Y-m-d'),
					'promo_settings' => json_encode($promo_settings),
					'promo_email' => json_encode($promo_email),
					'promo_categories' => implode(';', $promo_categories),
					'promo_active' => $this->input->post('visible') ? 1 : 0
				);

				$this->promo->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				checkPermisionAjax('manage_promo');

				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[200]');
				$this->form_validation->set_rules('promo_date', 'Дата активности', 'required|xss_clean');
				$this->form_validation->set_rules('id_promo', 'Промо', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_promo = (int) $this->input->post('id_promo');
				$promo = $this->promo->handler_get($id_promo);
				if(empty($promo)){
					jsonResponse('Данные не верны.');
				}

				if($this->input->post('promo_date')){
					list($date_start, $date_end) = explode(' - ', $this->input->post('promo_date'));

					if(!(validateDate($date_start, 'd.m.Y') && validateDate($date_end, 'd.m.Y')) ){
						jsonResponse('Дата активности неверное.');
					}
				}

				$id_banner = (int) $this->input->post('promo_banner');
				if($id_banner > 0){
					$banner = Modules::run('banners/_get', $id_banner);
					if(empty($banner)){
						jsonResponse('Баннер не найден.');
					}
				}

				$promo_settings = (array) $this->input->post('categories');
				$promo_categories = array();
				if(!empty($promo_settings)){
					foreach ($promo_settings as $key => $category) {
						$temp_id_categories = array();
						if(!empty($category['id_categories'])){
							$temp = explode(';', $category['id_categories']);
							$temp = array_filter($temp);
							if(!empty($temp)){
								$temp_id_categories = array_merge($temp_id_categories, $temp);
							}

							$promo_settings[$key]['id_categories'] = implode(';', $temp);
						}

						if(!empty($category['children'])){
							foreach ($category['children'] as $child_key => $child) {
								if(!empty($child['id_categories'])){
									$temp = explode(';', $child['id_categories']);
									$temp = array_filter($temp);
									if(!empty($temp)){
										$temp_id_categories = array_merge($temp_id_categories, $temp);
									}

									$promo_settings[$key]['children'][$child_key]['id_categories'] = implode(';', $temp);
								}
							}
						}

						$promo_settings[$key]['items_categories'] = array_keys(array_flip($temp_id_categories));

						$promo_categories = array_merge($promo_categories, $temp_id_categories);
					}
				}

				$promo_categories = array_keys(array_flip($promo_categories));

				$promo_email = array();
				if($this->input->post('promo_email_image')){
					$promo_email['image'] = $this->input->post('promo_email_image', true);
					$promo_email['text'] = $this->input->post('promo_email_text');
				}

				$update = array(
					'promo_name' => $this->input->post('title', true),
					'promo_url' => strForURL($this->input->post('title', true)),
					'promo_banner' => $id_banner,
					'promo_date_start' => getDateFormat($date_start, 'd.m.Y', 'Y-m-d'),
					'promo_date_end' => getDateFormat($date_end, 'd.m.Y', 'Y-m-d'),
					'promo_settings' => json_encode($promo_settings),
					'promo_email' => json_encode($promo_email),
					'promo_categories' => implode(';', $promo_categories),
					'promo_active' => $this->input->post('visible') ? 1 : 0
				);

				$this->promo->handler_update($id_promo, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				checkPermisionAjax('manage_promo');

				$this->form_validation->set_rules('promo', 'Промо страница', 'required');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_promo = (int) $this->input->post('promo');
				$promo = $this->promo->handler_get($id_promo);
				if(empty($promo)){
					jsonResponse('Промо страница не найдена.');
				}

				$this->promo->handler_delete($id_promo);
				
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				checkPermisionAjax('manage_promo');

				$this->form_validation->set_rules('promo', 'Промо страница', 'required');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_promo = (int) $this->input->post('promo');
				$promo = $this->promo->handler_get($id_promo);
				if(empty($promo)){
					jsonResponse('Промо страница не найдена.');
				}
				
				$this->promo->handler_update($id_promo, array(
					'promo_active' => (int) !((int) $promo['promo_active'])
				));

				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_promo');

				$params = array(
					'limit' 	=> (int) $this->input->post('iDisplayLength', true),
					'start' 	=> (int) $this->input->post('iDisplayStart', true),
					'sort_by' 	=> flat_dt_ordering($_POST, array(
						'dt_start' 	=> 'promo_date_start',
						'dt_end'   	=> 'promo_date_end'
					))
				);
		
				$records = $this->promo->handler_get_all($params);
				$records_total = $this->promo->handler_get_count($params);
		
				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){			
						return array(
							'dt_name'		=>  '<a href="'.site_url('promo/'.$record['promo_url']).'" target="_blank">'. $record['promo_name'] .'</a>',
							'dt_start'		=>  getDateFormat($record['promo_date_start'], 'Y-m-d', 'd/m/Y'),
							'dt_end'		=>  getDateFormat($record['promo_date_end'], 'Y-m-d', 'd/m/Y'),
							'dt_active'		=>  '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['promo_active'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-promo="'.$record['id_promo'].'"></a>',
							'dt_actions'	=> '<div class="dropdown">
													<a data-toggle="dropdown" href="#" aria-expanded="false">
														<i class="fas fa-ellipsis-h"></i>
													</a>
													<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/promo/popup/email/' . $record['id_promo']).'">
															<i class="fad fa-envelope-open-text"></i> Email Код
														</a>
														<div class="dropdown-divider"></div>
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/promo/popup/edit/' . $record['id_promo']).'">
															<i class="fad fa-pencil"></i> Редактировать
														</a>
														<div class="dropdown-divider"></div>
														<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите удалить промо страницу?" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-callback="delete_action" data-promo="'.$record['id_promo'].'" data-type="bg-danger">
															<i class="fad fa-trash-alt"></i> Удалить
														</a>
													</div>
												</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
		}
	}
}
