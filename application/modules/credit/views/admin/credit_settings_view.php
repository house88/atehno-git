<h1 class="page-header">
    Кредитная программа
	
</h1>
<div class="table-responsive">
	<form role="form" id="credit_form">
		<table class="table table-striped" id="credit_form-table">
			<thead>
				<tr>
					<th class="w-150 mnw-100 text-center">Кол. месяцев</th>
					<th class="w-200 mnw-200 text-center" colspan="2">Сумма</th>
					<th class="w-200 mnw-200 text-center">Аванс</th>
					<th class="">%</th>
					<th class="w-100 text-right"></th>
				</tr>
				<tr>
					<td class="w-150 mnw-100 text-center"></td>
					<th class="w-200 mnw-200 text-center">От</th>
					<th class="w-200 mnw-200 text-center">До</th>
					<th class="w-200 mnw-200 text-center"></th>
					<th class="w-200 mnw-200"></th>
					<th class="mnw-100 text-right"></th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($credit_settings)){?>
					<?php foreach($credit_settings as $credit_setting){?>
						<tr>
							<td class="w-150 mnw-100 text-center">
								<input type="text" class="form-control" name="months[]" value="<?php echo $credit_setting['months'];?>" placeholder="Кол. месяцев">
							</td>
							<td class="w-200 mnw-200 text-center">
								<input type="text" class="form-control" name="prices_from[]" value="<?php echo $credit_setting['price_from'];?>" placeholder="Сумма от">
							</td>
							<td class="w-200 mnw-200 text-center">
								<input type="text" class="form-control" name="prices_to[]" value="<?php echo $credit_setting['price_to'];?>" placeholder="Сумма до">
							</td>
							<td class="">
								<input type="text" class="form-control" name="pre_pay[]" value="<?php echo $credit_setting['pre_pay'];?>" placeholder="Аванс">
							</td>
							<td class="">
								<input type="text" class="form-control" name="percents[]" value="<?php echo $credit_setting['percents'];?>" placeholder="%">
							</td>
							<td class="w-100 text-left">
								<span class="btn btn-danger confirm-dialog" data-callback="delete_credit_row" data-message="Вы действительно хотите удалить эту настройку?"><i class="ca-icon ca-icon_remove"></i></span>
							</td>
						</tr>
					<?php }?>
				<?php } else{?>
					<tr>
						<td class="w-150 mnw-100 text-center">
							<input type="text" class="form-control" name="months[]" placeholder="Кол. месяцев">
						</td>
						<td class="w-200 mnw-200 text-center">
							<input type="text" class="form-control" name="prices_from[]" placeholder="Сумма от">
						</td>
						<td class="w-200 mnw-200 text-center">
							<input type="text" class="form-control" name="prices_to[]" placeholder="Сумма до">
						</td>
						<td class="">
							<input type="text" class="form-control" name="pre_pay[]" value="<?php echo $credit_setting['pre_pay'];?>" placeholder="Аванс">
						</td>
						<td class="">
							<input type="text" class="form-control" name="percents[]" placeholder="%">
						</td>
						<td class="w-100 text-left">
							<span class="btn btn-danger confirm-dialog" data-callback="delete_credit_row" data-message="Вы действительно хотите удалить эту настройку?"><i class="ca-icon ca-icon_remove"></i></span>
						</td>
					</tr>
				<?php }?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="5">
						<a href="#" class="btn btn-default pull-left mr-5 call-function" data-callback="add_credit_row">
							<i class="fa fa-plus"></i>
							Добавить настройку
						</a>
						<a href="#" class="btn btn-success pull-left mr-5 call-function" data-callback="save_credit_form">
							Сохранить
						</a>
					</td>
				</tr>
			</tfoot>
		</table>
	</form>
</div>
<script>
	$(function(){
		
	});
	var add_credit_row = function(){
		var row_template = '<tr>\
								<td class="w-150 mnw-100 text-center">\
									<input type="text" class="form-control" name="months[]" placeholder="Кол. месяцев">\
								</td>\
								<td class="w-200 mnw-200 text-center">\
									<input type="text" class="form-control" name="prices_from[]" placeholder="Сумма от">\
								</td>\
								<td class="w-200 mnw-200 text-center">\
									<input type="text" class="form-control" name="prices_to[]" placeholder="Сумма до">\
								</td>\
								<td class="">\
									<input type="text" class="form-control" name="pre_pay[]" placeholder="Аванс">\
								</td>\
								<td class="">\
									<input type="text" class="form-control" name="percents[]" placeholder="%">\
								</td>\
								<td class="w-100 text-left">\
									<span class="btn btn-danger confirm-dialog" data-callback="delete_credit_row" data-message="Вы действительно хотите удалить эту настройку?"><i class="ca-icon ca-icon_remove"></i></span>\
								</td>\
							</tr>';
		$('#credit_form-table tbody').append(row_template);
		return false;
	}

	var delete_credit_row = function(btn){
		var $this = $(btn);
		$this.closest('tr').remove();
	}

	var save_credit_form = function(btn){
		var $this = $(btn);
		var $form = $this.closest('form#credit_form');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/credit/ajax_operations/save_settings',
			data: $form.serialize(),
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	}
</script>
