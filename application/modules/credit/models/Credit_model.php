<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Credit_model extends CI_Model{
	var $credit_program = "settings_credit";
	function __construct(){
		parent::__construct();
	}
	
	function handler_update($setting_alias, $data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->where('setting_alias', $setting_alias);
		$this->db->update($this->credit_program, $data);
	}
	
	function handler_get($setting_alias){		
		$this->db->where('setting_alias', $setting_alias);
		return $this->db->get($this->credit_program)->row_array();
	}
}
?>
