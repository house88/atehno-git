<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/credit/';
		$this->active_menu = 'settings';
		$this->active_submenu = 'credit';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Credit_model", "credit");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		checkPermision('manage_credit', '/admin');

		$credit_program = $this->credit->handler_get('iute_credit');
		$this->data['credit_settings'] = json_decode($credit_program['setting_data'], true);

		$this->data['page_header'] = 'Кредитная программа';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'credit_scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add_option':
				checkPermisionAjax('manage_credit');

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit_option':
				checkPermisionAjax('manage_credit');

				$credit_program = $this->credit->handler_get('iute_credit');
				$credit_program_settings = json_decode($credit_program['setting_data'], true);

				$id_option = $this->uri->segment(5);
				if(empty($credit_program_settings[$id_option])){
					jsonResponse('Кредитная опция не существует.');
				}

				$this->data['credit_option'] = $credit_program_settings[$id_option];

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add_option':
				checkPermisionAjax('manage_credit');

				$this->form_validation->set_rules('months', 'Кол. месяцев', 'required|is_natural_no_zero');
				$this->form_validation->set_rules('pre_pay', 'Аванс', 'required|numeric|greater_than_equal_to[0]');
				$this->form_validation->set_rules('percents', '%', 'required|numeric|less_than_equal_to[100]');
				$this->form_validation->set_rules('price_from', 'Курс', 'required|numeric|greater_than_equal_to[0]');
				$this->form_validation->set_rules('price_to', 'Курс', 'required|numeric|greater_than_equal_to[0]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$credit_program = $this->credit->handler_get('iute_credit');
				$credit_program_options = json_decode($credit_program['setting_data'], true);
				$id_option = uniqid(rand(1000, 9999) . '_');

				$credit_program_options[$id_option] = array(
					'id_option' => $id_option,
					'months' => (int) $this->input->post('months'),
					'price_from' => $this->input->post('price_from'),
					'price_to' => $this->input->post('price_to'),
					'pre_pay' => $this->input->post('pre_pay'),
					'percents' => $this->input->post('percents')
				);

				$this->credit->handler_update('iute_credit', array(
					'setting_data' => json_encode($credit_program_options)
				));

				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit_option':
				checkPermisionAjax('manage_credit');

                $this->form_validation->set_rules('option', 'Опция', 'required|xss_clean');
				$this->form_validation->set_rules('months', 'Кол. месяцев', 'required|is_natural_no_zero');
				$this->form_validation->set_rules('pre_pay', 'Аванс', 'required|numeric|greater_than_equal_to[0]');
				$this->form_validation->set_rules('percents', '%', 'required|numeric|less_than_equal_to[100]');
				$this->form_validation->set_rules('price_from', 'Курс', 'required|numeric|greater_than_equal_to[0]');
				$this->form_validation->set_rules('price_to', 'Курс', 'required|numeric|greater_than_equal_to[0]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$credit_program = $this->credit->handler_get('iute_credit');
				$credit_program_options = json_decode($credit_program['setting_data'], true);

				$id_option = $this->input->post('option');
				if(empty($credit_program_options[$id_option])){
					jsonResponse('Кредитная опция не существует.');
				}

				$credit_program_options[$id_option] = array(
					'id_option' => $id_option,
					'months' => (int) $this->input->post('months'),
					'price_from' => $this->input->post('price_from'),
					'price_to' => $this->input->post('price_to'),
					'pre_pay' => $this->input->post('pre_pay'),
					'percents' => $this->input->post('percents')
				);

				$this->credit->handler_update('iute_credit', array(
					'setting_data' => json_encode($credit_program_options)
				));

				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete_option':
				checkPermisionAjax('manage_credit');

                $this->form_validation->set_rules('option', 'Опция', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$credit_program = $this->credit->handler_get('iute_credit');
				$credit_program_options = json_decode($credit_program['setting_data'], true);

				$id_option = $this->input->post('option');
				if(empty($credit_program_options[$id_option])){
					jsonResponse('Кредитная опция не существует.', 'info');
				}

				unset($credit_program_options[$id_option]);

				$this->credit->handler_update('iute_credit', array(
					'setting_data' => json_encode($credit_program_options)
				));
				
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_credit');

				$credit_program = $this->credit->handler_get('iute_credit');
				$records = json_decode($credit_program['setting_data'], true);
				$records_total = count($records);

				usort ( $records , function($record1, $record2){
					return (int) $record1['months'] >= (int) $record2['months'];
				});
				
				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){
						return array(
							'dt_months'		=>  $record['months'],
							'dt_price'		=>  "от <span class=\"badge badge-dark custom-font-size-16\">{$record['price_from']}</span> до <span class=\"badge badge-dark custom-font-size-16\">{$record['price_to']}</span>",
							'dt_prepayment'	=>  $record['pre_pay'],
							'dt_rate'		=>  $record['percents'],
							'dt_actions'	=> '<div class="dropdown">
													<a data-toggle="dropdown" href="#" aria-expanded="false">
														<i class="fas fa-ellipsis-h"></i>
													</a>
													<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/credit/popup/edit_option/' . $record['id_option']).'">
															<i class="fad fa-pencil"></i> Редактировать
														</a>
														<div class="dropdown-divider"></div>
														<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите удалить кредитную опцию?" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-callback="delete_action" data-option="'.$record['id_option'].'" data-type="bg-danger">
															<i class="fad fa-trash-alt"></i> Удалить
														</a>
													</div>
												</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
		}
	}
}
