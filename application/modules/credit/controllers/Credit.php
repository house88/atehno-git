<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Credit extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Credit_model", "credit");
        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
        redirect();

		$credit_program = $this->credit->handler_get('iute_credit');
		$this->data['credit_settings'] = json_decode($credit_program['setting_data'], true);
		$this->data['main_content'] = 'admin/credit_settings_view';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'save_settings':
				if(!$this->lauth->have_right('manage_credit')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$months = $this->input->post('months', true);
				$prices_from = $this->input->post('prices_from', true);
				$prices_to = $this->input->post('prices_to', true);
				$pre_pay = $this->input->post('pre_pay', true);
				$percents = $this->input->post('percents', true);

				$credit_settings = array();
				if(is_array($months) && is_array($prices_from) && is_array($prices_to) && is_array($pre_pay) && is_array($percents)){
					foreach ($months as $key => $month) {
						$credit_settings[] = array(
							'months' => $month,
							'price_from' => $prices_from[$key],
							'price_to' => $prices_to[$key],
							'pre_pay' => $pre_pay[$key],
							'percents' => $percents[$key]
						);
					}
				}
				$update = array(
					'setting_data' => json_encode($credit_settings)
				);

				$this->credit->handler_update('iute_credit', $update);
				jsonResponse('Сохранено.', 'success');
			break;
		}
	}

	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(3);
		switch ($action) {
			case 'credit_form':
				$credit_type = $this->input->get('type', true);
				switch ($credit_type) {
                    case 'product':
                        $this->load->model('items/Items_model', 'items');
                        $id_product = (int) $this->uri->segment(4);
                        $product = $this->items->handler_get($id_product);
                        if(empty($product)){
				            jsonResponse('Товар не найден.');
                        }

						$priceColumn = getPriceColumn();
						$price = getPriceObject($product[$priceColumn['db_price_column']], [
							'tempAmount' => $product['item_temp_price']
						]);
                        $credit = getCreditOptions($price['display_price'], $price['currency_symbol'], $product['item_zero_credit'], $product['item_zero_credit_months']);
                        if(!$credit){
                            jsonResponse('Покупка в кредит не доступна для этого товара.', 'warning');
						}
						
                        $this->data = array(
                            'price' => $price['display_price'],
                            'currency' => $price['currency_symbol'],
                            'credit' => $credit,
                            'credit_type' => 'product',
                            'credit_months' => implode(',', $credit['months_list']),
							'product' => $product
                        );
                    break;                   
                    default:
				        jsonResponse('Данные не верны.');
                    break;
                }

				$content = $this->load->view($this->theme->public_view('credit/popup_view'), $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function _get($key){
		$creditProgram = $this->credit->handler_get($key);
		$creditProgram['settings'] = json_decode($creditProgram['setting_data'], true);

		return $creditProgram;
	}
}
