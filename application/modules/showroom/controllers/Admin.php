<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}
		
		$this->view_module_path = 'modules/showroom/';
		$this->active_menu = 'orders';
		$this->active_submenu = 'showroom';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Showroom_model", "showroom");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		checkPermision('manage_showroom', '/admin');

		$this->data['page_header'] = 'Заказы на осмотр товаров';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'showroom_scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function edit(){
		if(!$this->lauth->have_right('manage_showroom')){
			redirect('/admin');
		}

		$id_showroom = (int)$this->uri->segment(4);
		$this->data['showroom'] = $this->showroom->handler_get($id_showroom);
		$this->data['main_content'] = 'admin/edit';
		$this->load->view('admin/page', $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'edit':
				checkPermisionAjax('manage_showroom');

				$id_showroom = (int) $this->uri->segment(5);
				$this->data['showroom'] = $this->showroom->handler_get($id_showroom);
				if(empty($this->data['showroom'])){
					jsonResponse('Данные не верны!');
				}

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'edit':
				checkPermisionAjax('manage_showroom');

				$id_showroom = (int) $this->input->post('showroom');
				$showroom_info = $this->showroom->handler_get($id_showroom);
				if(empty($showroom_info)){
					jsonResponse('Данные не верны!');
				}
				
				$this->showroom->handler_update($id_showroom, array(
					'showroom_comment_admin' => $this->input->post('admin_comment', true)
				));

				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				checkPermisionAjax('manage_showroom');

				$this->form_validation->set_rules('showroom', 'Заказ', 'required|xss_clean');
				$this->form_validation->set_rules('showroom_status', 'Статус', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_showroom = (int) $this->input->post('showroom');
				$status = (int) $this->input->post('showroom_status');
				$showroom = $this->showroom->handler_get($id_showroom);
				if(empty($showroom)){
					jsonResponse('Заказ не существует.');
				}

				if(empty(get_showroom_status($status))){
					jsonResponse('Статус не найден.');
				}
				
				$this->showroom->handler_update($id_showroom, array(
					'showroom_status' => $status
				));
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_showroom');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart'),
					'sort_by' => array(	'showroom_date-desc' )
				);

				$records = $this->showroom->handler_get_all($params);
				$records_total = $this->showroom->handler_get_count($params);
				
				$order_statuses = get_showroom_statuses();
				$record_statuses = '';
				foreach($order_statuses as $status_key => $order_status){
					$record_statuses .= '<a href="#" class="dropdown-item call-function" data-callback="change_status" data-status="'.$status_key.'" data-color="'.$order_status['color_class'].'" data-text="'.$order_status['title'].'">
											'.$order_status['title'].'
										</a>';
				}
				
				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record) use ($record_statuses, $order_statuses){
						$item_image_url = base_url(getImage('files/items/'.$record['item_photo']));
						
						return array(
							'dt_toggle'		=> '<a href="#" class="call-popup" data-popup="#general_popup_form" data-href="'.base_url('admin/showroom/popup/edit/' . $record['id_showroom']).'">
													<span class="fad fa-eye"></span>
												</a>',
							'dt_photo'		=>  '<a href="'.$item_image_url.'" data-fancybox="image_'.$record['item_prog_id'].'" data-caption="'.clean_output($record['item_title']).'">
													<img src="'.$item_image_url.'" class="img-thumbnail custom-width-50">
												</a>',
							'dt_item'		=>  "<div class=\"grid-text\">
													<div class=\"grid-text__item\">
														<strong>ИД:</strong> {$record['item_prog_id']}<br>
														<strong>Артикул:</strong> {$record['item_code']}<br>
														{$record['item_title']}
													</div>
												</div>",
							'dt_client'		=>  $record['showroom_username'].'<br/>'.$record['showroom_email'].'<br/>'.$record['showroom_phone'],
							'dt_date'		=>  getDateFormat($record['showroom_date']),
							'dt_status'		=>  '<div class="dropdown" data-showroom="'.$record['id_showroom'].'">
													<button type="button" class="btn btn-'.$order_statuses[$record['showroom_status']]['color_class'].' btn-flat btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														'.$order_statuses[$record['showroom_status']]['title'].'
													</button>
													<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
														'. $record_statuses .'
													</div>
												</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_list_dt(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		if(!$this->lauth->have_right('manage_showroom')){
			jsonDTResponse('Ошибка: Нет прав!');
		}


		$params = array(
            'limit' => intVal($_POST['iDisplayLength']),
            'start' => intVal($_POST['iDisplayStart'])
        );

        if ($_POST['iSortingCols'] > 0) {
            for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
                switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
                    case 'dt_date': $params['sort_by'][] = 'showroom_date-' . $_POST['sSortDir_' . $i];
                    break;
                }
            }
        }

        $records = $this->showroom->handler_get_all($params);
        $records_total = $this->showroom->handler_get_count($params);

        $output = array(
            "sEcho" => intval($_POST['sEcho']),
            "iTotalRecords" => $records_total,
            "iTotalDisplayRecords" => $records_total,
            "aaData" => array()
        );

		$order_statuses = get_showroom_statuses();
		$record_statuses = '';
		foreach($order_statuses as $status_key => $order_status){
			$record_statuses .= '<li><a href="#" class="call-function" data-toggle="dropdown" data-callback="change_status" data-status="'.$status_key.'" data-color="'.$order_status['color_class'].'" data-text="'.$order_status['title'].'"><strong class="text-'.$order_status['color_class'].'">'.$order_status['title'].'</strong></a></li>';
		}
        foreach ($records as $record) {
			$status = '<div class="btn-group dropdown" data-showroom="'.$record['id_showroom'].'">
							<button type="button" class="btn btn-'.$order_statuses[$record['showroom_status']]['color_class'].' btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								'.$order_statuses[$record['showroom_status']]['title'].' <span class="caret"></span>
							</button>
							<ul class="dropdown-menu dropdown-menu-right">
								'.$record_statuses.'
							</ul>
						</div>';
			$item_image_url = base_url(getImage('files/items/'.$record['item_photo']));
			$showroom_data = array(
				'showroom' => $record
			);
			$showroom_detail = $this->load->view('admin/order_view', $showroom_data, true);
            $output['aaData'][] = array(
                'dt_toggle'		=> '<span class="cur-pointer record_details record-'.$record['id_showroom'].'" href="#"><span class="ca-icon ca-icon_plus"></span></span>',
				'dt_id'			=>  $record['item_prog_id'],
				'dt_item'		=>  $record['item_title'],
				'dt_code'		=>  '<span class="custom-font-size-10">'.$record['item_code'].'</span>',
				'dt_photo'		=>  '<a href="'.$item_image_url.'" data-fancybox="image_'.$record['item_prog_id'].'" data-caption="'.clean_output($record['item_title']).'">
										<img src="'.$item_image_url.'" class="img-thumbnail w-30">
									</a>',
                'dt_client'		=>  $record['showroom_username'].'<br/>'.$record['showroom_email'].'<br/>'.$record['showroom_phone'],
                'dt_date'		=>  getDateFormat($record['showroom_date']),
                'dt_detail' 	=>  $showroom_detail,
				'dt_status'		=>  $status
            );
        }
        jsonResponse('', 'success', $output);
	}
}