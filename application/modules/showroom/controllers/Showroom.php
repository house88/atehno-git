<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Showroom extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Showroom_model", "showroom");
		$this->load->model('items/Items_Model', 'items');
        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
        return_404($this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(3);
		switch($option){
			case 'order_showroom':
				$this->form_validation->set_rules('item', 'Товар', 'required|xss_clean');
				$this->form_validation->set_rules('showroom_username', 'Имя', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('showroom_date', 'Коментарий', 'required|xss_clean');
				$this->form_validation->set_rules('showroom_user_email', 'Email', 'xss_clean|valid_email');
				$this->form_validation->set_rules('showroom_user_phone', 'Телефон', 'xss_clean|max_length[50]');
				
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$showroom_user_phone = null;
				if($this->input->post('showroom_user_phone')){
					$phoneFormatResult = formatPhoneNumber($this->input->post('showroom_user_phone'));
					if(true === $phoneFormatResult['is_valid']){
						$showroom_user_phone = $phoneFormatResult['formated'];
					}
				}

				if(!$this->input->post('showroom_user_email') && null === $showroom_user_phone){
					jsonResponse('Напишите email или номер телефона чтобы мы могли с Вами связаться.');
				}

				$showroom_date = getDateFormat($this->input->post('showroom_date', true), 'd.m.Y H:i', 'Y-m-d H:i:s');
				if($showroom_date == '&mdash;'){
					jsonResponse('Ошибка проверки даты.', 'error', array('date' => $showroom_date));
				}

				// GET PRODUCT
				$id_item = (int)$this->input->post('item');
				$product = $this->items->handler_get($id_item);
				if(empty($product)){
					jsonResponse('Товара не найден.');
				}

				$is_valid = $this->recaptcha->is_valid();
				if(!$is_valid['success']){
					jsonResponse('Ошибка проверки данных.', 'error', array('is_valid' => $is_valid));
				}

				// ORDER SHOWROOM
				$insert = array(
					'id_item' => $id_item,
					'showroom_date' => $showroom_date,
					'showroom_username' => $this->input->post('showroom_username', true),
					'showroom_email' => $this->input->post('showroom_user_email', true),
					'showroom_phone' => $showroom_user_phone ?? ''
				);
				$id_showroom = $this->showroom->handler_insert($insert);
				
				$email_data = array(
					'showroom' => $this->showroom->handler_get($id_showroom),
					'product' => $product,
					'title' => 'Оформлен заказ на осмотр товара',
					'email_content' => 'order_showroom_admin_detail_tpl'
				);

				Modules::run('email/send', array(
					'to' => $this->data['settings']['order_showroom_admin_email']['setting_value'],
					'subject' => 'Оформлен заказ на осмотр товара',
					'email_data' => $email_data
				));

				if($this->input->post('showroom_user_email')){
					$email_data['email_content'] = 'order_showroom_detail_tpl';
					$email_data['title'] = 'Вы оформили заказ на осмотр товара';

					Modules::run('email/send', array(
						'to' => $this->input->post('showroom_user_email', true),
						'subject' => 'Вы оформили заказ на осмотр товара',
						'email_data' => $email_data
					));
				}
				
				jsonResponse('Спасибо, скоро с вами свяжеться наш менеджер.', 'success');
			break;
		}
	}

	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(3);
		switch ($action) {
			case 'order_showroom':
				$this->data['id_item'] = (int)$this->uri->segment(4);
				$item = $this->items->handler_get($this->data['id_item']);
				if(empty($item)){
					jsonResponse('Данные не верны. Товар не найден.');
				}

				$this->data['recaptcha_widget'] = $this->recaptcha->create_box();
				$content = $this->load->view($this->theme->public_view('showroom/popup_request_view'), $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
}
