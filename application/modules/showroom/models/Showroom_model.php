<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Showroom_model extends CI_Model{
	var $showroom = "showroom";
	var $items = "items";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->showroom, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_showroom, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_showroom', $id_showroom);
		$this->db->update($this->showroom, $data);
	}

	function handler_delete($id_showroom){
		$this->db->where('id_showroom', $id_showroom);
		$this->db->delete($this->showroom);
	}

	function handler_get($id_showroom){
		$this->db->select("{$this->showroom}.*");
		$this->db->select("{$this->items}.item_title, {$this->items}.item_url, {$this->items}.item_photo, {$this->items}.item_prog_id, {$this->items}.item_code");
		$this->db->from($this->showroom);
		$this->db->join($this->items, " {$this->items}.id_item = {$this->showroom}.id_item ", 'left');

		$this->db->where("{$this->showroom}.id_showroom", $id_showroom);
		return $this->db->get()->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_showroom DESC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		$this->db->select("{$this->showroom}.*");
		$this->db->select("{$this->items}.item_title, {$this->items}.item_url, {$this->items}.item_photo, {$this->items}.item_prog_id, {$this->items}.item_code");
		$this->db->from($this->showroom);
		$this->db->join($this->items, " {$this->items}.id_item = {$this->showroom}.id_item ", 'left');

        if(isset($id_showroom)){
			$this->db->where_in('id_showroom', $id_showroom);
        }

        if(isset($active)){
			$this->db->where('showroom_active', $active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get()->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($id_showroom)){
			$this->db->where_in('id_showroom', $id_showroom);
        }

        if(isset($active)){
			$this->db->where('showroom_active', $active);
        }

		return $this->db->count_all_results($this->showroom);
	}
}
