<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Api extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		
        $this->data = [];
	}
	
	function index(){
		return_404($this->data);
	}

	function orderUpdate(){
		/**
		 * URL = https://atehno.md/api/orderUpdate?order=<ID>&status=<ID>&manager=<ID>token=<TOKEN>
		 * TOKEN = 38f97e1b6d00d6c5bbab5f6aa86d1506afd96bb8ba92574c5f3df27542d06fb1b5e2be3d28d533297ea86949025a7ba8f3f2077f4ae034a2283e481a1b0e5cad
		 * STATUSES:
		 * 	1 - Новый
		 * 	2 - В обработке
		 * 	3 - Ждет оплаты / оформление кредита
		 * 	4 - Ждет поставки товара
		 * 	5 - Готов, ждет прихода клиента
		 * 	6 - Выполнен
		 * 	7 - Аннулирован
		 */

		$params = $this->input->get();
		echo Modules::run('orders/_change_status', $params);
	}
}
