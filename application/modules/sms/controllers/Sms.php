<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use GuzzleHttp\Client;

class Sms extends MX_Controller{
	function __construct(){
		parent::__construct();
        
        /**
         * dlrmask
         * 1	Отчет об успешной доставке
         * 2	Отчет об неуспешной доставки
         * 4	Отчет о буферизации сообщения
         * 8	Отчет о принятии сообщения смс центром
         * 16	Отчет об отклоненных сообщениях
         */

		$this->apiConfig = [
            'url' => 'https://api.bulksms.md:4432/UnifunBulkSMSAPI.asmx/SendSMSNoneDigitsEncoded',
            'params' => [
                'username' => 'atehno',
                'password' => 'vhfb68W0',
                'from' => 'ATEHNO',
                'to' => null,
                'text' => null,
                'dlrmask' => 31,
                'dlrurl' => 'https://atehno.md/sms/delivery?state=%d&msgid=[MESSAGE_ID]',
                'charset' => 'UTF8',
                'coding' => '2'
            ]
        ];

        
        $this->load->model('Sms_model', 'sms');
        $this->config->load('sms', TRUE, TRUE);

        $this->smsConfigs = $this->config->item('sms');
	}

    function delivery(){
        $state = (int) $this->input->get('state');
        if(!in_array($state, [1, 2, 4, 8, 16])){
            header("Not Found", true, 404);
            return;
        }
        
        $messageKey = $this->input->get('msgid', true);
        $smsDetails = $this->sms->handlerGetLogByKey($messageKey);
        if(empty($smsDetails)){
            header("Not Found", true, 404);
            return;
        }

        $this->sms->handlerUpdateLogByKey($messageKey, [
            'state' => $state
        ]);

        header("Success", true, 200);
        return;
    }

    function _send($to, $params = []){
        if(1 !== (int) @$this->smsConfigs['sms']['is_active']){
            return [
                'info' => 'Отправка Смс отключена в настройках сайта.'
            ];
        }
        
        if(empty($params)){
            return [
                'error' => 'Отправка Смс невозможна.'
            ];
        }

        if(!isValidMobileNumber($to)){
            return [
                'error' => 'Смс не отправленно. Неверный номер.'
            ];
        }

        $idTemplate = NULL;
        if(isset($params['idTemplate'])){
            $idTemplate = (int) $params['idTemplate'];
            $smsTemplate = $this->sms->handlerGet($idTemplate);
            if(empty($smsTemplate)){
                return [
                    'error' => 'Шаблон сообшения для отправки не найденно.'
                ];
            }
    
            $smsText = $smsTemplate['text'];
        } else if(isset($params['message'])){
            $smsText = $params['message'];
        }

        if(empty($smsText)){
            return [
                'error' => 'Текст сообшения для отправки пустой. Отправка Смс невозможна.'
            ];
        }
        
        if(!empty($params['replace'])){
            $smsText = str_replace(array_keys($params['replace']), array_values($params['replace']), $smsText);
        }

        $to = $this->normalyzeNumber($to);
        $this->apiConfig['params']['to'] = $to;
        $this->apiConfig['params']['text'] = $smsText;

        $messageKey = uniqueId();
        $this->apiConfig['params']['dlrurl'] = str_replace('[MESSAGE_ID]', $messageKey, $this->apiConfig['params']['dlrurl']);

        $client = new Client();
        $response = $client->request('GET', $this->apiConfig['url'], [
            'query' => $this->apiConfig['params']
        ]);

        $this->sms->handlerInsertLog([
            'id_template' => $idTemplate,
            'id_sms' => $messageKey,
            'phone' => $to,
            'message' => $smsText,
            'context' => json_encode($this->apiConfig),
            'state' => 0
        ]);
        
        return $response;
    }

    private function normalyzeNumber(string $number){
        if ("0" === (string) substr($number, 0, 1)) {
            $number = (string) substr($number, 1);
        }

        return "373{$number}";
    }
	
	function _getOrderPaymentDeliveryTemplate(int $orderStatus, int $idRelation){
		return $this->sms->handlerGetOrderPaymentDeliveryTemplate($orderStatus, $idRelation);
	}
	
	function _getAll($params = []){
		return $this->sms->handlerGetAll((array) $params);
	}

    function _getConfigs(){
        return $this->smsConfigs['sms'];
    }
}