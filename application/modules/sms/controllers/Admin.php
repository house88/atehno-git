<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/sms/';
		$this->active_menu = 'settings';
		$this->active_submenu = 'sms';

        $this->data = array();
        
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

        $this->load->model('Sms_model', 'sms');

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
        checkPermision('manage_sms');

		$this->data['page_header'] = 'Шаблошы СМС';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->data['dt_filter'] = $this->theme->apanel_view($this->view_module_path . 'filter_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'setUpdate':
				checkPermisionAjax('manage_sms');

                $idTemplate = (int) $this->uri->segment(5);
                $this->data['smsTemplate'] = $this->sms->handlerGet($idTemplate);
                $this->data['smsTemplateConfigs'] = arrayByKey($this->sms->handlerGetOrderPaymentDeliveryTemplateSettings($idTemplate), 'order_status', true);

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
            case 'setUpdate':
                checkPermisionAjax('manage_sms');

                $this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('text', 'Текст', 'required|xss_clean|max_length[160]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }
                
                $setData = [
                    'title' => $this->input->post('title'),
                    'text' => $this->input->post('text'),
                    'is_active' => $this->input->post('is_active') ? 1 : 0
                ];
				
				$idTemplate = (int) $this->input->post('idTemplate');
				$smsTemplate = $this->sms->handlerGet($idTemplate);
                if(empty($smsTemplate)){
                    $idTemplate = $this->sms->handlerInsert($setData);                    
                } else{
                    $this->sms->handlerUpdate($idTemplate, $setData);
                }

				$this->sms->handlerDeleteOrderPaymentDeliveryTemplateSettings((int) $idTemplate);
				$paymentDeliveryRelations = array_column(Modules::run('payment/_get_delivery_relations'), 'id_relation');
				$sendSmsConfigs = $this->input->post('sms_config');
				if(is_array($sendSmsConfigs)){
					$insertConfigs = [];
					foreach ($sendSmsConfigs as $idOrderStatus => $paymentDeliveryRelation) {
						if(empty($paymentDeliveryRelation)){
							continue;
						}

						if(in_array('all', $paymentDeliveryRelation)){							
							$insertConfigs[] = [
								'id_template' => $idTemplate,
								'order_status' => (int) $idOrderStatus,
								'id_payment_delivery_relation' => NULL
							];
							
							continue;
						}

						foreach ($paymentDeliveryRelation as $idRelation) {
							$insertConfigs[] = [
								'id_template' => $idTemplate,
								'order_status' => (int) $idOrderStatus,
								'id_payment_delivery_relation' => (int) $idRelation
							];
						}
					}

					if(!empty($insertConfigs)){
						$this->sms->handlerInsertOrderPaymentDeliverySettings($insertConfigs);
					}
				}

				jsonResponse('Сохранено.', 'success');
            break;
            case 'smsSendBulk':
                checkPermisionAjax('manage_sms');

                $this->form_validation->set_rules('phone_numbers', 'Получатели', 'required|xss_clean');

				$idTemplate = (int) $this->input->post('id_template');
				if(0 === $idTemplate){
					$this->form_validation->set_rules('message', 'Текст сообшения', 'required|xss_clean|max_length[160]');
				}

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }
                
				if($idTemplate > 0){
					$smsTemplate = $this->sms->handlerGet($idTemplate);
					if(empty($smsTemplate)){
						jsonResponse('Ошибка: Шаблон не найден.');                  
					}

					$message = $smsTemplate['text'];
				} else{
					$message = $this->input->post('message');
				}

				$phoneNumbers = array_filter(explode(';', $this->input->post('phone_numbers')));
				$phoneNumbers = array_map('trim', $phoneNumbers);
				if(empty($phoneNumbers)){
					jsonResponse('Нет получателей.');
				}

				foreach ($phoneNumbers as $phoneNumber) {
					Modules::run('sms/_send', $phoneNumber, [
						'message' => $message
					]);
				}

				jsonResponse('Сохранено.', 'success');
            break;
			case 'delete':
                checkPermisionAjax('manage_sms');

                $this->form_validation->set_rules('id_sms', 'СМС шаблон', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$idTemplate = (int) $this->input->post('id_sms');
				$smsTemplate = $this->sms->handlerGet($idTemplate);
				if(empty($smsTemplate)){
                    jsonResponse('Операция прошла успешно.', 'success');
				}

				$this->sms->handlerDelete($idTemplate);
                
				jsonResponse('Операция прошла успешно.', 'success');
            break;
			case 'settings':
				// is_active
				// sms_groups_of_users
				checkPermisionAjax('manage_sms');

				$postGroups = $this->input->post('sms_groups_of_users');
				$postGroups = array_map('intval', $postGroups);
                
                $smsSettings = [
                    'is_active' => $this->input->post('is_active') ? 1 : 0,
                    'groups_of_users' => $postGroups
                ];

				$pathToFile = APPPATH . "config/sms.php";
				$file = fopen($pathToFile, "w");
				fwrite($file, '<?php defined(\'BASEPATH\') OR exit(\'No direct script access allowed\');'. PHP_EOL .'$config[\'sms\'] = ' . var_export($smsSettings, true) . ';');
				fclose($file);

				jsonResponse('Сохранено.', 'success');
			break;
            case 'list':
                checkPermisionAjaxDT('manage_sms');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength', true),
					'start' => (int) $this->input->post('iDisplayStart', true)
                );
        
                $records = $this->sms->handlerGetAll($params);
                $recordsCount = $this->sms->handlerCount($params);

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $recordsCount,
					"aaData" => array_map(function($record){
                        return array(
							'dt_id'			=>  $record['id'],
                            'dt_title'		=>  $record['title'],
                            'dt_text'		=>  $record['text'],
                            'dt_actions'	=> '<div class="dropdown">
                                                    <a data-toggle="dropdown" href="#" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
                                                        <a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/sms/popup/setUpdate/' . $record['id']).'">
                                                            <i class="fad fa-pencil"></i> Редактировать
                                                        </a>
                                                        <div class="dropdown-divider"></div>
                                                        <a href="#" class="dropdown-item call-function" data-callback="delete_action" data-sms="'.$record['id'].'">
                                                            <i class="fad fa-trash"></i> Удалить
                                                        </a>
                                                    </div>
                                                </div>'
						);
					}, $records)
				);

				jsonResponse('', 'success', $output);
            break;
		}
	}

	function _getSettings(){
		$this->config->load('sms', TRUE);

		return $this->config->item('sms');
	}
}
