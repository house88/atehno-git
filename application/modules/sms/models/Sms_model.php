<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sms_model extends CI_Model{
	var $smsTemplateTable = "sms_templates";
	var $smsSendOrderPaymentDeliveryTable = "sms_send_order_payment_delivery";
	var $smsLogsTable = "sms_logs";
	function __construct(){
		parent::__construct();
	}

	function handlerInsert($data = []){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->smsTemplateTable, $data);
		return $this->db->insert_id();
	}

	function handlerUpdate($idTemplate, $data = []){
		if(empty($data)){
			return;
		}

		$this->db->where('id', $idTemplate);
		$this->db->update($this->smsTemplateTable, $data);
	}

	function handlerDelete($idTemplate){
		$this->db->where('id', $idTemplate);
		$this->db->delete($this->smsTemplateTable);
	}

	function handlerGet($idTemplate){
		$this->db->where('id', $idTemplate);
		return $this->db->get($this->smsTemplateTable)->row_array();
	}

	function handlerGetAll($params = []){
        $order_by = " id ASC ";

        extract($params);

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->smsTemplateTable)->result_array();
	}

	function handlerCount($params = []){
        extract($params);

		return $this->db->count_all_results($this->smsTemplateTable);
	}

	//region SendOrderPaymentDelivery
	function handlerInsertOrderPaymentDeliverySettings(array $insertData){
		if(empty($insertData)){
			return false;
		}

		return $this->db->insert_batch($this->smsSendOrderPaymentDeliveryTable, $insertData);
	}

	function handlerGetOrderPaymentDeliveryTemplateSettings(int $idTemplate){
		$this->db->where('id_template', $idTemplate);

		return $this->db->get($this->smsSendOrderPaymentDeliveryTable)->result_array();
	}
	
	function handlerGetOrderPaymentDeliveryTemplate(int $orderStatus, int $idRelation){
		$this->db->where('order_status', $orderStatus);
		$this->db->group_start()
				 ->where('id_payment_delivery_relation', $idRelation)
				 ->or_where('id_payment_delivery_relation', NULL)
				 ->group_end();

		return $this->db->get($this->smsSendOrderPaymentDeliveryTable)->row_array();
	}

	function handlerDeleteOrderPaymentDeliveryTemplateSettings(int $idTemplate){
		$this->db->where('id_template', $idTemplate);

		return $this->db->delete($this->smsSendOrderPaymentDeliveryTable);
	}
	//endregion

	//region SEND LOGS
	function handlerInsertLog($data = []){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->smsLogsTable, $data);
		return $this->db->insert_id();
	}

	function handlerGetLogByKey($messageKey){
		$this->db->where('id_sms', $messageKey);
		return $this->db->get($this->smsLogsTable)->row_array();
	}

	function handlerUpdateLogByKey($messageKey, $data = []){
        if(empty($data)){
			return;
		}

		$this->db->where('id_sms', $messageKey);
		return $this->db->update($this->smsLogsTable, $data);
	}

	function handlerGetLogs($params = []){
        $order_by = " created_on ASC ";

        extract($params);

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->smsLogsTable)->result_array();
	}

	function handlerCountLogs($params = []){
        extract($params);

		return $this->db->count_all_results($this->smsLogsTable);
	}
	//endregion SEND LOGS
}
