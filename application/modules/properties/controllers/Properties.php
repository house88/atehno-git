<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Properties extends MX_Controller{
	
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("categories/Categories_model", "categories");
		$this->load->model("Properties_model", "properties");

		$this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}
	
	function index(){
		return_404($this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(3);
		switch($option){
			case 'get_filter':
				$idCategory = (int) $this->input->post('category');
				$idItems = $this->input->post('items');
				$idItems = explode(',', $idItems);
				$idItems = array_filter(array_map('intval', $idItems));
				jsonResponse('', 'success', [
					'filters' => $this->_get_category_properties(array('id_category' => $idCategory, 'items_list' => $idItems))
				]);
			break;
		}
	}

	function _get_category_properties($params = array()){
		extract($params);
		
		if(!isset($id_category)){
			return;
		}

		$this->db->cache_on();
		$category = Modules::run('categories/_get', (int) $id_category);
		if(empty($category)){
			return;
		}

		$categoryBread = json_decode("[{$category['category_breadcrumbs']}]", true);
		$idParentsCategories = array_column($categoryBread, 'category_id');

		$condition['id_categories'] = implode(',', $idParentsCategories);
		$condition['in_filter'] = 1;
		$condition['status_property'] = 1;
		$this->data['properties'] = $this->properties->handler_get_all($condition);
		if(empty($this->data['properties'])){
			return;
		}

		$idChildrenCategories = [];
		if(!empty($category['category_children'])){
			$idChildrenCategories = explode(',', $category['category_children']);
		}
		$idChildrenCategories[] = $id_category;		
		
		$condition['category_children'] = implode(',', $idChildrenCategories);

		$condition['id_properties'] = array_column($this->data['properties'], 'id_property');
		// $this->data['properties_values_counters'] = $this->properties->handler_get_items_properties_values_counters($condition);
		
		if(!empty($items_list)){
			$condition['items_list'] = $items_list;
		}
		
		$this->data['available_properties_values_counters'] = $this->properties->handler_get_items_properties_values_counters($condition);
		$this->db->cache_off();
		return $this->load->view($this->theme->public_view('shop/catalog/properties_view'), $this->data, true);
	}

    function _get_product_properties($params = array()){
        extract($params);

        if(empty($id_item)){
            return false;
        }
        
        $this->data['item_properties_values'] = $this->properties->handler_get_items_properties_values($id_item);		
        if(!empty($this->data['item_properties_values'])){
            $prop_list = array();
            foreach ($this->data['item_properties_values'] as $property) {
                $prop_list[$property['id_property']] = $property['id_property'];
            }

            if(!empty($prop_list)){
                $this->data['properties'] = arrayByKey($this->properties->handler_get_all(array('properties_list' => implode(',', $prop_list), 'on_item' => 1, 'status_property' => 1)), 'id_property');
            }

            
            foreach($this->data['item_properties_values'] as $item_property_value){
				if(array_key_exists($item_property_value['id_property'], $this->data['properties'])){
					if(in_array($this->data['properties'][$item_property_value['id_property']]['type_property'], array('select', 'multiselect'))){
						$available_values = json_decode($this->data['properties'][$item_property_value['id_property']]['property_values'], true);
						foreach ($available_values as $available_value) {
							if($available_value['id_value'] == $item_property_value['value']){
								$this->data['properties'][$item_property_value['id_property']]['item_values'][] = $available_value['value'];
							}
						}
					}
					
					if($this->data['properties'][$item_property_value['id_property']]['type_property'] == 'range'){
						$this->data['properties'][$item_property_value['id_property']]['item_values'][] = $item_property_value['value'];
					}
					
					if($this->data['properties'][$item_property_value['id_property']]['type_property'] == 'simple'){
						$this->data['properties'][$item_property_value['id_property']]['item_values'][] = $item_property_value['value'];
					}
				}
            }
        }

		return $this->load->view($this->theme->public_view('shop/product/properties_view'), $this->data, true);
    }
}
