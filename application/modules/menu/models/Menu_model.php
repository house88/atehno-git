<?php
/*
Author: Daniel Gutierrez
Date: 9/18/12
Version: 1.0
*/

class Menu_model extends CI_Model{
    var $menu = "menu";

    function __construct(){
        parent::__construct();
    }
	
	function menu_admin(){
		//Just a prototype
		$menu = array();
		$menu['orders'] = new stdClass();
		$menu['orders']->url = "";
		$menu['orders']->name = '<i class="ca-icon ca-icon_shopping-cart"></i> Заказы';
		$menu['orders']->rights = 'manage_orders,manage_showroom';
		$menu['orders']->submenu = array(
			array(
				'url' => 'orders',
				'name' => '<i class="ca-icon ca-icon_list"></i> Список заказов',
				'rights' => 'manage_orders'
			),
			array(
				'url' => 'showroom',
				'name' => '<i class="ca-icon ca-icon_eye"></i> Showroom',
				'rights' => 'manage_showroom'
			),
			array(
				'url' => 'tradein',
				'name' => '<i class="fad fa-sync-alt"></i> Trade-in',
				'rights' => 'manage_showroom'
			)
		);
		$menu['catalog'] = new stdClass();
		$menu['catalog']->url = "";
		$menu['catalog']->name = '<i class="ca-icon ca-icon_folder"></i> Каталог товаров';
		$menu['catalog']->rights = 'manage_categories,edit_items_full,moderate_items,manage_promo_code,view_moderators_reports,edit_items_simple,manage_reviews,manage_price_variants,manage_properties,manage_brands';
		$menu['catalog']->submenu = array(
			array(
				'url' => 'categories',
				'name' => '<i class="ca-icon ca-icon_tree-view"></i> Категории товаров',
				'rights' => 'manage_categories'
			),
			array(
				'url' => 'items',
				'name' => '<i class="ca-icon ca-icon_list"></i> Список товаров',
				'rights' => 'edit_items_full,edit_items_simple'
			),
			array(
				'url' => 'promo_code',
				'name' => '<i class="fa fa-tag"></i> Промо коды',
				'rights' => 'manage_promo_code'
			),
			array(
				'url' => 'stocks',
				'name' => '<i class="ca-icon ca-icon_list-small"></i> Склады',
				'rights' => 'manage_stocks'
			),
			array(
				'url' => 'suppliers',
				'name' => '<i class="ca-icon ca-icon_small-truck"></i> Поставщики',
				'rights' => 'manage_suppliers'
			),
			array(
				'url' => 'crons',
				'name' => '<i class="ca-icon ca-icon_gears"></i> Логи кронов',
				'rights' => 'manage_import'
			),
			array(
				'url' => 'moderator_report',
				'name' => '<i class="fa fa-users"></i> Отчет по модерации',
				'rights' => 'view_moderators_reports'
			),
			array(
				'url' => 'reviews',
				'name' => '<i class="fa fa-comments-o"></i> Отзывы о товарах',
				'rights' => 'manage_reviews'
			),
			array(
				'url' => 'price_variants',
				'name' => '<i class="ca-icon ca-icon_tree-view"></i> Ценовые категорий',
				'rights' => 'manage_price_variants'
			),
			array(
				'url' => 'dynamic_prices',
				'name' => '<i class="ca-icon ca-icon_tree-view"></i> Динамические цены',
				'rights' => 'manage_price_variants'
			),
			array(
				'url' => 'properties',
				'name' => '<i class="ca-icon ca-icon_filter"></i> Свойства товаров',
				'rights' => 'manage_properties'
			),
			array(
				'url' => 'brands',
				'name' => '<i class="ca-icon ca-icon_brands"></i> Бренды',
				'rights' => 'manage_brands'
			)
		);
		$menu['special_pages'] = new stdClass();
		$menu['special_pages']->url = "";
		$menu['special_pages']->name = '<i class="ca-icon ca-icon_papers"></i> Специальные страницы';
		$menu['special_pages']->rights = 'manage_news,manage_articles,manage_static_block,manage_banners,manage_site_menu';
		$menu['special_pages']->submenu = array(
			array(
				'url' => 'news',
				'name' => '<i class="ca-icon ca-icon_newspaper"></i> Новости',
				'rights' => 'manage_news'
			),
			array(
				'url' => 'articles',
				'name' => '<i class="ca-icon ca-icon_article"></i> Статьй',
				'rights' => 'manage_articles'
			),
			array(
				'url' => 'promo',
				'name' => '<i class="fa fa-gift"></i> Промо страницы',
				'rights' => 'manage_promo'
			),
			array(
				'url' => 'pages',
				'name' => '<i class="ca-icon ca-icon_article"></i> Страницы',
				'rights' => 'manage_pages'
			),
			array(
				'url' => 'static_block',
				'name' => '<i class="ca-icon ca-icon_article"></i> Статические блоки',
				'rights' => 'manage_static_block'
			),
			array(
				'url' => 'banners',
				'name' => '<i class="ca-icon ca-icon_gallery"></i> Баннеры',
				'rights' => 'manage_banners'
			),
			array(
				'url' => 'concurs',
				'name' => '<i class="ca-icon ca-icon_gift"></i> Конкурсы',
				'rights' => 'manage_concurs'
			),
			array(
				'url' => 'menu',
				'name' => '<i class="ca-icon ca-icon_list"></i> Меню сайта',
				'rights' => 'manage_site_menu'
			)
		);
		$menu['settings'] = new stdClass();
		$menu['settings']->url = "";
		$menu['settings']->name = '<i class="ca-icon ca-icon_gears"></i> Настройки';
		$menu['settings']->rights = 'manage_settings,manage_import,manage_currency,manage_delivery,manage_payment,manage_cities';
		$menu['settings']->submenu = array(
			array(
				'url' => 'settings',
				'name' => '<i class="ca-icon ca-icon_gears"></i> Настройки',
				'rights' => 'manage_settings'
			),
			array(
				'url' => 'import',
				'name' => '<i class="ca-icon ca-icon_coins"></i> Варианты импорта',
				'rights' => 'manage_import'
			),
			array(
				'url' => 'currency',
				'name' => '<i class="ca-icon ca-icon_coins"></i> Валюты сайта',
				'rights' => 'manage_currency'
			),
			array(
				'url' => 'delivery',
				'name' => '<i class="ca-icon ca-icon_truck"></i> Доставка',
				'rights' => 'manage_delivery'
			),
			array(
				'url' => 'payment',
				'name' => '<i class="ca-icon ca-icon_payments"></i> Оплата',
				'rights' => 'manage_payment'
			),
			array(
				'url' => 'credit',
				'name' => '<i class="ca-icon ca-icon_bank"></i> Кредитная программа',
				'rights' => 'manage_payment'
			),
			array(
				'url' => 'cities',
				'name' => '<i class="ca-icon ca-icon_marker"></i> Города',
				'rights' => 'manage_cities'
			)
		);
		$menu['users'] = new stdClass();
		$menu['users']->url = "";
		$menu['users']->name = '<i class="fa fa-users"></i> Пользователи';
		$menu['users']->rights = 'manage_users,manage_moderators,manage_groups,manage_rights,manage_group_rights';
		$menu['users']->submenu = array(
			array(
				'url' => 'users',
				'name' => '<i class="fa fa-users"></i> Клиенты',
				'rights' => 'manage_users'
			),
			array(
				'url' => 'moderators',
				'name' => '<i class="fa fa-users"></i> Модераторы',
				'rights' => 'manage_moderators'
			),
			array(
				'url' => 'groups',
				'name' => '<i class="ca-icon ca-icon_gears"></i> Группы пользователей',
				'rights' => 'manage_groups'
			),
			array(
				'url' => 'rights',
				'name' => '<i class="ca-icon ca-icon_gears"></i> Права пользователей',
				'rights' => 'manage_rights'
			),
			array(
				'url' => 'group_right',
				'name' => '<i class="ca-icon ca-icon_gears"></i> Права по группам',
				'rights' => 'manage_group_rights'
			)
		);

		return $menu;
	}
	
	function handler_apanel_menu(){
		return (object) array(
			'orders' => (object) array(
				'url' => '',
				'icon' => 'fad fa-shopping-cart',
				'name' => 'Заказы',
				'rights' => 'manage_orders,manage_showroom',
				'submenu' => (object) array(
					'orders' => (object) array(
						'url' => 'orders',
						'icon' => 'fad fa-clipboard-list',
						'name' => 'Список заказов',
						'rights' => 'manage_orders'
					),
					'showroom' => (object) array(
						'url' => 'showroom',
						'icon' => 'fad fa-box-full',
						'name' => 'Showroom',
						'rights' => 'manage_showroom'
					)
				)
			),
			'catalog' => (object) array(
				'url' => '',
				'icon' => 'fad fa-store',
				'name' => 'Магазин',
				'rights' => 'manage_categories,edit_items_full,moderate_items,manage_promo_code,view_moderators_reports,edit_items_simple,manage_reviews,manage_price_variants,manage_properties,manage_brands',
				'submenu' => (object) array(
					'categories' => (object) array(
						'url' => 'categories',
						'icon' => 'fad fa-folder-tree',
						'name' => 'Категории товаров',
						'rights' => 'manage_categories'
					),
					'items' => (object) array(
						'url' => 'items',
						'icon' => 'fad fa-th-list',
						'name' => 'Список товаров',
						'rights' => 'edit_items_full,edit_items_simple'
					),
					'promo_code' => (object) array(
						'url' => 'promo_code',
						'icon' => 'fad fa-tags',
						'name' => 'Промо коды',
						'rights' => 'manage_promo_code'
					),
					'stocks' => (object) array(
						'url' => 'stocks',
						'icon' => 'fad fa-warehouse-alt',
						'name' => 'Склады',
						'rights' => 'manage_stocks'
					),
					'suppliers' => (object) array(
						'url' => 'suppliers',
						'icon' => 'fad fa-dolly-flatbed-alt',
						'name' => 'Поставщики',
						'rights' => 'manage_suppliers'
					),
					'crons' => (object) array(
						'url' => 'crons',
						'icon' => 'fad fa-file-alt',
						'name' => 'Логи кронов',
						'rights' => 'manage_import'
					),
					'moderation_reports' => (object) array(
						'url' => 'moderation_reports',
						'icon' => 'fad fa-user-chart',
						'name' => 'Отчет по модерации',
						'rights' => 'view_moderators_reports'
					),
					'comments' => (object) array(
						'url' => 'comments',
						'icon' => 'fad fa-comments-alt',
						'name' => 'Отзывы о товарах',
						'rights' => 'manage_reviews'
					),
					'prices' => (object) array(
						'url' => 'prices',
						'icon' => 'fad fa-badge-dollar',
						'name' => 'Ценовые категорий',
						'rights' => 'manage_price_variants'
					),
					'dynamic_prices' => (object) array(
						'url' => 'dynamic_prices',
						'icon' => 'fad fa-badge-percent',
						'name' => 'Динамические цены',
						'rights' => 'manage_price_variants'
					),
					'properties' => (object) array(
						'url' => 'properties',
						'icon' => 'fad fa-filter',
						'name' => 'Свойства товаров',
						'rights' => 'manage_properties'
					),
					'brands' => (object) array(
						'url' => 'brands',
						'icon' => 'fad fa-copyright',
						'name' => 'Бренды',
						'rights' => 'manage_brands'
					)
				)
			),
			'special_pages' => (object) array(
				'url' => '',
				'icon' => 'fad fa-browser',
				'name' => 'Специальные страницы',
				'rights' => 'manage_news,manage_articles,manage_static_block,manage_banners,manage_site_menu',
				'submenu' => (object) array(
					// 'news' => (object) array(
					// 	'url' => 'news',
					// 	'icon' => 'fad fa-newspaper',
					// 	'name' => 'Новости',
					// 	'rights' => 'manage_news'
					// ),
					// 'articles' => (object) array(
					// 	'url' => 'articles',
					// 	'icon' => 'fad fa-typewriter',
					// 	'name' => 'Статьй',
					// 	'rights' => 'manage_articles'
					// ),
					'promo' => (object) array(
						'url' => 'promo',
						'icon' => 'fad fa-ad',
						'name' => 'Промо страницы',
						'rights' => 'manage_promo'
					),
					'pages' => (object) array(
						'url' => 'pages',
						'icon' => 'fad fa-file-alt',
						'name' => 'Страницы',
						'rights' => 'manage_pages'
					),
					'static_block' => (object) array(
						'url' => 'static_block',
						'icon' => 'fad fa-th',
						'name' => 'Статические блоки',
						'rights' => 'manage_static_block'
					),
					'banners' => (object) array(
						'url' => 'banners',
						'icon' => 'fad fa-images',
						'name' => 'Баннеры',
						'rights' => 'manage_banners'
					),
					'concurs' => (object) array(
						'url' => 'concurs',
						'icon' => 'fad fa-gifts',
						'name' => 'Конкурсы',
						'rights' => 'manage_concurs'
					),
					'menu' => (object) array(
						'url' => 'menu',
						'icon' => 'fad fa-layer-group',
						'name' => 'Меню сайта',
						'rights' => 'manage_site_menu'
					)
				)
			),
			'settings' => (object) array(
				'url' => '',
				'icon' => 'fad fa-cogs',
				'name' => 'Настройки',
				'rights' => 'manage_settings,manage_import,manage_currency,manage_delivery,manage_payment,manage_sms,manage_cities',
				'submenu' => (object) array(
					'settings' => (object) array(
						'url' => 'settings',
						'icon' => 'fad fa-sliders-v',
						'name' => 'Настройки',
						'rights' => 'manage_settings'
					),
					'import' => (object) array(
						'url' => 'import',
						'icon' => 'fad fa-file-import',
						'name' => 'Варианты импорта',
						'rights' => 'manage_import'
					),
					'currency' => (object) array(
						'url' => 'currency',
						'icon' => 'fad fa-usd-square',
						'name' => 'Валюты сайта',
						'rights' => 'manage_currency'
					),
					'delivery' => (object) array(
						'url' => 'delivery',
						'icon' => 'fad fa-truck',
						'name' => 'Доставка',
						'rights' => 'manage_delivery'
					),
					'payment' => (object) array(
						'url' => 'payment',
						'icon' => 'fad fa-credit-card-front',
						'name' => 'Оплата',
						'rights' => 'manage_payment'
					),
					'sms' => (object) array(
						'url' => 'sms',
						'icon' => 'fad fa-sms',
						'name' => 'СМС',
						'rights' => 'manage_sms'
					),
					'credit' => (object) array(
						'url' => 'credit',
						'icon' => 'fad fa-university',
						'name' => 'Кредитная программа',
						'rights' => 'manage_payment'
					),
					'cities' => (object) array(
						'url' => 'cities',
						'icon' => 'fad fa-map-marked-alt',
						'name' => 'Города',
						'rights' => 'manage_cities'
					)
				)
			),
			'users' => (object) array(
				'url' => '',
				'icon' => 'fad fa-users-cog',
				'name' => 'Пользователи',
				'rights' => 'manage_users,manage_moderators,manage_groups,manage_rights,manage_group_rights',
				'submenu' => (object) array(
					'users' => (object) array(
						'url' => 'users',
						'icon' => 'fad fa-users',
						'name' => 'Клиенты',
						'rights' => 'manage_users'
					),
					'moderators' => (object) array(
						'url' => 'moderators',
						'icon' => 'fad fa-user-shield',
						'name' => 'Модераторы',
						'rights' => 'manage_moderators'
					),
					'groups' => (object) array(
						'url' => 'groups',
						'icon' => 'fad fa-users-class',
						'name' => 'Группы пользователей',
						'rights' => 'manage_groups'
					),
					'rights' => (object) array(
						'url' => 'rights',
						'icon' => 'fad fa-user-crown',
						'name' => 'Права пользователей',
						'rights' => 'manage_rights'
					),
					'group_rights' => (object) array(
						'url' => 'group_rights',
						'icon' => 'fad fa-users-crown',
						'name' => 'Права по группам',
						'rights' => 'manage_group_rights'
					)
				)
			)
		);
	}
	
	function handler_insert($data = array()){
        if(empty($data)){
            return;
        }

        $this->db->insert($this->menu, $data);
        return $this->db->insert_id();
	}

	function handler_update($id_menu, $data = array()){
        if(empty($data)){
            return;
        }

        $this->db->where('id_menu', $id_menu);
        $this->db->update($this->menu, $data);
	}
	
	function handler_delete($id_menu){
        $this->db->where('id_menu', $id_menu);
        $this->db->delete($this->menu);
	}

	function handler_get($id_menu){
        $this->db->where('id_menu', $id_menu);
        return $this->db->get($this->menu)->row_array();
	}	

	function handler_get_by_title($menu_title){
        $this->db->where('menu_title', $menu_title);
        return $this->db->get($this->menu)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_menu ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($active)){
			$this->db->where('menu_visible', $active);
        }

		$this->db->order_by($order_by);
		return $this->db->get($this->menu)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($active)){
			$this->db->where('menu_visible', $active);
        }

		return $this->db->count_all_results($this->menu);
	}
}
