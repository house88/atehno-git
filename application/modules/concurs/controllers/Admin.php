<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->data = array();

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Concurs_model", "concurs");
        $this->load->model("menu/Menu_model", "menu");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		if(!$this->lauth->have_right('manage_concurs')){
			redirect('/admin');
		}

		$this->data['main_content'] = 'admin/list';
		$this->load->view('admin/page', $this->data);
	}

	function add(){
		if(!$this->lauth->have_right('manage_concurs')){
			redirect('/admin');
		}

		$this->data['main_content'] = 'admin/add';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		if(!$this->lauth->have_right('manage_concurs')){
			redirect('/admin');
		}

		$id_concurs = (int)$this->uri->segment(4);
		$this->data['concurs'] = $this->concurs->handler_get($id_concurs);
		if(empty($this->data['concurs'])){
			$this->session->set_flashdata('flash_message', '<li class="message-error zoomIn">Конкурс не найден. <i class="ca-icon ca-icon_remove"></i></li>');
			redirect('admin/concurs');
		}
		
		$this->data['main_content'] = 'admin/edit';
		$this->load->view('admin/page', $this->data);
	}

	function users(){
		if(!$this->lauth->have_right('manage_concurs')){
			redirect('/admin');
		}

		$id_concurs = (int)$this->uri->segment(4);
		$this->data['concurs'] = $this->concurs->handler_get($id_concurs);
		if(empty($this->data['concurs'])){
			$this->session->set_flashdata('flash_message', '<li class="message-error zoomIn">Конкурс не найден. <i class="ca-icon ca-icon_remove"></i></li>');
			redirect('admin/concurs');
		}

		$this->data['main_content'] = 'admin/users_list';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				if(!$this->lauth->have_right('manage_concurs')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$this->form_validation->set_rules('must_loggedin', 'Тип участников', 'required|xss_clean|in_list[no,site,social]');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('date_from', 'Дата с', 'required');
				$this->form_validation->set_rules('date_to', 'Дата по', 'required');
				$this->form_validation->set_rules('description', 'Текст', 'required');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'concurs_title' => $this->input->post('title'),
					'concurs_start' => getDateFormat($this->input->post('date_from'), 'd.m.Y', 'Y-m-d H:i:s'),
					'concurs_end' => getDateFormat($this->input->post('date_to'), 'd.m.Y', 'Y-m-d H:i:s'),
					'concurs_text' => $this->input->post('description'),
					'concurs_must_loggedin' => $this->input->post('must_loggedin', true)
				);

				if($this->input->post('active')){
					$insert['concurs_active'] = 1;
				}

				$id_concurs = $this->concurs->handler_insert($insert);

				$config = array(
					'table' => 'concurs',
					'id' => 'id_concurs',
					'field' => 'concurs_url',
					'title' => 'concurs_title',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$concurs_url = $this->slug->create_slug($insert['category_title'].'-'.$id_concurs);
				$this->concurs->handler_update($id_concurs, array('concurs_url' => $concurs_url));
				
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				if(!$this->lauth->have_right('manage_concurs')){
					jsonResponse('Ошибка: Нет прав!');
				}

                $this->form_validation->set_rules('concurs', 'Конкурс', 'required|xss_clean');
				$this->form_validation->set_rules('must_loggedin', 'Тип участников', 'required|xss_clean|in_list[no,site,social]');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('date_from', 'Дата с', 'required');
				$this->form_validation->set_rules('date_to', 'Дата по', 'required');
				$this->form_validation->set_rules('description', 'Текст', 'required');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_concurs = (int)$this->input->post('concurs');

				$config = array(
					'table' => 'concurs',
					'id' => 'id_concurs',
					'field' => 'concurs_url',
					'title' => 'concurs_title',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$concurs_url = $this->slug->create_slug($this->input->post('title').'-'.$id_concurs);

				$update = array(
					'concurs_title' => $this->input->post('title'),
					'concurs_url' => $concurs_url,
					'concurs_start' => getDateFormat($this->input->post('date_from'), 'd.m.Y', 'Y-m-d H:i:s'),
					'concurs_end' => getDateFormat($this->input->post('date_to'), 'd.m.Y', 'Y-m-d H:i:s'),
					'concurs_text' => $this->input->post('description'),
					'concurs_active' => 0,
					'concurs_must_loggedin' => $this->input->post('must_loggedin', true)
				);

				if($this->input->post('active')){
					$update['concurs_active'] = 1;
				}

				$this->concurs->handler_update($id_concurs, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				if(!$this->lauth->have_right('manage_concurs')){
					jsonResponse('Ошибка: Нет прав!');
				}

                $this->form_validation->set_rules('concurs', 'Конкурс', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_concurs = (int)$this->input->post('concurs');
				$concurs = $this->concurs->handler_get($id_concurs);
				if(empty($concurs)){
					jsonResponse('Конкурс не существует.');
				}

				$this->concurs->handler_delete($id_concurs);
				remove_dir('files/concurs/'.$id_concurs);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				if(!$this->lauth->have_right('manage_concurs')){
					jsonResponse('Ошибка: Нет прав!');
				}

                $this->form_validation->set_rules('concurs', 'Конкурс', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_concurs = (int)$this->input->post('concurs');
				$concurs = $this->concurs->handler_get($id_concurs);
				if(empty($concurs)){
					jsonResponse('Конкурс не существует.');
				}

				if($concurs['concurs_active'] == 1){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->concurs->handler_update($id_concurs, array('concurs_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list_dt':
				if(!$this->lauth->have_right('manage_concurs')){
					jsonDTResponse('Ошибка: Нет прав!');
				}
		
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);
		
				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_id': $params['sort_by'][] = 'id_concurs-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_name': $params['sort_by'][] = 'concurs_title-' . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}
		
				$records = $this->concurs->handler_get_all($params);
				$records_total = $this->concurs->handler_get_count($params);
		
				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);
		
				foreach ($records as $record) {
					$actions = '<a href="'.base_url('admin/concurs/users/'.$record['id_concurs']).'" class="btn btn-default btn-xs" title="Участники"><i class="ca-icon ca-icon_users"></i></a>';
					$actions .= ' <a href="'.base_url('admin/concurs/edit/'.$record['id_concurs']).'" class="btn btn-primary btn-xs"><i class="ca-icon ca-icon_pencil"></i></a>';
					$actions .= ' <a href="#" class="btn btn-danger btn-xs confirm-dialog" data-message="Вы уверены что хотите удалить конкурс?" data-callback="delete_action" data-concurs="'.$record['id_concurs'].'"><i class="ca-icon ca-icon_remove"></i></a>';
					$actions .= ' <a href="'.base_url('concurs/'.$record['concurs_url']).'" class="btn btn-primary btn-xs"><i class="ca-icon ca-icon_link"></i></a>';
		
					$output['aaData'][] = array(
						'dt_id'			=>  $record['id_concurs'],
						'dt_name'		=>  $record['concurs_title'],
						'dt_sdate'		=>  getDateFormat($record['concurs_start'], 'Y-m-d H:i:s', 'j M, Y'),
						'dt_edate'		=>  getDateFormat($record['concurs_end'], 'Y-m-d H:i:s', 'j M, Y'),
						'dt_active'		=>  '<input type="checkbox" name="active" '.set_checkbox("active", $record['concurs_active'], $record['concurs_active'] == 1).' class="switch-checkbox" data-concurs="'.$record['id_concurs'].'" data-action="active" data-size="mini" data-on-color="success"/>',
						'dt_actions'	=>  $actions,
					);
				}
				jsonResponse('', 'success', $output);
			break;
			case 'delete_user':
				if(!$this->lauth->have_right('manage_concurs')){
					jsonResponse('Ошибка: Нет прав!');
				}

                $this->form_validation->set_rules('relation', 'Участник', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_relation = (int)$this->input->post('relation');
				$concurs_user = $this->concurs->handler_get_user(array('id_relation' => $id_relation));
				if(empty($concurs_user)){
					jsonResponse('Участник не найден.');
				}

				$this->concurs->handler_delete_user(array('id_relation' => $id_relation));
				@unlink('files/concurs/'.$concurs_user['id_concurs'].'/'.$concurs_user['concurs_photo']);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_user_status':
				if(!$this->lauth->have_right('manage_concurs')){
					jsonResponse('Ошибка: Нет прав!');
				}

                $this->form_validation->set_rules('relation', 'Участник', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_relation = (int)$this->input->post('relation');
				$concurs_user = $this->concurs->handler_get_user(array('id_relation' => $id_relation));
				if(empty($concurs_user)){
					jsonResponse('Участник не найден.');
				}

				if($concurs_user['concurs_photo_moderated'] == 1){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->concurs->handler_update_user($id_relation, array('concurs_photo_moderated' => $status));
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'users_dt':
				if(!$this->lauth->have_right('manage_concurs')){
					jsonDTResponse('Ошибка: Нет прав!');
				}

				$id_concurs = (int)$this->uri->segment(5);
		
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart']),
					'id_concurs' => $id_concurs
				);
		
				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_id': $params['sort_by'][] = "{$this->concurs->concurs_users}.id_relation-" . $_POST['sSortDir_' . $i];
							break;
							case 'dt_votes': $params['sort_by'][] = "{$this->concurs->concurs_users}.votes-" . $_POST['sSortDir_' . $i];
							break;
							case 'dt_date': $params['sort_by'][] = "{$this->concurs->concurs_users}.create_date-" . $_POST['sSortDir_' . $i];
							break;
							case 'dt_moderated': $params['sort_by'][] = "{$this->concurs->concurs_users}.concurs_photo_moderated-" . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}
		
				$records = $this->concurs->handler_get_users_all($params);
				$records_total = $this->concurs->handler_get_users_count($params);
		
				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);
		
				foreach ($records as $record) {
					$actions = '<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-message="Вы уверены что хотите удалить участника?" data-callback="delete_action" data-relation="'.$record['id_relation'].'"><i class="ca-icon ca-icon_remove"></i></a>';
					
					$image_url = base_url(getImage('files/concurs/'.$id_concurs.'/'.$record['concurs_photo']));
					$output['aaData'][] = array(
						'dt_id'			=>  $record['id_relation'],
						'dt_photo'		=>  '<a href="'.$image_url.'" data-fancybox="image_'.$record['id_relation'].'">
												<img src="'.$image_url.'" class="img-thumbnail w-30">
											</a>',
						'dt_user'		=>  $record['user_nicename'],
						'dt_email'		=>  $record['user_email'],
						'dt_votes'		=>  $record['votes'],
						'dt_date'		=>  getDateFormat($record['create_date'], 'Y-m-d H:i:s', 'j M, Y'),
						'dt_moderated'	=>  '<input type="checkbox" name="moderated" '.set_checkbox("moderated", $record['concurs_photo_moderated'], $record['concurs_photo_moderated'] == 1).' class="switch-checkbox" data-relation="'.$record['id_relation'].'" data-action="moderated" data-size="mini" data-on-color="success"/>',
						'dt_actions'	=>  $actions,
					);
				}
				jsonResponse('', 'success', $output);
			break;
		}
	}
}
?>
