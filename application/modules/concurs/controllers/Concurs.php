<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Concurs extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value'];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value'];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value'];

		$this->load->model("Concurs_model", "concurs");
        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		$id_concurs = id_from_link($this->uri->segment(2));
		if(!$id_concurs){
			return_404($this->data);
		}

		$this->data['concurs'] = $this->concurs->handler_get($id_concurs);
		
		if(empty($this->data['concurs'])){
			return_404($this->data);
		}
		
		if($this->data['concurs']['concurs_active'] == 0){
			return_404($this->data);
		}
		
		// if(!in_array($this->data['concurs']['concurs_must_loggedin'], array('no', $this->lauth->signin_type()))){
		// 	return_404($this->data);
		// }
		
		$this->breadcrumbs[] = array(
			'title' => 'Конкурс: ' . $this->data['concurs']['concurs_title'],
			'link' => base_url('concurs/'.$this->data['concurs']['concurs_url'])
		);

		$params = array(
			'id_concurs' => $id_concurs,
			'concurs_photo_moderated' => 1
		);
		$this->data['concurs_users'] = $this->concurs->handler_get_users_all($params);

		if($this->lauth->logged_in()){
			$this->data['user_vote'] = $this->concurs->handler_get_user_vote(array('id_concurs' => $id_concurs, 'id_user' => $this->lauth->id_user()));
			$this->data['i_uploaded_photo'] = $this->concurs->handler_exist_user($id_concurs, $this->lauth->id_user());
		}


		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'concurs/index_view';
		$this->load->view($this->theme->public_view('shop_view'), $this->data);
	}

	function ajax_operations(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(3);
		switch ($action) {
			case 'remove_file':
				if(!$this->lauth->logged_in()){
					jsonResponse('Вы не авторизированы.', 'info');
				}
				
				if(!$this->input->post('temp_file')){
					jsonResponse('', 'success');
				}

				$config = array(
					array(
						'field' => 'temp_file',
						'label' => 'Файл',
						'rules' => 'trim|required|xss_clean',
					),
					array(
						'field' => 'concurs',
						'label' => 'Конкурс',
						'rules' => 'trim|required|xss_clean',
					)
				);
				$this->form_validation->set_rules($config);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_concurs = (int)$this->input->post('concurs');
				$concurs = $this->concurs->handler_get($id_concurs);
				if(empty($concurs)){
					jsonResponse('Данные не верны.');
				}
				
				if($concurs['concurs_active'] == 0){
					jsonResponse('Данные не верны.');
				}
				
				@unlink('files/temp/concurs/'.$id_concurs.'/'.$this->input->post('temp_file'));
				jsonResponse('', 'success');
			break;
			case 'save_file':
				if(!$this->lauth->logged_in()){
					jsonResponse('Вы не авторизированы.', 'info');
				}
				
				if(!$this->input->post('temp_file')){
					jsonResponse('', 'success');
				}
				
				$config = array(
					array(
						'field' => 'temp_file',
						'label' => 'Файл',
						'rules' => 'trim|required|xss_clean',
					),
					array(
						'field' => 'concurs',
						'label' => 'Конкурс',
						'rules' => 'trim|required|xss_clean',
					)
				);
				$this->form_validation->set_rules($config);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_concurs = (int)$this->input->post('concurs');
				$concurs = $this->concurs->handler_get($id_concurs);
				if(empty($concurs)){
					jsonResponse('Данные не верны.');
				}
				
				if($concurs['concurs_active'] == 0){
					jsonResponse('Данные не верны.');
				}

				if($this->concurs->handler_exist_user($id_concurs, $this->lauth->id_user())){
					jsonResponse('Вы уже загружали фото для этого конкурса.', 'info');
				}

				if($concurs['concurs_must_loggedin'] != 'no' && $concurs['concurs_must_loggedin'] != $this->lauth->signin_type()){
					jsonResponse('Для участия в конкурсе вы должны войти в личный кабинет'.(($concurs['concurs_must_loggedin'] == 'social')?' используя социальную сеть':'').'.');
				}

				$temp_file = 'files/temp/concurs/'.$id_concurs.'/' . $this->input->post('temp_file', true);
				
				$path = 'files/concurs/'.$id_concurs.'/';
				create_dir($path);
				
				$new_file = uniqid(rand()).$this->input->post('temp_file', true);
				
				if(!@copy($temp_file, $path.$new_file)){
					jsonResponse('Не могу загрузить фото, попробуйте еще раз.');
				}

				$config = array(
					'source_image'      => $path.$new_file,
					'create_thumb'      => true,
					'thumb_marker'      => 'thumb_500x500_',
					'new_image'         => FCPATH . $path,
					'maintain_ratio'    => true,
					'width'             => 500,
					'height'            => 500
				);
		
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$insert = array(
					'id_concurs' => $id_concurs,
					'id_user' => $this->lauth->id_user(),
					'concurs_photo' => $new_file
				);
				$this->concurs->handler_insert_user($insert);
				
				jsonResponse('Спасибо, ваша фотография отправлена на модерацью. После модерация она появиться на этой странице.', 'success', $insert);
			break;
			case 'concurs_vote':
				if(!$this->lauth->logged_in()){
					jsonResponse('Вы не авторизированы.', 'info');
				}
				
				$config = array(
					array(
						'field' => 'vote',
						'label' => 'Конкурс',
						'rules' => 'trim|required|xss_clean',
					)
				);
				$this->form_validation->set_rules($config);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_relation = (int)$this->input->post('vote');
				$concurs_user = $this->concurs->handler_get_user(array('id_relation' => $id_relation, 'concurs_photo_moderated' => 1));
				if(empty($concurs_user)){
					jsonResponse('Данные не верны.');
				}

				$concurs = $this->concurs->handler_get($concurs_user['id_concurs']);
				if(empty($concurs)){
					jsonResponse('Данные не верны.');
				}

				if($concurs['concurs_active'] == 0){
					jsonResponse('Данные не верны.');
				}

				$user_vote = $this->concurs->handler_get_user_vote(array('id_concurs' => $concurs_user['id_concurs'], 'id_user' => $this->lauth->id_user()));
				if(!empty($user_vote)){
					$this->concurs->handler_delete_user_vote(array('id_concurs' => $concurs_user['id_concurs'], 'id_user' => $this->lauth->id_user()));
					$concurs_user_old = $this->concurs->handler_get_user(array('id_relation' => $user_vote['id_relation']));
					if(!empty($concurs_user_old)){
						$this->concurs->handler_update_user($user_vote['id_relation'], array('votes' => abs($concurs_user_old['votes'] - 1)));
					}
				}

				$insert = array(
					'id_relation' => $id_relation,
					'id_user' => $this->lauth->id_user(),
					'id_concurs' => $concurs_user['id_concurs']
				);
				$this->concurs->handler_insert_user_vote($insert);
				$this->concurs->handler_update_user($concurs_user['id_relation'], array('votes' => abs($concurs_user['votes'] + 1)));
				
				jsonResponse('Спасибо за ваш голос.', 'success', $insert);
			break;
			case 'upload_file':
				$config = array(
					array(
						'field' => 'concurs',
						'label' => 'Конкурс',
						'rules' => 'trim|required|xss_clean',
					)
				);
				$this->form_validation->set_rules($config);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_concurs = (int)$this->input->post('concurs');
				$concurs = $this->concurs->handler_get($id_concurs);
				if(empty($concurs)){
					jsonResponse('Данные не верны.');
				}
				
				if($concurs['concurs_active'] == 0){
					jsonResponse('Данные не верны.');
				}

				if($concurs['concurs_must_loggedin'] != 'no' && $concurs['concurs_must_loggedin'] != $this->lauth->signin_type()){
					jsonResponse('Для участия в конкурсе вы должны войти в личный кабинет'.(($concurs['concurs_must_loggedin'] == 'social')?' используя социальную сеть':'').'.');
				}

				if($this->concurs->handler_exist_user($id_concurs, $this->lauth->id_user())){
					jsonResponse('Вы уже загружали фото для этого конкурса.', 'info');
				}
				
				$path = 'files/temp/concurs/'.$id_concurs.'/';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = uniqid();
				$config['max_size']	= '2000';

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}
				$data = $this->upload->data();

				$info = new StdClass;
				$info->name = $data['file_name'];
				$info->path = $path;
				$file = $info;
				jsonResponse('', 'success', array("file" => $file));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
}
