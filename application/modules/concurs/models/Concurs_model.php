<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Concurs_model extends CI_Model{
	var $users = "users";
	var $concurs = "concurs";
	var $concurs_users = "concurs_users";
	var $concurs_users_votes = "concurs_users_votes";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->concurs, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_concurs, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_concurs', $id_concurs);
		$this->db->update($this->concurs, $data);
	}

	function handler_delete($id_concurs){
		$this->db->where('id_concurs', $id_concurs);
		$this->db->delete($this->concurs);
	}

	function handler_get($id_concurs){
		$this->db->where('id_concurs', $id_concurs);
		return $this->db->get($this->concurs)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_concurs ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($active)){
			$this->db->where('concurs_active', $active);
        }

		$this->db->order_by($order_by);
		return $this->db->get($this->concurs)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($active)){
			$this->db->where('concurs_active', $active);
        }

		return $this->db->count_all_results($this->concurs);
	}

	function handler_insert_user($data = array()){
		if(empty($data)){
			return false;
		}

		$this->db->insert($this->concurs_users, $data);
		return $this->db->insert_id();
	}

	function handler_update_user($id_relation, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_relation', $id_relation);
		$this->db->update($this->concurs_users, $data);
	}

	function handler_get_user($conditions = array()){
		if(empty($conditions)){
			return false;
		}

		extract($conditions);

		if(isset($id_relation)){
			$this->db->where('id_relation', $id_relation);
		}

		if(isset($id_concurs)){
			$this->db->where('id_concurs', $id_concurs);
		}

		if(isset($id_user)){
			$this->db->where('id_user', $id_user);
		}

        if(isset($concurs_photo_moderated)){
			$this->db->where('concurs_photo_moderated', $concurs_photo_moderated);
        }

		return $this->db->get($this->concurs_users)->row_array();
	}

	function handler_delete_user($conditions = array()){
		if(empty($conditions)){
			return false;
		}

		extract($conditions);

		if(isset($id_relation)){
			$this->db->where('id_relation', $id_relation);
		}

		if(isset($id_concurs)){
			$this->db->where('id_concurs', $id_concurs);
		}

		if(isset($id_user)){
			$this->db->where('id_user', $id_user);
		}
		
		$this->db->delete($this->concurs_users);
	}

	function handler_exist_user($id_concurs = 0, $id_user = 0){
		$this->db->where('id_concurs', $id_concurs);
		$this->db->where('id_user', $id_user);

		return $this->db->count_all_results($this->concurs_users);
	}

	function handler_get_users_all($conditions = array()){
        $order_by = " id_relation ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		$this->db->select("{$this->concurs_users}.*");
		$this->db->select("{$this->users}.user_nicename,{$this->users}.user_email");
		$this->db->from($this->concurs_users);
		$this->db->join($this->users, "{$this->users}.id = {$this->concurs_users}.id_user", "left");

		if(isset($id_concurs)){
			$this->db->where("{$this->concurs_users}.id_concurs", $id_concurs);
		}

        if(isset($concurs_photo_moderated)){
			$this->db->where("{$this->concurs_users}.concurs_photo_moderated", $concurs_photo_moderated);
        }

        if(isset($id_concurs)){
			$this->db->where('id_concurs', $id_concurs);
        }

        if(isset($concurs_photo_moderated)){
			$this->db->where('concurs_photo_moderated', $concurs_photo_moderated);
        }

		$this->db->order_by($order_by);
		return $this->db->get()->result_array();
	}

	function handler_get_users_count($conditions = array()){
        extract($conditions);

        if(isset($id_concurs)){
			$this->db->where('id_concurs', $id_concurs);
        }

        if(isset($concurs_photo_moderated)){
			$this->db->where('concurs_photo_moderated', $concurs_photo_moderated);
        }

		return $this->db->count_all_results($this->concurs_users);
	}

	function handler_insert_user_vote($data = array()){
		if(empty($data)){
			return false;
		}

		$this->db->insert($this->concurs_users_votes, $data);
	}

	function handler_get_user_vote($conditions = array()){
		if(empty($conditions)){
			return false;
		}

		extract($conditions);

		if(isset($id_relation)){
			$this->db->where('id_relation', $id_relation);
		}

		if(isset($id_concurs)){
			$this->db->where('id_concurs', $id_concurs);
		}

		if(isset($id_user)){
			$this->db->where('id_user', $id_user);
		}

		return $this->db->get($this->concurs_users_votes)->row_array();
	}

	function handler_delete_user_vote($conditions = array()){
		if(empty($conditions)){
			return false;
		}

		extract($conditions);

		if(isset($id_relation)){
			$this->db->where('id_relation', $id_relation);
		}

		if(isset($id_concurs)){
			$this->db->where('id_concurs', $id_concurs);
		}

		if(isset($id_user)){
			$this->db->where('id_user', $id_user);
		}
		
		$this->db->delete($this->concurs_users_votes);
	}
}
?>
