<h1 class="page-header">
	Конкурсы
    <a href="<?php echo base_url('admin/concurs/add');?>" class="btn btn-success btn-xs pull-right">
        <i class="fa fa-plus"></i>
        Добавить конкурс
    </a>
</h1>
<div class="panel panel-default">
    <div class="panel-heading">
        Редактировать конкурс
    </div>
    <div class="panel-body">
		<div class="row">
			<div class="col-lg-12">
				<form role="form" id="edit_form">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label>
									<input type="checkbox" name="active" <?php echo set_checkbox('active', 1, $concurs['concurs_active'] == 1);?>>
									Активный
								</label>
							</div>
							<div class="form-group">
								<label class="w-100pr">Тип участников</label>
								<label class="w-100pr">
									<input type="radio" name="must_loggedin" value="no" <?php echo set_radio('must_loggedin', 'no', $concurs['concurs_must_loggedin'] == 'no');?>>
									<small>Для всех</small>
								</label>
								<label class="w-100pr">
									<input type="radio" name="must_loggedin" value="site" <?php echo set_radio('must_loggedin', 'site', $concurs['concurs_must_loggedin'] == 'site');?>>
									<small>Только для авторизованных</small>
								</label>
								<label class="w-100pr">
									<input type="radio" name="must_loggedin" value="social" <?php echo set_radio('must_loggedin', 'social', $concurs['concurs_must_loggedin'] == 'social');?>>
									<small>Только для авторизованных через соцсеть</small>
								</label>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title" value="<?php echo $concurs['concurs_title']?>">
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Дата</label>
								<div class="input-group">
									<span class="input-group-addon">с</span>
									<input type="text" class="form-control item_datepicker" data-title="Дата с" name="date_from" value="<?php echo getDateFormat($concurs['concurs_start'], 'Y-m-d H:i:s', 'd.m.Y');?>">
									<span class="input-group-addon">по</span>
									<input type="text" class="form-control item_datepicker" data-title="Дата по" name="date_to" value="<?php echo getDateFormat($concurs['concurs_end'], 'Y-m-d H:i:s', 'd.m.Y');?>">
								</div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="description"><?php echo $concurs['concurs_text'];?></textarea>
							</div>
						</div>
					</div>
					<input type="hidden" name="concurs" value="<?php echo $concurs['id_concurs'];?>">
					<button type="submit" class="btn btn-success">Сохранить</button>
				</form>
			</div>
		</div>
    </div>
</div>

<script>
	$(function(){
		$('.item_datepicker').datepicker({
            language: "ru"
		});
		
		var edit_form = $('#edit_form');
		edit_form.submit(function () {
			tinyMCE.triggerSave();
			var fdata = edit_form.serialize();
			$.ajax({
				type: 'POST',
				url: base_url+'admin/concurs/ajax_operations/edit',
				data: fdata,
				dataType: 'JSON',
				success: function(resp){
					systemMessages(resp.message, resp.mess_type);
				}
			});
			return false;
		});
	});
</script>
