<h1 class="page-header">
	Участники конкурса: <span><?php echo $concurs['concurs_title'];?></span>
</h1>
<div class="table-responsive">
	<div class="directory-search-key-b mt-15"></div>
	<table class="table table-striped table-bordered w-100pr" id="dtTable">
		<thead>
			<tr>
				<th class="text-center dt_id">#</th>
				<th class="text-center dt_photo">Фото</th>
				<th class="text-left dt_user">Имя</th>
				<th class="text-left dt_email">Email</th>
				<th class="text-center dt_votes"><span class="ca-icon ca-icon_hand-up"></span></th>
				<th class="text-center dt_date">Дата</th>
				<th class="text-center dt_moderated">Модерация</th>
				<th class="text-center dt_actions"></th>
			</tr>
		</thead>
	</table>
</div>
<script>
	var dtFilter; //obj for filters
	var dtTable; //obj of datatable
	$(function(){
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "/theme/admin/bower_components/datatable/js/Russian.json"
			},
			"sDom": '',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/concurs/ajax_operations/users_dt/<?php echo $concurs['id_concurs'];?>",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "w-50 text-center vam", "aTargets": ["dt_id"], "mData": "dt_id"},
				{ "sClass": "w-50 text-center vam", "aTargets": ["dt_photo"], "mData": "dt_photo", "bSortable": false},
				{ "sClass": "text-left vam", "aTargets": ["dt_user"], "mData": "dt_user", "bSortable": false},
				{ "sClass": "w-150 text-left vam", "aTargets": ["dt_email"], "mData": "dt_email", "bSortable": false},
				{ "sClass": "w-50 text-center vam", "aTargets": ["dt_votes"], "mData": "dt_votes"},
				{ "sClass": "w-120 text-center vam", "aTargets": ["dt_date"], "mData": "dt_date"},
				{ "sClass": "w-100 text-center vam", "aTargets": ["dt_moderated"], "mData": "dt_moderated"},
				{ "sClass": "w-50 text-center vam", "aTargets": ["dt_actions"], "mData": "dt_actions", "bSortable": false}

			],
			"aaSorting" : [[0,'asc']],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				if(!dtFilter){
					dtFilter = $('.dt_filter').dtFilters({
						'container': '.directory-search-key-b',
						callBack: function(){ dtTable.fnDraw(); }
					});
				}

				aoData = aoData.concat(dtFilter.getDTFilter());
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type == 'error')
							systemMessages(data.message, 'message-' + data.mess_type);
						if(data.mess_type == 'info')
							systemMessages(data.message, 'message-' + data.mess_type);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
				$(".switch-checkbox").bootstrapSwitch({
					onText: 'Да',
      				offText: 'Нет'
				}).on('switchChange.bootstrapSwitch', function(event, state) {
					var $this = $(this);
					var relation = $this.data('relation');
					$.ajax({
						type: 'POST',
						url: base_url+'admin/concurs/ajax_operations/change_user_status',
						data: {relation:relation},
						dataType: 'JSON',
						success: function(resp){
							systemMessages(resp.message, resp.mess_type);
							if(resp.mess_type == 'success'){
								dtTable.fnDraw(false);
							}
						}
					});
				});
			}

		});
	});
	var delete_action = function(btn){
		var $this = $(btn);
		var relation = $this.data('relation');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/concurs/ajax_operations/delete_user',
			data: {relation:relation},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>
