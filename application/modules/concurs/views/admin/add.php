<h1 class="page-header">
    Конкурсы
    <a href="<?php echo base_url('admin/concurs/add');?>" class="btn btn-success btn-xs pull-right">
        <i class="fa fa-plus"></i>
        Добавить конкурс
    </a>
</h1>
<div class="panel panel-default">
    <div class="panel-heading">
        Добавить конкурс
    </div>
    <div class="panel-body">
		<div class="row">
			<div class="col-lg-12">
				<form role="form" id="add_form">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label>
									<input type="checkbox" name="active">
									Активный
								</label>
							</div>
							<div class="form-group">
								<label class="w-100pr">Тип участников</label>
								<label class="w-100pr">
									<input type="radio" name="must_loggedin" value="no" checked>
									<small>Для всех</small>
								</label>
								<label class="w-100pr">
									<input type="radio" name="must_loggedin" value="site">
									<small>Только для авторизованных</small>
								</label>
								<label class="w-100pr">
									<input type="radio" name="must_loggedin" value="social">
									<small>Только для авторизованных через соцсеть</small>
								</label>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title" value="">
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Дата</label>
								<div class="input-group">
									<span class="input-group-addon">с</span>
									<input type="text" class="form-control item_datepicker" data-title="Дата с" name="date_from">
									<span class="input-group-addon">по</span>
									<input type="text" class="form-control item_datepicker" data-title="Дата по" name="date_to">
								</div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="description"></textarea>
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-success">Сохранить</button>
				</form>
			</div>
		</div>
    </div>
</div>

<script>
	$(function(){
		$('.item_datepicker').datepicker({
            language: "ru"
		});
		
		var add_form = $('#add_form');
		add_form.submit(function () {
			tinyMCE.triggerSave();
			var fdata = add_form.serialize();
			$.ajax({
				type: 'POST',
				url: base_url+'admin/concurs/ajax_operations/add',
				data: fdata,
				dataType: 'JSON',
				success: function(resp){
					if(resp.mess_type == 'success'){
						add_form.replaceWith('<div class="alert alert-success mb-0 ml-5 mr-5">'+resp.message+'</div>');
					} else{
						systemMessages(resp.message, resp.mess_type);
					}
				}
			});
			return false;
		});
	});
</script>
