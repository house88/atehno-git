<h1 class="page-header">
	Конкурсы
    <a href="<?php echo base_url('admin/concurs/add');?>" class="btn btn-success btn-xs pull-right">
        <i class="fa fa-plus"></i>
        Добавить конкурс
    </a>
</h1>
<div class="table-responsive">
	<div class="directory-search-key-b mt-15"></div>
	<table class="table table-striped table-bordered w-100pr" id="dtTable">
		<thead>
			<tr>
				<th class="text-center dt_id">#</th>
				<th class="text-left dt_name">Название</th>
				<th class="text-center dt_sdate">Дата с</th>
				<th class="text-center dt_edate">Дата по</th>
				<th class="text-center dt_active">Активный</th>
				<th class="text-center dt_actions"></th>
			</tr>
		</thead>
	</table>
</div>
<script>
	var dtFilter; //obj for filters
	var dtTable; //obj of datatable
	$(function(){
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "/theme/admin/bower_components/datatable/js/Russian.json"
			},
			"sDom": '',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/concurs/ajax_operations/list_dt",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "w-50 text-center vam", "aTargets": ["dt_id"], "mData": "dt_id", "bSortable": true},
				{ "sClass": "text-left vam", "aTargets": ["dt_name"], "mData": "dt_name", "bSortable": true},
				{ "sClass": "w-100 text-center vam", "aTargets": ["dt_sdate"], "mData": "dt_sdate", "bSortable": true},
				{ "sClass": "w-100 text-center vam", "aTargets": ["dt_edate"], "mData": "dt_edate", "bSortable": false},
				{ "sClass": "w-95 text-center vam", "aTargets": ["dt_active"], "mData": "dt_active" , "bSortable": false },
				{ "sClass": "w-120 text-center vam", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }

			],
			"aaSorting" : [[0,'asc']],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				if(!dtFilter){
					dtFilter = $('.dt_filter').dtFilters({
						'container': '.directory-search-key-b',
						callBack: function(){ dtTable.fnDraw(); }
					});
				}

				aoData = aoData.concat(dtFilter.getDTFilter());
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type == 'error')
							systemMessages(data.message, 'message-' + data.mess_type);
						if(data.mess_type == 'info')
							systemMessages(data.message, 'message-' + data.mess_type);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
				$(".switch-checkbox").bootstrapSwitch({
					onText: 'Да',
      				offText: 'Нет'
				}).on('switchChange.bootstrapSwitch', function(event, state) {
					var $this = $(this);
					var concurs = $this.data('concurs');
					$.ajax({
						type: 'POST',
						url: base_url+'admin/concurs/ajax_operations/change_status/',
						data: {concurs:concurs},
						dataType: 'JSON',
						success: function(resp){
							systemMessages(resp.message, resp.mess_type);
							if(resp.mess_type == 'success'){
								dtTable.fnDraw(false);
							}
						}
					});
				});
			}

		});
	});
	var delete_action = function(btn){
		var $this = $(btn);
		var concurs = $this.data('concurs');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/concurs/ajax_operations/delete',
			data: {concurs:concurs},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>
