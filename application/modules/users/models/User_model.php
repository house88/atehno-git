<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model{
	
	var $users_table = "users";
	var $users_groups_table = "users_groups";
	var $users_rights_table = "users_rights";
	var $users_groups_rights_table = "users_groups_rights";
	
	function __construct(){
		parent::__construct();
	}
	
	function create($data){
		$str = $this->db->insert_string($this->users_table, $data);
		
		$query = $this->db->query($str);
		
		if($query){
			return true;
		}else{
			return false;
		}
		
	}
	
	function read(){
		$query = $this->db->query("SELECT * FROM $this->users_table");
		return $query->result();
	}
	
	function user_by_id($id){
		$query = $this->db->query("
			SELECT * 
			FROM $this->users_table
			WHERE id = $id
		");
		
		$query->row()->role = $this->get_role($id);
		$query->row()->role_name = $this->get_role_name($query->row()->role);
		
		if($query->num_rows > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	
	function user_by_nicename($user_nicename){
		//Get ID
		$query = $this->db->query("SELECT id FROM $this->users_table WHERE user_nicename = ?", $user_nicename);
				
		if($query->num_rows > 0){
			return $this->user_by_id($query->row()->id);
		}else{
			return false;
		}
	}
	
	function update($userid, $userdata){
		$data = (array)$userdata;
		$where = "id = $userid"; 
		$str = $this->db->update_string($this->users_table, $data, $where);
		$query = $this->db->query($str);
		return $query;
	}
	
	
	function delete(){
		
	}
	
	function get_role($user_id){
		$query = $this->db->query("SELECT role_id FROM users_roles WHERE user_id = $user_id");
		if($query->num_rows > 0){
			return (int)$query->row()->role_id;
		}else{
			return 0;
		}
	}
	function get_role_name($role_id){
		$query = $this->db->query("SELECT name FROM roles WHERE id = $role_id");
		if($query->num_rows > 0){
			return $query->row()->name;
		}else{
			return false;
		}
	}
	
	function validate($user_email, $password){
		$query = $this->db->query("SELECT * FROM $this->users_table WHERE user_email = '$user_email' AND user_pass = '$password'");
		if($query->num_rows === 1){
			return $query->row();
		}else{
			return false;
		}
	}

	// ADMIN MANAGE USERS
	function handler_get($id_user, $params = []){
		$columns = "*";
		extract($params);
		
		$this->db->select("{$columns}");
		$this->db->from($this->users_table);
		$this->db->where("id", $id_user);
		return $this->db->get()->row();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id DESC ";

        extract($conditions);

		if (isset($sort_by)) {
			$multi_order_by = array();
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}

			if (!empty($multi_order_by)) {
				$order_by = implode(',', $multi_order_by);
			}
		}

		$this->db->select("$this->users_table.*, $this->users_groups_table.*");
		$this->db->from($this->users_table);
		$this->db->join($this->users_groups_table, "$this->users_table.id_group = $this->users_groups_table.id_group");

        if(isset($users_list)){
			$this->db->where_in("$this->users_table.id", $users_list);
        }

        if(isset($status)){
			$this->db->where("$this->users_table.status", $status);
        }

        if(isset($group_type)){
			$this->db->where_in("$this->users_groups_table.group_type", $group_type);
        }

        if(isset($registered_date_from)){
			$this->db->where("DATE($this->users_table.registered_date) >= DATE('{$registered_date_from}')");
        }

        if(isset($registered_date_to)){
			$this->db->where("DATE($this->users_table.registered_date) <= DATE('{$registered_date_to}')");
        }

		if (isset($keywords)) {
			$this->db->where(' (user_login LIKE "%'.$keywords.'%" OR user_email LIKE "%'.$keywords.'%" OR user_phone LIKE "%'.$keywords.'%" OR user_address LIKE "%'.$keywords.'%") ');
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get()->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

		$this->db->from($this->users_table);
		$this->db->join($this->users_groups_table, "$this->users_table.id_group = $this->users_groups_table.id_group");

        if(isset($users_list)){
			$this->db->where_in("$this->users_table.id", $users_list);
        }

        if(isset($status)){
			$this->db->where("$this->users_table.status", $status);
        }

        if(isset($group_type)){
			$this->db->where_in("$this->users_groups_table.group_type", $group_type);
        }

        if(isset($registered_date_from)){
			$this->db->where("DATE($this->users_table.registered_date) >= DATE('{$registered_date_from}')");
        }

        if(isset($registered_date_to)){
			$this->db->where("DATE($this->users_table.registered_date) <= DATE('{$registered_date_to}')");
        }

		if (isset($keywords)) {
			$this->db->where(' (user_login LIKE "%'.$keywords.'%" OR user_email LIKE "%'.$keywords.'%" OR user_phone LIKE "%'.$keywords.'%" OR user_address LIKE "%'.$keywords.'%") ');
		}

		return $this->db->count_all_results();
	}

	// GROUPS FUNCTIONS
	function handler_insert_group($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->users_groups_table, $data);
		return $this->db->insert_id();
	}

	function handler_update_group($id_group, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where("id_group", $id_group);
		return $this->db->update($this->users_groups_table, $data);
	}

	function handler_delete_group($id_group){
		$this->db->where('id_group', $id_group);			
		return $this->db->delete($this->users_groups_table);
	}

	function handler_get_group($id_group){
		$this->db->where("id_group", $id_group);
		return $this->db->get($this->users_groups_table)->row_array();
	}

	function handler_get_groups_all($conditions = array()){
        $order_by = " id_group ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($group_type)){
			$this->db->where("group_type", $group_type);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->users_groups_table)->result_array();
	}

	function handler_get_groups_count($conditions = array()){
        extract($conditions);

        if(isset($group_type)){
			$this->db->where("$this->users_groups_table.group_type", $group_type);
        }

		return $this->db->count_all_results($this->users_groups_table);
	}

	// RIGHTS FUNCTIONS
	function handler_insert_right($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->users_rights_table, $data);
		return $this->db->insert_id();
	}

	function handler_update_right($id_right, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where("id_right", $id_right);
		return $this->db->update($this->users_rights_table, $data);
	}

	function handler_delete_right($id_right){
		$this->db->where('id_right', $id_right);			
		return $this->db->delete($this->users_rights_table);
	}

	function handler_get_right($id_right){
		$this->db->where("id_right", $id_right);
		return $this->db->get($this->users_rights_table)->row_array();
	}

	function handler_get_rights_all($conditions = array()){
        $order_by = " id_right ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->users_rights_table)->result_array();
	}

	function handler_get_rights_count($conditions = array()){
        extract($conditions);

		return $this->db->count_all_results($this->users_rights_table);
	}

	// GROUP RIGHTS FUNCTIONS
	function handler_insert_group_right($data = array()){
		if(empty($data)){
			return;
		}
		
		return $this->db->insert($this->users_groups_rights_table, $data);
	}

	function handler_delete_group_right($group, $right){
		$this->db->where('id_group', $group);
		$this->db->where('id_right', $right);				
		return $this->db->delete($this->users_groups_rights_table);
	}

	function handler_get_group_right($group, $right){
		$this->db->where('id_group', $group);
		$this->db->where('id_right', $right);				
		return $this->db->get($this->users_groups_rights_table)->row();
	}

	function handler_get_groups_rights_all(){
		$this->db->select("CONCAT_WS('_', id_group, id_right) as gr_key, id_group, id_right");
		$this->db->from("{$this->users_groups_rights_table}");
		$this->db->order_by("id_group ASC, id_right ASC");
				
		return $this->db->get()->result_array();
	}

	function handler_get_group_rights($group){
		$this->db->where('id_group', $group);				
		return $this->db->get($this->users_groups_rights_table)->result();
	}

	function handler_get_group_users($group){
		$this->db->where('id_group', $group);				
		return $this->db->get($this->users_table)->result();
	}

	function handler_get_right_groups($right){
		$this->db->where('id_right', $right);				
		return $this->db->get($this->users_groups_rights_table)->result();
	}
}

?>