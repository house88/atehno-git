<h1 class="page-header">
   Статьй
    <a href="<?php echo base_url('admin/articles/add');?>" class="btn btn-success btn-xs pull-right">
        <i class="fa fa-plus"></i>
        Добавить статью
    </a>
</h1>
<div class="panel panel-default">
    <div class="panel-heading">
        Редактировать статью
    </div>
    <div class="panel-body">
		<div class="row">
			<div class="col-lg-12">
				<form role="form" id="edit_form">
                    <div>
						<div class="form-group">
							<label>Фото</label>
							<p class="help-block">Ширина: 800px, Высота: 640px</p>
							<div class="clearfix"></div>
							<span class="btn btn-default btn-file pull-left mb-15">
								<i class="fa fa-picture-o"></i>
								Добавить фото <input id="select_photo" type="file" name="userfile">
							</span>
							<div class="clearfix"></div>
							<div id="article_photo" class="files pull-left w-100pr">
							<?php if($article['article_photo'] != ''){?>
								<div class="user-image-thumbnail-wr">
									<div class="user-image-thumb">
										<img class="img-thumbnail" src="<?php echo base_url('files/articles/'.$article['article_photo']);?>">
									</div>
									<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="<?php echo $article['article_photo'];?>">
										<span class="glyphicon glyphicon-remove-circle"></span>
									</a>
									<input type="hidden" name="article_photo" value="<?php echo $article['article_photo'];?>">
								</div>
							<?php }?>
							</div>
						</div>
						<div class="form-group">
							<label>Название</label>
							<input class="form-control"
								   placeholder="Название"
								   name="title"
								   id="title"
								   value="<?php echo $article['article_title'];?>">
							<input type="hidden" name="url" id="url" value="<?php echo $article['url'];?>">
							<p class="help-block">Название не должно содержать более 100 символов.</p>
						</div>
						<div class="form-group">
							<label>Краткое описание</label>
							<textarea class="form-control" name="stext"><?php echo $article['article_small_description'];?></textarea>
							<p class="help-block">Не должно содержать более 250 символов.</p>
						</div>
						<div class="form-group">
							<label>Текст</label>
							<textarea class="description" name="description"><?php echo $article['article_description'];?></textarea>
						</div>
						<div class="form-group">
							<label>Meta keywords</label>
							<input class="form-control"
								   placeholder="Meta keywords"
								   name="mk"
								   value="<?php echo $article['mk'];?>">
							<p class="help-block">Не должно содержать более 250 символов.</p>
						</div>
						<div class="form-group">
							<label>Meta description</label>
							<input class="form-control"
								   placeholder="Meta description"
								   name="md"
								   value="<?php echo $article['md'];?>">
							<p class="help-block">Не должно содержать более 250 символов.</p>
						</div>
                    </div>
					<input type="hidden" name="article" value="<?php echo $article['id_article'];?>">
					<button type="submit" class="btn btn-success">Сохранить</button>
				</form>
			</div>
		</div>
    </div>
</div>

<script>
	$(function(){
		'use strict';
		$('#select_photo').fileupload({
			url: base_url+'admin/articles/upload_photo',
			dataType: 'json',
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
					$.each(data.result.files, function (index, file) {
						var template = '<div class="user-image-thumbnail-wr">';
						template += '<div class="user-image-thumb"><img class="img-thumbnail" src="'+base_url+'files/articles/'+file.filename+'"/></div>';
						template += '<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="'+file.filename+'"><span class="glyphicon glyphicon-remove-circle"></span></a>';
						template += '<input type="hidden" name="article_photo" value="'+file.filename+'">';
						template += '</div>';

						if($('#article_photo .user-image-thumbnail-wr').length > 0){
							var unused_photo = $('#article_photo .user-image-thumbnail-wr').find('input[name=article_photo]').val();
							$('#article_photo').closest('form').append('<input type="hidden" name="remove_photos[]" value="'+unused_photo+'"/>');
						}
						$('#article_photo').html(template);
					});
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');

		$('#title').liTranslit({
			elAlias: $('#url')
		});
		var edit_form = $('#edit_form');
		edit_form.submit(function () {
			tinyMCE.triggerSave();
			var fdata = edit_form.serialize();
			$.ajax({
				type: 'POST',
				url: base_url+'admin/articles/ajax_operations/edit',
				data: fdata,
				dataType: 'JSON',
				success: function(resp){
					systemMessages(resp.message, resp.mess_type);
				}
			});
			return false;
		});
	});

	var remove_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
		$this.closest('.user-image-thumbnail-wr').remove();
	}
</script>
