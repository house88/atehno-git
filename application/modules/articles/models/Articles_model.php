<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Articles_model extends CI_Model{
	var $articles = "articles";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->articles, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_article, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_article', $id_article);
		$this->db->update($this->articles, $data);
	}

	function handler_delete($id_article){
		$this->db->where('id_article', $id_article);
		$this->db->delete($this->articles);
	}

	function handler_get($id_article){
		$this->db->where('id_article', $id_article);
		return $this->db->get($this->articles)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_article ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($articles_list)){
			$this->db->where_in('id_article', $articles_list);
        }

        if(isset($active)){
			$this->db->where('article_active', $active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->articles)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($articles_list)){
			$this->db->where_in('id_article', $articles_list);
        }

        if(isset($active)){
			$this->db->where('article_active', $active);
        }

		return $this->db->count_all_results($this->articles);
	}
}
?>
