<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		redirect('/admin');

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->data = array();

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Articles_model", "articles");
        $this->load->model("menu/Menu_model", "menu");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		if(!$this->lauth->have_right('manage_articles')){
			redirect('/admin');
		}

		$this->data['main_content'] = 'admin/list';
		$this->load->view('admin/page', $this->data);
	}

	function add(){
		if(!$this->lauth->have_right('manage_articles')){
			redirect('/admin');
		}

		$this->data['main_content'] = 'admin/add';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		if(!$this->lauth->have_right('manage_articles')){
			redirect('/admin');
		}

		$id_article = (int)$this->uri->segment(4);
		$this->data['article'] = $this->articles->handler_get($id_article);
		$this->data['main_content'] = 'admin/edit';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				if(!$this->lauth->have_right('manage_articles')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('description', 'Текст статьи', 'required');
				$this->form_validation->set_rules('stext', 'Краткое описание', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('url', 'URL', 'required|xss_clean');
				$this->form_validation->set_rules('mk', 'Meta keywords', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md', 'Meta description', 'required|xss_clean|max_length[250]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/articles/'.$remove_photo);
						@unlink('files/articles/thumbs/thumb_200x200_'.$remove_photo);
					}
				}

				$insert = array(
					'article_title' => $this->input->post('title'),
					'article_description' => $this->input->post('description'),
					'article_small_description' => $this->input->post('stext'),
					'url' => $this->input->post('url'),
					'mk' => $this->input->post('mk'),
					'md' => $this->input->post('md'),
					'article_photo' => ($this->input->post('article_photo'))?$this->input->post('article_photo'):''
				);

				$this->articles->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				if(!$this->lauth->have_right('manage_articles')){
					jsonResponse('Ошибка: Нет прав!');
				}

                $this->form_validation->set_rules('article', 'Статья', 'required|xss_clean');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('description', 'Текст статьи', 'required');
				$this->form_validation->set_rules('stext', 'Краткое описание', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('url', 'URL', 'required|xss_clean');
				$this->form_validation->set_rules('mk', 'Meta keywords', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md', 'Meta description', 'required|xss_clean|max_length[250]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_article = (int)$this->input->post('article');
				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/articles/'.$remove_photo);
						@unlink('files/articles/thumbs/thumb_200x200_'.$remove_photo);
					}
				}

				$update = array(
					'article_title' => $this->input->post('title'),
					'article_description' => $this->input->post('description'),
					'article_small_description' => $this->input->post('stext'),
					'url' => $this->input->post('url'),
					'mk' => $this->input->post('mk'),
					'md' => $this->input->post('md'),
					'article_photo' => ($this->input->post('article_photo'))?$this->input->post('article_photo'):''
				);

				$this->articles->handler_update($id_article, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				if(!$this->lauth->have_right('manage_articles')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$this->form_validation->set_rules('article', 'Статья', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_article = (int)$this->input->post('article');
				$article = $this->articles->handler_get($id_article);
				if(empty($article)){
					jsonResponse('Статья не существует.');
				}

				if(!empty($article['article_photo'])){
					@unlink('files/articles/'.$article['article_photo']);
					@unlink('files/articles/thumbs/thumb_200x200_'.$article['article_photo']);
				}

				$this->articles->handler_delete($id_article);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				if(!$this->lauth->have_right('manage_articles')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$this->form_validation->set_rules('article', 'Статья', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_article = (int)$this->input->post('article');
				$article = $this->articles->handler_get($id_article);
				if(empty($article)){
					jsonResponse('Статья не существует.');
				}
				if($article['article_visible']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->articles->handler_update($id_article, array('article_visible' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
		}
	}

	function ajax_list_dt(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		if(!$this->lauth->have_right('manage_articles')){
			jsonDTResponse('Ошибка: Нет прав!');
		}

		$params = array(
            'limit' => intVal($_POST['iDisplayLength']),
            'start' => intVal($_POST['iDisplayStart'])
        );

        if ($_POST['iSortingCols'] > 0) {
            for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
                switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
                    case 'dt_id': $params['sort_by'][] = 'id_article-' . $_POST['sSortDir_' . $i];
                    break;
                    case 'dt_name': $params['sort_by'][] = 'article_title-' . $_POST['sSortDir_' . $i];
                    break;
                }
            }
        }

        $records = $this->articles->handler_get_all($params);
        $records_total = $this->articles->handler_get_count($params);

        $output = array(
            "sEcho" => intval($_POST['sEcho']),
            "iTotalRecords" => $records_total,
            "iTotalDisplayRecords" => $records_total,
            "aaData" => array()
        );

        foreach ($records as $record) {
			$status = '<a href="#" data-message="Вы уверены что хотите сделать статью активной?" data-title="Изменение статуса" data-callback="change_status" data-article="'.$record['id_article'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
			if($record['article_visible'] == 0){
				$status = '<a href="#" data-message="Вы уверены что хотите сделать статью неактивной?" data-title="Изменение статуса" data-callback="change_status" data-article="'.$record['id_article'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
			}

            $output['aaData'][] = array(
                'dt_id'			=>  $record['id_article'],
                'dt_photo'		=>  '<img src="'.base_url(getImage('files/articles/'.$record['article_photo'])).'" alt="'.$record['article_title'].'" class="img-thumbnail mw-100 mh-100">',
                'dt_name'		=>  '<a href="'.base_url('articles/detail/'.$record['url'].'-'.$record['id_article']).'">'.$record['article_title'].'</a>',
                'dt_url'		=>  'articles/detail/'.$record['url'].'-'.$record['id_article'],
				'dt_actions'	=>  $status.' '
									.'<a href="'.base_url('admin/articles/edit/'.$record['id_article']).'" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>'
                					.' <a href="#" class="btn btn-danger btn-xs confirm-dialog" data-message="Вы уверены что хотите удалить статью?" data-callback="delete_action" data-article="'.$record['id_article'].'"><i class="ca-icon ca-icon_remove"></i></a>'
            );
        }
        jsonResponse('', 'success', $output);
	}

	function upload_photo(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		if(!$this->lauth->have_right('manage_articles')){
			jsonResponse('Ошибка: Нет прав!');
		}

        $path = 'files/articles';
        if(!is_dir($path))
            mkdir($path, 0755, true);

        if(!is_dir($path.'/thumbs'))
            mkdir($path.'/thumbs', 0755, true);

        $config['upload_path'] = FCPATH . $path;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = uniqid();
        $config['min_width']	= '800';
        $config['min_height']	= '640';
        $config['max_size']	= '2000';

        $this->load->library('upload', $config);
		$this->load->library('image_lib');
        if ( ! $this->upload->do_upload()){
			jsonResponse($this->upload->display_errors('',''),'error');
        }
		$data = $this->upload->data();

		$config = array(
			'source_image'      => $data['full_path'],
			'create_thumb'      => true,
			'thumb_marker'      => 'thumb_200x200_',
			'new_image'         => FCPATH . $path . '/thumbs',
			'maintain_ratio'    => true,
			'width'             => 200,
			'height'            => 200
		);

		$this->image_lib->initialize($config);
		$this->image_lib->resize();

		$info = new StdClass;
		$info->filename = $data['file_name'];
		$files[] = $info;
		jsonResponse('', 'success', array("files" => $files));
    }
}
