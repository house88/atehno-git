<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
        }
        
		$this->view_module_path = 'modules/crons/';
		$this->active_menu = 'catalog';
		$this->active_submenu = 'crons';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

        $this->load->model("Crons_model", 'crons');

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
        $this->data['page_header'] = 'Логи кронов';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->data['dt_filter'] = $this->theme->apanel_view($this->view_module_path . 'filter_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'list':
				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart')
				);

				if($this->input->post('log_date')){
					list($log_date_from, $log_date_to) = explode(' - ', $this->input->post('log_date'));

					if(validateDate($log_date_from, 'd.m.Y')){
						$params['log_date_from'] = getDateFormat($log_date_from, 'd.m.Y', 'Y-m-d');
					}

					if(validateDate($log_date_to, 'd.m.Y')){
						$params['log_date_to'] = getDateFormat($log_date_to, 'd.m.Y', 'Y-m-d');
					}
				}
		
				$records = $this->crons->handler_get_logs_all($params);
                $records_total = $this->crons->handler_get_logs_count($params);

                $statuses = array(
                    'success' => 'fad fa-check-circle text-success',
                    'info' => 'fad fa-info-circle text-primary',
                    'warning' => 'fad fa-exclamation-circle text-warning',
                    'danger' => 'fad fa-ban text-danger',
                );
                
				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record) use ($statuses){
						return array(
							'dt_status'		=>  '<i class="custom-font-size-24 '. $statuses[$record['log_type']] .'"></i>',
							'dt_name'		=>  $record['log_name'],
							'dt_text'		=>  $record['log_text'],
							'dt_date'		=>  getDateFormat($record['log_date'])
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
		}
	}
}