<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Crons extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->data = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

        $this->load->model("Crons_model", 'crons');
        $this->load->model("import/Import_model", "import");
	}

    function index(){
        return false;
	}

	function update_sitemap(){
        $token = $this->uri->segment(3);
        if($token !== 'cronaction'){
            return;
        }

		ini_set('max_execution_time', 0);
		
		$content = Modules::run('seo/sitemap');
		$fp = fopen('sitemap.xml', 'w');
		fwrite($fp, $content);
		fclose($fp);
	}

	function new_import_items(){
		ini_set('max_execution_time', 0);
		
		$token = $this->uri->segment(3);
        if($token !== 'cronaction'){
            return;
		}

		$log_name = 'Импорт товаров по крону';
		$this->crons->handler_insert_log(array('log_name' => $log_name, 'log_text' => 'Запуск операций по импорту товаров.'));
		
		$import = $this->crons->handler_import_get();
		if(empty($import)){
			$this->_notify_admin('import_items', array('logs' => array('Вариант импорта не существует.')));
			$this->crons->handler_insert_log(array('log_name' => $log_name, 'log_text' => 'Вариант импорта не существует.', 'log_type' => 'danger'));
            return;
		}

		$this->load->model('items/Items_model', 'items');
		$this->load->model('categories/Categories_model', 'categories');
		$this->load->model('import/Import_items_model', 'import_items');
		$this->load->model('import/Import_categories_model', 'import_categories');
		$this->load->model('import/Import_suppliers_model', 'import_suppliers');
		$this->load->model('import/Import_stocks_model', 'import_stocks');

		$import_columns = json_decode($import['import_columns'], true);
		
		$import_records = array(
			'categories'	=> $this->import_categories->handler_get_all(array('updated' => false)),
			'suppliers' 	=> $this->import_suppliers->handler_get_all(array('updated' => false)),
			'stocks' 		=> $this->import_stocks->handler_get_all(array('updated' => false)),
			'items' 		=> $this->import_items->handler_get_all(array('updated' => false)),
		);

		if(!empty($import_records['categories']) && !empty($import_columns['categories'])){
			$import_categories_report = $this->import_categories_handler($import_records['categories']);
		}

		if(!empty($import_records['stocks']) && isset($import_columns['stocks'])){
			$import_stocks_report = $this->import_stocks_handler($import_records['stocks']);
		}

		if(!empty($import_records['suppliers']) && isset($import_columns['suppliers'])){
			$import_suppliers_report = $this->import_suppliers_handler($import_records['suppliers']);
		}
		
		if(!empty($import_records['items']) && !empty($import_columns['items'])){
			$import_items_report = $this->import_items_handler($import_records['items'], $import_columns['items'], $import['import_before_action']);
		}
		
		$log_text = array(
			"Добавленно категорий: {$import_categories_report['new_categories']}",
			"Обновленно категорий: {$import_categories_report['update_categories']}",
			"Добавленно складов: {$import_stocks_report['new_stocks']}",
			"Обновленно складов: {$import_stocks_report['update_stocks']}",
			"Добавленно поставщиков: {$import_suppliers_report['new_suppliers']}",
			"Обновленно поставщиков: {$import_suppliers_report['update_suppliers']}",
			"Добавленно товаров: {$import_items_report['new_items']}",
			"Обновленно товаров: {$import_items_report['updated_items']}",
			'Операция по импорту товаров прошла успешно.',
		);

		if(!empty($import_items_report['restored_items'])){
			$log_text[] = "Восстановленные товары: " . implode(', ', $import_items_report['restored_items']);
		}
		
		if(!empty($import_items_report['bad_items_price'])){
			$log_text[] = "Товары без цены: " . implode(', ', $import_items_report['bad_items_price']);
		}

		$this->_notify_admin('import_items', array('logs' => $log_text, 'title' => 'Результат операций импорта товаров по крону:'));
		$this->crons->handler_insert_log(array('log_name' => $log_name, 'log_text' => implode('<br>', $log_text), 'log_type' => empty($import_items_report['bad_items_price']) ? 'success' : 'warning'));
	}

	private function import_items_handler(array $items, array $import_columns, string $before_action = 'nothing'){
		$response = array(
			'bad_items_price'	=> array(),
			'restored_items' 	=> array(),
			'updated_items' 	=> 0,
			'new_items' 		=> 0,
		);
		
		//region additional processing before import
		$reset_columns_data = array(
			'item_suppliers_available'	=> 0,
			'item_stocks_quantity' 		=> 0,
			'item_suppliers' 			=> '',
			'item_quantity' 			=> 0,
			'item_stocks' 				=> '',
		);

		if('nothing' !== $before_action){
			switch ($before_action) {			
				case 'reset_quantity':
					$reset_columns_data['item_quantity'] = 0;
				break;			
				case 'hide_all':
					$reset_columns_data['item_visible'] = 0;
				break;
			}
		}

		$this->items->handler_update_all($reset_columns_data);
		//endregion additional processing before import

		$item_slug_configs = array(
			'replacement'	=> 'dash',
			'table' 		=> 'items',
			'field' 		=> 'item_url',
			'title' 		=> 'item_title',
			'id' 			=> 'item_prog_id',
		);
		
		$this->load->library('slug', $item_slug_configs);

		$datetime_of_update = date('Y-m-d H:i:s');
		$categories = array_column($this->categories->handler_get_all(array('category_items_count' => false)), null, 'category_prog_id');
		$db_items = array_column($this->items->handler_get_all_without_conditions(), null, 'item_prog_id');
		foreach($items as $item){
			$db_item = $db_items[$item['item_prog_id']] ?? null;

			if(empty($db_item) && !empty($db_item = $this->items->handler_get_by_prog_id_deleted($item['item_prog_id']))){
				$db_item['item_deleted'] = 0;
				$db_item['item_visible'] = 0;
				$db_item['item_suppliers'] = '';
				$db_item['item_suppliers_available'] = 0;
				$db_item['item_stocks'] = '';
				$db_item['item_stocks_quantity'] = 0;
				$db_item['item_quantity'] = 0;

				$this->items->handler_insert($db_item);
				$this->items->handler_delete_deleted($db_item['id_item']);
				$response['restored_items'][] = $item['item_prog_id'];
			}

			$is_empty_db_item = empty($db_item);
			$item_data = array();

			if($is_empty_db_item){
				$response['new_items']++;
			}else{
				$response['updated_items']++;
			}

			foreach($import_columns as $import_column){
				switch($import_column){
					case 'category_prog_id':
						$item_data['id_category'] = $categories[$item['category_prog_id']]['category_id'] ?? 0;
					break;
					case 'suppliers':
						if(!empty($item['suppliers'])){
							$item_suppliers = json_decode($item['suppliers'], true);
							$suppliers = isset($item_suppliers[0]) ? $item_suppliers : array($item_suppliers);
							usort($suppliers, function($a, $b){
								if($a['to'] == $b['to']){
									return 0;
								}

								return $a['to'] < $b['to'] ? -1 : 1;
							});

							$item_data['item_suppliers'] = json_encode($suppliers);
							$item_data['item_suppliers_available'] = count($suppliers);
						}
					break;
					case 'stocks':
						$item_stocks_quantity = 0;

						if(!empty($item['stocks'])){
							$items_stock_decoded = json_decode($item['stocks'], TRUE);
							$item_stocks = isset($items_stock_decoded[0]) ? $items_stock_decoded : array($items_stock_decoded);
							 
							$item_data['item_stocks'] = json_encode($item_stocks);
							foreach ($item_stocks as $stock) {
								$item_stocks_quantity += (int) $stock['quantity'];
							}
						}

						$item_data['item_stocks_quantity'] = $item_data['item_quantity'] = $item_stocks_quantity;
					break;
					case 'item_action':
						// this case was provided only for !empty($db_item)
						if($is_empty_db_item){
							break;
						}

						$item_action = $db_item['item_cashback_price'] > 0 ? 1 : $item['item_action'];
						if($db_item['item_action'] != $item_action){
							$item_data['item_action'] = $item_action;
						}
					break;
					case 'item_price':
						// this case was provided only for empty($db_item)
						if(!$is_empty_db_item){
							break;
						}
						if($item['item_price'] <= 0){
							$response['bad_items_price'][] = $item['item_prog_id'];
						}

						$item_data['item_price'] = $item['item_price'];
					break;
					default:
						$item_data[$import_column] = $item[$import_column]; 
					break;
				}
			}

			$item_url_variants = array_filter(array($db_item['old_url'], $db_item['item_url'], $this->slug->create_slug(cut_str($item['item_title'], 200) . '-' . $item['item_prog_id'])));
			$item_data['item_url'] = $item_data['old_url'] = array_shift($item_url_variants);
			$item_data['item_import_update'] = $datetime_of_update;

			if($is_empty_db_item){
				$item_data['item_created'] = $datetime_of_update;
				$this->items->handler_insert($item_data);
			}else{
				$this->items->handler_update($db_item['id_item'], $item_data);
			}
		}

		$this->items->handler_update_actionals();
		$this->items->handler_update_newest();
		$this->items->handler_update_display_price();

		foreach ($categories as $category) {
			$categories_list = array($category['category_id']);
			if(!empty($category['category_children'])){
				$childrens = explode(',', $category['category_children']);
				$categories_list = array_merge($categories_list,$childrens);
			}

			$counters_categories = array(
				'category_items_count' => $this->items->handler_get_count(array('id_category' => $categories_list, 'item_visible' => 1))
			);

			$this->categories->handler_update($category['category_id'], $counters_categories);
		}

		$this->import_items->handler_update_all(array('update_date' => date('Y-m-d H:i:s')), array('updated' => false));

		return $response;
	}

	private function import_categories_handler(array $categories){
		$db_categories = array_column($this->categories->handler_get_all(), null, 'category_prog_id');
		$import_categories = array_column($categories, null, 'category_prog_id');
		$update_categories = $insert_categories = array();

		foreach($import_categories as $category_prog_id => $import_category){
			if(isset($db_categories[$category_prog_id])){
				if($db_categories[$category_prog_id]['category_prog_parent_id'] != $import_category['category_prog_parent_id'] || $db_categories[$category_prog_id]['category_title'] != $import_category['category_title']){
					$update_categories[] = array(
						'category_id' => $db_categories[$category_prog_id]['category_id'],
						'category_parent' => (isset($db_categories[$import_category['category_prog_parent_id']])) ? $db_categories[$import_category['category_prog_parent_id']]['category_id'] : 0,
						'category_prog_parent_id' => $import_category['category_prog_parent_id'],
						'category_title' => str_replace('/', ',', $import_category['category_title'])
					);
				}
			} else{
				$insert_categories[] = array(
					'category_prog_id' => $import_category['category_prog_id'],
					'category_prog_parent_id' => $import_category['category_prog_parent_id'],
					'category_title' => str_replace('/', ',', $import_category['category_title'])
				);
			}
		}

		if(!empty($insert_categories)){
			$this->categories->handler_insert_batch($insert_categories);
		}

		if(!empty($update_categories)){
			$this->categories->handler_update_batch($update_categories, 'category_id');
		}

		$db_categories = array_column($this->categories->handler_get_all(), null, 'category_prog_id');
		$update_categories = array();

		$category_slug_config = array(
			'replacement'	=> 'dash',
			'table' 		=> 'categories',
			'field' 		=> 'category_url',
			'title' 		=> 'category_title',
			'id' 			=> 'category_id',
		);

		$this->load->library('slug', $category_slug_config);

		foreach($db_categories as $category){
			$category_url = $this->slug->create_slug($category['category_title'] . '-' . $category['category_id']);
			$category_breadcrumbs = array(
				'category_title'	=> $category['category_title'],
				'category_id' 		=> $category['category_id'],
				'special_url' 		=> $category['category_special_url'],
				'url' 				=> $category_url,
			);
			$update_categories[] = array(
				'category_breadcrumbs'	=> json_encode($category_breadcrumbs),
				'category_parent' 		=> $db_categories[$category['category_prog_parent_id']]['category_id'] ?? 0,
				'category_url' 			=> $category_url,
				'category_id' 			=> $category['category_id'],
			);
		}

		if(!empty($update_categories)){
			$this->categories->handler_update_batch($update_categories, 'category_id');
			$this->categories->handler_actualize_all();

			$db_categories = array_column($this->categories->handler_get_all(), null, 'category_id');
			$update_categories = array();

			foreach($db_categories as $category){
				$category_breadcrumbs = json_decode('[' . $category['category_breadcrumbs'] . ']', true);
				if($category_breadcrumbs[0]['category_id'] != $category['category_id']){
					$category_first_parent = $db_categories[$category_breadcrumbs[0]['category_id']];
					$category_parent_weight = $category_first_parent['category_weight'];
					
					$update_categories[] = array(
						'category_parent_weight' 	=> $category_parent_weight,
						'category_id' 				=> $category['category_id'],
					);
				}
			}

			if(!empty($update_categories)){
				$this->categories->handler_update_batch($update_categories, 'category_id');
			}
		}

		$this->load->model('properties/Properties_model', 'properties');

		$db_categories = $this->categories->handler_get_all();
		$insert_relation_category = array();

		foreach($db_categories as $category){
			$properties_relation = $this->properties->handler_get_category_relation_properties($category['category_id'], array('property_category_to_children' => 1));
			if(empty($properties_relation)){
				continue;
			}
			
			$category_children = array_filter(explode(',', $category['category_children']));
			if(empty($category_children)){
				continue;
			}

			foreach ($properties_relation as $property_relation) {
				foreach ($category_children as $category_child) {
					if(!$this->properties->handler_exist_relation_category($property_relation['id_property'], $category_child)){
						$insert_relation_category[] = array(
							'property_category_to_children' => 1,
							'property_category_weight' 		=> $property_relation['property_category_weight'],
							'id_property' 					=> $property_relation['id_property'],
							'id_category' 					=> $category_child,
						);
					}else{
						$update_relation_category = array(
							'property_category_to_children' => 1,
							'property_category_weight' 		=> $property_relation['property_category_weight'],
						);

						$this->properties->handler_update_relation_category($property_relation['id_property'], $category_child, $update_relation_category);
					}
				}	
			}
		}

		if(!empty($insert_relation_category)){
			$this->properties->handler_insert_relation_category($insert_relation_category);
		}

		$this->import_categories->handler_update_all(array('update_date' => date('Y-m-d H:i:s')), array('updated' => false));

		return array('new_categories' => count($insert_categories), 'update_categories' => count($update_categories));
	}

	private function import_stocks_handler(array $stocks){
		$db_stocks = array_column($this->items->handler_get_stocks_all(), null, 'stock_prog_id');
		$import_stocks = isset($stocks[0]) ? array_column($stocks, null, 'stock_prog_id') : array($stocks['stock_prog_id'] => $stocks);
		$update_stocks = $insert_stocks = array();
		
		foreach($import_stocks as $stock_prog_id => $import_stock){
			if(isset($db_stocks[$stock_prog_id])){
				$update_stocks[] = array(
					'stock_name'	=> $import_stock['stock_name'],
					'id_stock' 		=> $db_stocks[$stock_prog_id]['id_stock'],
				);
			} else{
				$insert_stocks[] = array(
					'stock_prog_id' => $import_stock['stock_prog_id'],
					'stock_name' 	=> $import_stock['stock_name']
				);
			}
		}

		if(!empty($insert_stocks)){
			$this->items->handler_insert_stocks_batch($insert_stocks);
		}

		if(!empty($update_stocks)){
			$this->items->handler_update_stocks_batch($update_stocks, 'id_stock');
		}

		$this->import_stocks->handler_update_all(array('update_date' => date('Y-m-d H:i:s')), array('updated' => false));

		return array('update_stocks' => count($update_stocks), 'new_stocks' => count($insert_stocks));
	}

	private function import_suppliers_handler(array $suppliers){
		$db_suppliers = array_column($this->items->handler_get_suppliers_all(), null, 'supplier_prog_id');
		$import_suppliers = isset($suppliers[0]) ? array_column($suppliers, null, 'supplier_prog_id') : array($suppliers['supplier_prog_id'] => $suppliers);
		$update_suppliers = $insert_suppliers = array();

		foreach($import_suppliers as $supplier_prog_id => $import_supplier){
			if(isset($db_suppliers[$supplier_prog_id])){
				$update_suppliers[] = array(
					'supplier_name' => $import_supplier['supplier_name'],
					'id_supplier' 	=> $db_suppliers[$supplier_prog_id]['id_supplier'],
				);
			} else{
				$insert_suppliers[] = array(
					'supplier_prog_id' 	=> $import_supplier['supplier_prog_id'],
					'supplier_name' 	=> $import_supplier['supplier_name']
				);
			}
		}

		if(!empty($insert_suppliers)){
			$this->items->handler_insert_suppliers_batch($insert_suppliers);
		}

		if(!empty($update_suppliers)){
			$this->items->handler_update_suppliers_batch($update_suppliers, 'id_supplier');
		}

		$this->import_suppliers->handler_update_all(array('update_date' => date('Y-m-d H:i:s')), array('updated' => false));

		return array('new_suppliers' => count($insert_suppliers), 'update_suppliers' => count($update_suppliers));
	}

	public function sanitize_import_tables(){
		$token = $this->uri->segment(3);
        if($token !== 'cronaction'){
            return;
		}

		$this->load->model('import/Import_items_model', 'import_items');
		$this->load->model('import/Import_categories_model', 'import_categories');
		$this->load->model('import/Import_suppliers_model', 'import_suppliers');
		$this->load->model('import/Import_stocks_model', 'import_stocks');
		
		$count_days = (int) $this->data['settings']['import_data_period']['setting_value'];
		$date = (new DateTime("-{$count_days} days"))->format('Y-m-d');
		
		$this->import_items->handler_delete_all(array('created_before_date' => $date));
		$this->import_categories->handler_delete_all(array('created_before_date' => $date));
		$this->import_suppliers->handler_delete_all(array('created_before_date' => $date));
		$this->import_stocks->handler_delete_all(array('created_before_date' => $date));
	}

	function delete_items(){
        $token = $this->uri->segment(3);
        if($token !== 'cronaction'){
            return;
        }

		ini_set('max_execution_time', 0);

		$this->load->model("items/Items_model", "items");

		$params = array(
			'item_import_update_to' => date_plus((-1)*$this->data['settings']['items_hide_days']['setting_value'])
		);
		$items = $this->items->handler_get_items_to_delete($params);
		$items_list = array();
		$items_prog_id = array();
		if(!empty($items)){
			foreach ($items as $item) {
				$this->items->handler_delete_deleted($item['id_item']);
				$items_list[] = $item['id_item'];
				$item['item_deleted'] = 1;
				$item['item_visible'] = 0;
				$this->items->handler_insert_deleted($item);
			}
			
			if(!empty($items_list)){
				$this->items->handler_delete($items_list);
			}
		}
		$log_text = array(
			count($items_list)." товаров перенесенны в временую таблицу."
		);

		if(!empty($items_prog_id)){
			$log_text[] = '<strong>Товары: </strong>'.implode(', ', $items_prog_id);
		}

		$log_name = 'Перенос товаров в временую таблицу по крону';
		$this->_notify_admin('delete_items', array('logs' => $log_text, 'title' => 'Результат операций переноса товаров в временую таблицу:'));
		$this->crons->handler_insert_log(array('log_name' => $log_name, 'log_text' => implode('<br>', $log_text)));
	}

	private function _notify_admin($type = 'import_items', $email_data = array()){
		switch ($type) {
			case 'delete_items':
			case 'import_items':
				$email_data['email_content'] = 'cron_log_tpl';	
			break;
		}

		Modules::run('email/send', array(
			'to' => $this->data['settings']['admin_email']['setting_value'],
			'subject' => 'Atehno.md Cron',
			'email_data' => $email_data
		));
	}

	function import_items(){
        $token = $this->uri->segment(3);
        if($token !== 'cronaction'){
            return;
        }

		ini_set('max_execution_time', 0);		
		
		$this->load->model("categories/Categories_model", "categories");
		$this->load->model("items/Items_model", "items");

		$log_name = 'Импорт товаров по крону';
        $this->crons->handler_insert_log(array('log_name' => $log_name, 'log_text' => 'Запуск операций по импорту товаров.'));

		$import = $this->crons->handler_import_get();
		if(empty($import)){
			$this->_notify_admin('import_items', array('logs' => array('Вариант импорта не существует.')));
			$this->crons->handler_insert_log(array('log_name' => $log_name, 'log_text' => 'Вариант импорта не существует.', 'log_type' => 'danger'));
            return;
		}

		if(!$import['import_active']){
			$this->_notify_admin('import_items', array('logs' => array('Вариант импорта не активен.')));
			$this->crons->handler_insert_log(array('log_name' => $log_name, 'log_text' => 'Вариант импорта не активен.', 'log_type' => 'danger'));
            return;
		}

		if(!$import['import_cron']){
			$this->_notify_admin('import_items', array('logs' => array('Вариант импорта не для cron-а.')));
			$this->crons->handler_insert_log(array('log_name' => $log_name, 'log_text' => 'Вариант импорта не для cron-а.', 'log_type' => 'danger'));
            return;
		}

        $import_columns = json_decode($import['import_columns'], true);
        
        $path = '1c_export';
        create_dir($path);

        if (!file_exists($path.'/import.xml')) {
			$this->_notify_admin('import_items', array('logs' => array('Не удалось найти файл import.xml')));
			$this->crons->handler_insert_log(array('log_name' => $log_name, 'log_text' => 'Не удалось найти файл import.xml', 'log_type' => 'danger'));
            return;
        }

		$import_report = array(
			'new_categories' => 0,
			'update_categories' => 0,
			'new_stocks' => 0,
			'update_stocks' => 0,
			'new_items' => 0,
			'updated_items' => 0,
			'bad_items_price' => array(),
			'restored_items' => array()
		);
		
        $this->load->library("Xml2Array", "xml2array");
        $file_name = date('d-m-Y_H-i-s').'_import.xml';
        if (!@copy($path.'/import.xml', $path.'/'.$file_name)) {
			$this->_notify_admin('import_items', array('logs' => array("Не удалось скопировать import.xml с именем {$file_name}")));
			$this->crons->handler_insert_log(array('log_name' => $log_name, 'log_text' => "Не удалось скопировать import.xml с именем {$file_name}", 'log_type' => 'danger'));
            return;
        }
		$xml = file_get_contents($path.'/'.$file_name);
        $import_records = $this->xml2array->createArray($xml);

		// BEFORE IMPORT
		$befre_update = array(
			'item_suppliers' => '',
			'item_suppliers_available' => 0,
			'item_stocks' => '',
			'item_stocks_quantity' => 0,
			'item_quantity' => 0
		);
		switch ($import['import_before_action']) {
			case 'nothing':	
			default:
			break;			
			case 'reset_quantity':
				$befre_update['item_quantity'] = 0;
			break;			
			case 'delete_all_items':
				$this->_notify_admin('import_items', array('logs' => array('Но могу удалить товары перед импортом.')));
				$this->crons->handler_insert_log(array('log_name' => $log_name, 'log_text' => "Но могу удалить товары перед импортом.", 'log_type' => 'danger'));
				return;
			break;			
			case 'hide_all':
				$befre_update['item_visible'] = 0;
			break;
		}
		$this->items->handler_update_all($befre_update);

		if(!empty($import_records['imports']['stocks']) && isset($import_columns['stocks'])){
			$stocks = arrayByKey($this->items->handler_get_stocks_all(), 'stock_prog_id');
			$update_stocks = array();
			$insert_stocks = array();
			if(isset($import_records['imports']['stocks'][0])){
				$xml_stocks = arrayByKey($import_records['imports']['stocks'], 'stock_prog_id');
				foreach($xml_stocks as $stock_prog_id => $xml_stock){
					if(isset($stocks[$stock_prog_id])){
						$update_stocks[] = array(
							'id_stock' => $stocks[$stock_prog_id]['id_stock'],
							'stock_name' => $xml_stock['stock_name']
						);
					} else{
						$insert_stocks[] = array(
							'stock_prog_id' => $xml_stock['stock_prog_id'],
							'stock_name' => $xml_stock['stock_name']
						);
					}
				}
			} else{
				$xml_stock = $import_records['imports']['stocks'];
				if(isset($stocks[$xml_stock['stock_prog_id']])){
					$update_stocks[] = array(
						'id_stock' => $stocks[$xml_stock['stock_prog_id']]['id_stock'],
						'stock_name' => $xml_stock['stock_name']
					);
				} else{
					$insert_stocks[] = array(
						'stock_prog_id' => $xml_stock['stock_prog_id'],
						'stock_name' => $xml_stock['stock_name']
					);
				}
			}
			$import_report['new_stocks'] = count($insert_stocks);
			$import_report['update_stocks'] = count($update_stocks);

			if(!empty($insert_stocks)){
				$this->items->handler_insert_stocks_batch($insert_stocks);
			}

			if(!empty($update_stocks)){
				$this->items->handler_update_stocks_batch($update_stocks, 'id_stock');
			}
		}

		if(!empty($import_records['imports']['suppliers']) && isset($import_columns['suppliers'])){
			$suppliers = arrayByKey($this->items->handler_get_suppliers_all(), 'supplier_prog_id');
			$update_suppliers = array();
			$insert_suppliers = array();

			if(isset($import_records['imports']['suppliers'][0])){
				$xml_suppliers = arrayByKey($import_records['imports']['suppliers'], 'supplier_prog_id');
				foreach($xml_suppliers as $supplier_prog_id => $xml_supplier){
					if(isset($suppliers[$supplier_prog_id])){
						$update_suppliers[] = array(
							'id_supplier' => $suppliers[$supplier_prog_id]['id_supplier'],
							'supplier_name' => $xml_supplier['supplier_name']
						);
					} else{
						$insert_suppliers[] = array(
							'supplier_prog_id' => $xml_supplier['supplier_prog_id'],
							'supplier_name' => $xml_supplier['supplier_name']
						);
					}
				}
			} else{
				$xml_supplier = $import_records['imports']['suppliers'];
				if(isset($suppliers[$xml_supplier['supplier_prog_id']])){
					$update_suppliers[] = array(
						'id_supplier' => $suppliers[$xml_supplier['supplier_prog_id']]['id_supplier'],
						'supplier_name' => $xml_supplier['supplier_name']
					);
				} else{
					$insert_suppliers[] = array(
						'supplier_prog_id' => $xml_supplier['supplier_prog_id'],
						'supplier_name' => $xml_supplier['supplier_name']
					);
				}
			}
						
			$import_report['new_suppliers'] = count($insert_suppliers);
			$import_report['update_suppliers'] = count($update_suppliers);

			if(!empty($insert_suppliers)){
				$this->items->handler_insert_suppliers_batch($insert_suppliers);
			}

			if(!empty($update_suppliers)){
				$this->items->handler_update_suppliers_batch($update_suppliers, 'id_supplier');
			}
		}
		
		if(!empty($import_records['imports']['categories']) && isset($import_columns['categories'])){
			$xml_categories = arrayByKey($import_records['imports']['categories'], 'category_prog_id');
			$categories = arrayByKey($this->categories->handler_get_all(), 'category_prog_id');
			$update_categories = array();
			$insert_categories = array();
			foreach($xml_categories as $category_prog_id => $xml_category){
				if(isset($categories[$category_prog_id])){
					if($categories[$category_prog_id]['category_prog_parent_id'] != $xml_category['category_prog_parent_id'] || $categories[$category_prog_id]['category_title'] != $xml_category['category_title']){
						$update_categories[] = array(
							'category_id' => $categories[$category_prog_id]['category_id'],
							'category_parent' => (isset($categories[$xml_category['category_prog_parent_id']]))?$categories[$xml_category['category_prog_parent_id']]['category_id']:0,
							'category_prog_parent_id' => $xml_category['category_prog_parent_id'],
							'category_title' => str_replace('/', ',', $xml_category['category_title'])
						);
					}
				} else{
					$insert_categories[] = array(
						'category_prog_id' => $xml_category['category_prog_id'],
						'category_prog_parent_id' => $xml_category['category_prog_parent_id'],
						'category_title' => str_replace('/', ',', $xml_category['category_title'])
					);
				}
			}
			$new_categories = count($insert_categories);
			$updated_categories = count($update_categories);

			if(!empty($insert_categories)){
				$this->categories->handler_insert_batch($insert_categories);
			}

			if(!empty($update_categories)){
				$this->categories->handler_update_batch($update_categories, 'category_id');
			}

			$import_report['new_categories'] = $new_categories;
			$import_report['update_categories'] = $updated_categories;
			$categories = arrayByKey($this->categories->handler_get_all(), 'category_prog_id');
			$update_categories = array();
			$config = array(
				'table' => 'categories',
				'id' => 'category_id',
				'field' => 'category_url',
				'title' => 'category_title',
				'replacement' => 'dash'
			);
			$this->load->library('slug', $config);
			foreach($categories as $record){
				$category_url = $this->slug->create_slug($record['category_title'].'-'.$record['category_id']);
				$category_breadcrumbs = array(
					'category_id' => $record['category_id'],
					'category_title' => $record['category_title'],
					'url' => $category_url,
					'special_url' => $record['category_special_url']
				);
				$update_categories[] = array(
					'category_id' => $record['category_id'],
					'category_parent' => (isset($categories[$record['category_prog_parent_id']]))?$categories[$record['category_prog_parent_id']]['category_id']:0,
					'category_breadcrumbs' => json_encode($category_breadcrumbs),
					'category_url' => $category_url
				);
			}
			
			if(!empty($update_categories)){
				$this->categories->handler_update_batch($update_categories, 'category_id');
				$this->categories->handler_actualize_all();
				$categories = arrayByKey($this->categories->handler_get_all(), 'category_id');
				$update_categories = array();
				foreach($categories as $record){
					$category_breadcrumbs = json_decode('['.$record['category_breadcrumbs'].']', true);
					if($category_breadcrumbs[0]['category_id'] != $record['category_id']){
						$category_first_parent = $categories[$category_breadcrumbs[0]['category_id']];
						$category_parent_weight = $category_first_parent['category_weight'];
						$update_categories[] = array(
							'category_id' => $record['category_id'],
							'category_parent_weight' => $category_parent_weight
						);
					}
				}
				$this->categories->handler_update_batch($update_categories, 'category_id');
			}

			$categories = $this->categories->handler_get_all();
			$this->load->model("properties/Properties_model", "properties");
			$insert_relation_category = array();
			foreach ($categories as $category_info) {
				$properties_relation = $this->properties->handler_get_category_relation_properties($category_info['category_id'], array('property_category_to_children' => 1));
				
				if(empty($properties_relation)){
					continue;
				}

				$category_children = array_filter(explode(',', $category_info['category_children']));
				if(empty($category_children)){
					continue;
				}

				foreach ($properties_relation as $property_relation) {
					foreach ($category_children as $category_child) {
						if(!$this->properties->handler_exist_relation_category($property_relation['id_property'], $category_child)){
							$insert_relation_category[] = array(
								'id_property' => $property_relation['id_property'],
								'property_category_weight' => $property_relation['property_category_weight'],
								'id_category' => $category_child,
								'property_category_to_children' => 1
							);
						} else{
							$update_relation_category = array(
								'property_category_weight' => $property_relation['property_category_weight'],
								'property_category_to_children' => 1
							);
							$this->properties->handler_update_relation_category($property_relation['id_property'], $category_child, $update_relation_category);
						}
					}	
				}

			}
			
			if(!empty($insert_relation_category)){
				$this->properties->handler_insert_relation_category($insert_relation_category);
			}
		}

		$item_import_update = date('Y-m-d H:i:s');
		if(!empty($import_records['imports']['items']) && isset($import_columns['items'])){
			$categories = arrayByKey($this->categories->handler_get_all(array('category_items_count' => false)), 'category_prog_id');

			$new_items = 0;
			$updated_items = 0;
			$config = array(
				'table' => 'items',
				'id' => 'item_prog_id',
				'field' => 'item_url',
				'title' => 'item_title',
				'replacement' => 'dash'
			);
			$this->load->library('slug', $config);
			foreach($import_records['imports']['items'] as $key => $record){
				$bd_item = $this->items->handler_get_by_prog_id($record['item_prog_id']);

				if(empty($bd_item)){
					$bd_item = $this->items->handler_get_by_prog_id_deleted($record['item_prog_id']);
					if(!empty($bd_item)){
						$bd_item['item_deleted'] = 0;
						$bd_item['item_visible'] = 0;
						$bd_item['item_suppliers'] = '';
						$bd_item['item_suppliers_available'] = 0;
						$bd_item['item_stocks'] = '';
						$bd_item['item_stocks_quantity'] = 0;
						$bd_item['item_quantity'] = 0;
						$bd_item['item_cashback_price'] = 0;
						$bd_item['item_zero_credit'] = 0;
						$bd_item['item_zero_credit_months'] = 0;

						$this->items->handler_insert($bd_item);
						$this->items->handler_delete_deleted($bd_item['id_item']);
						$import_report['restored_items'][] = $record['item_prog_id'];
					}
				}

				if(!empty($bd_item)){
					$update_data = array();
					foreach($import_columns['items'] as $column){
						switch ($column) {
							case 'category_prog_id':
								$update_data['id_category'] = (isset($categories[$record['category_prog_id']]))?$categories[$record['category_prog_id']]['category_id']:0;
							break;
							case 'suppliers':
								if(!empty($record['suppliers'])){
									if(isset($record['suppliers'][0])){
										$suppliers = $record['suppliers']; 
										usort($suppliers, function($a, $b){
											if ($a['to'] == $b['to']) {
												return 0;
											}

											return ($a['to'] < $b['to']) ? -1 : 1;
										});
										$update_data['item_suppliers'] = json_encode($suppliers);
										$update_data['item_suppliers_available'] = count($suppliers);
									} else{
										$update_data['item_suppliers'] = json_encode(array($record['suppliers']));
										$update_data['item_suppliers_available'] = 1;
									}
								}
							break;
							case 'stocks':
								$item_stocks_quantity = 0;
								if(!empty($record['stocks'])){
									if(isset($record['stocks'][0])){
										$update_data['item_stocks'] = json_encode($record['stocks']);
										foreach ($record['stocks'] as $stock) {
											$item_stocks_quantity += (int) $stock['quantity'];
										}
									} else{
										$update_data['item_stocks'] = json_encode(array($record['stocks']));
										$item_stocks_quantity += (int) $record['stocks']['quantity'];
									}
								}
								$update_data['item_stocks_quantity'] = $item_stocks_quantity;
							break;
							case 'item_action':
								$item_action = $record['item_action'];
								if($bd_item['item_cashback_price'] > 0){
									$item_action = 1;
								}

								if($bd_item['item_action'] != $item_action){
									$update_data['item_action'] = $item_action;
								}
							break;
							case 'item_zero_credit_months':
								$update_data[$column] = $record[$column];
								$update_data['item_zero_credit'] = $record[$column] > 0 ? 1 : 0;
							break;
							default:
								if($bd_item[$column] != $record[$column]){
									$update_data[$column] = $record[$column];
								}
							break;
						}
					}
					
					$update_data['item_url'] = $update_data['old_url'] = strForURL(cut_str($record['item_title'], 200)) . '-' . $record['item_prog_id'];

					if (isset($item_stocks_quantity)) {
						$update_data['item_quantity'] = $item_stocks_quantity;
					}

					$updated_items++;
					$update_data['item_import_update'] = $item_import_update;
					
					$this->items->handler_update($bd_item['id_item'], $update_data);
				} else{
					$new_items++;
					$insert_item = array();
					foreach($import_columns['items'] as $column){
						switch ($column) {
							case 'category_prog_id':
								$insert_item['id_category'] = (isset($categories[$record['category_prog_id']]))?$categories[$record['category_prog_id']]['category_id']:0;
							break;
							case 'suppliers':
								if(!empty($record['suppliers'])){
									if(isset($record['suppliers'][0])){
										$suppliers = $record['suppliers']; 
										usort($suppliers, function($a, $b){
											if ($a['to'] == $b['to']) {
												return 0;
											}

											return ($a['to'] < $b['to']) ? -1 : 1;
										});
										$insert_item['item_suppliers'] = json_encode($suppliers);
										$insert_item['item_suppliers_available'] = count($suppliers);
									} else{
										$insert_item['item_suppliers'] = json_encode(array($record['suppliers']));
										$insert_item['item_suppliers_available'] = 1;
									}
								}
							break;
							case 'stocks':
								$item_stocks_quantity = 0;
								if(!empty($record['stocks'])){
									if(isset($record['stocks'][0])){
										$insert_item['item_stocks'] = json_encode($record['stocks']);
										foreach ($record['stocks'] as $stock) {
											$item_stocks_quantity += (int) $stock['quantity'];
										}
									} else{
										$insert_item['item_stocks'] = json_encode(array($record['stocks']));
										$item_stocks_quantity += (int) $record['stocks']['quantity'];
									}
								}

								$insert_item['item_stocks_quantity'] = $item_stocks_quantity;
							break;
							case 'item_price':
								if($record[$column] <= 0){
									$import_report['bad_items_price'][] = $record['item_prog_id'];
								}

								$insert_item[$column] = $record[$column];
							break;
							case 'item_zero_credit_months':
								$insert_item[$column] = $record[$column];
								$insert_item['item_zero_credit'] = $record[$column] > 0 ? 1 : 0;
							break;
							default:
								$insert_item[$column] = $record[$column];
							break;
						}
					}

					if (isset($item_stocks_quantity)) {
						$insert_item['item_quantity'] = $item_stocks_quantity;
					}

					$insert_item['item_import_update'] = $item_import_update;
					$insert_item['item_created'] = $item_import_update;
					$insert_item['item_url'] = $insert_item['old_url'] = strForURL(cut_str($record['item_title'], 200)) . '-' . $record['item_prog_id'];
					$this->items->handler_insert($insert_item);
				}
			}

			$this->items->handler_update_actionals();
			$this->items->handler_update_newest();
			$this->items->handler_update_display_price();

			$import_report['new_items'] = $new_items;
			$import_report['updated_items'] = $updated_items;

			foreach ($categories as $category) {
				$categories_list = array($category['category_id']);
				if(!empty($category['category_children'])){
					$childrens = explode(',', $category['category_children']);
					$categories_list = array_merge($categories_list,$childrens);
				}
	
				$counters_categories = array(
					'category_items_count' => $this->items->handler_get_count(array('id_category' => $categories_list, 'item_visible' => 1))
				);
				$this->categories->handler_update($category['category_id'], $counters_categories);
			}
        }
		
		$log_text = array(
			"Добавленно категорий: {$import_report['new_categories']}",
			"Обновленно категорий: {$import_report['update_categories']}",
			"Добавленно товаров: {$import_report['new_items']}",
			"Обновленно товаров: {$import_report['updated_items']}",
			'Операция по импорту товаров прошла успешно.',
		);

		if(!empty($import_report['restored_items'])){
			$log_text[] = "Восстановленные товары: " . implode(', ', $import_report['restored_items']);
		}
		
		if(!empty($import_report['bad_items_price'])){
			$log_text[] = "Товары без цены: " . implode(', ', $import_report['bad_items_price']);
		}

		$this->_notify_admin('import_items', array('logs' => $log_text, 'title' => 'Результат операций импорта товаров по крону:'));
		$this->crons->handler_insert_log(array('log_name' => $log_name, 'log_text' => implode('<br>', $log_text), 'log_type' => !empty($import_report['bad_items_price']) ? 'warning' : 'success'));

		$this->db->cache_delete_all('catalog', '*');
        return;
	}
}