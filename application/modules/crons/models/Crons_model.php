<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Crons_model extends CI_Model{
	var $import = "import";
	var $cron_logs = "cron_logs";

	function __construct(){
		parent::__construct();
	}

	function handler_import_get(){
		$this->db->where('import_active', 1);
		$this->db->where('import_cron', 1);
		$this->db->limit(1);
		return $this->db->get($this->import)->row_array();
	}

	function handler_insert_log($data= array()){
		if(empty($data)){
			return false;
		}

		return $this->db->insert($this->cron_logs, $data);
	}

	function handler_get_logs_all($conditions = array()){
        $order_by = " log_date DESC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		if(isset($log_date_from)){
			$this->db->where("DATE(log_date) >= DATE('{$log_date_from}')");
        }

        if(isset($log_date_to)){
			$this->db->where("DATE(log_date) <= DATE('{$log_date_to}')");
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->cron_logs)->result_array();
	}

	function handler_get_logs_count($conditions = array()){
        extract($conditions);

		if(isset($log_date_from)){
			$this->db->where("DATE(log_date) >= DATE('{$log_date_from}')");
        }

        if(isset($log_date_to)){
			$this->db->where("DATE(log_date) <= DATE('{$log_date_to}')");
        }

		return $this->db->count_all_results($this->cron_logs);
	}
}
