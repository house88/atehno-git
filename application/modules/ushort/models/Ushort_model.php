<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ushort_model extends CI_Model{
	var $ushortTable = "ushort";
	function __construct(){
		parent::__construct();
	}

	function handlerInsert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->ushortTable, $data);
		return $this->db->insert_id();
	}

	function handlerGetKey($key){
		$this->db->where('key', $key);
		return $this->db->get($this->ushortTable)->row_array();
	}
}
