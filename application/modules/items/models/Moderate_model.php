<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Moderate_model extends CI_Model{
	var $users_table = "users";
	var $items_table = "items";
	var $moderation_table = "moderation_items";
	var $fill_table = "moderation_items_completed";
	var $issue_table = "moderation_items_issue";
	var $done_table = "moderation_items_done";

	function __construct(){
		parent::__construct();
	}

	function handlerGetStates(int $id_item){
		$record = $this->db->where("id_item", $id_item)
						   ->get($this->moderation_table)
						   ->row_array();
		if(empty($record)){
			$this->db->insert($this->moderation_table, [
				'id_item' => $id_item
			]);

			$record = $this->db->where("id_item", $id_item)
							   ->get($this->moderation_table)
							   ->row_array();
		}

		return $record;
	}

	function handlerUpdateStates(int $id_item, array $data){
		if(empty($data)){
			return;
		}

		$record = $this->db->where("id_item", $id_item)
						   ->get($this->moderation_table)
						   ->row_array();
		if(empty($record)){
			$this->db->insert($this->moderation_table, [
				'id_item' => $id_item
			]);
		}

		$this->db->where('id_item', $id_item)
				 ->update($this->moderation_table, $data);
	}

	//region FILL
	function handlerInsertFill($insert = array()){
		if(empty($insert)){
			return false;
		}

		$this->db->insert($this->fill_table, $insert);
		return $this->db->insert_id();
	}

	function handlerDeleteLastFill(int $id_item){
		$record = $this->db->where('id_item', $id_item)
						   ->order_by('id', 'DESC')
						   ->limit(1)
						   ->get($this->fill_table)
						   ->row_array();
		
		if(empty($record)){
			return true;
		}

		return $this->db->where('id', $record['id'])
				 		->delete($this->fill_table);
	}

	function _getFillParams($params = []){
		extract($params);

        if(isset($id_manager)){
			$this->db->where_in('id_manager', $id_manager);
		}

        if(isset($id_category)){
			$this->db->where_in('id_category', $id_category);
		}

        if(isset($date_from)){
			$this->db->where("DATE(filled_on) >= '{$date_from}'");
		}

        if(isset($date_to)){
			$this->db->where("DATE(filled_on) <= '{$date_to}'");
		}

        if(isset($is_filled)){
			$this->db->where('is_filled', $is_filled);
		}

        if(isset($is_moderated)){
			$this->db->where('is_moderated', $is_moderated);
		}

        if(isset($has_issue)){
			$this->db->where('has_issue', $has_issue);
		}
	}

	function handlerGetFill($params = []){
		$this->_getFillParams($params);
				
		$records = $this->db->get($this->moderation_table)->result_array();
		// dump([$this->db->last_query(), $records]);
		return $records;
	}

	function handlerGetFillUsers(int $id_item){
		$records = $this->db->select("f.*, u.user_nicename")
						->from("{$this->fill_table} f")
						->join("{$this->users_table} u", "f.id_manager = u.id")
						->where('f.id_item', $id_item)
						->order_by('f.completed_on', 'desc')
						->get()
						->result_array();

		return $records;
	}
	//endregion FILL

	//region ISSUE
	function handlerInsertIssue($insert = array()){
		if(empty($insert)){
			return false;
		}

		$this->db->insert($this->issue_table, $insert);
		return $this->db->insert_id();
	}

	function handlerDeleteLastIssue(int $id_item){
		$record = $this->db->where('id_item', $id_item)
						   ->order_by('id', 'DESC')
						   ->limit(1)
						   ->get($this->issue_table)
						   ->row_array();
		
		if(empty($record)){
			return true;
		}

		return $this->db->where('id', $record['id'])
				 		->delete($this->issue_table);
	}

	function _getIssueParams($params = []){
		$checkManagerColumn = 'id_manager';
		$checkDateColumn = 'filled_on';

		extract($params);

        if(isset($id_manager)){
			$this->db->where_in("`MODERATION`.`{$checkManagerColumn}`", $id_manager);
		}

        if(isset($id_category)){
			$this->db->where_in('`MODERATION`.`id_category`', $id_category);
		}

        if(isset($date_from)){
			$this->db->where("DATE(`MODERATION`.`{$checkDateColumn}`) >= '{$date_from}'");
		}

        if(isset($date_to)){
			$this->db->where("DATE(`MODERATION`.`{$checkDateColumn}`) <= '{$date_to}'");
		}

        if(isset($is_filled)){
			$this->db->where('`MODERATION`.`is_filled`', $is_filled);
		}

        if(isset($is_moderated)){
			$this->db->where('`MODERATION`.`is_moderated`', $is_moderated);
		}

        if(isset($has_issue)){
			$this->db->where('`MODERATION`.`has_issue`', $has_issue);
		}
	}

	function handlerGetIssue($params = []){
		$this->db->select("`MODERATION`.*, `PRODUCTS`.`item_title`, `PRODUCTS`.`item_url`");
		$this->db->from("{$this->moderation_table} `MODERATION`");
		$this->db->join("$this->items_table `PRODUCTS`", "`MODERATION`.`id_item` = `PRODUCTS`.`id_item`", "left");
		$this->_getIssueParams($params);
				
		$records = $this->db->get()->result_array();
		// return [$this->db->last_query(), $records];
		return $records;
	}

	function handlerGetIssuerUsers(int $id_item){
		return $this->db->select("b.*, u.user_nicename")
						->from("{$this->issue_table} b")
						->join("{$this->users_table} u", "b.id_issuer = u.id")
						->where('id_item', $id_item)
						->order_by('issue_on', 'desc')
						->get()
						->result_array();
	}
	//endregion ISSUE

	//region MODERATE
	function handlerInsertDone($insert = array()){
		if(empty($insert)){
			return false;
		}

		$this->db->insert($this->done_table, $insert);
		return $this->db->insert_id();
	}

	function handlerDeleteLastDone(int $id_item){
		$record = $this->db->where('id_item', $id_item)
						   ->order_by('id', 'DESC')
						   ->limit(1)
						   ->get($this->done_table)
						   ->row_array();
		
		if(empty($record)){
			return true;
		}

		return $this->db->where('id', $record['id'])
				 		->delete($this->done_table);
	}

	function _getDoneParams($params = []){
		extract($params);

        if(isset($id_moderator)){
			$this->db->where_in('id_moderator', $id_moderator);
		}

        if(isset($id_category)){
			$this->db->where_in('id_category', $id_category);
		}

        if(isset($date_from)){
			$this->db->where("DATE(moderated_on) >= '{$date_from}'");
		}

        if(isset($date_to)){
			$this->db->where("DATE(moderated_on) <= '{$date_to}'");
		}

        if(isset($is_filled)){
			$this->db->where('is_filled', $is_filled);
		}

        if(isset($is_moderated)){
			$this->db->where('is_moderated', $is_moderated);
		}

        if(isset($has_issue)){
			$this->db->where('has_issue', $has_issue);
		}
	}

	function handlerGetDone($params = []){
		$this->_getDoneParams($params);
				
		$records = $this->db->get($this->moderation_table)->result_array();
		// dump([$this->db->last_query(), $records]);
		return $records;
	}

	function handlerGetModerateUsers(int $id_item){
		return $this->db->select("d.*, u.user_nicename")
						->from("{$this->done_table} d")
						->join("{$this->users_table} u", "d.id_moderator = u.id")
						->where('id_item', $id_item)
						->order_by('moderated_on', 'desc')
						->get()
						->result_array();
	}
	//endregion MODERATE
}
