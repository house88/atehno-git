<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Items_model extends CI_Model{
	var $items = "items";
	var $moderation_table = "moderation_items";
	var $promo_items = "promo_items";
	var $items_deleted = "items_deleted";
	var $stocks = "stocks";
	var $suppliers = "suppliers";
	var $categories = "categories";
	var $brands = "brands";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->items, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_item, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_item', $id_item);
		$this->db->update($this->items, $data);
	}

	function handler_update_batch($data = array(), $key = 'id_item'){
		if (empty($data)) {
			return false;
		}

		$this->db->update_batch($this->items, $data, $key);
	}

	function handler_update_all($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->update($this->items, $data);
	}

	function handler_delete($id_item){
		$this->db->where_in('id_item', $id_item);
		$this->db->delete($this->items);
	}

	function handler_get($id_item){
		$this->db->where('id_item', $id_item);
		return $this->db->get($this->items)->row_array();
	}

	function handler_get_by_prog_id($item_prog_id){
		$this->db->where('item_prog_id', $item_prog_id);
		return $this->db->get($this->items)->row_array();
	}

	function handler_get_by_prog_id_deleted($item_prog_id){
		$this->db->where('item_prog_id', $item_prog_id);
		return $this->db->get($this->items_deleted)->row_array();
	}

	function handler_exist_by_prog_id($item_prog_id){
		$this->db->where('item_prog_id', $item_prog_id);
		return $this->db->count_all_results($this->items);
	}

	function handler_get_all_without_conditions(){
		return $this->db->get($this->items)->result_array();
	}

	function handler_get_all($conditions = array()){
		$order_by = " item_visible DESC, item_last_update ASC ";
		$coulmns = '*';
		$item_price_column = 'item_price';
		$exclude_bad_price = true;
		$moderation = false;
		
        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}

			if(!empty($multi_order_by)){
				$order_by = 'item_visible DESC, '.implode(',', $multi_order_by);
			}
		}
		
		if (isset($keywords)) {
			$search = get_keywords_combination($keywords);
			if(!empty($search)){
				$order_by .= " , REL_title DESC ";
				$coulmns .= " , IF({$this->items}.item_title LIKE '%".$this->db->escape_like_str($keywords)."%', (MATCH ({$this->items}.item_title) AGAINST ('".$this->db->escape_like_str($search)."') + 100), MATCH ({$this->items}.item_title) AGAINST ('".$this->db->escape_like_str($search)."')) as REL_title ";
			}
		}

		$this->db->select($coulmns);
		$this->db->from($this->items);
		
		if(true === $moderation){
			$this->db->join("{$this->moderation_table} im", "{$this->items}.id_item = im.id_item", 'inner');

			if(isset($completed_from)){
				$this->db->where("DATE(im.filled_on) >= DATE('{$completed_from}')");
			}

			if(isset($completed_to)){
				$this->db->where("DATE(im.filled_on) <= DATE('{$completed_to}')");
			}

			if(isset($issued_from)){
				$this->db->where("DATE(im.issued_on) >= DATE('{$issued_from}')");
			}

			if(isset($issued_to)){
				$this->db->where("DATE(im.issued_on) <= DATE('{$issued_to}')");
			}

			if(isset($moderated_from)){
				$this->db->where("DATE(im.moderated_on) >= DATE('{$moderated_from}')");
			}

			if(isset($moderated_to)){
				$this->db->where("DATE(im.moderated_on) <= DATE('{$moderated_to}')");
			}

			if(isset($is_filled)){
				$this->db->where("im.is_filled", $is_filled);
			}

			if(isset($has_issue)){
				$this->db->where("im.has_issue", $has_issue);
			}

			if(isset($is_moderated)){
				$this->db->where("im.is_moderated", $is_moderated);
			}

			if(isset($id_manager)){
				$this->db->where("im.id_manager", $id_manager);
			}

			if(isset($id_moderator)){
				$this->db->where("im.id_moderator", $id_moderator);
			}

			if(isset($id_issuer)){
				$this->db->where("im.id_issuer", $id_issuer);
			}
		}

		if (isset($keywords)) {			
			$search_in = $this->_get_search_items($keywords);
			if(!empty($search_in)){
				$this->db->where_in("{$this->items}.id_item", $search_in);
			}
		}

        if(isset($items_list)){
			$this->db->where_in($this->items.'.id_item', $items_list);
        }

        if(isset($not_items_list)){
			$this->db->where_not_in($this->items.'.id_item', $not_items_list);
        }

        if(isset($item_price_from)){
			$this->db->where($this->items.".{$item_price_column} >= ", $item_price_from);
        }

        if(isset($item_price_to)){
			$this->db->where($this->items.".{$item_price_column} <= ", $item_price_to);
        }

        if(isset($display_price_from)){
			$this->db->where("
				IF(
					{$this->items}.item_temp_price > 0 AND 
					{$this->items}.item_temp_price < {$this->items}.item_price AND 
					{$this->items}.item_temp_price < {$this->items}.{$item_price_column}, 
					{$this->items}.item_temp_price, 
					IF(
						{$this->items}.item_price < {$this->items}.{$item_price_column}, 
						{$this->items}.item_price, 
						{$this->items}.{$item_price_column}
					)
				) >= 
			", $display_price_from);
        }

        if(isset($display_price_to)){
			$this->db->where("
				IF(
					{$this->items}.item_temp_price > 0 AND 
					{$this->items}.item_temp_price < {$this->items}.item_price AND 
					{$this->items}.item_temp_price < {$this->items}.{$item_price_column}, 
					{$this->items}.item_temp_price, 
					IF(
						{$this->items}.item_price < {$this->items}.{$item_price_column}, 
						{$this->items}.item_price, 
						{$this->items}.{$item_price_column}
					)
				) <= 
			", $display_price_to);
        }

		if($exclude_bad_price){
			$this->db->where("{$this->items}.{$item_price_column} >", 0);
		}

        if(isset($id_category)){
			$this->db->where_in($this->items.'.id_category', $id_category);
        }

        if(isset($id_brand)){
			$this->db->where_in($this->items.'.id_brand', $id_brand);
        }

        if(isset($item_visible)){
			$this->db->where($this->items.'.item_visible', $item_visible);
        }

        if(isset($item_hit)){
			$this->db->where($this->items.'.item_hit', $item_hit);
        }

        if(isset($item_newest)){
			$this->db->where($this->items.'.item_newest', $item_newest);
        }

        if(isset($optional_filters)){
			$_filters = array();
			foreach ($optional_filters as $optional_filter) {
				switch ($optional_filter) {
					case 'actional':
						$_filters[] = " {$this->items}.item_action = 1 ";
					break;
					case 'newest':
						$_filters[] = " {$this->items}.item_newest = 1 ";
					break;
					case 'zerocredit':
						$_filters[] = " {$this->items}.item_zero_credit = 1 ";
					break;
				}
			}

			if (!empty($_filters)) {
				$this->db->where('( '.implode(' OR ', $_filters).' )', null);
			}
        }

        if(isset($item_zero_credit)){
			$this->db->where($this->items.'.item_zero_credit', $item_zero_credit);
        }

        if(isset($item_action)){
			$this->db->where($this->items.'.item_action', $item_action);
        }

        if(isset($item_popular)){
			$this->db->where($this->items.'.item_popular', $item_popular);
        }

        if(isset($item_on_home)){
			$this->db->where($this->items.'.item_on_home', $item_on_home);
        }

        if(isset($item_commented)){
			$this->db->where($this->items.'.item_commented', $item_commented);
        }

        if(isset($item_user_edit)){
			$this->db->where($this->items.'.item_user_edit', $item_user_edit);
        }
		
		if(isset($item_created_from)){
			$this->db->where("DATE($this->items.item_created) >= DATE('{$item_created_from}')");
		}
		
		if(isset($item_created_to)){
			$this->db->where("DATE($this->items.item_created) <= DATE('{$item_created_to}')");
		}

        if(isset($item_last_update_from)){
			$this->db->where("DATE($this->items.item_last_update) >= DATE('{$item_last_update_from}')");
        }

        if(isset($item_last_update_to)){
			$this->db->where("DATE($this->items.item_last_update) <= DATE('{$item_last_update_to}')");
        }

        if(isset($stock_available)){
			$stock_filters = array();			
			foreach ($stock_available as $value) {
				switch ($value) {
					case 'yes':
						$stock_filters[] = " {$this->items}.item_stocks_quantity > 0 ";
					break;
					case 'no':
						$stock_filters[] = " {$this->items}.item_stocks_quantity = 0 AND {$this->items}.item_suppliers_available = 0 ";
					break;
					case 'ordering':
						$stock_filters[] = " {$this->items}.item_stocks_quantity = 0 AND {$this->items}.item_suppliers_available > 0 ";
					break;
				}
			}

			if(!empty($stock_filters)){
				$this->db->where('('.implode(" OR ", $stock_filters).')');
			}
		}

        if(isset($id_supplier)){
			if(!is_array($id_supplier)){
				$id_supplier = explode(',', $id_supplier);
			}

			if(!empty($id_supplier)){
				foreach ($id_supplier as $value) {
					if($value == 'no'){
						$_where_suppliers[] = " {$this->items}.item_suppliers = '' ";
					} else{
						$value = str_replace('"', '', json_encode($value));
						$_where_suppliers[] = " {$this->items}.item_suppliers LIKE '%{$value}%' ";
					}
				}
			}
			
			if(!empty($_where_suppliers)){
				$this->db->where('('.implode(" OR ", $_where_suppliers).')');
			}
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		// return $this->db->last_query();

		return $this->db->get()->result_array();
	}

	function handler_get_all_query($conditions = array()){
		$order_by = " item_visible DESC, item_last_update ASC ";
		$coulmns = '*';
		$where = array();
		$params = array();
		$item_price_column = 'item_price';
		$exclude_bad_price = true;
		
        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			
			if(!empty($multi_order_by)){
				$order_by = 'item_visible DESC, '.implode(',', $multi_order_by);
			}
		}

		if (isset($keywords)) {
			$search = get_keywords_combination($keywords);
			if(!empty($search)){
				$order_by .= " , REL_title DESC ";
				$coulmns .= " , IF({$this->items}.item_title LIKE '%".$this->db->escape_like_str($keywords)."%', (MATCH ({$this->items}.item_title) AGAINST ('".$this->db->escape_like_str($search)."') + 100), MATCH ({$this->items}.item_title) AGAINST ('".$this->db->escape_like_str($search)."')) as REL_title ";
			}
		}

		$sql = "SELECT {$coulmns}
				FROM {$this->items} ";

        if(isset($items_list)){
			if(is_array($items_list)){
				$items_list = implode(',', $items_list);
			}
			$where[] = " {$this->items}.id_item IN ({$items_list})";
        }

        if(isset($not_items_list)){
			if(is_array($not_items_list)){
				$not_items_list = implode(',', $not_items_list);
			}
			$where[] = " {$this->items}.id_item NOT IN ({$not_items_list})";
        }
		
		if (isset($keywords)) {			
			$search_in = $this->_get_search_items($keywords);
			if(!empty($search_in)){
				$this->db->where_in("{$this->items}.id_item", $search_in);
			}
		}

        if(isset($item_price_from)){
			$where[] = " {$this->items}.{$item_price_column} >= ? ";
			$params[] = $item_price_from;
        }

        if(isset($item_price_to)){
			$where[] = " {$this->items}.{$item_price_column} <= ? ";
			$params[] = $item_price_to;
		}
		
		if($exclude_bad_price){
			$where[] = " {$this->items}.{$item_price_column} > ? ";
			$params[] = 0;
		}

        if(isset($id_category)){
			if(is_array($id_category)){
				$id_category = implode(',', $id_category);
			}
			$where[] = " {$this->items}.id_category IN ({$id_category})";
        }

        if(isset($id_brand)){
			if(is_array($id_brand)){
				$id_brand = implode(',', $id_brand);
			}
			$where[] = " {$this->items}.id_brand IN ({$id_brand}) ";
        }

        if(isset($item_visible)){
			$where[] = " {$this->items}.item_visible = ? ";
			$params[] = $item_visible;
        }

        if(isset($item_hit)){
			$where[] = " {$this->items}.item_hit = ? ";
			$params[] = $item_hit;
        }

        if(isset($item_newest)){
			$where[] = " {$this->items}.item_newest = ? ";
			$params[] = $item_newest;
        }

        if(isset($optional_filters)){
			$_filters = array();
			foreach ($optional_filters as $optional_filter) {
				switch ($optional_filter) {
					case 'actional':
						$_filters[] = " {$this->items}.item_action = 1 ";
					break;
					case 'newest':
						$_filters[] = " {$this->items}.item_newest = 1 ";
					break;
					case 'zerocredit':
						$_filters[] = " {$this->items}.item_zero_credit = 1 ";
					break;
				}
			}

			if (!empty($_filters)) {
				$where[] = " ( ".implode(' OR ', $_filters)." ) ";
			}
        }

        if(isset($item_zero_credit)){
			$where[] = " {$this->items}.item_zero_credit = ? ";
			$params[] = $item_zero_credit;
        }

        if(isset($item_action)){
			$where[] = " {$this->items}.item_action = ? ";
			$params[] = $item_action;
        }

        if(isset($item_popular)){
			$where[] = " {$this->items}.item_popular = ? ";
			$params[] = $item_popular;
        }

        if(isset($item_on_home)){
			$where[] = " {$this->items}.item_on_home = ? ";
			$params[] = $item_on_home;
        }

        if(isset($item_commented)){
			$where[] = " {$this->items}.item_commented = ? ";
			$params[] = $item_commented;
        }

        if(isset($item_user_edit)){
			$where[] = " {$this->items}.item_user_edit = ? ";
			$params[] = $item_user_edit;
        }

        if(isset($item_data_start)){
			$where[] = " {$this->items}.item_data_start = ? ";
			$params[] = $item_data_start;
        }

        if(isset($item_data_completed)){
			$where[] = " {$this->items}.item_data_completed = ? ";
			$params[] = $item_data_completed;
        }

        if(isset($item_data_issue)){
			$where[] = " {$this->items}.item_data_moderated_bug = ? ";
			$params[] = $item_data_issue;
        }

        if(isset($item_data_moderated)){
			$where[] = " {$this->items}.item_data_moderated = ? ";
			$params[] = $item_data_moderated;
        }

        if(isset($item_last_update_from)){
			$where[] = " DATE({$this->items}.item_last_update) >= DATE('{$item_last_update_from}') ";
        }

        if(isset($item_last_update_to)){
			$where[] = " DATE({$this->items}.item_last_update) >= DATE('{$item_last_update_to}') ";
        }
		
		if(isset($item_created_from)){
			$where[] = "DATE({$this->items}.item_created) >= DATE('{$item_created_from}')";
		}
		
		if(isset($item_created_to)){
			$where[] = "DATE({$this->items}.item_created) <= DATE('{$item_created_to}')";
		}

        if(isset($item_data_start_date_from)){
			$where[] = " DATE({$this->items}.item_data_start_date) >= DATE('{$item_data_start_date_from}') ";
        }

        if(isset($item_data_start_date_to)){
			$where[] = " DATE({$this->items}.item_data_start_date) >= DATE('{$item_data_start_date_to}') ";
        }

        if(isset($item_data_completed_date_from)){
			$where[] = " DATE({$this->items}.item_data_completed_date) >= DATE('{$item_data_completed_date_from}') ";
        }

        if(isset($item_data_completed_date_to)){
			$where[] = " DATE({$this->items}.item_data_completed_date) >= DATE('{$item_data_completed_date_to}') ";
        }

        if(isset($item_data_moderated_date_from)){
			$where[] = " DATE({$this->items}.item_data_moderated_date) >= DATE('{$item_data_moderated_date_from}') ";
        }

        if(isset($item_data_moderated_date_to)){
			$where[] = " DATE({$this->items}.item_data_moderated_date) >= DATE('{$item_data_moderated_date_to}') ";
		}

        if(isset($stock_available)){
			$stock_filters = array();			
			foreach ($stock_available as $value) {
				switch ($value) {
					case 'yes':
						$stock_filters[] = " {$this->items}.item_stocks_quantity > 0 ";
					break;
					case 'no':
						$stock_filters[] = " {$this->items}.item_stocks_quantity = 0 AND {$this->items}.item_suppliers_available = 0 ";
					break;
					case 'ordering':
						$stock_filters[] = " {$this->items}.item_stocks_quantity = 0 AND {$this->items}.item_suppliers_available > 0 ";
					break;
				}
			}
			if(!empty($stock_filters)){
				$where[] = " ( ".implode(" OR ", $stock_filters)." ) ";
			}
		}

        if(isset($id_supplier)){
			if(!is_array($id_supplier)){
				$id_supplier = explode(',', $id_supplier);
			}

			if(!empty($id_supplier)){
				foreach ($id_supplier as $value) {
					if($value == 'no'){
						$_where_suppliers[] = " {$this->items}.item_suppliers = '' ";
					} else{
						$value = str_replace('"', '', json_encode($value));
						$_where_suppliers[] = " {$this->items}.item_suppliers LIKE '%{$value}%' ";
					}
				}
			}
			
			if(!empty($_where_suppliers)){
				$where[] = '('.implode(" OR ", $_where_suppliers).')';
			}
        }

		if(!empty($where)){
			$sql .= " WHERE " . implode(" AND ", $where);
		}

		if(isset($group_by)){
			$sql .= " GROUP BY {$group_by} ";
		}

		$sql .= " ORDER BY {$order_by} ";

		if(isset($limit, $start)){
			$sql .= " LIMIT {$start},{$limit} ";
		}

		$records = $this->db->query($sql, $params)->result_array();
		return $records;
	}

	function handler_get_count($conditions = array()){
		$this->db->reset_query();

		$item_price_column = 'item_price';
		$exclude_bad_price = true;
		$moderation = false;

		extract($conditions);
		
		if(true === $moderation){
			$this->db->join("{$this->moderation_table} im", "{$this->items}.id_item = im.id_item", 'inner');

			if(isset($completed_from)){
				$this->db->where("DATE(im.filled_on) >= DATE('{$completed_from}')");
			}

			if(isset($completed_to)){
				$this->db->where("DATE(im.filled_on) <= DATE('{$completed_to}')");
			}

			if(isset($issued_from)){
				$this->db->where("DATE(im.issued_on) >= DATE('{$issued_from}')");
			}

			if(isset($issued_to)){
				$this->db->where("DATE(im.issued_on) <= DATE('{$issued_to}')");
			}

			if(isset($moderated_from)){
				$this->db->where("DATE(im.moderated_on) >= DATE('{$moderated_from}')");
			}

			if(isset($moderated_to)){
				$this->db->where("DATE(im.moderated_on) <= DATE('{$moderated_to}')");
			}

			if(isset($is_filled)){
				$this->db->where("im.is_filled", $is_filled);
			}

			if(isset($has_issue)){
				$this->db->where("im.has_issue", $has_issue);
			}

			if(isset($is_moderated)){
				$this->db->where("im.is_moderated", $is_moderated);
			}

			if(isset($id_manager)){
				$this->db->where("im.id_manager", $id_manager);
			}

			if(isset($id_moderator)){
				$this->db->where("im.id_moderator", $id_moderator);
			}

			if(isset($id_issuer)){
				$this->db->where("im.id_issuer", $id_issuer);
			}
		}

        if(isset($items_list)){
			$this->db->where_in($this->items.'.id_item', $items_list);
        }

        if(isset($not_items_list)){
			$this->db->where_not_in($this->items.'.id_item', $not_items_list);
        }
		
		if (isset($keywords)) {			
			$search_in = $this->_get_search_items($keywords);
			if(!empty($search_in)){
				$this->db->where_in("{$this->items}.id_item", $search_in);
			}
		}

        if(isset($item_price_from)){
			$this->db->where($this->items.".{$item_price_column} >= ", $item_price_from);
        }

        if(isset($item_price_to)){
			$this->db->where($this->items.".{$item_price_column} <= ", $item_price_to);
        }

		if($exclude_bad_price){
			$this->db->where("{$this->items}.{$item_price_column} >", 0);
		}

        if(isset($id_category)){
			$this->db->where_in($this->items.'.id_category', $id_category);
        }

        if(isset($id_brand)){
			$this->db->where_in($this->items.'.id_brand', $id_brand);
        }

        if(isset($item_visible)){
			$this->db->where($this->items.'.item_visible', $item_visible);
        }

        if(isset($item_hit)){
			$this->db->where($this->items.'.item_hit', $item_hit);
        }

        if(isset($item_newest)){
			$this->db->where($this->items.'.item_newest', $item_newest);
        }

        if(isset($optional_filters)){
			$_filters = array();
			foreach ($optional_filters as $optional_filter) {
				switch ($optional_filter) {
					case 'actional':
						$_filters[] = " {$this->items}.item_action = 1 ";
					break;
					case 'newest':
						$_filters[] = " {$this->items}.item_newest = 1 ";
					break;
					case 'zerocredit':
						$_filters[] = " {$this->items}.item_zero_credit = 1 ";
					break;
				}
			}

			if (!empty($_filters)) {
				$this->db->where('( '.implode(' OR ', $_filters).' )', null);
			}
        }

        if(isset($item_zero_credit)){
			$this->db->where($this->items.'.item_zero_credit', $item_zero_credit);
        }

        if(isset($item_action)){
			$this->db->where($this->items.'.item_action', $item_action);
        }

        if(isset($item_popular)){
			$this->db->where($this->items.'.item_popular', $item_popular);
        }

        if(isset($item_on_home)){
			$this->db->where($this->items.'.item_on_home', $item_on_home);
        }

        if(isset($item_commented)){
			$this->db->where($this->items.'.item_commented', $item_commented);
        }

        if(isset($item_last_update_from)){
			$this->db->where("DATE({$this->items}.item_last_update) >= DATE('{$item_last_update_from}')");
        }

        if(isset($item_last_update_to)){
			$this->db->where("DATE({$this->items}.item_last_update) <= DATE('{$item_last_update_to}')");
        }
		
		if(isset($item_created_from)){
			$this->db->where("DATE({$this->items}.item_created) >= DATE('{$item_created_from}')");
		}
		
		if(isset($item_created_to)){
			$this->db->where("DATE({$this->items}.item_created) <= DATE('{$item_created_to}')");
		}

        if(isset($stock_available)){
			$stock_filters = array();			
			foreach ($stock_available as $value) {
				switch ($value) {
					case 'yes':
						$stock_filters[] = " {$this->items}.item_stocks_quantity > 0 ";
					break;
					case 'no':
						$stock_filters[] = " {$this->items}.item_stocks_quantity = 0 AND {$this->items}.item_suppliers_available = 0 ";
					break;
					case 'ordering':
						$stock_filters[] = " {$this->items}.item_stocks_quantity = 0 AND {$this->items}.item_suppliers_available > 0 ";
					break;
				}
			}

			if(!empty($stock_filters)){
				$this->db->where('('.implode(" OR ", $stock_filters).')');
			}
		}

        if(isset($id_supplier)){
			if(!is_array($id_supplier)){
				$id_supplier = explode(',', $id_supplier);
			}

			if(!empty($id_supplier)){
				foreach ($id_supplier as $value) {
					if($value == 'no'){
						$_where_suppliers[] = " {$this->items}.item_suppliers = '' ";
					} else{
						$value = str_replace('"', '', json_encode($value));
						$_where_suppliers[] = " {$this->items}.item_suppliers LIKE '%{$value}%' ";
					}
				}
			}
			
			if(!empty($_where_suppliers)){
				$this->db->where('('.implode(" OR ", $_where_suppliers).')');
			}
        }

		$count = $this->db->count_all_results($this->items);
		// echo '<pre>';
		// print_r($conditions);
		// echo '</pre>';
		// echo $this->db->last_query();
		return $count;
	}

	function handler_get_search_categories($conditions = array()){
		$order_by = " items.item_visible DESC, items.item_last_update ASC, categories.category_weight ASC ";
		$coulmns = "categories.category_id, categories.category_title, categories.category_breadcrumbs, categories.category_weight, COUNT(items.id_item) as search_count";
		
        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}

			if(!empty($multi_order_by)){
				$order_by = 'items.item_visible DESC, '.implode(',', $multi_order_by);
			}
		}

		if (isset($keywords)) {
			$search = get_keywords_combination($keywords);
			if(!empty($search)){
				$order_by .= " , REL_title DESC ";
				$coulmns .= " , IF({$this->items}.item_title LIKE '%".$this->db->escape_like_str($keywords)."%', (MATCH ({$this->items}.item_title) AGAINST ('".$this->db->escape_like_str($search)."') + 100), MATCH ({$this->items}.item_title) AGAINST ('".$this->db->escape_like_str($search)."')) as REL_title ";
			}
		}
		
		$this->db->select($coulmns);
		$this->db->from($this->items);
		$this->db->join($this->categories, 'items.id_category = categories.category_id', 'inner');
		
		if (isset($keywords)) {			
			$search_in = $this->_get_search_items($keywords);
			if(!empty($search_in)){
				$this->db->where_in("{$this->items}.id_item", $search_in);
			}
		}

        if(isset($item_newest)){
			$this->db->where($this->items.'.item_newest', $item_newest);
        }

        if(isset($optional_filters)){
			$_filters = array();
			foreach ($optional_filters as $optional_filter) {
				switch ($optional_filter) {
					case 'actional':
						$_filters[] = " {$this->items}.item_action = 1 ";
					break;
					case 'newest':
						$_filters[] = " {$this->items}.item_newest = 1 ";
					break;
					case 'zerocredit':
						$_filters[] = " {$this->items}.item_zero_credit = 1 ";
					break;
				}
			}

			if (!empty($_filters)) {
				$this->db->where('( '.implode(' OR ', $_filters).' )', null);
			}
		}
		
		if(isset($id_supplier)){
			if(!is_array($id_supplier)){
				$id_supplier = explode(',', $id_supplier);
			}

			if(!empty($id_supplier)){
				foreach ($id_supplier as $value) {
					if($value == 'no'){
						$_where_suppliers[] = " {$this->items}.item_suppliers = '' ";
					} else{
						$value = str_replace('"', '', json_encode($value));
						$_where_suppliers[] = " {$this->items}.item_suppliers LIKE '%{$value}%' ";
					}
				}
			}
			
			if(!empty($_where_suppliers)){
				$this->db->where('('.implode(" OR ", $_where_suppliers).')');
			}
        }

        if(isset($item_zero_credit)){
			$this->db->where($this->items.'.item_zero_credit', $item_zero_credit);
        }

        if(isset($item_action)){
			$this->db->where($this->items.'.item_action', $item_action);
        }

        if(isset($id_brand)){
			$this->db->where_in($this->items.'.id_brand', $id_brand);
        }

		$this->db->group_by("categories.category_id");
		$this->db->order_by($order_by);

		return $this->db->get()->result_array();
	}
	
	function handler_get_search_brands($conditions = array()){
		$order_by = " {$this->brands}.brand_title ASC ";
		
		extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}

			if(!empty($multi_order_by)){
				$order_by = implode(',', $multi_order_by);
			}
		}
		
		$this->db->select("{$this->brands}.*");
		$this->db->select("COUNT({$this->items}.id_item) as items_count");
		$this->db->from($this->brands);
		$this->db->join($this->items, "{$this->brands}.id_brand = {$this->items}.id_brand", 'inner');
		
		if(isset($id_item)){
			$this->db->where_in("{$this->items}.id_item", $id_item);
		}

		$this->db->group_by("{$this->brands}.id_brand");
		$this->db->order_by($order_by);

		return $this->db->get()->result_array();
	}

	function _get_search_items($keywords = ''){
		$keywords = trim($keywords);
		$result = array();
		if(empty($keywords)){
			return $result;
		}
		
		$where = array();
		$where_title = array();
		$words = array_filter(explode(' ', $keywords));
		$filtered_words = array();
		foreach($words as $word){
			$temp_word = trim($word);
			if(!empty($temp_word)){
				$filtered_words[] = $temp_word;
			}
		}

		if(empty($filtered_words)){
			return $result[] = 0;
		}
		
		foreach ($filtered_words as $key_word => $word) {
			if(mb_strlen($word) >= 1){
				$where_title[] = " item_title LIKE '%".$this->db->escape_like_str($word)."%' ESCAPE '!' ";
			}
		}

		if(!empty($where_title)){
			$where[] = implode(" AND ", $where_title);
		}

		$where[] = " item_prog_id LIKE '".$this->db->escape_like_str($keywords)."' ESCAPE '!' ";
		$where[] = " item_code LIKE '".$this->db->escape_like_str($keywords)."' ESCAPE '!' ";
			
		$sql = "SELECT id_item
				FROM {$this->items} 
				WHERE " . implode(" OR ", $where);

		$records = $this->db->query($sql)->result();
		if(!empty($records)){
			foreach ($records as $record) {
				$result[] = $record->id_item;
			}
		} else{
			$result[] = 0;
		}
		
		return $result;
	}

	function handler_get_search($conditions = array()){
		$order_by = " {$this->items}.item_visible DESC, {$this->items}.item_last_update ASC ";

		$price_column = getPriceColumn();
		$item_price_column = $price_column['db_price_column'];
		$coulmns = "{$this->items}.*";
		
		$exclude_bad_price = true;
		extract($conditions);
		
		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			
			if(!empty($multi_order_by)){
				$order_by = "{$this->items}.item_visible DESC, ".implode(",", $multi_order_by);
			}
		}

		if (isset($keywords)) {
			$search = get_keywords_combination($keywords);
			if(!empty($search)){
				$order_by .= ", REL_title DESC ";
				$coulmns .= " , IF({$this->items}.item_title LIKE '%".$this->db->escape_like_str($keywords)."%', (MATCH ({$this->items}.item_title) AGAINST ('".$this->db->escape_like_str($search)."') + 100), MATCH ({$this->items}.item_title) AGAINST ('".$this->db->escape_like_str($search)."')) as REL_title ";
			}
		}

		$this->db->select("{$coulmns}, IF( 
			(
				{$this->items}.item_temp_price > 0 AND 
				{$this->items}.item_temp_price < {$this->items}.item_price AND 
				{$this->items}.item_temp_price < {$this->items}.{$price_column['db_price_column']}
			), 
			{$this->items}.item_temp_price, 
			(
				IF(
					{$this->items}.item_price < {$this->items}.{$price_column['db_price_column']}, 
					{$this->items}.item_price, 
					{$this->items}.{$price_column['db_price_column']}
				)
			) 
		) as item_order_price", false);
		$this->db->from($this->items);
		$this->db->join($this->categories, "{$this->items}.id_category = categories.category_id", 'inner');

		if (isset($keywords)) {			
			$search_in = $this->_get_search_items($keywords);
			if(!empty($search_in)){
				$this->db->where_in("{$this->items}.id_item", $search_in);
			}
		}

        if(isset($item_price_from)){
			$this->db->where("{$this->items}.{$item_price_column} >= ", $item_price_from);
        }

        if(isset($item_price_to)){
			$this->db->where("{$this->items}.{$item_price_column} <= ", $item_price_to);
        }

		if($exclude_bad_price){
			$this->db->where("{$this->items}.{$item_price_column} > ", 0);
		}

        if(isset($id_category)){
			$this->db->where_in("{$this->items}.id_category", $id_category);
        }

        if(isset($id_brand)){
			$this->db->where_in("{$this->items}.id_brand", $id_brand);
        }

        if(isset($item_newest)){
			$this->db->where($this->items.'.item_newest', $item_newest);
		}

        if(isset($optional_filters)){
			$_filters = array();
			foreach ($optional_filters as $optional_filter) {
				switch ($optional_filter) {
					case 'actional':
						$_filters[] = " {$this->items}.item_action = 1 ";
					break;
					case 'newest':
						$_filters[] = " {$this->items}.item_newest = 1 ";
					break;
					case 'zerocredit':
						$_filters[] = " {$this->items}.item_zero_credit = 1 ";
					break;
				}
			}

			if (!empty($_filters)) {
				$this->db->where('( '.implode(' OR ', $_filters).' )', null);
			}
        }

        if(isset($item_zero_credit)){
			$this->db->where($this->items.'.item_zero_credit', $item_zero_credit);
		}
		
        if(isset($item_created_from)){
			$this->db->where("DATE({$this->items}.item_created) >= DATE('{$item_created_from}')");
        }
		
        if(isset($item_created_to)){
			$this->db->where("DATE({$this->items}.item_created) <= DATE('{$item_created_to}')");
        }

        if(isset($item_action)){
			$this->db->where($this->items.'.item_action', $item_action);
        }

        if(isset($stock_available)){
			$stock_filters = array();			
			foreach ($stock_available as $value) {
				switch ($value) {
					case 'yes':
						$stock_filters[] = " {$this->items}.item_stocks_quantity > 0 ";
					break;
					case 'no':
						$stock_filters[] = " {$this->items}.item_stocks_quantity = 0 AND {$this->items}.item_suppliers_available = 0 ";
					break;
					case 'ordering':
						$stock_filters[] = " {$this->items}.item_stocks_quantity = 0 AND {$this->items}.item_suppliers_available > 0 ";
					break;
				}
			}

			if(!empty($stock_filters)){
				$this->db->where('('.implode(" OR ", $stock_filters).')');
			}
		}

        if(isset($id_supplier)){
			if(!is_array($id_supplier)){
				$id_supplier = explode(',', $id_supplier);
			}

			if(!empty($id_supplier)){
				foreach ($id_supplier as $value) {
					if($value == 'no'){
						$_where_suppliers[] = " {$this->items}.item_suppliers = '' ";
					} else{
						$value = str_replace('"', '', json_encode($value));
						$_where_suppliers[] = " {$this->items}.item_suppliers LIKE '%{$value}%' ";
					}
				}
			}
			
			if(!empty($_where_suppliers)){
				$this->db->where('('.implode(" OR ", $_where_suppliers).')');
			}
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}
		// echo $this->db->get_compiled_select();
		return $this->db->get()->result_array();
	}

	function handler_get_search_count($conditions = array()){
		$exclude_bad_price = true;

        extract($conditions);

		if(isset($item_price_from)){
			$this->db->where("{$item_price_column} >= ", $item_price_from);
        }

        if(isset($item_price_to)){
			$this->db->where("{$item_price_column} <= ", $item_price_to);
        }

		if($exclude_bad_price){
			$this->db->where("{$item_price_column} >", 0);
		}

        if(isset($id_category)){
			$this->db->where_in('id_category', $id_category);
        }
		
		if(isset($id_brand)){
			$this->db->where_in("id_brand", $id_brand);
		}

        if(isset($item_newest)){
			$this->db->where('item_newest', $item_newest);
        }

        if(isset($optional_filters)){
			$_filters = array();
			foreach ($optional_filters as $optional_filter) {
				switch ($optional_filter) {
					case 'actional':
						$_filters[] = " item_action = 1 ";
					break;
					case 'newest':
						$_filters[] = " item_newest = 1 ";
					break;
					case 'zerocredit':
						$_filters[] = " item_zero_credit = 1 ";
					break;
				}
			}

			if (!empty($_filters)) {
				$this->db->where('( '.implode(' OR ', $_filters).' )', null);
			}
        }

        if(isset($item_zero_credit)){
			$this->db->where('item_zero_credit', $item_zero_credit);
        }
		
        if(isset($item_created_from)){
			$this->db->where("DATE(item_created) >= DATE('{$item_created_from}')");
        }
		
        if(isset($item_created_to)){
			$this->db->where("DATE(item_created) <= DATE('{$item_created_to}')");
        }

        if(isset($item_action)){
			$this->db->where('item_action', $item_action);
        }
		
		if (isset($keywords)) {			
			$search_in = $this->_get_search_items($keywords);
			if(!empty($search_in)){
				$this->db->where_in("id_item", $search_in);
			}
		}

        if(isset($stock_available)){
			$stock_filters = array();			
			foreach ($stock_available as $value) {
				switch ($value) {
					case 'yes':
						$stock_filters[] = " {$this->items}.item_stocks_quantity > 0 ";
					break;
					case 'no':
						$stock_filters[] = " {$this->items}.item_stocks_quantity = 0 AND {$this->items}.item_suppliers_available = 0 ";
					break;
					case 'ordering':
						$stock_filters[] = " {$this->items}.item_stocks_quantity = 0 AND {$this->items}.item_suppliers_available > 0 ";
					break;
				}
			}

			if(!empty($stock_filters)){
				$this->db->where('('.implode(" OR ", $stock_filters).')');
			}
		}

        if(isset($id_supplier)){
			if(!is_array($id_supplier)){
				$id_supplier = explode(',', $id_supplier);
			}

			if(!empty($id_supplier)){
				foreach ($id_supplier as $value) {
					if($value == 'no'){
						$_where_suppliers[] = " {$this->items}.item_suppliers = '' ";
					} else{
						$value = str_replace('"', '', json_encode($value));
						$_where_suppliers[] = " {$this->items}.item_suppliers LIKE '%{$value}%' ";
					}
				}
			}
			
			if(!empty($_where_suppliers)){
				$this->db->where('('.implode(" OR ", $_where_suppliers).')');
			}
        }

		return $this->db->count_all_results($this->items);
	}

	function handler_min_price($conditions = array()){
		$item_price_column = 'item_price';
		
		extract($conditions);

		$this->db->select_min($item_price_column);

        if(isset($id_category)){
			$this->db->where_in('id_category', $id_category);
        }

		if (isset($keywords)) {			
			$search_in = $this->_get_search_items($keywords);
			if(!empty($search_in)){
				$this->db->where_in("id_item", $search_in);
			}
		}

        if(isset($item_newest)){
			$this->db->where('item_newest', $item_newest);
        }

        if(isset($optional_filters)){
			$_filters = array();
			foreach ($optional_filters as $optional_filter) {
				switch ($optional_filter) {
					case 'actional':
						$_filters[] = " item_action = 1 ";
					break;
					case 'newest':
						$_filters[] = " item_newest = 1 ";
					break;
					case 'zerocredit':
						$_filters[] = " item_zero_credit = 1 ";
					break;
				}
			}

			if (!empty($_filters)) {
				$this->db->where('( '.implode(' OR ', $_filters).' )', null);
			}
        }

        if(isset($item_zero_credit)){
			$this->db->where('item_zero_credit', $item_zero_credit);
        }

        if(isset($item_action)){
			$this->db->where('item_action', $item_action);
        }

		$result = $this->db->get($this->items)->row();
		$amount = floor($result->$item_price_column);
		if($amount < 0){
			$amount = 0;
		}
		
		$price = getPriceObject($amount);
		return $price['display_price'];
	}

	function handler_max_price($conditions = array()){
		$item_price_column = 'item_price';
		extract($conditions);
		$this->db->select_max($item_price_column);

        if(isset($id_category)){
			$this->db->where_in('id_category', $id_category);
        }

		if (isset($keywords)) {			
			$search_in = $this->_get_search_items($keywords);
			if(!empty($search_in)){
				$this->db->where_in("id_item", $search_in);
			}
		}

        if(isset($item_newest)){
			$this->db->where('item_newest', $item_newest);
        }

        if(isset($optional_filters)){
			$_filters = array();
			foreach ($optional_filters as $optional_filter) {
				switch ($optional_filter) {
					case 'actional':
						$_filters[] = " item_action = 1 ";
					break;
					case 'newest':
						$_filters[] = " item_newest = 1 ";
					break;
					case 'zerocredit':
						$_filters[] = " item_zero_credit = 1 ";
					break;
				}
			}

			if (!empty($_filters)) {
				$this->db->where('( '.implode(' OR ', $_filters).' )', null);
			}
        }

        if(isset($item_zero_credit)){
			$this->db->where('item_zero_credit', $item_zero_credit);
        }

        if(isset($item_action)){
			$this->db->where('item_action', $item_action);
        }

		$result = $this->db->get($this->items)->row();

		$amount = floor($result->$item_price_column);
		if($amount < 0){
			$amount = 0;
		}
		
		$price = getPriceObject(ceil($amount));
		return $price['display_price'];
	}

	function handler_import($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert_on_duplicate_update_batch($this->items, $data);
	}
	
	function handler_update_actionals(){
		$this->db->update($this->items, array('item_action' => 0));
		$this->db->where('(item_temp_price > 0 OR item_cashback_price > 0)');
		$this->db->where('item_visible', 1);
		$this->db->update($this->items, array('item_action' => 1));
	}
	
	function handler_update_newest(){
		$this->db->update($this->items, array('item_newest' => 0));
		$this->db->where('setting_alias', 'items_newest_days');
		$settings_newest = $this->db->get('settings')->row_array();
		$today = date('Y-m-d H:i:s');
		$item_created_from = date('Y-m-d H:i:s', strtotime("{$today} - {$settings_newest['setting_value']}days"));
		$this->db->where("DATE(item_created) >= DATE('{$item_created_from}')");
		$this->db->where("item_visible", 1);
		$this->db->update($this->items, array('item_newest' => 1));
	}

	function handler_get_items_to_delete($conditions = array()){	
		extract($conditions);

		$this->db->select("*");
		$this->db->from($this->items);

        if(isset($item_import_update_to)){
			$this->db->where("item_import_update <= ", $item_import_update_to);
        }

		$this->db->limit(1000);

		$records = $this->db->get()->result_array();
		return $records;
	}

	function handler_insert_deleted($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->items_deleted, $data);
		return $this->db->insert_id();
	}

	function handler_delete_deleted($id_item){
		$this->db->where_in('id_item', $id_item);
		$this->db->delete($this->items_deleted);
	}

	function handler_get_all_promo($conditions = array()){
		$order_by = array(" item_visible DESC ");
		$item_price_column = 'item_price';
		$exclude_bad_price = true;
		
		extract($conditions);

		$order_by[] = " order_price ASC ";

		$this->db->select("i.*");
		$this->db->select("
			IF(
				i.item_temp_price > 0 AND 				
				i.item_temp_price < i.item_price AND 
				i.item_temp_price < i.{$item_price_column}, 
				i.item_temp_price, 
				IF(
					i.item_price < i.{$item_price_column}, 
					i.item_price, 
					i.{$item_price_column}
				)
			) as order_price
		", false);
		$this->db->from("{$this->items} i");
		$this->db->join("{$this->promo_items} pi", "i.id_item = pi.id_item", "inner");

		if($exclude_bad_price){
			$this->db->where("i.{$item_price_column} >", 0);
		}

        if(isset($id_promo)){
			$this->db->where('pi.id_promo', $id_promo);
        }

        if(isset($id_category)){
			$this->db->where_in('i.id_category', $id_category);
        }

        if(isset($item_visible)){
			$this->db->where('i.item_visible', $item_visible);
        }

		$this->db->order_by(implode(', ', $order_by ));

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get()->result_array();
	}
	
	function handler_get_count_promo($conditions = array()){
		$item_price_column = 'item_price';
		$exclude_bad_price = true;

		$this->db->select("COUNT(i.id_item) as total_rows");
		$this->db->from("{$this->items} i");
		$this->db->join("{$this->promo_items} pi", "i.id_item = pi.id_item", "inner");

		extract($conditions);

		if($exclude_bad_price){
			$this->db->where("i.{$item_price_column} >", 0);
		}

		if(isset($id_promo)){
			$this->db->where('pi.id_promo', $id_promo);
        }

        if(isset($id_category)){
			$this->db->where_in('i.id_category', $id_category);
        }

        if(isset($item_visible)){
			$this->db->where('i.item_visible', $item_visible);
        }

		$result = $this->db->get()->row_array();
		// echo '<pre>';
		// print_r($conditions);
		// echo '</pre>';
		// echo $this->db->last_query();
		return (int) $result['total_rows'];
	}

	// STOCKS
	function handler_insert_stocks_batch($data = array()){
		if (empty($data)) {
			return false;
		}

		return $this->db->insert_batch($this->stocks, $data);
	}

	function handler_update_stocks_batch($data = array(), $key = 'id_stock'){
		if (empty($data)) {
			return false;
		}

		$this->db->update_batch($this->stocks, $data, $key);
	}

	function handler_get_stocks_all($conditions = array()){
        extract($conditions);

		return $this->db->get($this->stocks)->result_array();
	}

	// SUPPLIERS
	function handler_insert_suppliers_batch($data = array()){
		if (empty($data)) {
			return false;
		}

		return $this->db->insert_batch($this->suppliers, $data);
	}

	function handler_update_suppliers_batch($data = array(), $key = 'id_supplier'){
		if (empty($data)) {
			return false;
		}

		$this->db->update_batch($this->suppliers, $data, $key);
	}

	function handler_get_supplier($id_supplier = 0){
        $this->db->where('id_supplier', $id_supplier);

		return $this->db->get($this->suppliers)->row_array();
	}

	function handler_get_suppliers_all($conditions = array()){
        extract($conditions);

		return $this->db->get($this->suppliers)->result_array();
	}

	// REPORTS
	function _getReportParams($params = []){
		extract($params);

        if(isset($id_category)){
			$this->db->where_in('id_category', $id_category);
		}

        if(isset($has_bugs)){
			$this->db->where('item_data_moderated_bug', $has_bugs);
		}

        if(isset($is_completed)){
			$this->db->where('item_data_completed', $is_completed);
		}

        if(isset($id_manager_completed)){
			$this->db->where_in('item_user_edit', $id_manager_completed);
		}

        if(isset($completed_date_from)){
			$this->db->where("DATE(item_data_completed_date) >= '{$completed_date_from}'");
		}

        if(isset($completed_date_to)){
			$this->db->where("DATE(item_data_completed_date) <= '{$completed_date_to}'");
		}

        if(isset($is_moderated)){
			$this->db->where('item_data_moderated', $is_moderated);
		}

        if(isset($id_manager_moderated)){
			$this->db->where('item_user_moderate', $id_manager_moderated);
		}

        if(isset($moderated_date_from)){
			$this->db->where("DATE(item_data_moderated_date) >= '{$moderated_date_from}'");
		}

        if(isset($moderated_date_to)){
			$this->db->where("DATE(item_data_moderated_date) <= '{$moderated_date_to}'");
		}

		if(!empty($groupOr)){
			$this->db->group_start();
			foreach ($groupOr as $groupParams) {
				$this->db->or_group_start();
					if(isset($groupParams['column'], $groupParams['value'])){
						$this->db->where("{$groupParams['column']}", $groupParams['value']);
					}

					if(isset($groupParams['manager'])){
						$this->db->where("{$groupParams['manager']['column']}", $groupParams['manager']['value']);
					}

					if(!empty($groupParams['date'])){
						$this->db->where("DATE({$groupParams['date']['column']}) >= '{$groupParams['date']['from']}'");
						$this->db->where("DATE({$groupParams['date']['column']}) <= '{$groupParams['date']['to']}'");
					}
				$this->db->group_end();
			}
			$this->db->group_end();
		}
	}

	function handler_report_get_items($conditions = array()){
        $this->_getReportParams($conditions);
				
		$records = $this->db->get($this->items)->result_array();
		// dump($this->db->last_query());
		return $records;
	}

	function handler_report_get_count($conditions = array()){
        $this->_getReportParams($conditions);

		return $this->db->count_all_results($this->items);
	}

	function handler_update_display_price($conditions = array()){
		$where = array();

		extract($conditions);

		if(isset($id_item)){
			if(!is_array($id_item)){
				$id_item = explode(',', $id_item);
				$id_item = array_map('intval', $id_item);
				$id_item = array_filter(array_map('intval', $id_item));

				if(!empty($id_item)){
					$id_item = implode(',', $id_item);
					$where[] = " i.id_item IN ({$id_item}) ";
				}
			}
		}

		if(isset($id_category)){
			if(!is_array($id_category)){
				$id_category = explode(',', $id_category);
				$id_category = array_map('intval', $id_category);
				$id_category = array_filter(array_map('intval', $id_category));

				if(!empty($id_category)){
					$id_category = implode(',', $id_category);
					$where[] = " c.category_id IN ({$id_category}) ";
				}
			}
		}

		$where = !empty($where) ? " AND " .implode(" AND ", $where) : "";
		
		$sql = "UPDATE items as i, categories as c
				SET i.item_hide_price = c.category_hide_price
				WHERE i.id_category = c.category_id {$where}";

		return $this->db->simple_query($sql);
	}
}
