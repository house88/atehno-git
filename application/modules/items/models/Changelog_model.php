<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Changelog_model extends CI_Model{
	var $logTable = "items_changelog";
	var $usersTable = "users";

	function __construct(){
		parent::__construct();
	}

	//region CHANGELOG
	function handlerInsertLog($insert = array()){
		if(empty($insert)){
			return false;
		}

		$this->db->insert_batch($this->logTable, $insert);
		return $this->db->insert_id();
	}

	function handlerGetLog($idItem){
		$this->db->select("cl.*");
		$this->db->select("u.user_nicename as username");
		$this->db->from("{$this->logTable} cl");
		$this->db->join("{$this->usersTable} u", "cl.id_user = u.id", "left");

		$this->db->where("cl.id_item", $idItem);

		$this->db->order_by('cl.created_on DESC');

		return $this->db->get()->result_array();
	}
	//endregion CHANGELOG
}
