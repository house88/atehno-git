<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Prices_model extends CI_Model{
	var $items_prices = "items_prices";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->items_prices, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_price_variant, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_price_variant', $id_price_variant);
		$this->db->update($this->items_prices, $data);
	}

	function handler_delete($id_price_variant){
		$this->db->where('id_price_variant', $id_price_variant);
		$this->db->delete($this->items_prices);
	}

	function handler_get($id_price_variant){
		$this->db->where('id_price_variant', $id_price_variant);
		return $this->db->get($this->items_prices)->row_array();
	}

	function handler_get_all($conditions = array()){
		$order_by = " id_price_variant ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($active)){
			$this->db->where('price_variant_active', $active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->items_prices)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($active)){
			$this->db->where('price_variant_active', $active);
        }

		return $this->db->count_all_results($this->items_prices);
	}
}
