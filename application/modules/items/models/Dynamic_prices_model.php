<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dynamic_prices_model extends CI_Model{
	public $items_dynamic_prices = "items_dynamic_prices";
	public $items_prices = "items_prices";
	public $currency = "currency";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->items_dynamic_prices, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_dynamic_price, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_dynamic_price', $id_dynamic_price);
		$this->db->update($this->items_dynamic_prices, $data);
	}

	function handler_delete($id_dynamic_price){
		$this->db->where('id_dynamic_price', $id_dynamic_price);
		$this->db->delete($this->items_dynamic_prices);
	}

	function handler_get($id_dynamic_price){
		$this->db->where('id_dynamic_price', $id_dynamic_price);
		return $this->db->get($this->items_dynamic_prices)->row_array();
	}

	function handler_get_all($conditions = array()){
        extract($conditions);

		$this->db->select('*');
		$this->db->from($this->items_dynamic_prices);

        if(isset($active)){
			$this->db->where($this->items_dynamic_prices.'.dinamic_price_active', $active);
        }
		$this->db->join($this->items_prices, $this->items_prices.'.id_price_variant = '.$this->items_dynamic_prices.'.id_price_variant', 'left');
		$this->db->join($this->currency, $this->currency.'.id_currency = '.$this->items_dynamic_prices.'.id_price_currency', 'left');

		$this->db->order_by('id_dynamic_price ASC');

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get()->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($active)){
			$this->db->where('dinamic_price_active', $active);
        }

		return $this->db->count_all_results($this->items_dynamic_prices);
	}
}
