<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Moderate extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->data = array();

		$this->load->model("Moderate_model", "moderate");
	}

	function index(){
		return_404($this->data);
	}

	function _getStates(int $id_item){
		return $this->moderate->handlerGetStates($id_item);
	}

	function _updateStates(int $id_item, array $update){
		if(empty($update)){
			return false;
		}

		return $this->moderate->handlerUpdateStates($id_item, $update);
	}

	function _getStatesDetails(int $id_item){
		$statesDetails = $this->moderate->handlerGetStates($id_item);

		$statesDetails['managers'] = $this->moderate->handlerGetFillUsers($id_item);
		$statesDetails['moderators'] = $this->moderate->handlerGetModerateUsers($id_item);
		$statesDetails['issuers'] = $this->moderate->handlerGetIssuerUsers($id_item);
		$statesDetails['lastFillManager'] = !empty($statesDetails['managers']) ? current($statesDetails['managers']) : [];
		$statesDetails['lastModerateManager'] = !empty($statesDetails['moderators']) ? current($statesDetails['moderators']) : [];
		$statesDetails['lastIssueManager'] = !empty($statesDetails['issuers']) ? current($statesDetails['issuers']) : [];

		return $statesDetails;
	}

	function _setFill(array $insert){
		if(empty($insert)){
			return false;
		}

		return $this->moderate->handlerInsertFill($insert);
	}

	function _deleteLastFill(int $id_item){
		return $this->moderate->handlerDeleteLastFill($id_item);
	}

	function _getFill($params = []){
		if(empty($params)){
			return false;
		}

		return $this->moderate->handlerGetFill($params);
	}

	function _setIssue(array $insert){
		if(empty($insert)){
			return false;
		}

		return $this->moderate->handlerInsertIssue($insert);
	}

	function _deleteLastIssue(int $id_item){
		return $this->moderate->handlerDeleteLastIssue($id_item);
	}

	function _getIssue($params = []){
		return $this->moderate->handlerGetIssue($params);
	}

	function _setDone(array $insert){
		if(empty($insert)){
			return false;
		}

		return $this->moderate->handlerInsertDone($insert);
	}

	function _deleteLastDone(int $id_item){
		return $this->moderate->handlerDeleteLastDone($id_item);
	}

	function _getDone($params = []){
		if(empty($params)){
			return false;
		}

		return $this->moderate->handlerGetDone($params);
	}
}
