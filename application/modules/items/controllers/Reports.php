<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}
		
		$this->view_module_path = 'modules/items/reports/';
		$this->active_menu = 'catalog';
		$this->active_submenu = 'moderation_reports';

		$this->data = array();

		$this->load->model("Items_model", "items");
		$this->load->model("categories/Categories_model", "categories");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		checkPermision('view_moderators_reports');

		$this->data['contentManagers'] = Modules::run('users/_get_all', array('group_type' => array('content_menedger')));
		$this->data['contentModerators'] = Modules::run('users/_get_all', array('group_type' => array('moderator')));
		$this->data['adminManagers'] = Modules::run('users/_get_all', array('group_type' => array('admin')));

		$this->data['page_header'] = 'Отчет по модерации товаров';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'index_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'fill':
				checkPermisionAjax('view_moderators_reports');

				$this->form_validation->set_rules('id_manager', 'Менеджер', 'required|xss_clean');
				$this->form_validation->set_rules('id_category', 'Категория', 'xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$params = [
					'is_filled' => 1
				];

				//region MANAGER
				$idManager = (int) $this->input->post('id_manager');
				$manager = $this->lauth->get_user_data($idManager);
				if(empty($manager)){
					jsonResponse('Менеджер не найден.');
				}
				
				$params['id_manager'] = $idManager;
				//endregion MANAGER

				//region CATEGORY
				$idCategory = (int) $this->input->post('id_category');
				if($idCategory > 0){
					$category = $this->categories->handler_get($idCategory);
					if(!empty($category)){
						$idCategories = (array) explode(',', $category['category_children']);
						$idCategories = array_filter(array_map('intval', $idCategories));
						$idCategories[] = $idCategories;
						$params['id_category'] = $idCategories;
					}
				}
				//endregion CATEGORY

				//region DATE
				if($this->input->post('report_date')){
					list($dateFrom, $dateTo) = explode(' - ', $this->input->post('report_date'));
				}

				if(isset($dateFrom, $dateTo) && validateDate($dateFrom, 'd.m.Y') && validateDate($dateTo, 'd.m.Y') ){
					$params['date_from'] = getDateFormat($dateFrom, 'd.m.Y', 'Y-m-d');
					$params['date_to'] = getDateFormat($dateTo, 'd.m.Y', 'Y-m-d');
				}
				//endregion DATE
								
				//region MODERATED
				if($this->input->post('is_moderated')){
					$params['is_moderated'] = 1;
				}				
				//endregion MODERATED
								
				//region ISSUE
				$params['has_issue'] = 0;
				if($this->input->post('has_issue')){
					$params['has_issue'] = 1;
				}				
				//endregion ISSUE

				$items = Modules::run('items/moderate/_getFill', $params);
				if(empty($items)){
					jsonResponse('Нет данных.', 'success');
				}

				$categories = array();
				$categories_counters = array();
				$categories_counters_moderated = array();
				foreach ($items as $item) {
					$categories[] = $item['id_category'];
					if(array_key_exists($item['id_category'], $categories_counters)){
						$categories_counters[$item['id_category']] += 1;
					} else{
						$categories_counters[$item['id_category']] = 1;
					}

					if((int) $item['is_moderated'] === 1){
						if(array_key_exists($item['id_category'], $categories_counters_moderated)){
							$categories_counters_moderated[$item['id_category']] += 1;
						} else{
							$categories_counters_moderated[$item['id_category']] = 1;
						}
					}
				}

				$categories = $this->categories->handler_get_all(array('id_category' => $categories));
				foreach ($categories as $category) {
					$category_breads = json_decode('['.$category['category_breadcrumbs'].']', true);
					foreach ($category_breads as $category_bread) {
						$categories_list[] = $category_bread['category_id'];						
					}
				}
				$categories = $this->categories->handler_get_tree(array('id_category' => $categories_list));
				$categories_all = arrayByKey($this->categories->handler_get_all(array('id_category' => $categories_list)), 'category_id');
				foreach ($categories_all as $category_key => $category_all) {
					$categories_all[$category_key]['edit_items_count'] = 0;
					$categories_all[$category_key]['edit_items_count_moderated'] = 0;
					$categories_all[$category_key]['edit_item_price'] = 0;
					if(array_key_exists($category_key, $categories_counters)){
						$categories_all[$category_key]['edit_items_count'] += $categories_counters[$category_key];
					}

					if(array_key_exists($category_key, $categories_counters_moderated)){
						$categories_all[$category_key]['edit_items_count_moderated'] += $categories_counters_moderated[$category_key];
					}

					$category_children = array_filter((array) explode(',', $category_all['category_children']));
					if(!empty($category_children)){
						foreach($category_children as $category_child){
							if(array_key_exists($category_child, $categories_counters)){
								$categories_all[$category_key]['edit_items_count'] += $categories_counters[$category_child];
							}
					
							if(array_key_exists($category_child, $categories_counters_moderated)){
								$categories_all[$category_key]['edit_items_count_moderated'] += $categories_counters_moderated[$category_child];
							}
						}
					}
			
					$category_aditional_fields = $this->categories->handler_get_additional_fields($category_all);
					if(!empty($category_aditional_fields)){
						foreach ($category_aditional_fields as $category_aditional_field) {
							if($category_aditional_field['active'] == 1){
								$categories_all[$category_key]['edit_item_price'] += $category_aditional_field['price'];
							}
						}
					}
				}

				$totals = [
					'count_completed' => 0,
					'count_moderated' => 0,
					'amount' => 0
				];
				foreach ($categories_all as $category) {
					if(0 < (int) $category['category_parent']){
						continue;
					}

					$totals['count_completed'] += $category['edit_items_count'];
					$totals['count_moderated'] += $category['edit_items_count_moderated'];
					$totals['amount'] += $category['edit_item_price'] * $category['edit_items_count'];
				}

				$totals['amount'] = niceDisplayPrice($totals['amount'], ['useDecimals' => true]);

				$content_categories = $this->categoriesTreeTable($categories, 0, $categories_all);
				jsonResponse('Success', 'success', array('categories' => $content_categories, 'totals' => $totals, 'params' => $params));
			break;
			case 'moderate':
				checkPermisionAjax('view_moderators_reports');

				$this->form_validation->set_rules('id_moderator', 'Модератор', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$params = [
					'is_moderated' => 1
				];

				//region MODERATOR
				$idModerator = (int) $this->input->post('id_moderator');
				$moderator = $this->lauth->get_user_data($idModerator);
				if(empty($moderator)){
					jsonResponse('Модератор не найден.');
				}
				
				$params['id_moderator'] = $idModerator;
				//endregion MODERATOR

				//region CATEGORY
				$idCategory = (int) $this->input->post('id_category');
				if($idCategory > 0){
					$category = $this->categories->handler_get($idCategory);
					if(!empty($category)){
						$idCategories = (array) explode(',', $category['category_children']);
						$idCategories = array_filter(array_map('intval', $idCategories));
						$idCategories[] = $idCategories;
						$params['id_category'] = $idCategories;
					}
				}
				//endregion CATEGORY

				//region DATE
				if($this->input->post('report_date')){
					list($dateFrom, $dateTo) = explode(' - ', $this->input->post('report_date'));
				}

				if(isset($dateFrom, $dateTo) && validateDate($dateFrom, 'd.m.Y') && validateDate($dateTo, 'd.m.Y') ){
					$params['date_from'] = getDateFormat($dateFrom, 'd.m.Y', 'Y-m-d');
					$params['date_to'] = getDateFormat($dateTo, 'd.m.Y', 'Y-m-d');
				}
				//endregion DATE
								
				//region FILLED
				$params['is_filled'] = 1;
				if($this->input->post('is_not_filled')){
					$params['is_filled'] = 0;
				}				
				//endregion FILLED
								
				//region ISSUE
				$params['has_issue'] = 0;
				if($this->input->post('has_issue')){
					$params['has_issue'] = 1;
				}				
				//endregion ISSUE
				
				$items = Modules::run('items/moderate/_getDone', $params);
				if(empty($items)){
					jsonResponse('Нет данных.', 'success');
				}

				$categories = array();
				$categories_counters = array();
				foreach ($items as $item) {
					$categories[] = $item['id_category'];
					if(array_key_exists($item['id_category'], $categories_counters)){
						$categories_counters[$item['id_category']] += 1;
					} else{
						$categories_counters[$item['id_category']] = 1;
					}
				}

				$categories = $this->categories->handler_get_all(array('id_category' => $categories));
				foreach ($categories as $category) {
					$category_breads = json_decode('['.$category['category_breadcrumbs'].']', true);
					foreach ($category_breads as $category_bread) {
						$categories_list[] = $category_bread['category_id'];						
					}
				}
				$categories = $this->categories->handler_get_tree(array('id_category' => $categories_list));
				$categories_all = arrayByKey($this->categories->handler_get_all(array('id_category' => $categories_list)), 'category_id');
				foreach ($categories_all as $category_key => $category_all) {
					$categories_all[$category_key]['moderated_count'] = 0;
					$categories_all[$category_key]['moderate_price'] = 0;
					if(array_key_exists($category_key, $categories_counters)){
						$categories_all[$category_key]['moderated_count'] += $categories_counters[$category_key];
					}

					$category_children = array_filter((array) explode(',', $category_all['category_children']));
					if(!empty($category_children)){
						foreach($category_children as $category_child){
							if(array_key_exists($category_child, $categories_counters)){
								$categories_all[$category_key]['moderated_count'] += $categories_counters[$category_child];
							}
						}
					}
			
					$category_aditional_fields = $this->categories->handler_get_additional_fields($category_all);
					if(!empty($category_aditional_fields) && array_key_exists('moderation', $category_aditional_fields)){
						$categories_all[$category_key]['moderate_price'] = $category_aditional_fields['moderation']['price'] * 1;
					}
				}

				$totals = [
					'count_moderated' => 0,
					'amount' => 0
				];
				foreach ($categories_all as $category) {
					if(0 < (int) $category['category_parent']){
						continue;
					}

					$totals['count_moderated'] += $category['moderated_count'];
					$totals['amount'] += $category['moderate_price'] * $category['moderated_count'];
				}

				$totals['amount'] = niceDisplayPrice($totals['amount'], ['useDecimals' => true]);

				$content_categories = $this->categoriesTreeModerationTable($categories, 0, $categories_all);
				jsonResponse('Success', 'success', array('categories' => $content_categories, 'totals' => $totals, 'params' => $params));
			break;
			case 'issues':
				checkPermisionAjax('view_moderators_reports');
				
				$params = [
					'has_issue' => 1,
					'checkManagerColumn' => 'id_manager',
					'checkDateColumn' => 'filled_on',
				];

				switch ($this->input->post('type')) {
					case 'issuer':
						$params['checkManagerColumn'] = 'id_issuer';
						$params['checkDateColumn'] = 'issued_on';
					break;
					case 'moderator':
						$params['checkManagerColumn'] = 'id_moderator';
						$params['checkDateColumn'] = 'moderated_on';
					break;
				}

				$idManager = [];
				$postManagers = $this->input->post('id_manager');
				if(is_array($postManagers)){
					$idManager = array_filter(array_map('intval', $postManagers));
					if(!empty($idManager)){
						$params['id_manager'] = $idManager;
					}
				}

				if($this->input->post('completed_date')){
					list($completedDateFrom, $completedDateTo) = explode(' - ', $this->input->post('completed_date'));
					if(isset($completedDateFrom, $completedDateTo) && validateDate($completedDateFrom, 'd.m.Y') && validateDate($completedDateTo, 'd.m.Y') ){
						$params['date_from'] = getDateFormat($completedDateFrom, 'd.m.Y', 'Y-m-d');
						$params['date_to'] = getDateFormat($completedDateTo, 'd.m.Y', 'Y-m-d');
					}
				}

				//region FILLED
				if($this->input->post('is_filled')){
					$params['is_filled'] = 1;
				}				
				//endregion FILLED
								
				//region MODERATED
				if($this->input->post('is_moderated')){
					$params['is_moderated'] = 1;
				}				
				//endregion MODERATED
				$items = Modules::run('items/moderate/_getIssue', $params);
				if(empty($items)){
					jsonResponse('Нет данных.', 'success');
				}

				$managersCompleted = array_column($items, $params['checkManagerColumn']);
				$itemsByManager = arrayByKey($items, $params['checkManagerColumn'], true);
				$managers = [];
				foreach ($managersCompleted as $id_manager) {
					$managerInfo = Modules::run('users/_get', $id_manager, ['columns' => 'user_nicename']);
					if(empty($itemsByManager[$id_manager]) || empty($managerInfo)){
						continue;
					}

					$categoriesId = [];
					$categoriesCounters = [];
					foreach ($itemsByManager[$id_manager] as $item) {
						$categoriesId[] = $item['id_category'];
						$categoriesCounters[$item['id_category']] = (int) @$categoriesCounters[$item['id_category']] + 1;
					}

					if(empty($categoriesId)){
						continue;
					}
	
					$allCategoriesId = [];
					$categories = $this->categories->handler_get_all(array('id_category' => $categoriesId));
					foreach ($categories as $category) {
						$category_breads = json_decode('['.$category['category_breadcrumbs'].']', true);
						foreach ($category_breads as $category_bread) {
							$allCategoriesId[] = (int) $category_bread['category_id'];
						}
					}

					if(empty($allCategoriesId)){
						continue;
					}
					
					$categoriesItems = arrayByKey($itemsByManager[$id_manager], 'id_category', true);
					$allCategories = $this->categories->handler_get_all(array('id_category' => $allCategoriesId));
					$allCategories = arrayByKey($allCategories, 'category_id');
					foreach ($allCategories as $category_key => $category) {
						if(!isset($allCategories[$category_key]['issues_count'])){
							$allCategories[$category_key]['issues_count'] = 0;
						}

						if(array_key_exists($category_key, $categoriesItems)){
							$allCategories[$category_key]['products'] = $categoriesItems[$category_key];
						}

						if(array_key_exists($category_key, $categoriesCounters)){
							$allCategories[$category_key]['issues_count'] = $categoriesCounters[$category_key];
							$category_breads = json_decode('['.$category['category_breadcrumbs'].']', true);
							foreach ($category_breads as $category_bread) {
								if($category_key == $category_bread['category_id']){
									continue;
								}

								if(!isset($allCategories[$category_bread['category_id']]['issues_count'])){
									$allCategories[$category_bread['category_id']]['issues_count'] = 0;
								}

								$allCategories[$category_bread['category_id']]['issues_count'] += $categoriesCounters[$category_key];
							}
						}
					}

					$displayItemsList = false;
					if($this->input->post('show_items')){
						$displayItemsList = true;
					}
					
					$treeCategories = arrayByKey($allCategories, 'category_parent', true);
					$content_categories = $this->categoriesTreeIssuesTable($treeCategories, 0, $allCategories, $displayItemsList);
					$managers[$id_manager] = [
						'info' => $managerInfo,
						'categories' => $content_categories,
						'totals' => 0
					];
				}
				jsonResponse('Success', 'success', array('managers' => $managers));
			break;
		}
	}
	
	private function categoriesTreeTable($tree, $category_parent = 0, $categories_list) {
        if (empty($tree[$category_parent])){
            return;
		}
		
        $str = '';
        foreach ($tree[$category_parent] as $k => $row) {
			$parent_category = (!empty($categories_list[$row['category_parent']]))?$categories_list[$row['category_parent']]:array();
			$row['edit_items_count'] = $categories_list[$row['category_id']]['edit_items_count'];
			$row['edit_items_count_moderated'] = (int) @$categories_list[$row['category_id']]['edit_items_count_moderated'];
			$row['edit_item_price'] = (float) @$categories_list[$row['category_id']]['edit_item_price'];
			
            $str .= $this->load->view($this->theme->apanel_view($this->view_module_path . 'categories_table_tree'), array('category' => $row, 'category_parent' => $parent_category), true);
            if (isset($tree[$row['category_id']])){
                $str .= $this->categoriesTreeTable($tree, $row['category_id'], $categories_list);
			}
        }

        return $str;
	}
	
	private function categoriesTreeModerationTable($tree, $category_parent = 0, $categories_list) {
        if (empty($tree[$category_parent])){
            return;
		}
		
        $str = '';
        foreach ($tree[$category_parent] as $k => $row) {
			$parent_category = (!empty($categories_list[$row['category_parent']]))?$categories_list[$row['category_parent']]:array();
			$row['moderated_count'] = $categories_list[$row['category_id']]['moderated_count'];
			$row['moderate_price'] = (float) @$categories_list[$row['category_id']]['moderate_price'];
			
            $str .= $this->load->view($this->theme->apanel_view($this->view_module_path . 'categories_tree_moderation_table'), array('category' => $row, 'category_parent' => $parent_category), true);
            if (isset($tree[$row['category_id']])){
                $str .= $this->categoriesTreeModerationTable($tree, $row['category_id'], $categories_list);
			}
        }

        return $str;
	}
	
	private function categoriesTreeIssuesTable($tree, $category_parent = 0, $categories_list, $displayItemsList = false) {
        if (empty($tree[$category_parent])){
            return;
		}
		
        $str = '';
        foreach ($tree[$category_parent] as $k => $row) {
			$parent_category = (!empty($categories_list[$row['category_parent']]))?$categories_list[$row['category_parent']]:array();
			$row['issues_count'] = $categories_list[$row['category_id']]['issues_count'];

            $str .= $this->load->view($this->theme->apanel_view($this->view_module_path . 'categories_tree_issues_table'), array('category' => $row, 'category_parent' => $parent_category, 'displayItemsList' => $displayItemsList, 'hasChildren' => !empty($tree[$row['category_id']])), true);
            if (isset($tree[$row['category_id']])){
                $str .= $this->categoriesTreeIssuesTable($tree, $row['category_id'], $categories_list, $displayItemsList);
			}
        }

        return $str;
	}
}
