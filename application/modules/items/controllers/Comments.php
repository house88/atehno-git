<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Comments extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/items/comments/';
		$this->active_menu = 'catalog';
		$this->active_submenu = 'comments';

		$this->data = array();

		$this->load->model("Comments_model", "comments");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}
	
	function index(){
		checkPermision('manage_reviews', '/admin');

		$this->data['page_header'] = 'Отзывы';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->data['dt_filter'] = $this->theme->apanel_view($this->view_module_path . 'filter_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'edit':
				checkPermisionAjax('manage_reviews');

				$id_comment = (int) $this->uri->segment(5);
				$this->data['comment'] = $this->comments->handler_get($id_comment);
				if(empty($this->data['comment'])){
					jsonResponse('Коментарий не найден.');
				}

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'edit':
				checkPermisionAjax('manage_reviews');

				$this->form_validation->set_rules('comment', 'Коментарий', 'required|xss_clean');
				$this->form_validation->set_rules('text', 'Текст', 'required|xss_clean|max_length[500]');
				$this->form_validation->set_rules('text_reply', 'Ответ', 'xss_clean|max_length[1000]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_comment = (int) $this->input->post('comment');
				$comment = $this->comments->handler_get($id_comment);
				if(empty($comment)){
					jsonResponse('Коментарий не найден.');
				}

				$update = array(
					'comment_text' => $this->input->post('text', true),
					'comment_text_reply' => $this->input->post('text_reply', true)
				);

				if(!empty($update['comment_text_reply']) && $comment['comment_text_reply'] != $update['comment_text_reply']){
					$update['comment_date_reply'] = date('Y-m-d H:i:s');
				} elseif(!empty($comment['comment_text_reply']) && empty($update['comment_text_reply'])){
					$update['comment_date_reply'] = date('0000-00-00 00:00:00');
				}

				$this->comments->handler_update($id_comment, $update);

				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				checkPermisionAjax('manage_reviews');

				$this->form_validation->set_rules('comment', 'Коментарий', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_comment = (int) $this->input->post('comment');
				$comment = $this->comments->handler_get($id_comment);
				if(empty($comment)){
					jsonResponse('Коментарий не найден.');
				}

				$this->comments->handler_delete($id_comment);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_reviews');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart')
				);

				if($this->input->post('comment_date')){
					list($comment_date_from, $comment_date_to) = explode(' - ', $this->input->post('comment_date'));

					if(validateDate($comment_date_from, 'd.m.Y')){
						$params['comment_date_from'] = getDateFormat($comment_date_from, 'd.m.Y', 'Y-m-d');
					}

					if(validateDate($comment_date_to, 'd.m.Y')){
						$params['comment_date_to'] = getDateFormat($comment_date_to, 'd.m.Y', 'Y-m-d');
					}
				}
		
				if($this->input->post('comment_date_reply')){
					list($comment_date_reply_from, $comment_date_reply_to) = explode(' - ', $this->input->post('comment_date_reply'));

					if(validateDate($comment_date_reply_from, 'd.m.Y')){
						$params['comment_date_reply_from'] = getDateFormat($comment_date_reply_from, 'd.m.Y', 'Y-m-d');
					}

					if(validateDate($comment_date_reply_to, 'd.m.Y')){
						$params['comment_date_reply_to'] = getDateFormat($comment_date_reply_to, 'd.m.Y', 'Y-m-d');
					}
				}
		
				if($this->input->post('keywords')){
					$params['keywords'] = $this->input->post('keywords', true);
				}
		
				$records = $this->comments->handler_get_all($params);
				$records_total = $this->comments->handler_get_count($params);

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){
						return array(
							'dt_name'		=>  '<div class="d-flex flex-column">
													<p class="custom-margin-bottom-5">
														<strong>Имя: </strong> '. $record['comment_username'] .'
													</p>
													<p class="custom-margin-bottom-0">
														<strong>Email: </strong> '. $record['comment_user_email'] .'
													</p>
												</div>',
							'dt_text'		=>  '<div class="d-flex flex-column">
													<p class="custom-margin-bottom-5">
														<strong>Коментарий: </strong> '. $record['comment_text'] .'
													</p>
													<p class="custom-margin-bottom-0">
														<strong>Ответ: </strong> '. (!empty($record['comment_text_reply']) ? $record['comment_text_reply'] : '&mdash;') .'
													</p>
												</div>',
							'dt_date'		=>  getDateFormat($record['comment_date']),
							'dt_actions'	=> '<div class="dropdown">
													<a data-toggle="dropdown" href="#" aria-expanded="false">
														<i class="fas fa-ellipsis-h"></i>
													</a>
													<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/comments/popup/edit/' . $record['id_comment']).'">
															<i class="fad fa-pencil"></i> Редактировать
														</a>
														<div class="dropdown-divider"></div>
														<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите удалить тип цены?" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-callback="delete_action" data-comment="'.$record['id_comment'].'" data-type="bg-danger">
															<i class="fad fa-trash-alt"></i> Удалить
														</a>'.
														(!empty($record['item_url']) ?
														'<div class="dropdown-divider"></div>
														<a href="'. site_url('products/'.$record['item_url']) .'" class="dropdown-item">
															<i class="fad fa-link"></i> Страница товара
														</a>' : '') .
													'</div>
												</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
		}
	}
}
