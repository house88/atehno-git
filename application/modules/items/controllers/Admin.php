<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}
		
		$this->view_module_path = 'modules/items/';
		$this->active_menu = 'catalog';
		$this->active_submenu = 'items';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['show_moderator_date_filters'] = false;

		$this->load->model("Items_model", "items");
		$this->load->model("Prices_model", "prices");
		$this->load->model("categories/Categories_model", "categories");
		$this->load->model("properties/Properties_model", "properties");
		$this->load->model("brands/Brands_model", "brands");
		$this->load->model("users/User_model", "users");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		checkPermision('edit_items_full,edit_items_simple,moderate_items');

		// GET CURRENCIES
		$this->data['currency'] = Modules::run('currency/_get_default_apanel');

		$category_params = array(
			'parent' => 0
		);

		$manager_params = array();

		if($this->lauth->have_right('edit_items_full')){
			$manager_params = array(
				'group_type' => array(
					'content_menedger','moderator','admin'
				)
			);
		} elseif($this->lauth->have_right('moderate_items')){
			$manager_params = array(
				'group_type' => array(
					'content_menedger'
				)
			);
		} else{
			$user_info = $this->lauth->user_data();
			if(!empty($user_info->user_categories_access_full) && !$this->lauth->have_right('edit_items_full')){
				$category_params['id_category'] = array_filter(array_map('intval', explode(',', $user_info->user_categories_access_full)));
			}
		}

		$this->data['categories'] = $this->categories->handler_get_all($category_params);		
		$this->data['content_menedgers'] = !empty($manager_params) ? $this->users->handler_get_all($manager_params) : array();
		
		$this->data['brands'] = Modules::run('brands/_get_all');
		$this->data['page_header'] = 'Список товаров';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->data['dt_filter'] = $this->theme->apanel_view($this->view_module_path . 'filter_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function add(){
		if(!$this->lauth->have_right('edit_items_full')){
			redirect('/admin');
		}

		$categories = $this->categories->handler_get_tree();
		$this->data['categories'] = $this->categories->tree_select($categories, 0, 0);
		$this->data['price_variants'] = $this->prices->handler_get_all(array('active' => 1));
		$this->data['brands_list'] = $this->brands->handler_get_all();
		$this->data['main_content'] = 'admin/add';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		if(!$this->lauth->have_right_or('edit_items_full,edit_items_simple,moderate_items')){
			redirect('/admin');
		}

		$id_item = (int)$this->uri->segment(4);
		$this->data['item'] = $this->items->handler_get($id_item);
		if(empty($this->data['item'])){
			redirect('admin/items');
		}

		$this->data['item_category'] = $this->categories->handler_get($this->data['item']['id_category']);
		
		$this->data['aditional_fields'] = $this->categories->handler_get_additional_fields($this->data['item_category']);

		$this->data['brands_list'] = $this->brands->handler_get_all();
		$this->data['supliers'] = arrayByKey($this->items->handler_get_suppliers_all(), 'supplier_prog_id');
		if($this->lauth->have_right('edit_items_full')){
			$this->data['price_variants'] = $this->prices->handler_get_all(array('active' => 1));

			$this->load->model("credit/Credit_model", "credit");
			$credit_program = $this->credit->handler_get('iute_credit');
			$this->data['credit_settings'] = json_decode($credit_program['setting_data'], true);

			uasort($this->data['credit_settings'], function($c1, $c2){
				if ((int) $c1['months'] === (int) $c2['months']) {
					return 0;
				}

				return ((int) $c1['months'] < (int) $c2['months']) ? -1 : 1;
			});

			$this->data['promo_list'] = Modules::run('promo/_get_all');

			$item_promo_list = Modules::run('promo/_get_item_all', $id_item);
			$this->data['item_promo'] = array();
			if(!empty($item_promo_list)){
				foreach ($item_promo_list as $relation) {
					$this->data['item_promo'][] = $relation['id_promo'];
				}
			}
		}

		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'edit_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'edit_scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				if(!$this->lauth->have_right('edit_items_full')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$this->form_validation->set_rules('category_parent', 'Категория', 'required|xss_clean');
				$this->form_validation->set_rules('brand', 'Брэнд', 'required|xss_clean');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[2000]');
				$this->form_validation->set_rules('price', 'Цена', 'required|xss_clean');
				$this->form_validation->set_rules('temp_price', 'Temp price', 'xss_clean');
				$this->form_validation->set_rules('quantity', 'Количество', 'required|xss_clean');
				$this->form_validation->set_rules('product_code', 'Артикул', 'required|xss_clean');
				$this->form_validation->set_rules('small_description', 'Краткое описание', 'xss_clean');
				$this->form_validation->set_rules('mk', 'Meta keywords', 'xss_clean');
				$this->form_validation->set_rules('md', 'Meta description', 'xss_clean');
				$this->form_validation->set_rules('photo', 'Логотип', 'xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/items/'.$remove_photo);
					}
				}

				$config = array(
					'table' => 'items',
					'id' => 'id_item',
					'field' => 'item_url',
					'title' => 'item_title',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$insert = array(
					'id_category' => (int)$this->input->post('category_parent'),
					'id_brand' => (int)$this->input->post('brand'),
					'item_title' => $this->input->post('title'),
					'item_url' => $this->slug->create_slug($this->input->post('title')),
					'item_price' => (float)$this->input->post('price'),
					'item_temp_price' => (float)$this->input->post('temp_price'),
					'item_discount' => (int)$this->input->post('discount'),
					'item_quantity' => (int)$this->input->post('quantity'),
					'item_code' => $this->input->post('product_code'),
					'item_guaranty' => (int)$this->input->post('product_guaranty')
				);

				if($this->input->post('video')){
					$this->load->library('Videothumb', 'videothumb');
					$video_info = $this->videothumb->process($this->input->post('video'));
					
					if(empty($video_info['error'])) {
						$path = 'files/video';
						create_dir($path);
						$config = array(
							'source_image'      => $video_info['image'],
							'create_thumb'      => false,
							'new_image'         => FCPATH . $path,
							'maintain_ratio'    => true,
							'width'             => 800,
							'height'            => 450
						);
						$this->load->library('image_lib');
						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						
						$insert['item_video_source'] = $video_info['type'];
						$insert['item_video_image'] = $video_info['image'];
						$insert['item_video_link'] = $this->input->post('video', true);
						$insert['item_video_code'] = $video_info['v_id'];
					}
				}

				if($this->input->post('small_description')){
					$insert['item_small_description'] = $this->input->post('small_description');
				}

				if($this->input->post('description')){
					$this->load->helper('htmlpurifier');

					$insert['item_description'] = $this->input->post('description');
					$insert['item_description_cleaned'] = html_purify($this->input->post('description'), 'item_description');
				}

				if($this->input->post('mk')){
					$insert['mk'] = $this->input->post('mk');
				}

				if($this->input->post('md')){
					$insert['md'] = $this->input->post('md');
				}

				if($this->input->post('photo')){
					$insert['item_photo'] = $this->input->post('photo');
				}

				if($this->input->post('photos')){
					$insert['item_photos'] = json_encode($this->input->post('photos'));
				}

				if($this->input->post('visible')){
					$insert['item_visible'] = 1;
				}

				if($this->input->post('hit')){
					$insert['item_hit'] = 1;
				}

				if($this->input->post('newest')){
					$insert['item_newest'] = 1;
				}

				if($this->input->post('action')){
					$insert['item_action'] = 1;
				}

				if($this->input->post('popular')){
					$update['item_popular'] = 1;
				}

				if($this->input->post('on_home')){
					$update['item_on_home'] = 1;
				}

                $vprices = $this->input->post('vprice');
                $price_variants = $this->prices->handler_get_all();
                foreach($price_variants as $price_variant){
                    if(isset($vprices[$price_variant['id_price_variant']])){
                        $insert['item_price_'.$price_variant['id_price_variant']] = (float)$vprices[$price_variant['id_price_variant']];
                    } else{
                        $insert['item_price_'.$price_variant['id_price_variant']] = 0;
                    }
                }

				$id_item = $this->items->handler_insert($insert);

				$item_properties = $this->input->post('properties');
				if(!empty($item_properties)){
					$insert_item_properties = array();
					foreach($item_properties as $key=>$properties){
						switch($key){
							case 'simple':
								foreach($properties as $property_key => $property){
									if(!empty($property)){
										$insert_item_properties[] = array(
											'id_item' => $id_item,
											'id_property' => $property_key,
											'value' => $property
										);
									}
								}
							break;
							case 'range':
								foreach($properties as $property_key => $property){
									if(!empty($property)){
										$insert_item_properties[] = array(
											'id_item' => $id_item,
											'id_property' => $property_key,
											'value' => $property
										);
									}
								}
							break;
							case 'select':
								foreach($properties as $property_key => $property){
									if(!empty($property)){
										$insert_item_properties[] = array(
											'id_item' => $id_item,
											'id_property' => $property_key,
											'value' => $property
										);
									}
								}
							break;
							case 'multiselect':
								foreach($properties as $property_key => $property_values){
									if(!empty($property_values)){
										foreach($property_values as $property_value){
											$insert_item_properties[] = array(
												'id_item' => $id_item,
												'id_property' => $property_key,
												'value' => $property_value
											);
										}
									}
								}
							break;
						}
					}

					if(!empty($insert_item_properties)){
						$this->properties->handler_insert_items_properties_values($insert_item_properties);
					}
				}
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				if(!$this->lauth->have_right_or('edit_items_full,edit_items_simple,moderate_items')){
					jsonResponse('Ошибка: Нет прав!');
				}
	
				$id_item = (int)$this->input->post('item');
				$item_info = $this->items->handler_get($id_item);
				if(empty($item_info)){
					jsonResponse('Ошибка: Товар не найден.');
				}

				$item_category = $this->categories->handler_get($item_info['id_category']);
				$aditional_fields = $this->categories->handler_get_additional_fields($item_category);

				$checkIfIsCompleted = false;
				if($this->input->post('data_completed')){
					$checkIfIsCompleted = true;

					if($this->input->post('data_bug')){
						$checkIfIsCompleted = false;
					}
				}

				
				$can_complete = false;
				$is_formatted_description = false;

				if($this->lauth->have_right('edit_items_full')){
					$this->form_validation->set_rules('price', 'Цена', 'required|xss_clean');
					$this->form_validation->set_rules('temp_price', 'Temp price', 'xss_clean');
					$this->form_validation->set_rules('cashback_price', 'Кэшбэк', 'xss_clean');
					$this->form_validation->set_rules('discount', 'Скидка', 'xss_clean');
					$this->form_validation->set_rules('quantity', 'Количество', 'required|xss_clean');				
					$this->form_validation->set_rules('zero_credit_months', 'Кол. месяцев для Кредита 0%', 'xss_clean');
				}

				if(true === $checkIfIsCompleted && !empty($aditional_fields)){
					$this->form_validation->set_rules('brand', 'Брэнд', 'required|xss_clean');

					if(@$aditional_fields['small_description']['active'] == 1){
						$this->form_validation->set_rules('small_description', 'Краткое описание', 'required|xss_clean');
					} else{
						$this->form_validation->set_rules('small_description', 'Краткое описание', 'xss_clean');
					}

					if(@$aditional_fields['full_description']['active'] == 1){
						$this->form_validation->set_rules('description', 'Полное описание', 'required');
					}

					if(@$aditional_fields['mk']['active'] == 1){
						$this->form_validation->set_rules('mk', 'Meta keywords', 'required|xss_clean');
					} else{
						$this->form_validation->set_rules('mk', 'Meta keywords', 'xss_clean');
					}

					if(@$aditional_fields['md']['active'] == 1){
						$this->form_validation->set_rules('md', 'Meta description', 'required|xss_clean');
					} else{
						$this->form_validation->set_rules('md', 'Meta description', 'xss_clean');
					}

					if(@$aditional_fields['item_photo']['active'] == 1){
						$this->form_validation->set_rules('photo', 'Логотип', 'required|xss_clean');
					} else{
						$this->form_validation->set_rules('photo', 'Логотип', 'xss_clean');
					}
					
					if(@$aditional_fields['video']['active'] == 1){
						$this->form_validation->set_rules('video', 'Видео', 'required|valid_url|xss_clean');
					} else{
						$this->form_validation->set_rules('video', 'Видео', 'valid_url|xss_clean');
					}
				} else{
					$this->form_validation->set_rules('brand', 'Брэнд', 'xss_clean');
					$this->form_validation->set_rules('small_description', 'Краткое описание', 'xss_clean');
					$this->form_validation->set_rules('mk', 'Meta keywords', 'xss_clean');
					$this->form_validation->set_rules('md', 'Meta description', 'xss_clean');
					$this->form_validation->set_rules('photo', 'Логотип', 'xss_clean');
					$this->form_validation->set_rules('video', 'Видео', 'valid_url|xss_clean');
				}

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/items/'.$id_item.'/'.$remove_photo);
						@unlink('files/items/'.$id_item.'/thumb_500x500_'.$remove_photo);
					}
				}

				$photos = $this->input->post('photos');
				if(true === $checkIfIsCompleted && !empty($aditional_fields)){
					if($aditional_fields['item_photos']['active'] == 1){
						if(empty($photos) || !is_array($photos)){
							jsonResponse('Ошибка: Для завершения редактирования товара Дополнительные фото обязательны к заполнению.');
						}
						
						$required_photos = (isset($aditional_fields['item_photos']['count']))?(int)$aditional_fields['item_photos']['count']:0;
						if(count($photos) < $required_photos){
							jsonResponse("Ошибка: Для завершения редактирования товара нужно загрузить мин. {$required_photos} Дополнительных фото.");							
						}
					}
				}

				$is_formatted_description = $this->input->post('is_formatted_description') ? true : false;
				$this->load->helper('htmlpurifier');

				$update = array(
					'id_brand' => (int)$this->input->post('brand'),
					'item_small_description' => $this->input->post('small_description'),
					'item_description' => $this->input->post('description'),
					'item_description_cleaned' => $is_formatted_description ? $this->input->post('description') : html_purify($this->input->post('description'), 'item_description'),
					'mk' => $this->input->post('mk'),
					'md' => $this->input->post('md'),
					'item_photo' => $this->input->post('photo') ? $this->input->post('photo') : '',
					'item_photos' => empty($photos) ? '' : json_encode($photos)
				);
				if($this->lauth->have_right('edit_items_full')){
					$update['item_price'] = (float) $this->input->post('price');
					$update['item_temp_price'] = (float) $this->input->post('temp_price');
					$update['item_cashback_price'] = (float) $this->input->post('cashback_price');
					$update['item_quantity'] = (int)$this->input->post('quantity');
					$update['item_guaranty'] = (int)$this->input->post('product_guaranty');
					$update['item_visible'] = ($this->input->post('visible')) ? 1 : 0;
					$update['item_hit'] = ($this->input->post('hit')) ? 1 : 0;
					$update['item_newest'] = ($this->input->post('newest')) ? 1 : 0;
					$update['item_action'] = ($this->input->post('action') || $update['item_cashback_price'] > 0) ? 1 : 0;
					$update['item_popular'] = ($this->input->post('popular')) ? 1 : 0;
					$update['item_on_home'] = ($this->input->post('on_home')) ? 1 : 0;
					$update['item_zero_credit_months'] = (int) $this->input->post('zero_credit_months');
					$update['item_zero_credit'] = $update['item_zero_credit_months'] > 0 ? 1 : 0;
					
					$vprices = $this->input->post('vprice');
					$price_variants = $this->prices->handler_get_all();
					foreach($price_variants as $price_variant){
						if(isset($vprices[$price_variant['id_price_variant']])){
							$update['item_price_'.$price_variant['id_price_variant']] = (float)$vprices[$price_variant['id_price_variant']];
						} else{
							$update['item_price_'.$price_variant['id_price_variant']] = 0;
						}
					}

					Modules::run('promo/_delete_item_promo', $id_item);
					$item_promo = $this->input->post('item_promo');
					if(!empty($item_promo)){
						$insert_item_promo = array();
						foreach($item_promo as $promo){
							$promo = (int) $promo;

							if($promo > 0){
								$insert_item_promo[] = array(
									'id_promo' => $promo,
									'id_item' => $id_item
								);
							}
						}

						if(!empty($insert_item_promo)){
							Modules::run('promo/_set_item_promo', $insert_item_promo);
						}
					}
				}

				if($this->input->post('video') != $item_info['item_video_link'] ){
					$this->load->library('Videothumb', 'videothumb');
					$video_info = $this->videothumb->process($this->input->post('video'));
					
					if(empty($video_info['error'])) {
						$path = 'files/video';
						@unlink($item_info['item_video_image']);
						create_dir($path);
						$config = array(
							'source_image'      => $video_info['image'],
							'create_thumb'      => false,
							'new_image'         => FCPATH . $path,
							'maintain_ratio'    => true,
							'width'             => 800,
							'height'            => 450
						);
						$this->load->library('image_lib');
						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						
						$update['item_video_source'] = $video_info['type'];
						$update['item_video_image'] = $video_info['image'];
						$update['item_video_link'] = $this->input->post('video', true);
						$update['item_video_code'] = $video_info['v_id'];
					} else{
						jsonResponse($video_info['error']);
					}
				}

				$manageItemDate = date('Y-m-d H:i:s');
				$currentUser = $this->lauth->user_data();
				$changeLog = [];

				if((int) $item_info['item_data_start'] === 0){
					$update['item_data_start'] = 1;
					$update['item_data_start_date'] = $manageItemDate;

					$changeLog[] = [
						'id_item' => $id_item,
						'id_user' => $this->lauth->id_user(),
						'created_on' => $manageItemDate,
						'text' => 'Начал редактирование товара.',
						'changes' => NULL
					];
				}

				$itemStates = Modules::run('items/moderate/_getStates', (int) $id_item);
				$updateStates = [
					'id_category' => (int) $item_info['id_category']
				];
				if($this->input->post('data_completed')){
					$update['item_data_completed'] = 1;
					$updateStates['is_filled'] = 1;

					if((int) $itemStates['is_filled'] === 0){
						$update['item_user_edit'] = $this->lauth->id_user();
						$update['item_data_completed_date'] = $manageItemDate;

						$updateStates['id_manager'] = $this->lauth->id_user();
						$updateStates['filled_on'] = $manageItemDate;

						$changeLog[] = [
							'id_item' => $id_item,
							'id_user' => $this->lauth->id_user(),
							'created_on' => $manageItemDate,
							'text' => 'Пометил товар как Заверщенный.',
							'changes' => NULL
						];

						Modules::run('items/moderate/_setFill', [
							'id_item' => $id_item,
							'id_category' => $item_info['id_category'],
							'id_manager' => $this->lauth->id_user()
						]);
					} else if((int) $itemStates['id_manager'] === 0){
						$update['item_user_edit'] = $this->lauth->id_user();
						$update['item_data_completed_date'] = $manageItemDate;

						$updateStates['id_manager'] = $this->lauth->id_user();
						$updateStates['filled_on'] = $manageItemDate;

						$changeLog[] = [
							'id_item' => $id_item,
							'id_user' => $this->lauth->id_user(),
							'created_on' => $manageItemDate,
							'text' => 'Товар был пемечен как Заверщенный, но небыло информации о пользователе. Новый пользователь ответственен за информацию о товаре являеться: ' . $currentUser->user_nicename,
							'changes' => NULL
						];
					}
				} else{
					$update['item_data_completed'] = 0;

					$updateStates['is_filled'] = 0;
					$updateStates['id_manager'] = NULL;
					$updateStates['filled_on'] = NULL;

					if((int) $itemStates['is_filled'] === 1){
						$changeLog[] = [
							'id_item' => $id_item,
							'id_user' => $this->lauth->id_user(),
							'created_on' => $manageItemDate,
							'text' => 'Убрал признак Пометить как завершенный.',
							'changes' => NULL
						];
					}
				}

				if($this->lauth->have_right('moderate_items')){
					if($this->input->post('data_moderated')){
						$updateStates['is_moderated'] = 1;
						
						if((int) $itemStates['is_moderated'] === 0){
							$update['item_data_moderated'] = 1;
							$update['item_user_moderate'] = $this->lauth->id_user();
							$update['item_data_moderated_date'] = $manageItemDate;

							$updateStates['id_moderator'] = $this->lauth->id_user();
							$updateStates['moderated_on'] = $manageItemDate;

							$changeLog[] = [
								'id_item' => $id_item,
								'id_user' => $this->lauth->id_user(),
								'created_on' => $manageItemDate,
								'text' => 'Пометил товар как Модерирован.',
								'changes' => NULL
							];

							Modules::run('items/moderate/_setDone', [
								'id_item' => $id_item,
								'id_category' => $item_info['id_category'],
								'id_moderator' => $this->lauth->id_user(),
								'id_manager' => $item_info['item_user_edit']
							]);
						} else if((int) $itemStates['id_moderator'] === 0){
							$update['item_user_moderate'] = $this->lauth->id_user();
							$update['item_data_moderated_date'] = $manageItemDate;
							
							$updateStates['id_moderator'] = $this->lauth->id_user();
							$updateStates['moderated_on'] = $manageItemDate;

							$changeLog[] = [
								'id_item' => $id_item,
								'id_user' => $this->lauth->id_user(),
								'created_on' => $manageItemDate,
								'text' => 'Товар был пемечен как Модерирован, но небыло информации о пользователе. Новый пользователь ответственен за Модерацию являеться: ' . $currentUser->user_nicename,
								'changes' => NULL
							];
						}
					} else{
						$update['item_data_moderated'] = 0;

						$updateStates['is_moderated'] = 0;
						$updateStates['id_moderator'] = NULL;
						$updateStates['moderated_on'] = NULL;

						if((int) $itemStates['is_moderated'] === 1){
							$changeLog[] = [
								'id_item' => $id_item,
								'id_user' => $this->lauth->id_user(),
								'created_on' => $manageItemDate,
								'text' => 'Убрал признак Модерирован.',
								'changes' => NULL
							];
						}
					}

					if($this->input->post('data_bug')){
						$updateStates['has_issue'] = 1;

						if((int) $itemStates['has_issue'] === 0){
							$update['item_user_moderate_bug'] = $this->lauth->id_user();
							$update['item_data_moderated_bug'] = 1;
							$update['item_data_moderated_bug_text'] = $this->input->post('data_bug_text');
							$update['item_data_moderated_bug_date'] = $manageItemDate;
							
							$updateStates['id_issuer'] = $this->lauth->id_user();
							$updateStates['issue_text'] = $this->input->post('data_bug_text');
							$updateStates['issued_on'] = $manageItemDate;

							$changeLog[] = [
								'id_item' => $id_item,
								'id_user' => $this->lauth->id_user(),
								'created_on' => $manageItemDate,
								'text' => 'Пометил товар как Информация заполнена с ошибками. Примечание: '. $update['item_data_moderated_bug_text'],
								'changes' => NULL
							];

							Modules::run('items/moderate/_setIssue', [
								'id_item' => $id_item,
								'id_category' => $item_info['id_category'],
								'id_issuer' => $this->lauth->id_user(),
								'id_manager' => $item_info['item_user_edit']
							]);
						} else if((int) $itemStates['id_issuer'] === 0){
							$update['item_user_moderate_bug'] = $this->lauth->id_user();
							$update['item_data_moderated_bug_text'] = $this->input->post('data_bug_text');
							$update['item_data_moderated_bug_date'] = $manageItemDate;
							
							
							$updateStates['id_issuer'] = $this->lauth->id_user();
							$updateStates['issue_text'] = $this->input->post('data_bug_text');
							$updateStates['issued_on'] = $manageItemDate;

							$changeLog[] = [
								'id_item' => $id_item,
								'id_user' => $this->lauth->id_user(),
								'created_on' => $manageItemDate,
								'text' => 'Товар был пемечен как Информация заполнена с ошибками, но небыло информации о пользователе. Новый пользователь ответственен за пометку об ошибке являеться: ' . $currentUser->user_nicename . ' Примечание: '. $update['item_data_moderated_bug_text'],
								'changes' => NULL
							];
						} else if($itemStates['issue_text'] !== $this->input->post('data_bug_text')){
							$changeLog[] = [
								'id_item' => $id_item,
								'id_user' => $this->lauth->id_user(),
								'created_on' => $manageItemDate,
								'text' => 'Обновил текст примечания для флага Информация заполнена с ошибками.',
								'changes' => json_encode([
									'item_data_moderated_bug_text' => [
										'old' => $itemStates['issue_text'],
										'new' => $this->input->post('data_bug_text'),
									]
								])
							];
						}
					} else{
						$update['item_data_moderated_bug'] = 0;
						$update['item_data_moderated_bug_text'] = '';
						
						$updateStates['has_issue'] = 0;
						$updateStates['id_issuer'] = NULL;
						$updateStates['issue_text'] = NULL;
						$updateStates['issued_on'] = NULL;

						if((int) $itemStates['has_issue'] === 1){
							$changeLog[] = [
								'id_item' => $id_item,
								'id_user' => $this->lauth->id_user(),
								'created_on' => $manageItemDate,
								'text' => 'Убрал признак Информация заполнена с ошибками.',
								'changes' => json_encode([
									'item_data_moderated_bug_text' => [
										'old' => $itemStates['issue_text'],
										'new' => '&mdash;',
									]
								])
							];
						}
					}
				}

				Modules::run('items/moderate/_updateStates', $id_item, $updateStates);

				$properties_required = false;
				if(true === $checkIfIsCompleted && !empty($aditional_fields) && $aditional_fields['properties']['active'] == 1){
					$properties_required = true;
				}

				$item_properties = $this->input->post('properties');
				if(!empty($item_properties)){
					$insert_item_properties = array();
					$all_properties_selected = true;
					foreach($item_properties as $key=>$properties){
						switch($key){
							case 'simple':
								foreach($properties as $property_key => $property){
									if(!empty($property)){
										$insert_item_properties[] = array(
											'id_item' => $id_item,
											'id_property' => $property_key,
											'value' => $property
										);
									} elseif($properties_required){
										$all_properties_selected = false;
									}
								}
							break;
							case 'range':
								foreach($properties as $property_key => $property){
									if(!empty($property)){
										$insert_item_properties[] = array(
											'id_item' => $id_item,
											'id_property' => $property_key,
											'value' => $property
										);
									} elseif($properties_required){
										$all_properties_selected = false;
									}
								}
							break;
							case 'select':
								foreach($properties as $property_key => $property){
									if(!empty($property)){
										$insert_item_properties[] = array(
											'id_item' => $id_item,
											'id_property' => $property_key,
											'value' => $property
										);
									} elseif($properties_required){
										$all_properties_selected = false;
									}
								}
							break;
							case 'multiselect':
								foreach($properties as $property_key => $property_values){
									if(!empty($property_values)){
										foreach($property_values as $property_value){
											$insert_item_properties[] = array(
												'id_item' => $id_item,
												'id_property' => $property_key,
												'value' => $property_value
											);
										}
									} elseif($properties_required){
										$all_properties_selected = false;
									}
								}
							break;
						}
					}

					if($properties_required && !$all_properties_selected){
						jsonResponse("Ошибка: Для завершения редактирования товара нужно заполнить все свойства.");
					}

					$this->properties->handler_delete_items_properties_values($id_item);
					if(!empty($insert_item_properties)){
						$this->properties->handler_insert_items_properties_values($insert_item_properties);
					}
				} elseif($properties_required){
					jsonResponse("Ошибка: Для завершения редактирования товара нужно заполнить все свойства.");
				}
				
				$this->items->handler_update($id_item, $update);
				$newItemInfo = $this->items->handler_get($id_item);

				$changesOnItem = array_diff_assoc($newItemInfo, $item_info);
				$changesInLog = [];
				if(!empty($changesOnItem)){
					foreach ($changesOnItem as $changedKey => $changedValue) {
						$changesInLog[$changedKey] = [
							'old' => $item_info[$changedKey],
							'new' => $changedValue
						];
					}

					$changeLog[] = [
						'id_item' => $id_item,
						'id_user' => $this->lauth->id_user(),
						'created_on' => $manageItemDate,
						'text' => 'Внес изменения в товаре.',
						'changes' => json_encode($changesInLog)
					];
				}

				if(!empty($changeLog)){
					Modules::run('items/changelog/_set', $changeLog);
				}

				jsonResponse('Сохранено.', 'success');
			break;
			case 'copy':
				if(!$this->lauth->have_right('copy_item')){
					jsonResponse('Ошибка: Нет прав!');
				}
	
				$id_item = (int)$this->input->post('id_item');
				$item_info = $this->items->handler_get($id_item);
				if(empty($item_info)){
					jsonResponse('Ошибка: Товар не найден.');
				}
	
				$id_item_copy = (int)$this->input->post('id_item_copy');
				$item_copy_info = $this->items->handler_get_by_prog_id($id_item_copy);
				if(empty($item_copy_info)){
					jsonResponse('Ошибка: Товар с которого копировать не найден.');
				}

				$copy_fields = $this->input->post('copy_fields');
				if(!is_array($copy_fields) || empty('copy_fields')){
					jsonResponse('Выберите поля для копирования');
				}

				$item_category = $this->categories->handler_get($item_info['id_category']);
				$copy_item_category = $this->categories->handler_get($item_copy_info['id_category']);
				$update = array();

				foreach ($copy_fields as $field => $is_set) {
					switch ($field) {
						case 'brand':
							$update['id_brand'] = (int) $item_copy_info['id_brand']; 
						break;
						case 'small_description':
							$update['item_small_description'] = $item_copy_info['item_small_description']; 
						break;
						case 'description':
							$update['item_description'] = $item_copy_info['item_description']; 
						break;
						case 'main_photo':
							$path = 'files/items' ;
							$copy_file_name = $item_copy_info['item_photo'];
							if(file_exists("files/items/{$copy_file_name}")){
								$config = array(
									'table' => 'items',
									'id' => 'item_prog_id',
									'field' => 'item_url',
									'title' => 'item_title',
									'replacement' => 'dash'
								);
								$this->load->library('slug', $config);
								$file_name = $this->slug->create_slug(cut_str($item_info['item_title'], 200).'-'.$item_info['item_prog_id']) . '.' . pathinfo("{$path}/{$copy_file_name}", PATHINFO_EXTENSION);
								copy("{$path}/{$copy_file_name}", "{$path}/{$file_name}");

								if(file_exists("{$path}/thumb_500x500_{$copy_file_name}")){
									copy("{$path}/thumb_500x500_{$copy_file_name}", "{$path}/thumb_500x500_{$file_name}");
								} else{
									$config = array(
										'source_image'      => FCPATH . "{$path}/{$copy_file_name}",
										'create_thumb'      => true,
										'thumb_marker'      => 'thumb_500x500_',
										'new_image'         => FCPATH . $path,
										'maintain_ratio'    => true,
										'width'             => 500,
										'height'            => 500
									);
							
									$this->load->library('image_lib');
									$this->image_lib->initialize($config);
									$this->image_lib->resize();
								}

								$update['item_photo'] = $file_name;
							}
						break;
						case 'other_photos':
							$this->load->library('upload');
							$this->load->library('image_lib');
							
							$path = 'files/items/';
							create_dir($path);
							$this->upload->upload_path = FCPATH . $path;
							$this->upload->validate_upload_path();
							$full_path = $this->upload->upload_path;

							$copy_files = array_filter(json_decode($item_copy_info['item_photos']));
							$photos = array();
							if(!empty($copy_files)){
								foreach ($copy_files as $key => $copy_file) {
									$file_index = $key + 1;
									$copy_file_name = $copy_file;
									if(file_exists("{$full_path}{$copy_file_name}")){
										$file_name = strForURL(cut_str($item_info['item_title'], 200).'-'.$item_info['item_prog_id']) . "_{$file_index}." . pathinfo("{$full_path}{$copy_file_name}", PATHINFO_EXTENSION);
										
										copy("{$full_path}{$copy_file_name}", "{$full_path}{$file_name}");
		
										if(file_exists("{$path}/thumb_500x500_{$copy_file_name}")){
											copy("{$path}/thumb_500x500_{$copy_file_name}", "{$path}/thumb_500x500_{$file_name}");
										} else{
											$config = array(
												'source_image'      => "{$full_path}{$file_name}",
												'create_thumb'      => true,
												'thumb_marker'      => 'thumb_500x500_',
												'new_image'         => $full_path,
												'maintain_ratio'    => true,
												'width'             => 500,
												'height'            => 500
											);
									
											$this->image_lib->initialize($config);
											$this->image_lib->resize();
										}
										
										$photos[] = $file_name;
									}
								}
							}
							
							$update['item_photos'] = json_encode($photos);
						break;
						case 'video':
							if(!empty($item_copy_info['item_video_link'])){
								$this->load->library('Videothumb', 'videothumb');
								$video_info = $this->videothumb->process($item_copy_info['item_video_link']);
								
								if(empty($video_info['error'])) {
									$this->load->library('upload');
									$this->load->library('image_lib');

									$path = 'files/video';
									create_dir($path);

									$this->upload->upload_path = FCPATH . $path;
									$this->upload->validate_upload_path();
									$full_path = $this->upload->upload_path;
									
									@unlink($item_info['item_video_image']);
									$config = array(
										'source_image'      => $video_info['image'],
										'create_thumb'      => false,
										'new_image'         => FCPATH . $path,
										'maintain_ratio'    => true,
										'width'             => 800,
										'height'            => 450
									);
									$this->image_lib->initialize($config);
									$this->image_lib->resize();
									
									$update['item_video_source'] = $video_info['type'];
									$update['item_video_image'] = $video_info['image'];
									$update['item_video_link'] = $item_copy_info['item_video_link'];
									$update['item_video_code'] = $video_info['v_id'];
								}
							}
						break;
						case 'properties':
							$properties = $this->properties->handler_get_items_properties_values($item_copy_info['id_item']);
							$insert_properties = array_map(function($element) use ($id_item){
								$element['id_item'] = $id_item;
								return $element;
							}, $properties);
							
							if(!empty($insert_properties)){
								$this->properties->handler_delete_items_properties_values($id_item);
								$this->properties->handler_insert_items_properties_values($insert_properties);
							}
						break;
					}
				}

				if(!empty($update)){
					$this->items->handler_update($id_item, $update);
				}

				$this->session->set_flashdata('flash_message', '<li class="message-success zoomIn">Копирование прошло успешно. <i class="ca-icon ca-icon_remove"></i></li>');
				
				jsonResponse('', 'success');
			break;
			case 'change_marketing_status':
				if(!$this->lauth->have_right('manage_items_marketing_fields')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$this->form_validation->set_rules('item', 'ИД товарв', 'required|xss_clean');
				$this->form_validation->set_rules('marketing', 'Тип маркетингого поля', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_item = (int)$this->input->post('item');
				$item = $this->items->handler_get($id_item);
				if(empty($item)){
					jsonResponse('Не могу завершить операцию.');
				}

				$marketing = $this->input->post('marketing');
				$valid_marketing = array('visible','hit','newest','action','popular','commented','on_home');
				if(!in_array($marketing, $valid_marketing)){
					jsonResponse('Не могу завершить операцию.');
				}

				$item_column = 'item_'.$marketing;
				if(!array_key_exists($item_column, $item)){
					jsonResponse('Не могу завершить операцию.');
				}

				if($item[$item_column]){
					$status = 0;
				} else{
					$status = 1;
				}

				$this->items->handler_update($id_item, array($item_column => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
			case 'list':
				checkPermisionAjaxDT('edit_items_full,edit_items_simple,moderate_items');

				$params = array(
					'limit' 			=> (int) $this->input->post('iDisplayLength'),
					'start' 			=> (int) $this->input->post('iDisplayStart'),
					'exclude_bad_price' => false,
					'moderation'		=> false,
					'sort_by' 			=> flat_dt_ordering($_POST, array(
						'dt_name'    => 'item_title',
						'dt_price'   => 'item_price',
						'dt_create'  => 'item_created',
						'dt_update'  => 'item_last_update'
					))
				);

				//region CURENCY
				$currency = Modules::run('currency/_get_default_apanel');
				//endregion CURENCY

				//region FILTERS
				if(isset($_POST['keywords'])){
					$params['keywords'] = $this->input->post('keywords');
				}

				$items_list = array();
				if($this->input->post('id_category')){
					$id_category = (int) $this->input->post('id_category');
					
					if($this->input->post('id_subcategory')){
						$id_category = (int) $this->input->post('id_subcategory');
					}

					$category = $this->categories->handler_get($id_category);
					if(!empty($category)){
						$categories_list = array_map('intval', (array) explode(',', $category['category_children']));
						$categories_list = array_filter($categories_list);
						$categories_list[] = $id_category;
						$params['id_category'] = $categories_list;

						$property_select = array();
						$property_range = array();
						foreach ($_POST as $key => $value) {
							if (preg_match('/^property_select_/', $key)) {
								$prop_id = explode('_', $key);
								$property_select[end($prop_id)] = $value;
							}
							if (preg_match('/^property_range_/', $key)) {
								$components = explode('_', $key);
								$property_range[$components[2]][$components[3]] = $value;
							}
						}

						if (!empty($property_select)){
							$properties_params['properties_select'] = $property_select;
						}

						if (!empty($property_range)){
							$properties_params['properties_range'] = $property_range;
						}

						if(!empty($properties_params)){
							$items_by_properties = $this->properties->handler_get_items_by_properties_and($properties_params);
							if(!empty($items_by_properties)){
								foreach ($items_by_properties as $item_by_properties) {
									$items_list[$item_by_properties['id_item']] = $item_by_properties['id_item'];
								}
							} else{
								$items_list[] = 0;
							}
						}
					}
				}

				if(!empty($items_list)){
					$items_by_properties_inverse = (int) $this->input->post('items_by_properties_inverse');
					if($items_by_properties_inverse){
						$params['not_items_list'] = $items_list;
					} else{
						$params['items_list'] = $items_list;
					}
				}
				
				if($this->input->post('brand')){
					$params['id_brand'] = (int) $this->input->post('brand');
				}
				
				if($this->input->post('item_price_from')){
					$params['item_price_from'] = $this->input->post('item_price_from') / $currency['currency_rate'];
				}

				if($this->input->post('item_price_to')){
					$params['item_price_to'] = $this->input->post('item_price_to') / $currency['currency_rate'];
				}
		
				if($this->input->post('item_last_update')){
					list($item_updated_from, $item_updated_to) = explode(' - ', $this->input->post('item_last_update'));

					if(validateDate($item_updated_from, 'd.m.Y')){
						$params['item_last_update_from'] = getDateFormat($item_updated_from, 'd.m.Y', 'Y-m-d');
					}

					if(validateDate($item_updated_to, 'd.m.Y')){
						$params['item_last_update_to'] = getDateFormat($item_updated_to, 'd.m.Y', 'Y-m-d');
					}
				}

				if($this->input->post('item_visible')){
					$params['item_visible'] = (int)$this->input->post('item_visible');
				}

				if($this->input->post('item_hit')){
					$params['item_hit'] = (int)$this->input->post('item_hit');
				}

				if($this->input->post('item_newest')){
					$params['item_newest'] = (int)$this->input->post('item_newest');
				}

				if($this->input->post('item_action')){
					$params['item_action'] = (int)$this->input->post('item_action');
				}

				if($this->input->post('item_popular')){
					$params['item_popular'] = (int)$this->input->post('item_popular');
				}

				if($this->input->post('item_on_home')){
					$params['item_on_home'] = (int)$this->input->post('item_on_home');
				}

				if($this->input->post('item_commented')){
					$params['item_commented'] = (int)$this->input->post('item_commented');
				}
				
				if($this->input->post('id_manager')){
					$params['id_manager'] = (int) $this->input->post('id_manager');
					$params['moderation'] = true;
				}
				
				if($this->input->post('id_moderator')){
					$params['id_moderator'] = (int) $this->input->post('id_moderator');
					$params['moderation'] = true;
				}
				
				if($this->input->post('id_issuer')){
					$params['id_issuer'] = (int) $this->input->post('id_issuer');
					$params['moderation'] = true;
				}
		
				if($this->input->post('completed_on')){
					list($completed_from, $completed_to) = explode(' - ', $this->input->post('completed_on'));

					if(validateDate($completed_from, 'd.m.Y')){
						$params['completed_from'] = getDateFormat($completed_from, 'd.m.Y', 'Y-m-d');
						$params['moderation'] = true;
					}

					if(validateDate($completed_to, 'd.m.Y')){
						$params['completed_from'] = getDateFormat($completed_to, 'd.m.Y', 'Y-m-d');
						$params['moderation'] = true;
					}
				}
		
				if($this->input->post('issued_on')){
					list($issued_from, $issued_to) = explode(' - ', $this->input->post('issued_on'));

					if(validateDate($issued_from, 'd.m.Y')){
						$params['issued_from'] = getDateFormat($issued_from, 'd.m.Y', 'Y-m-d');
						$params['moderation'] = true;
					}

					if(validateDate($issued_to, 'd.m.Y')){
						$params['issued_to'] = getDateFormat($issued_to, 'd.m.Y', 'Y-m-d');
						$params['moderation'] = true;
					}
				}
		
				if($this->input->post('moderated_on')){
					list($moderated_from, $moderated_to) = explode(' - ', $this->input->post('moderated_on'));

					if(validateDate($moderated_from, 'd.m.Y')){
						$params['moderated_from'] = getDateFormat($moderated_from, 'd.m.Y', 'Y-m-d');
						$params['moderation'] = true;
					}

					if(validateDate($moderated_to, 'd.m.Y')){
						$params['moderated_to'] = getDateFormat($moderated_to, 'd.m.Y', 'Y-m-d');
						$params['moderation'] = true;
					}
				}

				if($this->input->post('is_filled') != ''){
					$params['is_filled'] = (int) $this->input->post('is_filled');
					$params['moderation'] = true;
				}
				
				if($this->input->post('has_issue') != ''){
					$params['has_issue'] = (int) $this->input->post('has_issue');
					$params['moderation'] = true;
				}
				
				if($this->input->post('is_moderated') != ''){
					$params['is_moderated'] = (int) $this->input->post('is_moderated');
					$params['moderation'] = true;
				}
				
				if($this->input->post('item_data_moderated') != ''){
					$params['item_data_moderated'] = (int)$this->input->post('item_data_moderated');
				}
				//endregion FILTERS
				// dump($params);

				$records = $this->items->handler_get_all($params);
				$records_total = $this->items->handler_get_count($params);
				
				$records_categories = array_column($records, 'id_category');
				$categories = !empty($records_categories) ? arrayByKey(Modules::run('categories/_get_all', array('id_category' => $records_categories)), 'category_id') : array();

				$currency = Modules::run('currency/_get_default_apanel');
				$managers = arrayByKey(Modules::run('users/_get_all', array('group_type' => array('content_menedger', 'moderator', 'admin'))), 'id');
				// dump($managers);
				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record) use ($categories, $currency, $managers){
						$item_url = base_url('products/'.$record['item_url']);
						$item_image_url = base_url(getImage('files/items/'.$record['item_photo']));

						//region CATEGORY
						$category_bread = array();
						$category_link = '';
						if(isset($categories[$record['id_category']])){
							$item_categories = json_decode("[".$categories[$record['id_category']]['category_breadcrumbs']."]", true);
							foreach($item_categories as $category){
								$category_bread[] = $category['category_title'];
							}
							$category_link = '<span class="custom-font-size-11" title="'.implode(' &raquo; ', $category_bread).'"><i class="fad fa-folder-tree"></i> '.$categories[$record['id_category']]['category_title'].'</span>';
						}
						//endregion CATEGORY

						//region MARKETTING
						$item_marketing = '';
						if($record['item_visible']){
							$item_marketing .= '<span class="fad fa-eye" title="Признак виден на сайте"></span> ';
						}

						if($record['item_hit']){
							$item_marketing .= '<span class="fad fa-fire" title="Признак хит"></span> ';
						}

						if($record['item_newest']){
							$item_marketing .= '<span class="fad fa-gift" title="Признак новинка"></span> ';
						}

						if($record['item_action']){
							$item_marketing .= '<span class="fad fa-stars" title="Признак акция"></span> ';
						}

						if($record['item_popular']){
							$item_marketing .= '<span class="fad fa-heart" title="Признак популярен"></span> ';
						}
						
						if($record['item_on_home']){
							$item_marketing .= '<span class="fad fa-home-heart" title="Признак показать на главной"></span> ';
						}
			
						if($record['item_commented']){
							$item_marketing .= '<span class="fad fa-comment-alt-dots" title="Признак обсуждаемый"></span> ';
						}
						//endregion MARKETTING
						
						//region MODERATION
						$moderationInfo = [];
						if(1 === (int) @$record['is_filled']){
							$contentManagerDetail = !empty($managers[(int) $record['id_manager']]) ? $managers[$record['id_manager']]['user_nicename'] : 'нет данных';
							$filledOnDate = validateDate($record['filled_on']) ? getDateFormat($record['filled_on']) : 'нет данных';
							$moderationInfo[] = '<span class="badge bg-primary" tabindex="1" role="button" data-toggle="popover" data-trigger="hover" data-html="true" title="Заполнен" data-content="<strong>Заполнен: </strong>' . $filledOnDate .'<br><strong>Менеджер: </strong>'. $contentManagerDetail .'">Заполнен</span>';
						}

						if(1 === (int) @$record['has_issue']){
							$issuedManagerDetail = !empty($managers[(int) $record['id_issuer']]) ? $managers[$record['id_issuer']]['user_nicename'] : 'нет данных';
							$issuedOnDate = validateDate($record['issued_on']) ? getDateFormat($record['issued_on']) : 'нет данных';
							$moderationInfo[] = '<span class="badge bg-danger" tabindex="1" role="button" data-toggle="popover" data-trigger="hover" data-html="true" title="С ошибками" data-content="<strong>С ошибками: </strong>' . $issuedOnDate .'<br><strong>Менеджер: </strong>'. $issuedManagerDetail .'">С ошибками</span>';
						}

						if(1 === (int) @$record['is_moderated']){
							$moderatedManagerDetail = !empty($managers[(int) $record['id_issuer']]) ? $managers[$record['id_issuer']]['user_nicename'] : 'нет данных';
							$moderatedOnDate = validateDate($record['moderated_on']) ? getDateFormat($record['moderated_on']) : 'нет данных';
							$moderationInfo[] = '<span class="badge bg-success" tabindex="1" role="button" data-toggle="popover" data-trigger="hover" data-html="true" title="Отмодерирован" data-content="<strong>Отмодерирован: </strong>' . $moderatedOnDate .'<br><strong>Менеджер: </strong>'. $moderatedManagerDetail .'">Отмодерирован</span>';
						}

						if(empty($moderationInfo)){
							$moderationInfo[] = '<span class="badge bg-secondary" title="Нет данных">Нет данных</span>';
						}
						//endregion MODERATION

						return array(
							'dt_photo'				=> '<a href="'.$item_image_url.'" data-fancybox="image_'.$record['item_prog_id'].'" data-caption="'.clean_output($record['item_title']).'">
															<img src="'.$item_image_url.'" class="img-thumbnail custom-width-100">
														</a>',
							'dt_name'				=> "<a href=\"{$item_url}\">{$record['item_title']}</a>
														<br><strong class=\"custom-font-size-12\">Маркетинг: </strong>{$item_marketing}
														<br><strong class=\"custom-font-size-12\">Категория: </strong>{$category_link}
														<br><strong class=\"custom-font-size-12\">ИД 1C: </strong>{$record['item_prog_id']}
														<br><strong class=\"custom-font-size-12\">Артикул: </strong>{$record['item_code']}",
							'dt_price'				=> (!empty($currency))?numberFormat($currency['currency_rate'] * $record['item_price'], true):$record['item_price'],
							'dt_create'				=> getDateFormat($record['item_created']),
							'dt_update'				=> getDateFormat($record['item_last_update']),
							'dt_moderation_status'	=> implode('<br>', $moderationInfo),
							'dt_status'				=> '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['item_visible'] === 1, 'fad fa-minus-square text-danger') .' call-function" title="'. get_choice('Активен', (int) $record['item_visible'] === 1, 'Не активен') .'" data-callback="change_marketing_status" data-marketing="visible" data-item="'.$record['id_item'].'"></a>',
							'dt_actions'			=> '<div class="dropdown">
															<a data-toggle="dropdown" href="#" aria-expanded="false">
																<i class="fas fa-ellipsis-h"></i>
															</a>
															<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
																<a class="dropdown-item" href="'.base_url('admin/items/edit/' . $record['id_item']).'">
																	<i class="fad fa-pencil"></i> Редактировать
																</a>
															</div>
														</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
		}
	}

	function upload_photo(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$id_item = (int) $this->input->post('id_item');
		$item = $this->items->handler_get($id_item);
		if(empty($item)){
			jsonResponse('Не могу завершить операцию.');
		}

        $path = 'files/items';
		create_dir($path);

		$file_name = strForURL(get_words(translit($item['item_title']), 6) . '' . $item['item_prog_id']);

        $config['upload_path'] = FCPATH . $path;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $file_name;
        $config['min_width']	= '1024';
        $config['min_height']	= '768';
		$config['max_size']	= '6000';
		
		$this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('croppedImage')){
			jsonResponse($this->upload->display_errors('',''),'error');
        }
		$data = $this->upload->data();

		$config = array(
			'source_image'      => $data['full_path'],
			'create_thumb'      => true,
			'thumb_marker'      => 'thumb_500x500_',
			'new_image'         => FCPATH . $path,
			'maintain_ratio'    => true,
			'width'             => 500,
			'height'            => 500
		);

		$this->load->library('image_lib');
		$this->image_lib->initialize($config);
		$this->image_lib->resize();

		$file = new StdClass;
		$file->filename = $data['file_name'];
		jsonResponse('', 'success', array("file" => $file));
	}
	
	function clean_description(){
		ini_set('max_execution_time', 0);
		
		$this->load->helper('htmlpurifier');

		$records = $this->items->handler_get_all(array(
			'coulmns' => "id_item, item_description"
		));

		dump(count($records));
		
		// foreach ($records as $record) {
		// 	$this->items->handler_update($record['id_item'], array(
		// 		'item_description_cleaned' => html_purify($record['item_description'], 'item_description')
		// 	));
		// }
	}
}
