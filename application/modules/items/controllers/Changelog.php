<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Changelog extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->data = array();

		$this->load->model("Changelog_model", "changelog");
	}

	function index(){
		return_404($this->data);
	}

	function _set(array $insert){
		if(empty($insert)){
			return false;
		}

		return $this->changelog->handlerInsertLog($insert);
	}

	function _get($params = []){
		$records = $this->changelog->handlerGetLog($params);

		$records = array_map(function($record){
			$record['changes'] = json_decode($record['changes'], true);
			
			return $record;
		}, $records);

		return $records;
	}
}
