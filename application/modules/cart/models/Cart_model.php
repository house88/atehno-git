<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cart_model extends CI_Model{
	var $basket = "basket";

	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->basket, $data);
		return $this->db->insert_id();
	}

	function handler_update($basket_hash, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('basket_hash', $basket_hash);
		$this->db->update($this->basket, $data);
	}

	function handler_delete($basket_hash){
		$this->db->where('basket_hash', $basket_hash);
		$this->db->delete($this->basket);
	}

	function handler_get($basket_hash){
		$this->db->where('basket_hash', $basket_hash);
		return $this->db->get($this->basket)->row_array();
	}

	/**
	 * @param $id_item array
	 * @return array
	 */
	function handler_get_items($id_item = array()){
		if(empty($id_item) || empty($original_items = Modules::run('items/_get_all', array('items_list' => $id_item)))){
			return [];
		}

		$ordered_items = [];
		$priceColumn = getPriceColumn();
		foreach ($original_items as $item) {
			$price = getPriceObject($item[$priceColumn['db_price_column']], [
				'tempAmount' => $item['item_temp_price']
			]);

			$ordered_items[] = [
				// 'id_item' => (int) $item['id_item'],
				// 'item_code' => $item['item_code'],
				// 'item_prog_id' => $item['item_prog_id'],
				// 'item_url' => $item['item_url'],
				// 'item_photo' => $item['item_photo'],
				// 'item_title' => $item['item_title'],
				// 'item_price',
				// 'item_price_final',
				// 'item_cashback_price',
				// 'item_discount',
				// 'item_currency',
				// 'item_quantity',
				// 'item_zero_credit',
				// 'item_zero_credit_months',
				// 'item_has_promo_discount',
				// 'is_oversize'

				// 'id_item' => ,
				// 'item_title' => ,
				// 'item_code' => ,
				// 'item_prog_id' => ,
				// 'item_url' => ,
				// 'item_photo' => ,
				// 'item_price' => $item_price,
				// 'item_price_final' => $item_price * $quantity,
				// 'item_currency' => $price['currency_symbol'],
				// 'item_quantity' => $quantity,
				// 'item_cashback_price' => ($this->lauth->group_view_cashback())?$item['item_cashback_price']:0,
				// 'item_zero_credit' => $item['item_zero_credit'],
				// 'item_zero_credit_months' => $item['item_zero_credit_months'],
				// 'item_has_promo_discount' => ($price['has_promo_discount'] == true) ? 1 : 0,
				// 'is_oversize' => (int) $basket_items_categories[$item['id_category']]['category_is_oversized']
			];
		}
	}
}
