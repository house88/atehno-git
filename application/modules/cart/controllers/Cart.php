<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value'];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value'];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value'];

		$this->load->model('Cart_model', 'cart');
		$this->load->model('items/Items_Model', 'items');
		$this->load->model("delivery/Delivery_model", "delivery");
		$this->load->model("payment/Payment_model", "payment");
		$this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
		

	}

	function index(){
		$this->breadcrumbs[] = [
			'title' => 'Корзина',
			'link' => base_url('cart')
		];

		$basket_hash = get_cookie('_cart_products');
		if(!empty($basket_hash)){
			$basket = $this->cart->handler_get($basket_hash);
			if(!empty($basket)){
				$basketProductsDetails = json_decode($basket['basket_data'], true);
				$basket = array_merge($basket, [
					'userInfo' => $this->lauth->getClient(true),
					'currency' => [
						'code' => '',
						'symbol' => '',
						'number' => '',
						'rate' => '',
					],
					'existPromoCode' => false,
					'promoCode' => [],
					'credit' => false,
					'applyBonusAmount' => 0,
					'nextOrderCashbackAmount' => 0,
					'countOversizeItems' => 0,
					'deliveryOptions' => [],
					'paymentOptions' => [],
					'defaultPaymentMethodId' => 0,
					'defaultDeliveryMethodId' => 0,
					'relationPaymentDelivery' => [],
					'methodsPaymentDelivery' => [],
					'methodsDeliveryPayment' => [],
				]);

				if(!empty($basketProductsDetails)){
					$products = Modules::run('items/_get_all', [
						'items_list' => array_keys($basketProductsDetails)
					]);
					
					$categoriesId = array_unique(array_map('intval', array_column($products, 'id_category')));
					$oversizedCategoriesId = array_column(Modules::run('categories/_get_all', [
						'id_category' => $categoriesId, 
						'is_oversized' => 1]
					), 'category_id');
					
					$promoCodeParams = [];
					if($basket['id_pcode'] > 0){
						$promoCodeParams['id_pcode'] = (int) $basket['id_pcode'];
						$promoCodeParams['id_hash'] = (int) $basket['id_pcode_hash'];
					}

					$promoCode = Modules::run('promo_code/_get_active', $promoCodeParams);
					$basket['existPromoCode'] = !empty($promoCode);
					if($basket['id_pcode'] == 0 || !empty($promoCode) && (int) $basket['id_pcode'] !== (int) $promoCode['id_pcode']){
						$promoCode = [];
					}

					$basket['promoCode'] = $promoCode;

					$cashbackAmount = 0;
					$applyBonusForAmount = 0;
					$potentialCashbackAmount = 0;
					$potentialApplyBonusForAmount = 0;
					$basketAmount = 0;
					$basket['products'] = [];
					foreach($products as $productKey => $product){
						$itemQuantity = (int) $basketProductsDetails[$product['id_item']]['quantity'];

						$product['item_cart_qty'] = $itemQuantity;
						if(in_array($product['id_category'], $oversizedCategoriesId)){
							$basket['countOversizeItems'] += $itemQuantity;
						}

						$potentialCashbackAmount += $product['item_cashback_price'] * $itemQuantity;
						if(1 === (int) @$basket['userInfo']['group_view_cashback']){
							$cashbackAmount += $product['item_cashback_price'] * $itemQuantity;
						}

						$priceColumn = getPriceColumn();
						$product['priceObject'] = getPriceObject($product[$priceColumn['db_price_column']], [
							'tempAmount' => $product['item_temp_price'],
							'pcode' => $basket['promoCode']
						]);

						$productAmount = $product['priceObject']['display_price'] * $itemQuantity;
						$basketAmount += $productAmount;
						$potentialApplyBonusForAmount += $productAmount;
						if($product['item_cashback_price'] == 0 && 1 === (int) @$basket['userInfo']['group_view_cashback']){
                            $applyBonusForAmount += $productAmount;
						}
						
						$basket['currency'] = [
							'code' => $product['priceObject']['currency_code'],
							'symbol' => $product['priceObject']['currency_symbol'],
							'number' => $product['priceObject']['currency_number'],
							'rate' => $product['priceObject']['currency_rate'],
						];

						
						$basket['products'][$productKey] = [
							'id_item' => $product['id_item'],
							'item_code' => $product['item_code'],
							'item_prog_id' => $product['item_prog_id'],
							'item_url' => $product['item_url'],
							'item_photo' => $product['item_photo'],
							'item_title' => $product['item_title'],
							'item_price' => $product['priceObject']['display_price'],
							'item_price_final' => $productAmount,
							'item_cashback_price' => 1 === (int) @$basket['userInfo']['group_view_cashback'] ? 1 * $product['item_cashback_price'] : 0,
							'item_discount' => $product['item_discount'],
							'item_currency' => $product['priceObject']['currency_symbol'],
							'item_quantity' => $itemQuantity,
							'item_zero_credit' => $basket['id_pcode'] > 0 ? 0 : (int) $product['item_zero_credit'],
							'item_zero_credit_months' => $basket['id_pcode'] > 0 ? 0 : (int) $product['item_zero_credit_months'],
							'item_has_promo_discount' => ($product['priceObject']['has_promo_discount'] == true) ? 1 : 0,
							'is_oversize' => (int) in_array($product['id_category'], $oversizedCategoriesId),
							'priceObject' => $product['priceObject']
						];
					}

					if($basket['id_pcode'] > 0){
						$cashbackAmount = 0;
					}

					$userBonus = 1 === (int) @$basket['userInfo']['group_view_cashback'] && (float) @$basket['userInfo']['user_cashback_balance'] > 0 ? $basket['userInfo']['user_cashback_balance'] * 1: 0;
					$basket['applyBonusForAmount'] = $applyBonusForAmount;
					$basket['applyBonusAmount'] = 0 < $applyBonusForAmount ? min([($cashbackAmount + $userBonus), $applyBonusForAmount]): 0;
					$basket['nextOrderCashbackAmount'] = 0;
					if(0 == $applyBonusForAmount){
						$basket['nextOrderCashbackAmount'] =  (($cashbackAmount + $userBonus) <= $applyBonusForAmount) ? 0 : (($cashbackAmount + $userBonus) - $applyBonusForAmount);
					}
					$basket['amount'] = $basketAmount;
					$basket['amountFinal'] = $basketAmount - $basket['applyBonusAmount'];
					$basket['credit'] = getItemsCreditOptions($basket['products'], 1 === (int) @$basket['userInfo']['group_view_cashback'] ? (float) @$basket['userInfo']['user_cashback_balance'] : 0);

					$basket['useDecimals'] = $useDecimals = 1 === (int) @$basket['userInfo']['group_view_decimals'];
					$basketSettings['default'] = [
						'deliveryPrice' => [
							'amount' => 0,
							'text' => niceDisplayPrice(0, [
								'useDecimals' => $useDecimals
							])
						],
						'paymentAddPrice' => [
							'amount' => 0,
							'text' => niceDisplayPrice(0, [
								'useDecimals' => $useDecimals
							])
						],
						'applyBonus' => [
							'amount' => $basket['applyBonusAmount'],
							'text' => niceDisplayPrice($basket['applyBonusAmount'], [
								'useDecimals' => $useDecimals
							])
						],
						'cashbackAmount' => [
							'amount' => $basket['nextOrderCashbackAmount'],
							'text' => niceDisplayPrice($basket['nextOrderCashbackAmount'], [
								'useDecimals' => $useDecimals
							])
						],
						'price' => [
							'amount' => $basket['amount'],
							'text' => niceDisplayPrice($basket['amount'], [
								'useDecimals' => $useDecimals
							])
						],
						'priceFinal' => [
							'amount' => $basket['amountFinal'],
							'text' => niceDisplayPrice($basket['amountFinal'], [
								'useDecimals' => $useDecimals
							])
						],
						'credit' => false,
						'creditMonths' => 0
					];

					$deliveryOptions = Modules::run('delivery/_get_all', ['active' => 1]);
					foreach ($deliveryOptions as $deliveryOption) {
						$deliveryPriceOptions = json_decode($deliveryOption['delivery_price_options'], true);
						if(!isset($deliveryPriceOptions[$basket['currency']['code']])){
							continue;
						}

						$deliveryPriceOption = $deliveryPriceOptions[$basket['currency']['code']];
						$deliveryPrice = $basket['amountFinal'] >= $deliveryPriceOption['price_free'] ? 0 : $deliveryPriceOption['price'];
						if(!empty($deliveryPriceOption['price_for_oversize'])){
							$deliveryPrice += $deliveryPriceOption['price_for_oversize'] * $basket['countOversizeItems'];
						}

						$deliveryOption['amount'] = $deliveryPrice;

						$basket['deliveryOptions'][$deliveryOption['id_delivery']] = $deliveryOption;

						if($basket['defaultDeliveryMethodId'] === 0){
							$basket['defaultDeliveryMethodId'] = $deliveryOption['id_delivery'];
						}
					}

					$paymentOptions = Modules::run('payment/_get_all', ['active' => 1]);
					$paymentByCreditId = 0;
					foreach ($paymentOptions as $paymentOption) {
						$paymentAddOptions = json_decode($paymentOption['payment_add_options'], true);
						$paymentOption['paymentAddPercent'] = (isset($paymentAddOptions[$basket['currency']['code']])) ? $paymentAddOptions[$basket['currency']['code']] : 0;
						$paymentOption['paymentAddAmount'] = $basket['amountFinal'] * $paymentOption['paymentAddPercent']/100;
						
						$basket['paymentOptions'][$paymentOption['id_payment']] = $paymentOption;

						if(1 === (int) $paymentOption['payment_by_credit']){
							$paymentByCreditId = (int) $paymentOption['id_payment'];
						}
					}

					$basketSettings['defaultCredit'] = [];
					$basketSettings['byRelation'] = [];

					if(false !== $basket['credit'] && !empty($basket['credit'])){
						$basketSettingsCreditDefault = $basketSettings['default'];
						$basketSettingsCreditDefault['applyBonus']['amount'] = $basket['credit']['default']['appliedBonusAmount'] ?? 0;
						$basketSettingsCreditDefault['applyBonus']['text'] = niceDisplayPrice($basket['credit']['default']['appliedBonusAmount'] ?? 0, [
							'useDecimals' => $useDecimals
						]);

						$basketSettingsCreditDefault['priceFinal']['amount'] = $basket['credit']['default']['creditFinalAmount'] ?? 0;
						$basketSettingsCreditDefault['priceFinal']['text'] = niceDisplayPrice($basket['credit']['default']['creditFinalAmount'] ?? 0, [
							'useDecimals' => $useDecimals
						]);

						$basketSettingsCreditDefault['creditMonthlyAmount']['amount'] = $basket['credit']['default']['creditMonthlyAmount'];
						$basketSettingsCreditDefault['creditMonthlyAmount']['text'] = niceDisplayPrice($basket['credit']['default']['creditMonthlyAmount'], [
							'useDecimals' => $useDecimals
						]);

						$basketSettingsCreditDefault['cashbackAmount']['amount'] = $basket['credit']['default']['cashbackAmount'];
						$basketSettingsCreditDefault['cashbackAmount']['text'] = niceDisplayPrice($basket['credit']['default']['cashbackAmount'], [
							'useDecimals' => $useDecimals
						]);

						$basketSettingsCreditDefault['credit'] = true;
						$basketSettingsCreditDefault['creditMonths'] = $basket['credit']['default']['months'];
						$basketSettings['defaultCredit'] = $basketSettingsCreditDefault;
					}
					
					$basket['relationPaymentDelivery'] = [];
					$relationPaymentDelivery = Modules::run('payment/_get_delivery_relations');
					if(!empty($relationPaymentDelivery)){
						foreach ($relationPaymentDelivery as $relation) {
							$relation_key = "{$relation['id_payment']}_{$relation['id_delivery']}";
							$basket['relationPaymentDelivery'][$relation_key] = $relation['relation_text'];
							$basket['methodsPaymentDelivery'][$relation['id_payment']][] = $relation['id_delivery'];
							$basket['methodsDeliveryPayment'][$relation['id_delivery']][] = $relation['id_payment'];

							$deliveryOption = @$basket['deliveryOptions'][$relation['id_delivery']];

							$relationSettings = $basketSettings['default'];
							if(!empty($paymentOption = $basket['paymentOptions'][$relation['id_payment']])){
								$relationSettings['paymentAddPrice']['amount'] = $paymentOption['paymentAddAmount'];
								$relationSettings['paymentAddPrice']['text'] = niceDisplayPrice($paymentOption['paymentAddAmount'], [
									'useDecimals' => $useDecimals
								]);

								if(1 === (int) $paymentOption['payment_by_credit'] && false !== $basket['credit'] && !empty($basket['credit'])){
									foreach ($basket['credit']['options'] as $creditKey => $creditOption) {
										$relationSettings = $basketSettingsCreditDefault;
										$tempRelationKey = $relation_key;
			
										$relationSettings['applyBonus']['amount'] = $creditOption['appliedBonusAmount'];
										$relationSettings['applyBonus']['text'] = niceDisplayPrice($creditOption['appliedBonusAmount'], [
											'useDecimals' => $useDecimals
										]);
			
										$relationSettings['priceFinal']['amount'] = $creditOption['creditFinalAmount'];
										$relationSettings['priceFinal']['text'] = niceDisplayPrice($creditOption['creditFinalAmount'], [
											'useDecimals' => $useDecimals
										]);
			
										$relationSettings['cashbackAmount']['amount'] = $creditOption['cashbackAmount'];
										$relationSettings['cashbackAmount']['text'] = niceDisplayPrice($creditOption['cashbackAmount'], [
											'useDecimals' => $useDecimals
										]);
			
										$relationSettings['creditMonths'] = $creditOption['months'];
										$creditMonthlyAmount = $creditOption['creditMonthlyAmount'];
										$tempRelationKey .= "-{$creditOption['months']}-months";

										if(!empty($deliveryOption)){
											$relationSettings['deliveryPrice']['amount'] = $deliveryOption['amount'];
											$relationSettings['deliveryPrice']['text'] = niceDisplayPrice($deliveryOption['amount'], [
												'useDecimals' => $useDecimals
											]);

											$relationSettings['priceFinal']['amount'] = $creditOption['creditFinalAmount'] + $deliveryOption['amount'];
											$relationSettings['priceFinal']['text'] = niceDisplayPrice($relationSettings['priceFinal']['amount'], [
												'useDecimals' => $useDecimals
											]);
											
											$creditMonthlyAmount = getCreditMonthlyAmount($creditOption, $deliveryOption['amount']);
										}

										$relationSettings['creditMonthlyAmount']['amount'] = $creditMonthlyAmount;
										$relationSettings['creditMonthlyAmount']['text'] = niceDisplayPrice($creditMonthlyAmount, [
											'useDecimals' => $useDecimals
										]);
										
										$basketSettings['byRelation'][$tempRelationKey] = $relationSettings;
									}
								} else{
									$relationSettings['priceFinal']['amount'] = $basket['amountFinal'] + $paymentOption['paymentAddAmount'];
									$relationSettings['priceFinal']['text'] = niceDisplayPrice($relationSettings['priceFinal']['amount'], [
										'useDecimals' => $useDecimals
									]);

									if(!empty($deliveryOption)){
										$relationSettings['deliveryPrice']['amount'] = $deliveryOption['amount'];
										$relationSettings['deliveryPrice']['text'] = niceDisplayPrice($deliveryOption['amount'], [
											'useDecimals' => $useDecimals
										]);

										$relationSettings['priceFinal']['amount'] += $deliveryOption['amount'];
										$relationSettings['priceFinal']['text'] = niceDisplayPrice($relationSettings['priceFinal']['amount'], [
											'useDecimals' => $useDecimals
										]);
									}

									$basketSettings['byRelation'][$relation_key] = $relationSettings;
								}
							}
						}

						// $basket['relationPaymentDelivery'] = json_encode($basket['relationPaymentDelivery']);
					}

					$basket['basketRelationSettings'] = array_merge(
						$basketSettings['byRelation'], 
						[
							'0_0' => $basketSettings['default'],
							'default' => $basketSettings['default']
						]
					);

					if(!empty($basketSettings['defaultCredit'])){
						$basket['basketRelationSettings']['byRelation']["{$paymentByCreditId}_0-{$basketSettings['defaultCredit']['creditMonths']}-months"] = $basketSettings['defaultCredit'];
						$basket['basketRelationSettings']['byRelation']["defaultCredit"] = $basketSettings['defaultCredit'];
					}
				}

				$this->data['basket'] = $basket;
			}
		}
		// dump($basket);
		// exit();
		
		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'cart/index_view';
		$this->load->view($this->theme->public_view('shop_view'), $this->data);
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(3);
		switch($option){
			case 'add':
				$this->form_validation->set_rules('item', 'Товар', 'required|xss_clean');
				$this->form_validation->set_rules('quantity', 'Количество', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$basket_hash = get_cookie('_cart_products');
				$item = (int)$this->input->post('item');				
				$quantity = (int)$this->input->post('quantity');
				$product = $this->items->handler_get($item);
				if(empty($product)){
					jsonResponse('Товар не найден.');
				}

				$priceColumn = getPriceColumn();
				$price = getPriceObject($product[$priceColumn['db_price_column']], [
					'tempAmount' => $product['item_temp_price']
				]);
				if($price['display_price'] <= 0){
					jsonResponse('Этот товар не может быть добавлен в корзину.');
				}

				$credit_months = 0;
				if($this->input->post('months')){
					$credit = getCreditOptions($price['display_price'], $price['currency_symbol'], $product['item_zero_credit'], $product['item_zero_credit_months']);                        
					if(!$credit){
						jsonResponse('Покупка в кредит не доступна для этого товара.', 'warning');
					}
					
					$credit_months = (int) $this->input->post('months');
					$months_list = implode(', ', $credit['months_list']);
					if(!in_array($credit_months, $credit['months_list'])){
						jsonResponse('Количество месяцев должно быть одно из: '.$months_list);
					}
				}

				if(!empty($basket_hash)){
					$basket = $this->cart->handler_get($basket_hash);
					if(empty($basket)){
						$basket_hash = md5(uniqid('@tehno6asket').session_id());
						$insert = array(
							'basket_hash' => $basket_hash,
							'basket_data' => json_encode(array($item => array('item' => $item, 'quantity' => $quantity)))
						);
						$basket = $this->cart->handler_insert($insert);
						set_cookie('_cart_products', $basket_hash, 604800);
					} else{
						$items = json_decode($basket['basket_data'], true);
						if(!array_key_exists($item, $items)){
							$items[$item] = array(
								'item' => $item, 
								'quantity' => $quantity
							);
						} else{
							$items[$item]['quantity'] += $quantity;
						}
						
						$update = array(
							'basket_data' => json_encode($items)
						);
						$this->cart->handler_update($basket_hash, $update);
					}
				} else{
					$basket_hash = md5(uniqid('@tehno6asket').session_id());
					$insert = array(
						'basket_hash' => $basket_hash,
						'basket_data' => json_encode(array($item => array('item' => $item, 'quantity' => $quantity)))
					);
					$basket = $this->cart->handler_insert($insert);
					set_cookie('_cart_products', $basket_hash, 604800);
				}

				jsonResponse('', 'success');
			break;
			case 'change_quantity':
				$this->form_validation->set_rules('item', 'Товар', 'required|xss_clean');
				$this->form_validation->set_rules('quantity', 'Количество', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$basket_hash = get_cookie('_cart_products');
				$item = (int)$this->input->post('item');
				$quantity = (int)$this->input->post('quantity');
				if(!empty($basket_hash)){
					$basket = $this->cart->handler_get($basket_hash);
					if(!empty($basket)){
						$items = json_decode($basket['basket_data'], true);
						if(array_key_exists($item, $items)){
							$items[$item]['quantity'] = $quantity;

							$update = array(
								'basket_data' => json_encode($items)
							);
							$this->cart->handler_update($basket_hash, $update);
						}
					}
				}

				jsonResponse('', 'success');
			break;
			case 'delete':
				$this->form_validation->set_rules('item', 'Товар', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$basket_hash = get_cookie('_cart_products');
				$item = (int)$this->input->post('item');
				if(!empty($basket_hash)){
					$basket = $this->cart->handler_get($basket_hash);
					if(!empty($basket)){
						$items = json_decode($basket['basket_data'], true);
						if(array_key_exists($item, $items)){
							unset($items[$item]);

							if(empty($items)){
								$this->cart->handler_delete($basket_hash);
								delete_cookie('_cart_products');
							} else{
								$update = array(
									'basket_data' => json_encode($items)
								);
								$this->cart->handler_update($basket_hash, $update);
							}
						}
					}
				}
				jsonResponse('', 'success');
			break;
			case 'update':
				$basket_hash = get_cookie('_cart_products');
				$cart_count = 0;
				$cart_price = 0;
				$cart_currency = '';
				if(!empty($basket_hash)){
					$basket = $this->cart->handler_get($basket_hash);
					if(!empty($basket)){
						$items = json_decode($basket['basket_data'], true);
						$items_list = array_keys($items);
						if(!empty($items_list)){
							$pcode_params = array();
							if($basket['id_pcode'] > 0){
								$pcode_params['id_pcode'] = (int) $basket['id_pcode'];
								$pcode_params['id_hash'] = (int) $basket['id_pcode_hash'];
							}
							$pcode = $this->data['pcode'] = Modules::run('promo_code/_get_active', $pcode_params);
							if(!empty($pcode) && $basket['id_pcode'] != $pcode['id_pcode']){
								$pcode = $this->data['pcode'] = array();
							}

							$params = array(
								'items_list' => $items_list
							);
							$this->data['items'] = $this->items->handler_get_all($params);
							$priceColumn = getPriceColumn();
							foreach($this->data['items'] as $key => $item){
								$cart_count += (int) $items[$item['id_item']]['quantity'];

								$this->data['items'][$key]['item_cart_qty'] = $items[$item['id_item']]['quantity'];
								$price = getPriceObject($item[$priceColumn['db_price_column']], [
									'tempAmount' => $item['item_temp_price'],
									'pcode' => $pcode
								]);
								$cart_price += $price['display_price'] * ((int) $items[$item['id_item']]['quantity']);
								$cart_currency = $price['currency_symbol'];
							}
						}
					}
				}

				$cart_content = $this->load->view($this->theme->public_view('cart/small_view'), $this->data, true);
				jsonResponse('', 'success', array('cart_content' => $cart_content, 'cart_count' => $cart_count, 'cart_price' => $cart_price.' '.$cart_currency));
			break;
			case 'apply_pcode':
				$basket_hash = get_cookie('_cart_products');
				if(!empty($basket_hash)){
					$basket = $this->cart->handler_get($basket_hash);
					if(!empty($basket)){
						$this->form_validation->set_rules('pcode', 'Промо код', 'required');
						if ($this->form_validation->run() == false){
							jsonResponse($this->form_validation->error_array());
						}

						$pcode_hash = $this->input->post('pcode', true);
						$pcode = Modules::run('promo_code/_get_active', array('pcode_hash' => $pcode_hash));
						if(empty($pcode)){
							jsonResponse('Промо код не существует.');
						}

						$update = array(
							'id_pcode' => $pcode['id_pcode'],
							'id_pcode_hash' => $pcode['id_hash']
						);
						$this->cart->handler_update($basket_hash, $update);
						jsonResponse('', 'success');
					}
				}

				jsonResponse('Промо код не существует.');
			break;
		}
	}
}
