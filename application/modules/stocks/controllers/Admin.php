<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/stocks/';
		$this->active_menu = 'catalog';
		$this->active_submenu = 'stocks';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

        $this->load->model("Stocks_model", 'stocks');

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		checkPermision('manage_stocks', '/admin');

		$this->data['page_header'] = 'Склады';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'edit':
				checkPermisionAjax('manage_stocks');

				$id_stock = (int) $this->uri->segment(5);
				$this->data['stock'] = $this->stocks->handler_get($id_stock);
				if(empty($this->data['stock'])){
					jsonResponse('Склад не найден.');
				}

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'edit':
				checkPermisionAjax('manage_stocks');

                $this->form_validation->set_rules('stock', 'Склад', 'required|xss_clean');
				$this->form_validation->set_rules('name_display', 'Название на сайте', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('stock_city', 'Регион', 'required|xss_clean|is_natural');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }
				
				$id_stock = (int) $this->input->post('stock');
				$stock = $this->stocks->handler_get($id_stock);
				if(empty($stock)){
					jsonResponse('Склад не найден.');
				}

				$this->stocks->handler_update($id_stock, array(
					'stock_name_display' => $this->input->post('name_display'),
					'stock_city' => (int) $this->input->post('stock_city')
				));

				jsonResponse('Сохранено.', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_stocks');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart')
				);
        
                $records = $this->stocks->handler_get_all($params);
				$records_total = $this->stocks->handler_get_count($params);

				$_cities = arrayByKey(Modules::run('cities/_get_cities'), 'id_city');
                $output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record) use ($_cities){
						return array(
							'dt_id'				=> $record['stock_prog_id'],
							'dt_name'			=> $record['stock_name'],
							'dt_name_display'	=> $record['stock_name_display'],
							'dt_city'			=> !empty($_cities[$record['stock_city']]) ? $_cities[$record['stock_city']]['city_name'] : '&mdash;',
							'dt_actions'		=> '<div class="dropdown">
														<a data-toggle="dropdown" href="#" aria-expanded="false">
															<i class="fas fa-ellipsis-h"></i>
														</a>
														<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
															<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/stocks/popup/edit/' . $record['id_stock']).'">
																<i class="fad fa-pencil"></i> Редактировать
															</a>
														</div>
													</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
		}
	}
}
