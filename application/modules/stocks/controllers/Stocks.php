<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Stocks extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();

        $this->load->model("Stocks_model", 'stocks');
	}

	function index(){
		return_404($this->data);
    }
    
    function _get_all($params = array()){
        return $this->stocks->handler_get_all($params);
    }
}
