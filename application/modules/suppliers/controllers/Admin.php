<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/suppliers/';
		$this->active_menu = 'catalog';
		$this->active_submenu = 'suppliers';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

        $this->load->model("Suppliers_model", 'suppliers');

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		checkPermision('manage_suppliers', '/admin');

		$this->data['page_header'] = 'Поставщики';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'edit':
				checkPermisionAjax('manage_suppliers');

				$id_supplier = (int) $this->uri->segment(5);
				$this->data['supplier'] = $this->suppliers->handler_get($id_supplier);
				if(empty($this->data['supplier'])){
					jsonResponse('Поставщик не найден.');
				}

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'edit':
                checkPermisionAjax('manage_suppliers');

                $this->form_validation->set_rules('supplier', 'Поставщик', 'required|xss_clean');
				$this->form_validation->set_rules('name_display', 'Название на сайте', 'xss_clean|max_length[250]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }
				
				$id_supplier = (int) $this->input->post('supplier');
				$supplier = $this->suppliers->handler_get($id_supplier);
				if(empty($supplier)){
					jsonResponse('Поставщик не найден.');
				}

				$this->suppliers->handler_update($id_supplier, array(
					'supplier_name_display' => $this->input->post('name_display')
				));

				jsonResponse('Сохранено.', 'success');
			break;
			case 'list':
                checkPermisionAjaxDT('manage_suppliers');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart')
				);
        
                $records = $this->suppliers->handler_get_all($params);
                $records_total = $this->suppliers->handler_get_count($params);
				
				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){
						return array(
							'dt_id'				=> $record['supplier_prog_id'],
							'dt_name'			=> $record['supplier_name'],
							'dt_name_display'	=> $record['supplier_name_display'],
							'dt_actions'		=> $this->lauth->have_right('manage_suppliers') || $this->lauth->have_right('filter_by_suppliers') ? 
													'<div class="dropdown">
														<a data-toggle="dropdown" href="#" aria-expanded="false">
															<i class="fas fa-ellipsis-h"></i>
														</a>
														<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">'.
															($this->lauth->have_right('manage_suppliers') ?
																'<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/suppliers/popup/edit/' . $record['id_supplier']).'">
																	<i class="fad fa-pencil"></i> Редактировать
																</a>' : ''
															).
															($this->lauth->have_right('filter_by_suppliers') ?
															'<div class="dropdown-divider"></div>
															<a href="'. site_url('supplier/'.$record['id_supplier']) .'" class="dropdown-item">
																<i class="fad fa-link"></i> Страница поставщика
															</a>' : '') .
														'</div>
													</div>' : ''
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
		}
	}
}
