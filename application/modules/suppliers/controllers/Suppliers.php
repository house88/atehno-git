<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Suppliers extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();

        $this->load->model("Suppliers_model", 'suppliers');
	}

	function index(){
		return_404($this->data);
    }
    
    function _get_all($params = array()){
        return $this->suppliers->handler_get_all($params);
    }
}
