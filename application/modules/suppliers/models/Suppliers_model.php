<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Suppliers_model extends CI_Model{
	var $suppliers = "suppliers";

	function __construct(){
		parent::__construct();
	}

	function handler_get($id_supplier = 0){
		$this->db->where('id_supplier', $id_supplier);
		return $this->db->get($this->suppliers)->row_array();
	}

	function handler_update($id_supplier = 0, $data = array()){
		if(empty($data)){
			return false;
		}

		$this->db->where('id_supplier', $id_supplier);
		return $this->db->update($this->suppliers, $data);
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_supplier ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->suppliers)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

		return $this->db->count_all_results($this->suppliers);
	}
}
