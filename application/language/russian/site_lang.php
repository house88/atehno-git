<?php
$lang['home_link'] = 'Главная';
$lang['services_link'] = 'Услуги';
$lang['about_link'] = 'О нас';
$lang['news_link'] = 'Новости';
$lang['gallery_link'] = 'Фото блог';
$lang['video_link'] = 'Видео блог';
$lang['contacts_link'] = 'Контакты';
$lang['portfolio_link'] = 'Наши работы';
$lang['offers_link'] = 'Акций';
$lang['heading_contacts'] = 'Контакты';
$lang['heading_news'] = 'Новости';
$lang['heading_phone_booking'] = 'Запись на прием';
$lang['heading_where_we_are'] = 'Где мы находимся';
$lang['heading_address'] = 'мун. Бэлць, Дечебал 144';
$lang['no_articles_in_section'] = 'В этом разделе нет пока статей.';
$lang['no_data'] = 'Нет данных для отображения.';