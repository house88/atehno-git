<?php
$lang['home_link'] = 'Home';
$lang['services_link'] = 'Services';
$lang['about_link'] = 'About us';
$lang['news_link'] = 'News';
$lang['feedbacks_link'] = 'Feedbacks';
$lang['certificates_link'] = 'Certificates';
$lang['contacts_link'] = 'Contacts';
$lang['heading_contacts'] = 'Contacts';
$lang['heading_phone_booking'] = 'Make an appointment';
$lang['heading_where_we_are'] = 'Our location';
$lang['heading_address'] = 'Balti, str. Decebal 120';
$lang['no_articles_in_section'] = 'There are no articles in this section.';
$lang['no_data'] = 'There are nothing to display.';