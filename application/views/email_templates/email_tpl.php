<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>

<body>
<div style="background: #ededed; width: 100%;">
<table width="620" border="0" cellpadding="0" cellspacing="0" style="margin:0 auto; padding:0">
    <tr>
        <td style="height: 10px;">
        	<div style="height: 10px; width: 100%;"></div>
        </td>
    </tr>
    <tr>
        <td>
            <table width="620" height="80" border="0" cellpadding="0" cellspacing="0" style="margin:0; padding:15px;background:#ffffff">
                <tr>
					<td><a href="<?php echo base_url();?>"><img style="vertical-align: top;" src="<?php echo base_url('theme/accent/css/images/logo.png');?>" alt="ATEHNO" border="0" width="80"></a></td>
                    <td style="width: 250px;">
                    	<div style="text-align:right;">
                    		<a style="margin-left: 10px;" href="https://ok.ru/atehno" target="_blank"><img style="vertical-align: top;" src="<?php echo base_url('theme/images/ok.png');?>" alt="icon ok" border="0" width="30" height="30"></a>
                    		<a style="margin-left: 5px;" href="https://www.facebook.com/atehno.md" target="_blank"><img style="vertical-align: top;" src="<?php echo base_url('theme/images/fb.png');?>" alt="icon facebook" border="0" width="30" height="30"></a>
                    		<a style="margin-left: 5px;" href="https://vk.com/club86490904" target="_blank"><img style="vertical-align: top;" src="<?php echo base_url('theme/images/vk.png');?>" alt="icon vk" border="0" width="30" height="30"></a>
                    		<a style="margin-left: 5px;" href="https://www.instagram.com/atehno.md" target="_blank"><img style="vertical-align: top;" src="<?php echo base_url('theme/images/ig.png');?>" alt="icon vk" border="0" width="30" height="30"></a>
                    	</div>
                    	<div style="border: 0px; background: #ffffff; width: 250px; height: 50px; text-align: right;">
                    		<div style="padding-top: 10px; line-height: 18px; font-size: 12px; font-family: Arial,sans-serif; color: #bf0000;">Магазин - 0(231) 6 30 90</div>
                    		<div style=" line-height: 18px; font-size: 12px; font-family: Arial,sans-serif; color: #bf0000;">Сервис Центр - 0(231) 6 31 32</div>
                    	</div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="border:0px; padding: 15px; background: #ffffff;">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin:0; padding: 0; background: #ffffff;">
            	<tr>
                    <td style="">
                        <table width="100%" cellpadding="0" cellspacing="0" style="margin:0 auto; padding:0;">
                            <tr>
                                <td>
                                    <p style="font: 16px Arial, sans-serif; color: #555555;">Добрый день,</p>
                                </td>
                            </tr>
                        </table>

                        <table width="100%" cellpadding="0" cellspacing="0" style="margin:0 auto; padding:0;">
                            <tr>
                                <td>
                                    <?php $this->load->view('email_templates/'.$email_content);?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
    		</table>
        </td>
    </tr>
    <tr>
        <td style="padding-top: 15px; text-align:center;">
			<a style="" href="https://ok.ru/atehno" target="_blank"><img style="vertical-align: top;" src="<?php echo base_url('theme/images/ok.png');?>" alt="icon ok" border="0" width="35" height="35"></a>
			<a style="margin-left: 5px;" href="https://www.facebook.com/atehno.md" target="_blank"><img style="vertical-align: top;" src="<?php echo base_url('theme/images/fb.png');?>"  alt="icon facebook" border="0" width="35" height="35"></a>
			<a style="margin-left: 5px;" href="https://vk.com/club86490904" target="_blank"><img style="vertical-align: top;" src="<?php echo base_url('theme/images/vk.png');?>" alt="icon vk" border="0" width="35" height="35"></a>
		</td>
    </tr>
    <tr>
        <td style="padding: 15px 0; font-family: Arial,sans-serif; vertical-align: moddle; text-align:center; color: #a2a2a2; font-size: 11px; line-height: 20px;">
            &copy; <?php echo date('Y');?> atehno.md &mdash; Balti, Moldova <br>
            <a style="font-family: Arial,sans-serif; vertical-align: moddle; text-align:center; color: #a2a2a2; font-size: 11px; line-height: 20px; text-decoration: underline;" href="<?php echo base_url();?>">www.atehno.md</a> | Магазин - 0(231) 6 30 90 | Сервис Центр - 0(231) 6 31 32
        </td>
    </tr>
</table>
</div>
</body>
</html>
