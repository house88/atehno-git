<p style="font: 22px Arial, sans-serif; color: #555555;"><?php echo $title;?></p>
<table width="100%" cellpadding="0" cellspacing="0" style="font: 12px Arial, sans-serif; color: #555555;margin:0 auto; padding:0;">
    <thead>
        <tr>
            <th style="width:75%;text-align:left;border-bottom:2px solid #d2d2d2;font-size:14px; padding-bottom:5px;">Название товара</th>
            <th style="width:25%; text-align:right;border-bottom:2px solid #d2d2d2;font-size:14px; padding-bottom:5px;">Цена</th>
        </tr>
    </thead>
    <tbody>
        <?php $products = json_decode($order['order_ordered_items'], true);?>
        <?php foreach($products as $product){?>
            <tr>
                <td style="width:75%;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                    <a href="<?php echo base_url('products/'.$product['item_url']);?>" style="text-decoration:none;color:#555555;"><?php echo clean_output($product['item_title']);?></a>
                </td>
                <td style="width:25%; text-align:right;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                    <?php echo $product['item_quantity'];?> x <?php echo niceDisplayPrice($product['item_price'], ['useDecimals' => 1 === (int) $order['order_price_decimals']]);?> <?php echo $product['item_currency'];?>
                </td>
            </tr>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td style="font-size:12px;text-align:right;padding-bottom:5px; padding-top:5px;">
                Сумма заказа:
            </td>
            <td style="font-size:12px;text-align:right;padding-bottom:5px; padding-top:5px;">
                <b><?php echo niceDisplayPrice($order['order_price'], ['useDecimals' => 1 === (int) $order['order_price_decimals']]);?> <?php echo $order['order_currency'];?></b>
            </td>
        </tr>
        <?php if((float) $order['order_delivery_price'] > 0){?>
            <tr>
                <td style="font-size:12px;text-align:right;padding-bottom:5px; padding-top:5px;">
                    Доставка:
                </td>
                <td style="font-size:12px;text-align:right;padding-bottom:5px; padding-top:5px;">
                    <b>+ <?php echo niceDisplayPrice($order['order_delivery_price'], ['useDecimals' => 1 === (int) $order['order_price_decimals']]);?> <?php echo $order['order_currency'];?></b>
                </td>
            </tr>
        <?php }?>

        <?php if((float) $order['order_payment_add_price'] > 0){?>
            <tr>
                <td style="font-size:12px;text-align:right;padding-bottom:5px; padding-top:5px;">Оплата, %:</td>
                <td style="font-size:12px;text-align:right;padding-bottom:5px; padding-top:5px;">
                    <b>+ <?php echo niceDisplayPrice($order['order_payment_add_price'], ['useDecimals' => 1 === (int) $order['order_price_decimals']]);?> <?php echo $order['order_currency'];?></b>
                </td>
            </tr>
        <?php }?>

        <?php if($order['order_cashback'] > 0){?>
            <tr>
                <td style="font-size:12px;text-align:right;padding-bottom:5px; padding-top:5px;">
                    Бонусные баллы:
                </td>
                <td style="font-size:12px;text-align:right;padding-bottom:5px; padding-top:5px;">
                    <b>- <?php echo niceDisplayPrice($order['order_cashback'], ['useDecimals' => 1 === (int) $order['order_price_decimals']]);?> <?php echo $order['order_currency'];?></b>
                </td>
            </tr>                       
        <?php }?>
        <tr>
            <td style="font-size:14px;text-align:right;padding-bottom:5px; padding-top:5px;">
                К оплате:
            </td>
            <td style="font-size:14px;text-align:right;padding-bottom:5px; padding-top:5px;">
                <b><?php echo niceDisplayPrice($order['order_price'] + $order['order_delivery_price'] - $order['order_cashback'], ['useDecimals' => 1 === (int) $order['order_price_decimals']]);?> <?php echo $order['order_currency'];?></b>
            </td>
        </tr>
    </tfoot>
</table>

<table width="100%" cellpadding="0" cellspacing="0" style="font: 12px Arial, sans-serif; color: #555555;margin:0 auto; padding:0;">
    <tbody>
        <tr>
            <td style="width:120px;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                Состояние заказа
            </td>
            <td style="text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <?php $order_status = get_order_status($order['order_status']);?>
                <?php echo $order_status['title'];?>
            </td>
        </tr>
        <tr>
            <td style="width:120px;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                Дата
            </td>
            <td style="text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <?php echo getDateFormat($order['order_created']);?>
            </td>
        </tr>
        <tr>
            <td style="width:120px;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                Покупатель
            </td>
            <td style="text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <?php echo $order['order_user_name'];?>
            </td>
        </tr>
        <tr>
            <td style="width:120px;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                Е-мейл
            </td>
            <td style="text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <?php echo $order['order_user_email'];?>
            </td>
        </tr>
        <tr>
            <td style="width:120px;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                Телефон
            </td>
            <td style="text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <?php echo $order['order_user_phone'];?>
            </td>
        </tr>
        <tr>
            <td style="width:120px;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                Адрес доставки
            </td>
            <td style="text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <?php echo $order['order_user_address'];?>
            </td>
        </tr>
        <tr>
            <td style="width:120px;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                Метод доставки
            </td>
            <td style="text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <?php echo $delivery['delivery_title'];?>
            </td>
        </tr>
        <tr>
            <td style="width:120px;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                Способ оплаты
            </td>
            <td style="text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <?php echo $payment['payment_title'];?>
                <?php if($payment['payment_by_credit'] && !empty($payment['credit_option'])){?>
                    &mdash; на <?php echo $payment['credit_option']['months'];?> месяцев, по <?php echo niceDisplayPrice($payment['credit_option']['creditMonthlyAmount'], ['useDecimals' => 1 === (int) $order['order_price_decimals']]);?> <?php echo $payment['credit_option']['credit_currency'];?>/мес.
                <?php }?>
            </td>
        </tr>
        <tr>
            <td style="width:120px;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                Пояснение
            </td>
            <td style="text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <?php echo $order['order_user_comment'];?>
            </td>
        </tr>
        <tr>
            <td style="width:120px;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                Страница заказа
            </td>
            <td style="text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <a href="<?php echo base_url('order/'.$order['order_hash']);?>" style="text-decoration:none;color:#555555;">перейти</a>
            </td>
        </tr>
    </tbody>
</table>
<?php if(!empty($changes)){?>
    <p style="font: 20px Arial, sans-serif; color: #555555;">Изменения в заказе:</p>
    <?php foreach($changes as $change){?>        
        <p style="font: 14px Arial, sans-serif; color: #555555;">&mdash; <?php echo $change;?></p>
    <?php }?>
<?php }?>