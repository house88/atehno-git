<p style="font: 22px Arial, sans-serif; color: #555555;"><?php echo $title;?></p>
<table width="100%" cellpadding="0" cellspacing="0" style="font: 12px Arial, sans-serif; color: #555555;margin:0 auto; padding:0;">
    <thead>
        <tr>
            <th style="width:100%;text-align:left;border-bottom:2px solid #d2d2d2;font-size:14px; padding-bottom:5px;">Название товара</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="width:100%;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <a href="<?php echo base_url('products/'.$product['item_url']);?>" style="text-decoration:none;color:#555555;"><?php echo clean_output($product['item_title']);?></a>
                <p>Артикул: <?php echo $product['item_code'];?><br>Ид товара: <?php echo $product['item_prog_id'];?></p>
            </td>
        </tr>
    </tbody>
</table>

<table width="100%" cellpadding="0" cellspacing="0" style="font: 12px Arial, sans-serif; color: #555555;margin:0 auto; padding:0;">
    <tbody>
        <tr>
            <td style="width:120px;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                Дата
            </td>
            <td style="text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <?php echo getDateFormat($showroom['showroom_date']);?>
            </td>
        </tr>
    </tbody>
</table>