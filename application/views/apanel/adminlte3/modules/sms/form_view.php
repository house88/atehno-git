<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($delivery) ? 'Редактировать' : 'Добавить';?> СМС щаблон
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="title" value="<?php echo @$smsTemplate['title'];?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Текст</label>
                            <div class="alert alert-warning rounded-0">Текст отправляемого сообщения. Не более 160 символов.</div>
                            <textarea class="form-control form-control-sm rounded-0" name="text" rows="4"><?php echo @$smsTemplate['text'];?></textarea>
                        </div>
                    </div>
                </div>
                <fieldset>
                    <legend>Настройки для Заказов</legend>
                    <table class="table table-bordered table-hover main-data-table" id="dtTable" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Статус заказа</th>
                                <th>Доставка + Оплата</th>
                            </tr>
                        </thead>
                        <body>
                            <?php $orderStatusesDetails = get_order_statuses();?>
                            <?php foreach($orderStatusesDetails as $orderStatusDetails){?>
                                <tr>
                                    <td>
                                        <?php echo $orderStatusDetails['title'];?>
                                        <input type="hidden" name="sms_config[<?php echo $orderStatusDetails['id'];?>]">
                                    </td>
                                    <td>
                                        <?php 
                                            $selectedRelationsForStatus = !empty($smsTemplateConfigs[$orderStatusDetails['id']]) ? array_column($smsTemplateConfigs[$orderStatusDetails['id']], 'id_payment_delivery_relation') : [];
                                            $deliveryPaymentRelationsOptions = Modules::run('orders/_getDeliveryPaymentsRelationsOptions', $selectedRelationsForStatus);
                                        ?>
                                        <select name="sms_config[<?php echo $orderStatusDetails['id'];?>][]" class="js-multiselect-payment-delivery-relation d-flex custom-width-auto_i custom-width-max-450 custom-margin-right-0" multiple>
                                            <option value="all" <?php echo get_choice('selected', in_array(NULL, $selectedRelationsForStatus));?>>Все</option>
                                            <?php echo $deliveryPaymentRelationsOptions;?>
                                        </select>
                                    </td>
                                </tr>
                            <?php }?>
                        </body>
                    </table>
                </fieldset>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <div class="icheck-primary">
                    <input name="is_active" type="checkbox" id="js-checkbox-sms-active" <?php echo set_checkbox('active', 1, !empty($smsTemplate) ? (int) $smsTemplate['is_active'] === 1 : false);?>>
                    <label for="js-checkbox-sms-active">
                        Активный
                    </label>
                </div>
                <div class="d-flex">
                    <?php if(!empty($smsTemplate)){?>
                        <input type="hidden" name="idTemplate" value="<?php echo $smsTemplate['id'];?>">
                    <?php }?>
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(".js-multiselect-payment-delivery-relation").selectpicker();
    
    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/sms/ajax_operations/setUpdate',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });
</script>
