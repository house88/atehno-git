<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12 col-lg-6">
			<div class="card card-widget custom-margin-top-10">
				<div class="card-header pl-2 pl-sm-3 pr-2 pr-sm-3">
					<h3 class="card-title">
                        Отчет по контенту
					</h3>
				</div>
				<div class="card-body pl-2 pl-sm-3 pr-2 pr-sm-3">
                    <form role="form" id="report_content_menedger_form">
                        <div class="row">
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <label>Менеджер</label>
                                    <select name="id_manager" class="form-control form-control-sm rounded-0 bs-select-element" data-live-search="true" data-style="btn btn-default btn-sm btn-flat bg-white" data-title="Выберите менеджера">
                                        <option>Выберите менеджера</option>
                                        <?php if(!empty($contentManagers)){?>
                                            <optgroup label="Контент менеджеры">
                                                <?php foreach($contentManagers as $contentManager){?>
                                                    <option value="<?php echo $contentManager['id'];?>"><?php echo $contentManager['user_nicename'];?></option>                                        
                                                <?php }?>
                                            </optgroup>
                                        <?php }?>

                                        <?php if(!empty($contentModerators)){?>
                                            <optgroup label="Модераторы">
                                                <?php foreach($contentModerators as $contentModerator){?>
                                                    <option value="<?php echo $contentModerator['id'];?>"><?php echo $contentModerator['user_nicename'];?></option>                                        
                                                <?php }?>
                                            </optgroup>
                                        <?php }?>
                                        
                                        <?php if(!empty($adminManagers)){?>
                                            <optgroup label="Администраторы">
                                                <?php foreach($adminManagers as $adminManager){?>
                                                    <option value="<?php echo $adminManager['id'];?>"><?php echo $adminManager['user_nicename'];?></option>                                        
                                                <?php }?>
                                            </optgroup>
                                        <?php }?>
                                    </select>
                                </div>                                
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group" id="js-content-manager-categories">
                                    <label>Категория</label>
                                    <select name="id_category" data-title="Категория" class="form-control form-control-sm rounded-0">
                                        <option value="">Выберите категорию</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <label>Дата</label>
                                    <div class="input-group input-group-sm">
                                        <div class="input-group-prepend rounded-0">
                                            <span class="input-group-text"><i class="fad fa-calendar-week"></i></span>
                                        </div>
                                        <input type="text" name="report_date" class="form-control float-right rounded-0" data-title="Дата" placeholder="ex. 21.01.2020 - 31.01.2020" id="js-manager-report-date" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group clearfix">
                                    <div class="icheck-primary">
                                        <input type="checkbox" id="js-report-content-is-moderated" name="is_moderated">
                                        <label for="js-report-content-is-moderated">Отмодерированные</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group clearfix">
                                    <div class="icheck-primary">
                                        <input type="checkbox" id="js-report-content-has_issue" name="has_issue">
                                        <label for="js-report-content-has_issue">С ошибками</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group text-right">
                                    <button type="button" class="btn btn-success btn-sm btn-flat call-function" data-callback="completedReport">Отчет</button>
                                    <button type="button" class="btn btn-secondary btn-sm btn-flat call-function" data-callback="clearCompletedReport">Очистить</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover main-data-table" id="cManagerTable">
                            <thead>
                                <tr>
                                    <th class="custom-width-50 text-center">
                                        <button type="button" class="btn btn-default btn-flat btn-sm disabled call-function" data-callback="toggle_all">
											<i class="fad fa-plus"></i>
										</button>
                                    </th>
                                    <th>Категория</th>
                                    <th class="custom-width-50 text-center" title="Завершено"><i class="fad fa-check text-success"></i></th>
                                    <th class="custom-width-50 text-center" title="Отмодерировано"><i class="fad fa-check-double text-primary"></i></th>
                                    <th class="custom-width-70 text-center" title="Вознаграждение"><i class="fad fa-badge-dollar text-primary"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="odd dataTables_empty"><td valign="top" colspan="5" class="dataTables_empty text-center">В таблице отсутствуют данные</td></tr>
                            </tbody>
                            <tfoot style="display: none;">
                                <tr>
                                    <th colspan="2" class="text-right">
                                        Итого
                                    </th>
                                    <th class="custom-width-50 text-center" id="js-report-total-completed">0</th>
                                    <th class="custom-width-50 text-center" id="js-report-total-moderated">0</th>
                                    <th class="custom-width-70 text-center" id="js-report-total-amount">0</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
				</div>
			</div>
			<div class="card card-widget custom-margin-top-10">
				<div class="card-header pl-2 pl-sm-3 pr-2 pr-sm-3">
					<h3 class="card-title">
                        Отчет по модерации
					</h3>
				</div>
				<div class="card-body pl-2 pl-sm-3 pr-2 pr-sm-3">
                    <form role="form" id="report_content_moderator_form">
                        <div class="row">
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <label>Модератор</label>
                                    <select name="id_moderator" class="form-control form-control-sm rounded-0 bs-select-element" data-live-search="true" data-style="btn btn-default btn-sm btn-flat bg-white" data-title="Выберите модератора">
                                        <option>Выберите модератора</option>
                                        <?php if(!empty($contentManagers)){?>
                                            <optgroup label="Контент менеджеры">
                                                <?php foreach($contentManagers as $contentManager){?>
                                                    <option value="<?php echo $contentManager['id'];?>"><?php echo $contentManager['user_nicename'];?></option>                                        
                                                <?php }?>
                                            </optgroup>
                                        <?php }?>

                                        <?php if(!empty($contentModerators)){?>
                                            <optgroup label="Модераторы">
                                                <?php foreach($contentModerators as $contentModerator){?>
                                                    <option value="<?php echo $contentModerator['id'];?>"><?php echo $contentModerator['user_nicename'];?></option>                                        
                                                <?php }?>
                                            </optgroup>
                                        <?php }?>
                                        
                                        <?php if(!empty($adminManagers)){?>
                                            <optgroup label="Администраторы">
                                                <?php foreach($adminManagers as $adminManager){?>
                                                    <option value="<?php echo $adminManager['id'];?>"><?php echo $adminManager['user_nicename'];?></option>                                        
                                                <?php }?>
                                            </optgroup>
                                        <?php }?>
                                    </select>
                                </div>                                
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group" id="js-content-moderator-categories">
                                    <label>Категория</label>
                                    <select name="id_category" data-title="Категория" class="form-control form-control-sm rounded-0">
                                        <option value="">Выберите категорию</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <label>Дата</label>
                                    <div class="input-group input-group-sm">
                                        <div class="input-group-prepend rounded-0">
                                            <span class="input-group-text"><i class="fad fa-calendar-week"></i></span>
                                        </div>
                                        <input type="text" name="report_date" class="form-control float-right rounded-0" data-title="Дата" placeholder="ex. 21.01.2020 - 31.01.2020" id="js-moderator-report-date" readonly>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12 col-lg-4">
                                <div class="form-group clearfix">
                                    <div class="icheck-primary">
                                        <input type="checkbox" id="js-report-manager-is-moderated" name="is_not_filled">
                                        <label for="js-report-manager-is-moderated">Не Заполненно</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group clearfix">
                                    <div class="icheck-primary">
                                        <input type="checkbox" id="js-report-manager-has_issue" name="has_issue">
                                        <label for="js-report-manager-has_issue">С ошибками</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group text-right">
                                    <button type="button" class="btn btn-success btn-sm btn-flat call-function" data-callback="moderatedReport">Отчет</button>
                                    <button type="button" class="btn btn-secondary btn-sm btn-flat call-function" data-callback="clearModeratedReport">Очистить</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover main-data-table" id="cModeratorTable">
                            <thead>
                                <tr>
                                    <th class="custom-width-50 text-center">
                                        <button type="button" class="btn btn-default btn-flat btn-sm disabled call-function" data-callback="toggle_all">
											<i class="fad fa-plus"></i>
										</button>
                                    </th>
                                    <th>Категория</th>
                                    <th class="custom-width-50 text-center" title="Отмодерировано"><i class="fad fa-check-double text-primary"></i></th>
                                    <th class="custom-width-70 text-center" title="Вознаграждение"><i class="fad fa-badge-dollar text-primary"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="odd dataTables_empty"><td valign="top" colspan="5" class="dataTables_empty text-center">В таблице отсутствуют данные</td></tr>
                            </tbody>
                            <tfoot style="display: none;">
                                <tr>
                                    <th colspan="2" class="text-right">
                                        Итого
                                    </th>
                                    <th class="custom-width-50 text-center" id="js-moderated-report-total">0</th>
                                    <th class="custom-width-70 text-center" id="js-moderated-report-total-amount">0</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
				</div>
			</div>
		</div>
		<div class="col-12 col-lg-6">
			<div class="card card-widget custom-margin-top-10">
				<div class="card-header pl-2 pl-sm-3 pr-2 pr-sm-3">
					<h3 class="card-title">
                        Отчет по ошибкам
					</h3>
				</div>
				<div class="card-body pl-2 pl-sm-3 pr-2 pr-sm-3">
                    <form role="form" id="report_moderator_form">
                        <div class="row">
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <label>Тип</label>
                                    <select name="type" class="form-control form-control-sm rounded-0">
                                        <option value="content">Заполнял информацию</option>
                                        <option value="moderator">Отмодерировал</option>
                                        <option value="issuer">Нащел ошибку</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <label>Менеджер</label>
                                    <select name="id_manager[]" class="form-control form-control-sm rounded-0 bs-select-element" data-live-search="true" data-style="btn btn-default btn-sm btn-flat bg-white" data-title="Выберите менеджера" multiple>
                                        <?php if(!empty($contentManagers)){?>
                                            <optgroup label="Контент менеджеры">
                                                <?php foreach($contentManagers as $contentManager){?>
                                                    <option value="<?php echo $contentManager['id'];?>"><?php echo $contentManager['user_nicename'];?></option>                                        
                                                <?php }?>
                                            </optgroup>
                                        <?php }?>

                                        <?php if(!empty($contentModerators)){?>
                                            <optgroup label="Модераторы">
                                                <?php foreach($contentModerators as $contentModerator){?>
                                                    <option value="<?php echo $contentModerator['id'];?>"><?php echo $contentModerator['user_nicename'];?></option>                                        
                                                <?php }?>
                                            </optgroup>
                                        <?php }?>
                                        
                                        <?php if(!empty($adminManagers)){?>
                                            <optgroup label="Администраторы">
                                                <?php foreach($adminManagers as $adminManager){?>
                                                    <option value="<?php echo $adminManager['id'];?>"><?php echo $adminManager['user_nicename'];?></option>                                        
                                                <?php }?>
                                            </optgroup>
                                        <?php }?>
                                    </select>
                                </div>                                
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <label>Дата</label>
                                    <div class="input-group input-group-sm">
                                        <div class="input-group-prepend rounded-0">
                                            <span class="input-group-text"><i class="fad fa-calendar-week"></i></span>
                                        </div>
                                        <input type="text" name="completed_date" class="form-control float-right rounded-0" data-title="Дата" placeholder="ex. 21.01.2020 - 31.01.2020" id="js-issue-report-date" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-lg-4">
                                <div class="form-group clearfix">
                                    <div class="icheck-primary">
                                        <input type="checkbox" id="js-report-issue-is-completed" name="is_filled">
                                        <label for="js-report-issue-is-completed">Помеченны как Заполненно</label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12 col-lg-3">
                                <div class="form-group clearfix">
                                    <div class="icheck-primary">
                                        <input type="checkbox" id="js-report-issue-is-moderated" name="is_moderated">
                                        <label for="js-report-issue-is-moderated">Отмодерированные</label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12 col-lg-3">
                                <div class="form-group clearfix">
                                    <div class="icheck-primary">
                                        <input type="checkbox" id="js-report-issue-show-items" name="show_items">
                                        <label for="js-report-issue-show-items">Показать товары</label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12 col-lg-2">
                                <div class="form-group text-right">
                                    <button type="button" class="btn btn-success btn-sm btn-flat call-function" title="Отчет" data-callback="issuesReport">
                                        <i class="fad fa-file-chart-line"></i>
                                    </button>
                                    <button type="button" class="btn btn-secondary btn-sm btn-flat call-function" title="Очистить" data-callback="clearIssuesReport">
                                        <i class="fad fa-eraser"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    
                    <div class="table-responsive" id="js-issue-reports-results">
                    
                    </div>
				</div>
			</div>
		</div>
	</div>
</section>