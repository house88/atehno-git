<?php $this->load->view($this->theme->apanel_view('includes/plugins/daterangepicker'));?>

<script type="text/template" id="js-manager-issue-report-template">
	<table class="table table-bordered table-hover main-data-table" id="js-report-issues-{{id_manager}}-table">
		<thead>
			<tr>
				<th colspan="2">{{name}}</th>
				<th class="custom-width-50 text-center" title="Количество"><i class="fad fa-bug text-danger"></i></th>
			</tr>
		</thead>
		<tbody>
			{{report}}
		</tbody>
		<tfoot style="display: none;">
			<tr>
				<th colspan="2" class="text-right">
					Итого
				</th>
				<th class="custom-width-50 text-center" id="js-report-issues-total">{{total}}</th>
			</tr>
		</tfoot>
	</table>
</script>

<script>	
    var cManagerTable;
    var cModeratorTable;

	$(function(){
		cManagerTable = $('#cManagerTable');
		cModeratorTable = $('#cModeratorTable');
		
		$('select[name="id_manager"]').on('change', function(){
			var user = intval($(this).val());
			var user_categories_html = '<option value="">Выберите категорию</option>';
			if(user > 0){
				$.ajax({
					type: 'POST',
					url: base_url+'admin/categories/ajax_operations/get_user_categories',
					data: {user:user},
					dataType: 'JSON',
					beforeSend: function(){
						showLoader('body');
						clearSystemMessages();
					},
					success: function(resp){
                		hideLoader('body');
						if(resp.mess_type == 'success'){
							user_categories_html += resp.user_categories;
							$('#js-content-manager-categories select').html(user_categories_html);
						} else{
							systemMessages(resp.message, resp.mess_type);
						}
					}
				});
			} else{
				$('#user_categories_block select').html(user_categories_html);
			}
		});

		createDateRangePicker('#js-manager-report-date');
		createDateRangePicker('#js-moderator-report-date');
		createDateRangePicker('#js-issue-report-date');

		$('.selectpicker').selectpicker('refresh');
	});

	function createDateRangePicker(element){
		$(element).daterangepicker({
			timePicker: false,
			locale: {
				format: 'DD.MM.YYYY'
			},
			autoUpdateInput: false,
			autoApply: true
		}).on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD.MM.YYYY') + ' - ' + picker.endDate.format('DD.MM.YYYY')).trigger('change');
		}).on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('').trigger('change');
		});
	}

	var clearCompletedReport = function(element){
		var $this = $(element);
		var form = $this.closest('form');
		form.trigger("reset");
		form.find('.bs-select-element').selectpicker('refresh');
		createDateRangePicker('#js-manager-report-date');
		
		cManagerTable.find('tbody').html('<tr class="odd dataTables_empty"><td valign="top" colspan="5" class="dataTables_empty text-center">В таблице отсутствуют данные</td></tr>');
		cManagerTable.find('tfoot').hide();
	}

	var clearModeratedReport = function(element){
		var $this = $(element);
		var form = $this.closest('form');
		form.trigger("reset");
		form.find('.bs-select-element').selectpicker('refresh');
		createDateRangePicker('#js-moderator-report-date');
		
		cModeratorTable.find('tbody').html('<tr class="odd dataTables_empty"><td valign="top" colspan="4" class="dataTables_empty text-center">В таблице отсутствуют данные</td></tr>');
		cModeratorTable.find('tfoot').hide();
	}

	var clearIssuesReport = function(element){
		var $this = $(element);
		var form = $this.closest('form');
		form.trigger("reset");
		form.find('.bs-select-element').selectpicker('refresh');
		createDateRangePicker('#js-issue-report-date');
		$('#js-issue-reports-results').html('');
	}

	var completedReport = function (btn){
		var $this = $(btn);
		var $form = $this.closest('form');            
		$.ajax({
			type: 'POST',
			url: base_url+'items/reports/ajax_operations/fill',
			data: $form.serialize(),
			dataType: 'JSON',
			beforeSend: function(){
				showLoader('body');
				clearSystemMessages();
				cManagerTable.find('tbody').html('<tr class="odd dataTables_empty"><td valign="top" colspan="5" class="dataTables_empty text-center">Загрузка данных...</td></tr>');
				cManagerTable.find('thead [data-callback="toggle_all"]').addClass('disabled');
			},
			success: function(resp){
				hideLoader('body');
				if(resp.mess_type == 'success'){
					if(resp.categories){
						cManagerTable.find('tbody').html(resp.categories);
						cManagerTable.find('thead [data-callback="toggle_all"]').removeClass('disabled');
						cManagerTable.find('#js-report-total-completed').html(resp.totals.count_completed);
						cManagerTable.find('#js-report-total-moderated').html(resp.totals.count_moderated);
						cManagerTable.find('#js-report-total-amount').html(resp.totals.amount + ' <small>лей</small>');
						cManagerTable.find('tfoot').show();
					} else{
						cManagerTable.find('tbody').html('<tr class="odd dataTables_empty"><td valign="top" colspan="5" class="dataTables_empty text-center">В таблице отсутствуют данные</td></tr>');
						cManagerTable.find('tfoot').hide();
					}
				} else{
					systemMessages(resp.message, resp.mess_type);
					cManagerTable.find('tbody').html('<tr class="odd dataTables_empty"><td valign="top" colspan="5" class="dataTables_empty text-center">В таблице отсутствуют данные</td></tr>');
					cManagerTable.find('tfoot').hide();
				}
			}
		});
		return false;
	}

	var moderatedReport = function (btn){
		var $this = $(btn);
		var $form = $this.closest('form');            
		$.ajax({
			type: 'POST',
			url: base_url+'items/reports/ajax_operations/moderate',
			data: $form.serialize(),
			dataType: 'JSON',
			beforeSend: function(){
				showLoader('body');
				clearSystemMessages();
				cModeratorTable.find('tbody').html('<tr class="odd dataTables_empty"><td valign="top" colspan="4" class="dataTables_empty text-center">Загрузка данных...</td></tr>');
				cModeratorTable.find('thead [data-callback="toggle_all"]').addClass('disabled');
			},
			success: function(resp){
				hideLoader('body');
				if(resp.mess_type == 'success'){
					if(resp.categories){
						cModeratorTable.find('tbody').html(resp.categories);
						cModeratorTable.find('thead [data-callback="toggle_all"]').removeClass('disabled');
						cModeratorTable.find('#js-moderated-report-total').html(resp.totals.count_moderated);
						cModeratorTable.find('#js-moderated-report-total-amount').html(resp.totals.amount + ' <small>лей</small>');
						cModeratorTable.find('tfoot').show();
					} else{
						cModeratorTable.find('tbody').html('<tr class="odd dataTables_empty"><td valign="top" colspan="4" class="dataTables_empty text-center">В таблице отсутствуют данные</td></tr>');
						cModeratorTable.find('tfoot').hide();
					}
				} else{
					systemMessages(resp.message, resp.mess_type);
					cModeratorTable.find('tbody').html('<tr class="odd dataTables_empty"><td valign="top" colspan="4" class="dataTables_empty text-center">В таблице отсутствуют данные</td></tr>');
					cModeratorTable.find('tfoot').hide();
				}
			}
		});
		return false;
	}
       
	var issuesReport = function (btn) {
		var $this = $(btn);
		var $form = $this.closest('form');
		$.ajax({
			type: 'POST',
			url: base_url+'items/reports/ajax_operations/issues',
			data: $form.serialize(),
			dataType: 'JSON',
			beforeSend: function(){
				showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				hideLoader('body');

				if(resp.mess_type == 'success'){
					var reports = [];
					$.each(resp.managers, function(id_manager, manager){
						var template = $('#js-manager-issue-report-template').text();
						template = template.replace(/{{id_manager}}/g, id_manager);
						template = template.replace(/{{name}}/g, manager.info.user_nicename??'N/A');
						template = template.replace(/{{report}}/g, manager.categories);
						template = template.replace(/{{total}}/g, manager.totals.count_issues);
						reports.push(template);
					});

					$('#js-issue-reports-results').html(reports.join(''));

				} else{
					systemMessages(resp.message, resp.mess_type);
				}
			}
		});
		return false;
	}
	
	var toggle_all = function(element){
		var $this = $(element);
		var table = $this.closest('table');
		var $icon = $this.find('i.fad');

		if($icon.hasClass('fa-plus')){
			$icon.removeClass('fa-plus').addClass('fa-minus');
			table.find('tbody > tr').show().find('.js-toggle-categories > i').removeClass('fa-plus').addClass('fa-minus');;
		} else{
			$icon.removeClass('fa-minus').addClass('fa-plus');
			table.find('tbody > tr[data-parent="0"]').find('.js-toggle-categories > i').removeClass('fa-minus').addClass('fa-plus');
			table.find('tbody > tr:not([data-parent="0"])').hide().find('.js-toggle-categories > i').removeClass('fa-minus').addClass('fa-plus');
		}
	}

	var toggle_one = function(element){
		var $this = $(element);
		var table = $this.closest('table');
		var $tr = $this.closest('tr');
		var $icon = $this.find('i.fad');

		if($icon.hasClass('fa-plus')){
			$icon.removeClass('fa-plus').addClass('fa-minus');
			table.find('tr[data-parent="'+ $tr.data('category') +'"]').show();
		} else{
			$icon.removeClass('fa-minus').addClass('fa-plus');
			table.find('tr[data-parents*="'+ $this.data('toggle') +'-"]').hide().find('.js-toggle-categories > i').removeClass('fa-minus').addClass('fa-plus');
		}
	}
</script>