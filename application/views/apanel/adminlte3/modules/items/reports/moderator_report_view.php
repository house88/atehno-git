<div class="card card-widget widget-user-2">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-info">
        <h3 class="widget-user-username ml-0"><?php echo $user_info->user_nicename;?></h3>
    </div>
    <div class="card-footer p-0">
        <ul class="nav flex-column">
            <li class="nav-item">
                <span class="nav-link">
                    Всего обработано <span class="float-right badge bg-success"><?php echo $report_done_count;?></span>
                </span>
            </li>
            <li class="nav-item">
                <span class="nav-link">
                    Обработано с ошибками <span class="float-right badge bg-danger"><?php echo $report_done_bug_count;?></span>
                </span>
            </li>
        </ul>
    </div>
</div>