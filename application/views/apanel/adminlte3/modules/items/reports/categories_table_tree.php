<?php if(!empty($category)){?>
		<?php 
			$breads = json_decode('['.$category['category_breadcrumbs'].']', true);
			$cat_parents = array_column($breads, 'category_id');
		?>
		<tr class="parent_<?php echo $category['category_parent'];?>" data-category="<?php echo $category['category_id'];?>" data-parent="<?php echo $category['category_parent'];?>" data-parents="p<?php echo implode('-', $cat_parents);?>" <?php if($category['category_parent'] != 0){echo 'style="display:none;"';}?>>
			<td class="text-center">
				<?php if(!empty($category['category_children'])){?>
					<button type="button" class="btn btn-default btn-xs js-toggle-categories call-function" data-callback="toggle_one" data-toggle="p<?php echo implode('-', $cat_parents);?>"><i class="fad fa-plus"></i></button>
				<?php }?>
			</td>
			<td>
				<?php if((int) $category['category_level'] === 2){?>
					<span class="custom-padding-left-15">
						<i class="fad fa-arrow-from-left"></i>
					</span>
				<?php }?>
				<?php if($category['category_level'] > 2){?>
					<span class="custom-padding-left-<?php echo $category['category_level'] * 10;?>">
						<i class="fad fa-arrow-right"></i>
					</span>
				<?php }?>
				<?php echo $category['category_title'];?>
			</td>
			<td class="text-center"><?php echo $category['edit_items_count'];?></td>
			<td class="text-center"><?php echo $category['edit_items_count_moderated'];?></td>
			<td class="text-center"><?php echo numberFormat($category['edit_item_price']*$category['edit_items_count'], true);?></td>
		</tr>
<?php }?>
