<?php if(!empty($category)){?>
		<?php 
			$breads = json_decode('['.$category['category_breadcrumbs'].']', true);
			$cat_parents = array_column($breads, 'category_id');
		?>
		<tr class="parent_<?php echo $category['category_parent'];?>" data-category="<?php echo $category['category_id'];?>" data-parent="<?php echo $category['category_parent'];?>" data-parents="p<?php echo implode('-', $cat_parents);?>" <?php if($category['category_parent'] != 0){echo 'style="display:none;"';}?>>
			<td class="custom-width-50 text-center">
				<?php if(!empty($hasChildren)){?>
					<button type="button" class="btn btn-default btn-xs js-toggle-categories call-function" data-callback="toggle_one" data-toggle="p<?php echo implode('-', $cat_parents);?>"><i class="fad fa-plus"></i></button>
				<?php }?>
			</td>
			<td>
				<?php if((int) $category['category_level'] === 2){?>
					<span class="custom-padding-left-15">
						<i class="fad fa-arrow-from-left"></i>
					</span>
				<?php }?>
				<?php if($category['category_level'] > 2){?>
					<span class="custom-padding-left-<?php echo $category['category_level'] * 10;?>">
						<i class="fad fa-arrow-right"></i>
					</span>
				<?php }?>
				<?php echo $category['category_title'];?>

				<?php if(true === $displayItemsList && !empty($category['products'])){?>
					<div class="list-group custom-margin-top-15 custom-margin-left-<?php echo $category['category_level'] * 10;?>">
						<a href="#" class="list-group-item list-group-item-action bg-secondary disabled" aria-current="true">Товары</a>
						<?php foreach($category['products'] as $product){?>
							<?php if(!empty($product['item_title'])){?>
								<a href="<?php echo base_url("admin/items/edit/{$product['id_item']}");?>" target="_blank" class="list-group-item list-group-item-action"><?php echo $product['item_title'];?></a>
							<?php } else{?>
								<a href="#" class="list-group-item list-group-item-action disabled">ID: <?php echo $product['id_item'];?>, <span class="badge bg-danger">удален</span></a>
							<?php }?>
						<?php }?>
					</div>
				<?php }?>
			</td>
			<td class="text-center"><?php echo $category['issues_count'];?></td>
		</tr>
<?php }?>
