<!-- Main content -->
<section class="content">
    <div class="card card-primary card-outline card-outline-tabs custom-margin-top-10">
        <div class="card-header p-0 border-bottom-0">
            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" title="Редактировать товар" id="js-edit-tab" data-toggle="pill" href="#js-edit-tab-content" role="tab" aria-controls="js-edit-tab-content" aria-selected="true">
                        <span class="fad fa-pencil"></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" title="Копировать информацию" id="js-copy-tab" data-toggle="pill" href="#js-copy-tab-content" role="tab" aria-controls="js-copy-tab-content" aria-selected="false">
                        <span class="fad fa-copy"></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" title="Правила редактирования" id="js-edit-rules-tab" data-toggle="pill" href="#js-edit-rules-tab-content" role="tab" aria-controls="js-edit-rules-tab-content" aria-selected="false">
                        <span class="fad fa-pencil-ruler"></span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content" id="custom-tabs-three-tabContent">
                <div class="tab-pane fade show active" id="js-edit-tab-content" role="tabpanel" aria-labelledby="js-edit-tab">
                    <form role="form" id="edit_form">
                        <div>
                            <div class="row">
                                <div class="col-12 col-md-8">
                                    <div class="form-group">
                                        <label>Название</label>
                                        <?php if($this->lauth->have_right('edit_items_full')){?>
                                            <div class="input-group input-group-sm">
                                                <input class="form-control form-control-sm rounded-0" placeholder="Название" name="title" value="<?php echo clean_output($item['item_title']);?>">
                                                <div class="input-group-prepend">
                                                    <a href="<?php echo base_url('products/'.$item['item_url']);?>" class="btn btn-primary btn-flat btn-sm" target="_blank">
                                                        <span class="fad fa-link"></span>
                                                    </a>
                                                </div>
                                            </div>								
                                        <?php } else{?>
                                            <div>
                                                <a href="<?php echo base_url('products/'.$item['item_url']);?>" target="_blank"><?php echo $item['item_title'];?></a>
                                            </div>
                                        <?php }?>
                                    </div>
                                    <div class="form-group">
                                        <label>Категория</label>
                                        <?php 
                                            $category_breadcrumbs = json_decode('['.$item_category['category_breadcrumbs'].']', true);
                                            $cbread = array();
                                            foreach ($category_breadcrumbs as $category_breadcrumb) {
                                                $cbread[] = $category_breadcrumb['category_title'];
                                            }
                                        ?>
                                        <div><?php echo implode(' / ', $cbread);?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Артикул</label>
                                                <div><?php echo $item['item_code'];?></div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Код</label>
                                                <div><?php echo $item['item_prog_id'];?></div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label>Поставщики</label>
                                        <?php $item_supliers = json_decode($item['item_suppliers'], true);?>
                                        <?php if(!empty($item_supliers)){?>
                                            <ul class="list-group">
                                                <?php foreach($item_supliers as $item_suplier){?>
                                                    <?php if(!empty($supliers[$item_suplier['supplier_prog_id']])){?>
                                                        <li class="list-group-item rounded-0 d-flex justify-content-between align-items-center">
                                                            <?php echo $supliers[$item_suplier['supplier_prog_id']]['supplier_name'];?>, <?php echo $item_suplier['from'];?> &mdash; <?php echo $item_suplier['to'];?> дня
                                                            <span class="badge badge-primary badge-pill">кол. <?php echo $item_suplier['quantity'];?></span>
                                                        </li>
                                                    <?php }?>
                                                <?php }?>
                                            </ul>
                                        <?php } else{?>
                                            <div class="alert alert-info">
                                                <h5><i class="icon fas fa-info"></i> Информация!</h5>
                                                Не заполнено.
                                            </div>
                                        <?php }?>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <?php if(!empty($brands_list)){?>
                                        <div class="form-group">
                                            <label>Брэнд <strong class="text-danger">*</strong></label>
                                            <select name="brand" class="form-control rounded-0 bs-select-element" data-live-search="true" data-style="btn btn-default btn-sm btn-flat bg-white">
                                                <option value="">Выберите brand</option>
                                                <?php foreach($brands_list as $brand){?>
                                                    <option value="<?php echo $brand['id_brand'];?>" <?php echo set_select('brand', $brand['id_brand'], $item['id_brand'] == $brand['id_brand']);?>><?php echo $brand['brand_title'];?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    <?php }?>
                                </div>

                                <?php if($this->lauth->have_right('edit_items_full')){?>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <div class="icheck-primary">
                                                <input type="checkbox" name="visible" id="js-check-item-visible" <?php echo get_choice('checked', (int) $item['item_visible'] === 1);?>>
                                                <label for="js-check-item-visible">Активный</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="icheck-primary">
                                                <input type="checkbox" name="hit" id="js-check-item-hit" <?php echo get_choice('checked', (int) $item['item_hit'] === 1);?>>
                                                <label for="js-check-item-hit">Хит</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="icheck-primary">
                                                <input type="checkbox" name="newest" id="js-check-item-newest" <?php echo get_choice('checked', (int) $item['item_newest'] === 1);?>>
                                                <label for="js-check-item-newest">Новинка</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="icheck-primary">
                                                <input type="checkbox" name="action" id="js-check-item-action" <?php echo get_choice('checked', (int) $item['item_action'] === 1);?>>
                                                <label for="js-check-item-action">Акция</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="icheck-primary">
                                                <input type="checkbox" name="popular" id="js-check-item-popular" <?php echo get_choice('checked', (int) $item['item_popular'] === 1);?>>
                                                <label for="js-check-item-popular">Популярный</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="icheck-primary">
                                                <input type="checkbox" name="on_home" id="js-check-item-on-home" <?php echo get_choice('checked', (int) $item['item_on_home'] === 1);?>>
                                                <label for="js-check-item-on-home">Показать на главной</label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if(!empty($credit_settings)){?>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Кредит 0%</label>
                                                <select name="zero_credit_months" class="form-control form-control-sm rounded-0">
                                                    <option value="">Выберите кол. месяцев</option>
                                                    <?php foreach($credit_settings as $credit_setting){?>
                                                        <option value="<?php echo $credit_setting['months'];?>" <?php echo set_select('zero_credit_months',$credit_setting['months'], $item['item_zero_credit_months'] == $credit_setting['months'])?>>на <?php echo $credit_setting['months'];?> месяцев</option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php }?>

                                    <?php if(!empty($promo_list)){?>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Промо страницы</label>
                                                <select name="item_promo[]" class="form-control rounded-0 bs-select-element" data-live-search="true" data-style="btn btn-default btn-sm btn-flat bg-white" multiple>
                                                    <option value="" data-value-text="">Выберите промо страницы</option>
                                                    <?php foreach($promo_list as $promo){?>
                                                        <option value="<?php echo $promo['id_promo'];?>" <?php echo set_select('item_promo[]', $promo['id_promo'], in_array($promo['id_promo'], (array) $item_promo));?>><?php echo $promo['promo_name'];?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php }?>
                                    
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>Цена</label>
                                            <div class="input-group input-group-sm">
                                                <div class="btn-group" role="group">
                                                    <button id="js-item-price-variants" type="button" class="btn btn-default btn-sm btn-flat dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="d-none d-md-inline">Цены по группам</span>
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="js-item-price-variants">
                                                        <label class="dropdown-item">Цены по группам</label>
                                                        <?php if(!empty($price_variants)){?>
                                                            <?php $index = 1;?>
                                                            <?php foreach($price_variants as $price_variant){?>
                                                                <div class="dropdown-item">
                                                                    <div class="d-flex align-items-center">
                                                                        <input class="form-control form-control-sm rounded-0 custom-width-100 custom-margin-right-10" placeholder="Цена" name="vprice[<?php echo $price_variant['id_price_variant']?>]" value="<?php echo $item['item_price'.'_'.$price_variant['id_price_variant']];?>">
                                                                        <span><?php echo $index .'. '. $price_variant['price_variant_title']?></span>
                                                                    </div>
                                                                </div>
                                                                <?php $index++;?>
                                                            <?php }?>
                                                        <?php } else{?>
                                                            <div class="dropdown-item">Нет груп.</div>
                                                        <?php }?>
                                                    </div>
                                                </div>
                                                <input class="form-control form-control-sm rounded-0" placeholder="Цена" name="price" value="<?php echo $item['item_price'];?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-12 col-sm-6 col-lg-3">
                                                    <label>Temp price</label>
                                                    <input class="form-control form-control-sm rounded-0" placeholder="0.00" name="temp_price" value="<?php echo $item['item_temp_price'];?>">
                                                </div>
                                                <div class="col-12 col-sm-6 col-lg-3">
                                                    <label>Кэшбэк</label>
                                                    <input class="form-control form-control-sm rounded-0" placeholder="0.00" name="cashback_price" value="<?php echo $item['item_cashback_price'];?>">
                                                </div>
                                                <div class="col-12 col-sm-6 col-lg-3">
                                                    <label>Количество</label>
                                                    <input class="form-control form-control-sm rounded-0" placeholder="0" name="quantity" value="<?php echo $item['item_quantity'];?>">
                                                </div>
                                                <div class="col-12 col-sm-6 col-lg-3">
                                                    <label>Гарантия, мес.</label>
                                                    <input class="form-control form-control-sm rounded-0" placeholder="ex. 6" name="product_guaranty" value="<?php echo $item['item_guaranty'];?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }?>

                                <div class="col-12">
                                    <div class="form-group">
                                        <label>
                                            Главная фото товара
                                            <?php if(@$aditional_fields['item_photo']['active'] == 1){?>
                                                <strong class="text-danger">*</strong>
                                            <?php }?>
                                        </label>
                                        <p class="help-block">Мин высота: 1024px, Мин ширина: 768px</p>
                                        <div class="btn-group">
                                            <button class="btn btn-primary btn-flat btn-sm btn-file">
                                                <i class="fad fa-upload"></i>
                                                Загрузить фото <input type="file" id="inputLogo" name="file" accept=".jpg,.jpeg,.png,.gif">
                                            </button>
                                            <button class="btn btn-default btn-flat btn-sm btn-file call-function" data-callback="pasteIntoTextarea" data-wrapper="#paste-main-image-popup">
                                                <i class="fad fa-paste"></i>
                                                Вставить фото
                                            </button>
                                        </div>
                                        <div id="js-uploaded_photo" class="custom-margin-top-10">
                                            <?php if(!empty($item['item_photo'])){?>
                                                <div class="user-image-thumbnail-wr">
                                                    <img class="img-thumbnail" src="<?php echo base_url('files/items/'.$item['item_photo']);?>">
                                                    <div class="actions">
                                                        <a href="#" class="btn btn-danger btn-sm btn-flat btn-block confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="<i class='fad fa-trash-alt'></i> Удалить" data-type="bg-danger" data-photo="<?php echo $item['item_photo'];?>"><i class="fad fa-trash-alt"></i> Удалить</a>
                                                    </div>
                                                    <input type="hidden" name="photo" value="<?php echo $item['item_photo'];?>">
                                                </div>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>
                                            Дополнительные фото товара
                                            <?php if(@$aditional_fields['item_photos']['active'] == 1){?>
                                                <strong class="text-danger fs-18">*</strong>
                                            <?php }?>
                                        </label>
                                        <p class="help-block">Мин высота: 1024px, Мин ширина: 768px</p>
                                        <div class="btn-group">
                                            <button class="btn btn-primary btn-flat btn-sm btn-file">
                                                <i class="fad fa-upload"></i>
                                                Загрузить фото <input type="file" id="inputImages" name="file" accept=".jpg,.jpeg,.png,.gif">
                                            </button>
                                            <button class="btn btn-default btn-flat btn-sm btn-file call-function" data-callback="pasteIntoTextarea" data-wrapper="#paste-additional-image-popup">
                                                <i class="fad fa-paste"></i>
                                                Вставить фото
                                            </button>
                                        </div>

                                        <div id="js-uploaded_photos" class="d-flex flex-wrap custom-margin-top-10">
                                            <?php if(!empty($item['item_photos'])){?>
                                                <?php $photos = json_decode($item['item_photos']);?>
                                                <?php foreach($photos as $photo){?>
                                                    <div class="user-image-thumbnail-wr custom-margin-right-10">
                                                        <img class="img-thumbnail" src="<?php echo base_url('files/items/'.$photo);?>">
                                                        <div class="actions">
                                                            <a href="#" class="btn btn-danger btn-sm btn-flat btn-block confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="<i class='fad fa-trash-alt'></i> Удалить" data-type="bg-danger" data-photo="<?php echo $photo;?>"><i class="fad fa-trash-alt"></i> Удалить</a>
                                                        </div>
                                                        <input type="hidden" name="photos[]" value="<?php echo $photo;?>">
                                                    </div>
                                                <?php }?>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label>
                                    Краткое описание
                                    <?php if(@$aditional_fields['small_description']['active'] == 1){?>
                                        <strong class="text-danger fs-18">*</strong>
                                    <?php }?>
                                </label>
                                <textarea name="small_description" class="form-control" rows="3"><?php echo $item['item_small_description'];?></textarea>
                                <p class="help-block">Не должно содержать более 1000 символов.</p>
                            </div>
                            <div class="form-group">
                                <label>
                                    Полное описание
                                    <?php if(@$aditional_fields['full_description']['active'] == 1){?>
                                        <strong class="text-danger fs-18">*</strong>
                                    <?php }?>
                                </label>
                                <textarea class="description" name="description"><?php echo $item['item_description'];?></textarea>
                            </div>
                            <div class="form-group">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="js-check-use-formatting" name="is_formatted_description">
                                    <label for="js-check-use-formatting">
                                        Использовать формитирование
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>
                                    Видео
                                    <?php if(@$aditional_fields['video']['active'] == 1){?>
                                        <strong class="text-danger fs-18">*</strong>
                                    <?php }?>
                                </label>
                                <input class="form-control" maxlength="250" placeholder="Видео" name="video" value="<?php echo $item['item_video_link'];?>">
                            </div>
                            <div class="form-group">
                                <label>
                                    Meta keywords
                                    <?php if(@$aditional_fields['mk']['active'] == 1){?>
                                        <strong class="text-danger fs-18">*</strong>
                                    <?php }?>
                                </label>
                                <input class="form-control" placeholder="Meta keywords" name="mk" value="<?php echo $item['mk'];?>">
                                <p class="help-block">Не должно содержать более 250 символов.</p>
                            </div>
                            <div class="form-group">
                                <label>
                                    Meta description
                                    <?php if(@$aditional_fields['md']['active'] == 1){?>
                                        <strong class="text-danger fs-18">*</strong>
                                    <?php }?>
                                </label>
                                <input class="form-control" placeholder="Meta description" name="md" value="<?php echo $item['md'];?>">
                                <p class="help-block">Не должно содержать более 250 символов.</p>
                            </div>
                            <div class="form-group clearfix">
                                <label>Название товара</label>
                                <p class="mb-0"><?php echo $item['item_title'];?></p>
                            </div>
                            <div id="properties_block"></div>
                        </div>
                        <input type="hidden" name="item" value="<?php echo $item['id_item'];?>">
                        
                        <fieldset>
                            <legend>Модерация товара</legend>

                            <?php
                                $statesDetails = Modules::run('items/moderate/_getStatesDetails', (int) $item['id_item']);
                                $isItemCompleted = (int) $statesDetails['is_filled'] === 1;
                                $isItemCompletedBug = (int) $statesDetails['has_issue'] === 1;
                                $isItemModerated = (int) $statesDetails['is_moderated'] === 1;
                            ?>

                            <div class="form-group mb-0">
                                <div class="d-flex">
                                    <div class="icheck-primary">
                                        <input type="checkbox" id="js-check-ready" name="data_completed" <?php echo get_choice('checked', $isItemCompleted);?>>
                                        <label for="js-check-ready">
                                            Пометить как завершенный
                                            <?php if(!empty($statesDetails['lastFillManager'])){?>
                                                <small>(<?php echo $statesDetails['lastFillManager']['user_nicename'];?>)</small>
                                            <?php }?>
                                        </label>
                                    </div>

                                    <?php if($this->lauth->have_right('view_item_states_details')){?>
                                        <?php if(!empty($statesDetails['managers'])){?>
                                            <div class="btn-group btn-group-sm custom-margin-left-10">
                                                <button type="button" class="btn btn-light btn-sm dropdown-toggle" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="false">
                                                    <span class="fad fa-info"></span>
                                                </button>
                                                <div class="dropdown-menu p-4 text-muted">
                                                    <table class="table table-bordered mb-0">
                                                        <thead class="thead-light">
                                                            <tr>
                                                                <th>Дата</th>
                                                                <th>Менеджер</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach($statesDetails['managers'] as $stateDetailsManager){?>
                                                                <tr>
                                                                    <td class="text-nowrap font-weight-normal"><?php echo getDateFormat($stateDetailsManager['completed_on'])?></td>
                                                                    <td class="text-nowrap font-weight-normal"><?php echo $stateDetailsManager['user_nicename'];?></td>
                                                                </tr>
                                                            <?php }?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        <?php }?>
                                    <?php }?>
                                </div>
                            </div>
                            <?php if($this->lauth->have_right('moderate_items')){?>
                                <div class="form-group mb-0">
                                    <div class="d-flex">
                                        <div class="icheck-primary">
                                            <input type="checkbox" id="js-check-moderated" name="data_moderated" <?php echo get_choice('checked', $isItemModerated);?>>
                                            <label for="js-check-moderated">
                                                Модерировать товар
                                                <?php if(!empty($statesDetails['lastModerateManager'])){?>
                                                    <small>(<?php echo $statesDetails['lastModerateManager']['user_nicename'];?>)</small>
                                                <?php }?>
                                            </label>
                                        </div>

                                        <?php if($this->lauth->have_right('view_item_states_details')){?>
                                            <?php if(!empty($statesDetails['moderators'])){?>
                                                <div class="btn-group btn-group-sm custom-margin-left-10">
                                                    <button type="button" class="btn btn-light btn-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="false">
                                                        <span class="fad fa-info"></span>
                                                    </button>
                                                    <div class="dropdown-menu p-4 text-muted">
                                                        <table class="table table-bordered mb-0">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>Дата</th>
                                                                    <th>Менеджер</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach($statesDetails['moderators'] as $stateDetailsModerator){?>
                                                                    <tr>
                                                                        <td class="text-nowrap font-weight-normal"><?php echo getDateFormat($stateDetailsModerator['moderated_on'])?></td>
                                                                        <td class="text-nowrap font-weight-normal"><?php echo $stateDetailsModerator['user_nicename'];?></td>
                                                                    </tr>
                                                                <?php }?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <?php }?>
                                        <?php }?>
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <div class="d-flex">
                                        <div class="icheck-primary">
                                            <input type="checkbox" id="js-check-ready-bugs" name="data_bug" <?php echo get_choice('checked', $isItemCompletedBug);?>>
                                            <label for="js-check-ready-bugs">
                                                Информация заполнена с ошибками
                                                <?php if(!empty($statesDetails['lastIssueManager'])){?>
                                                    <small>(<?php echo $statesDetails['lastIssueManager']['user_nicename'];?>)</small>
                                                <?php }?>
                                            </label>
                                        </div>

                                        <?php if($this->lauth->have_right('view_item_states_details')){?>
                                            <?php if(!empty($statesDetails['issuers'])){?>
                                                <div class="btn-group btn-group-sm custom-margin-left-10">
                                                    <button type="button" class="btn btn-light btn-sm dropdown-toggle" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="false">
                                                        <span class="fad fa-info"></span>
                                                    </button>
                                                    <div class="dropdown-menu p-4 text-muted">
                                                        <table class="table table-bordered mb-0">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>Дата</th>
                                                                    <th>Менеджер</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach($statesDetails['issuers'] as $stateDetailsIssuer){?>
                                                                    <tr>
                                                                        <td class="text-nowrap font-weight-normal"><?php echo getDateFormat($stateDetailsIssuer['issue_on'])?></td>
                                                                        <td class="text-nowrap font-weight-normal"><?php echo $stateDetailsIssuer['user_nicename'];?></td>
                                                                    </tr>
                                                                <?php }?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <?php }?>
                                        <?php }?>
                                    </div>
                                </div>
                                <div class="form-group mt-4">
                                    <label>Примечание</label>
                                    <textarea name="data_bug_text" class="form-control form-control-sm rounded-0" rows="3"><?php echo $statesDetails['issue_text'];?></textarea>
                                    <p class="help-block">Не должно содержать более 1000 символов.</p>
                                </div>
                            <?php }?>
                        </fieldset>

                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </form>
                </div>

                <?php if($this->lauth->have_right('copy_item')){?>
                    <div class="tab-pane fade" id="js-copy-tab-content" role="tabpanel" aria-labelledby="js-copy-tab">
                        <form role="form" id="copy_form">
                            <div class="row">
                                <div class="col-12 col-xl-6">
                                    <div class="form-group">
                                        <label>ИД товара</label>
                                        <input class="form-control form-control-sm rounded-0" placeholder="ex. 7770000000081" name="id_item_copy" value="">
                                    </div>
                                    <div class="form-group clearfix">
                                        <div class="icheck-primary">
                                            <input type="checkbox" id="js-copy-brand" name="copy_fields[brand]" checked>
                                            <label for="js-copy-brand">Брэнд</label>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <div class="icheck-primary">
                                            <input type="checkbox" id="js-copy-main-photo" name="copy_fields[main_photo]" checked>
                                            <label for="js-copy-main-photo">Главная фото товара</label>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <div class="icheck-primary">
                                            <input type="checkbox" id="js-copy-other-photo" name="copy_fields[other_photos]" checked>
                                            <label for="js-copy-other-photo">Дополнительные фото товара</label>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <div class="icheck-primary">
                                            <input type="checkbox" id="js-copy-small-description" name="copy_fields[small_description]" checked>
                                            <label for="js-copy-small-description">Краткое описание</label>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <div class="icheck-primary">
                                            <input type="checkbox" id="js-copy-description" name="copy_fields[description]" checked>
                                            <label for="js-copy-description">Полное описание</label>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <div class="icheck-primary">
                                            <input type="checkbox" id="js-copy-properties" name="copy_fields[properties]" checked>
                                            <label for="js-copy-properties">Свойства</label>
                                        </div>
                                    </div>
                                    <div class="form-group custom-margin-bottom-0">
                                        <input type="hidden" name="id_item" value="<?php echo $item['id_item'];?>">
                                        <button type="submit" class="btn btn-success btn-sm btn-flat">Скопировать</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                <?php }?>

                <div class="tab-pane fade" id="js-edit-rules-tab-content" role="tabpanel" aria-labelledby="js-edit-rules-tab">
                    <div class="row">
                        <div class="col-12 col-xl-6">
                            <?php if(!empty($aditional_fields)){?>
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <i class="<?php echo get_choice('fad fa-check text-success', @$aditional_fields['item_photo']['active'] == 1, 'fad fa-times text-danger')?>"></i> Главная фото
                                    </li>
                                    <li class="list-group-item">
                                        <i class="<?php echo get_choice('fad fa-check text-success', @$aditional_fields['item_photos']['active'] == 1, 'fad fa-times text-danger')?>"></i> Дополнительные фото <?php if(isset($aditional_fields['item_photos']['count'])){?><span class="badge badge-success"><?php echo $aditional_fields['item_photos']['count'];?></span><?php }?>
                                    </li>
                                    <li class="list-group-item">
                                        <i class="<?php echo get_choice('fad fa-check text-success', @$aditional_fields['small_description']['active'] == 1, 'fad fa-times text-danger')?>"></i> Краткое описание
                                    </li>
                                    <li class="list-group-item">
                                        <i class="<?php echo get_choice('fad fa-check text-success', @$aditional_fields['full_description']['active'] == 1, 'fad fa-times text-danger')?>"></i> Полное описание
                                    </li>
                                    <li class="list-group-item">
                                        <i class="<?php echo get_choice('fad fa-check text-success', @$aditional_fields['video']['active'] == 1, 'fad fa-times text-danger')?>"></i> Видео
                                    </li>
                                    <li class="list-group-item">
                                        <i class="<?php echo get_choice('fad fa-check text-success', @$aditional_fields['properties']['active'] == 1, 'fad fa-times text-danger')?>"></i> Свойства товаров, по категориям
                                    </li>
                                    <li class="list-group-item">
                                        <i class="<?php echo get_choice('fad fa-check text-success', @$aditional_fields['mk']['active'] == 1, 'fad fa-times text-danger')?>"></i> SEO keywords
                                    </li>
                                    <li class="list-group-item">
                                        <i class="<?php echo get_choice('fad fa-check text-success', @$aditional_fields['md']['active'] == 1, 'fad fa-times text-danger')?>"></i> SEO description
                                    </li>
                                </ul>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card -->
    </div>
</section>

<div class="modal fade" id="paste-main-image-popup" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body text-center">
				Для вставки картинки нажмите<br><strong>CTRL + V</strong>
				<textarea name="pasteMainImage" id="pasteMainImage" style="opacity: 0;width: 0;height: 0;position: absolute;"></textarea>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="paste-additional-image-popup" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body text-center">
				Для вставки картинки нажмите<br><strong>CTRL + V</strong>
				<textarea name="pasteAdditionalImage" id="pasteAdditionalImage" style="opacity: 0;width: 0;height: 0;position: absolute;"></textarea>
			</div>
		</div>
	</div>
</div>

<script type="text/template" id="js-image-cropper-template">
	<div class="modal-dialog modal-dialog-cropper">
		<div class="modal-content">
			<div class="modal-header">
                <h4 class="modal-title">{{title}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
			</div>
			<div class="modal-body" id="js-uploaded_photo">
				<div class="cropper-modal-1600x1200">
					<img id="js-image-cropp" src="{{image_src}}">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary call-function" data-file-name="{{file_name}}" data-file-type="{{file_type}}" data-callback="cropImage" data-settings="{{settings}}">Применить изминения</button>
			</div>
		</div>
	</div>
</script>

<script type="text/template" id="js-image-template">    
    <div class="user-image-thumbnail-wr custom-margin-right-10">
        <img class="img-thumbnail" src="{{image_src}}">
        <div class="actions">
            <a href="#" class="btn btn-danger btn-sm btn-flat btn-block confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="<i class='fad fa-trash-alt'></i> Удалить" data-type="bg-danger" data-photo="{{file_name}}"><i class="fad fa-trash-alt"></i> Удалить</a>
        </div>
		<input type="hidden" name="{{input_name}}" value="{{file_name}}">
    </div>
</script>