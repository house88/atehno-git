<div class="form-group">
    <div class="input-group input-group-sm">
        <span class="input-group-prepend">
            <button type="button" class="btn btn-default btn-flat"><i class="fad fa-search"></i></button>
        </span>
        <input class="form-control rounded-0 dt_filter" type="text" name="keywords" data-title="Поиск" placeholder="Поиск">
    </div>
</div>
<div class="form-group">
    <label class="text-gray custom-font-size-12">Дата добавления</label>
    <div class="input-group input-group-sm">
        <div class="input-group-prepend rounded-0">
            <span class="input-group-text"><i class="fad fa-calendar-week"></i></span>
        </div>
        <input type="text" name="comment_date" class="form-control float-right rounded-0 dt_filter" data-title="Дата добавления" placeholder="ex. 21/01/2020 - 31/01/2020" id="js-comment-created-date">
    </div>
</div>
<div class="form-group">
    <label class="text-gray custom-font-size-12">Дата ответа</label>
    <div class="input-group input-group-sm">
        <div class="input-group-prepend rounded-0">
            <span class="input-group-text"><i class="fad fa-calendar-week"></i></span>
        </div>
        <input type="text" name="comment_date_reply" class="form-control float-right rounded-0 dt_filter" data-title="Дата ответа" placeholder="ex. 21/01/2020 - 31/01/2020" id="js-comment-reply-date">
    </div>
</div>