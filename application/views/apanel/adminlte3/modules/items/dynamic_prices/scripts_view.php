<?php $this->load->view($this->theme->apanel_view('includes/plugins/datatable'));?>

<script>
	var dtTable; //obj of datatable
	$(function(){
		'use strict';
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/Russian.json'));?>"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/dynamic_prices/ajax_operations/list",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "vam text-left", "aTargets": ["dt_name"], "mData": "dt_name", "bSortable": false},
				{ "sClass": "vam text-center custom-width-100", "aTargets": ["dt_category"], "mData": "dt_category", "bSortable": false},
				{ "sClass": "vam text-center custom-width-100", "aTargets": ["dt_currency"], "mData": "dt_currency", "bSortable": false},
				{ "sClass": "vam text-center custom-width-100", "aTargets": ["dt_status"], "mData": "dt_status" , "bSortable": false },
				{ "sClass": "vam text-center custom-width-50", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }
			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					beforeSend: function(){
						showLoader('body');
					},
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, data.mess_type);
						}

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                fixDataTablePagination(this);

				mobileDataTable($('.main-data-table'));
				
                hideLoader('body');
			}
		});
	});

	var change_status = function(btn){
		var $this = $(btn);
		var price = $this.data('price');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/dynamic_prices/ajax_operations/change_status',
			data: {price:price},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
	}

	var delete_action = function(btn){
		var $this = $(btn);
		var price = $this.data('price');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/dynamic_prices/ajax_operations/delete',
			data: {price:price},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>