<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                Редактировать ценовую категорию
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="title" value="<?php echo $price_variant['price_variant_title'];?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Валюта</label>
                            <select name="currency" class="form-control form-control-sm rounded-0">
                                <option value="">Выберите валюту</option>
                                <?php foreach($currencies as $currency){?>
                                    <option value="<?php echo $currency['id_currency'];?>" <?php echo set_select('currency', $currency['id_currency'], $currency['id_currency']==$price_variant['price_variant_currency']);?>><?php echo $currency['currency_name'];?></option>									
                                <?php }?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <div class="icheck-primary">
                    <input name="active" type="checkbox" id="js-checkbox-price-category-active" <?php echo set_checkbox('active', 1, (int) $price_variant['price_variant_active'] === 1);?>>
                    <label for="js-checkbox-price-category-active">Активная</label>
                </div>
                <div class="d-flex">
					<input type="hidden" name="price" value="<?php echo $price_variant['id_price_variant'];?>">
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/prices/ajax_operations/edit',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });
</script>
