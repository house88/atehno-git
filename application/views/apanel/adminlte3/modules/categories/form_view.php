<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($category) ? 'Редактировать' : 'Добавить';?> категорию
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 col-md-6">                        
						<div class="form-group">
							<label>Родитель</label>
							<select name="category_parent" class="form-control rounded-0 bs-select-categories" data-live-search="true" data-style="btn btn-default btn-sm btn-flat" data-show-content="false">
                                <option value="0">Корень</option>
								<?php if(!empty($categories)){?>
									<?php echo $categories;?>
								<?php }?>
							</select>
						</div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label>Специальная ссылка</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Специальная ссылка" name="special_url" value="<?php echo !empty($category) ? $category['category_special_url'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="title" value="<?php echo !empty($category) ? $category['category_title'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label>Название <small>на сайте</small></label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название на сайте" name="site_title" value="<?php echo !empty($category) ? $category['category_site_title'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="icheck-primary">
                            <input name="is_oversized" type="checkbox" id="js-checkbox-is_oversized" <?php echo empty($category['category_is_oversized']) ? '' : 'checked="checked"';?>>
                            <label for="js-checkbox-is_oversized">
                                Крупногабаритные товары
                            </label>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>SEO текст</label>
                            <textarea name="seo_text" class="description"><?php echo !empty($category) ? $category['category_seo'] : '';?></textarea>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Meta keywords</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Meta keywords" name="mk" value="<?php echo !empty($category) ? $category['category_mk'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Meta description</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Meta description" name="md" value="<?php echo !empty($category) ? $category['category_md'] : '';?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-end">
                <div class="d-flex">
                    <?php if(!empty($category)){?>
                        <input type="hidden" name="category" value="<?php echo $category['category_id'];?>">
                    <?php }?>
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(".bs-select-categories").selectpicker();

    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        tinyMCE.triggerSave();
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/categories/ajax_operations/<?php echo !empty($category) ? 'edit' : 'add';?>',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                if(resp.mess_type === 'success'){
                    location.reload(true);
                    //     $popup_parent.modal('hide');
                } else{
                    hideLoader('body');                    
                }
            }
        });
        return false;
    });

    initTinyMce();
</script>
