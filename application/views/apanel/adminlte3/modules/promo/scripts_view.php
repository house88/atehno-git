<?php $this->load->view($this->theme->apanel_view('includes/plugins/datatable'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/daterangepicker'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/fancybox'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/ace'));?>

<script>
	var dtTable;
	$(function(){
		'use strict';
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/Russian.json'));?>"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/promo/ajax_operations/list",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [				
				{ "sClass": "vam text-left", "aTargets": ["dt_name"], "mData": "dt_name", "bSortable": false},
				{ "sClass": "vam text-left custom-width-100", "aTargets": ["dt_start"], "mData": "dt_start"},
				{ "sClass": "vam text-left custom-width-100", "aTargets": ["dt_end"], "mData": "dt_end"},
				{ "sClass": "vam text-center custom-width-100", "aTargets": ["dt_active"], "mData": "dt_active", "bSortable": false},
				{ "sClass": "vam text-center custom-width-50", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }
			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					beforeSend: function(){
						showLoader('body');
					},
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, data.mess_type);
						}

						$('#total_dtTable-counter').text(data.iTotalDisplayRecords);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                fixDataTablePagination(this);

				mobileDataTable($('.main-data-table'));
				
                hideLoader('body');
			}
		});
	});

	var change_status = function(btn){
		var $this = $(btn);
		var promo = $this.data('promo');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/promo/ajax_operations/change_status',
			data: {promo:promo},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
	}

	var delete_action = function(btn){
		var $this = $(btn);
		var promo = $this.data('promo');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/promo/ajax_operations/delete',
			data: {promo:promo},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>