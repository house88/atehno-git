<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body style="margin:0;padding:0;">
	<div style="background: #ededed; width: 100%;">
		<table width="620" border="0" cellpadding="0" cellspacing="0" style="margin:0 auto; padding:0">
			<tbody>
				<tr>
					<td colspan="4" style="height: 10px;">
						<div style="height: 10px; width: 100%;"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<table width="620" height="80" border="0" cellpadding="0" cellspacing="0" style="margin:0; padding:15px;background:#ffffff">
							<tr>
								<td><a href="<?php echo base_url();?>"><img style="vertical-align: top;" src="<?php echo base_url('theme/accent/css/images/logo_shadow.png');?>" alt="ATEHNO" border="0" width="80"></a></td>
								<td style="width: 250px;">
									<div style="text-align:right;">
										<a style="margin-left: 10px;" href="https://ok.ru/atehno" target="_blank"><img style="vertical-align: top;" src="<?php echo base_url('theme/images/ok.png');?>" alt="icon ok" border="0" width="30" height="30"></a>
										<a style="margin-left: 5px;" href="https://www.facebook.com/atehno.md" target="_blank"><img style="vertical-align: top;" src="<?php echo base_url('theme/images/fb.png');?>" alt="icon facebook" border="0" width="30" height="30"></a>
										<a style="margin-left: 5px;" href="https://vk.com/club86490904" target="_blank"><img style="vertical-align: top;" src="<?php echo base_url('theme/images/vk.png');?>" alt="icon vk" border="0" width="30" height="30"></a>
										<a style="margin-left: 5px;" href="https://www.instagram.com/atehno.md" target="_blank"><img style="vertical-align: top;" src="<?php echo base_url('theme/images/ig.png');?>" alt="icon vk" border="0" width="30" height="30"></a>
									</div>
									<div style="border: 0px; background: #ffffff; width: 250px; height: 50px; text-align: right;">
										<div style="padding-top: 10px; line-height: 18px; font-size: 12px; font-family: Arial,sans-serif; color: #bf0000;">Магазин - 0(231) 6 30 90</div>
										<div style=" line-height: 18px; font-size: 12px; font-family: Arial,sans-serif; color: #bf0000;">Сервис Центр - 0(231) 6 31 32</div>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center" style="background-color:#ffffff;font-size:0 !important;line-height:100%">
	
						<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px">
							<tbody>
								<?php $promo_email = json_decode($promo['promo_email'], true);?>
								<?php if(!empty($promo_email)){?>
									<?php if(!empty($promo_email['image'])){?>
										<tr>
											<td colspan="4" align="center" style="font-family:'trebuchet ms', 'arial' , 'lucida grande' , 'lucida sans unicode' , 'lucida sans' , 'tahoma' , sans-serif;font-size:13px;line-height:20px">
												<p style="margin-bottom:0px;margin-top:0px">
													<a href="<?php echo site_url('promo/'.$promo['promo_url']);?>" target="_blank" style="color:#141414;display:inline-block;text-decoration-line:none">
														<img alt="Промо" src="<?php echo site_url('files/uploads/'.$promo_email['image']);?>" width="600" style="border-bottom-style:none;border-bottom-width:0;border-left-style:none;border-left-width:0;border-right-style:none;border-right-width:0;border-top-style:none;border-top-width:0;height:auto;line-height:100%;max-width:600px;text-decoration-line:none;vertical-align:middle;width:100%" />
													</a>
												</p>
											</td>
										</tr>
									<?php }?>
									<?php if(!empty($promo_email['text'])){?>
										<tr>
											<td colspan="4">
												<div style="font-size:16px;height:16px;line-height:16px"> </div>
											</td>
										</tr>
										<tr>
											<td colspan="4" align="center" style="font-family:'trebuchet ms', 'arial' , 'lucida grande' , 'lucida sans unicode' , 'lucida sans' , 'tahoma' , sans-serif;font-size:13px;line-height:20px">
												<?php echo $promo_email['text'];?>
											</td>
										</tr>
									<?php }?>
									<tr>
										<td colspan="4">
											<div style="font-size:16px;height:16px;line-height:16px"> </div>
										</td>
									</tr>
								<?php }?>
								
								<?php if(!empty($products)){?>
									<?php $_col = 1;?>
									<?php $_row_completed = false;?>
									<?php foreach($products as $item){?>
										<?php if($_col == 1){?>
											<?php $_row_completed = false;?>
											<tr>									
										<?php }?>
										<?php
											$price = getPriceObject($item['item_price'], [
												'tempAmount' => $item['item_temp_price'],
												'default' => true
											]);
										?>
	
										<td style="font-size:0 !important;line-height:100%; vertical-align: top;" valign="top">
		
											<div style="display:inline-block !important;max-width:150px;overflow:hidden;vertical-align:top;width:100%">
												<div style="padding-left:8px;padding-right:8px;text-align:center">
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tbody>
															<tr>
																<td align="center" height="32"></td>
															</tr>
														</tbody>
													</table>
		
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tbody>
															<tr>
																<td align="center" height="134" style="vertical-align:middle;" valign="middle">
																	<a href="<?php echo base_url('products/'.$item['item_url']);?>" style="color:#141414;display:inline-block;text-decoration-line:none">
																		<img 
																			src="<?php echo base_url(getImage('files/items/thumb_500x500_'.$item['item_photo']));?>"
																			width="134"
																			height="auto"
																			style="border-bottom-style:none;border-bottom-width:0;border-left-style:none;border-left-width:0;border-right-style:none;border-right-width:0;border-top-style:none;border-top-width:0;text-decoration-line:none;vertical-align:middle" />
																	</a>
																</td>
															</tr>
														</tbody>
													</table>

													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tbody>
															<tr>
																<td align="center" height="16"></td>
															</tr>
														</tbody>
													</table>
		
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tbody>
															<tr>
																<td align="center" height="80" style="color:#141414;font-family:'trebuchet ms', 'arial' , 'lucida grande' , 'lucida sans unicode' , 'lucida sans' , 'tahoma' , sans-serif;font-size:12px;line-height:20px;min-height:100px;vertical-align:top">
																	<p style="margin-bottom:0px;margin-top:0px;font-size:12px;line-height:16px">
																		<a href="<?php echo base_url('products/'.$item['item_url']);?>" style="color:#141414;display:inline-block;text-decoration-line:none">
																			<span style="color:#141414"><?php echo clean_output(text_elipsis($item['item_title'], 60));?></span>
																		</a>
																	</p>
																</td>
															</tr>
														</tbody>
													</table>
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tbody>
															<tr>
																<td align="center" height="8"></td>
															</tr>
														</tbody>
													</table>
		
													<table align="center" border="0" cellpadding="0" cellspacing="0" style="margin-bottom:0;margin-left:auto;margin-right:auto;margin-top:0">
														<tbody>
															<tr>
																<td align="center" style="font-family:'trebuchet ms', 'arial' , 'lucida grande' , 'lucida sans unicode' , 'lucida sans' , 'tahoma' , sans-serif;font-size:16px;line-height:24px;padding-top:8px;vertical-align:top">
		
																	<p style="font-size:36px;line-height:30px;margin-bottom:0px;margin-top:0px;letter-spacing: -2px;">
																		<strong><?php echo $price['display_price'];?></strong>
																	</p>
																</td>
																<td align="left" style="font-family:'trebuchet ms', 'arial' , 'lucida grande' , 'lucida sans unicode' , 'lucida sans' , 'tahoma' , sans-serif;font-size:12px;line-height:19px;vertical-align:bottom">
																	<p style="margin-bottom:0px;margin-top:0px;margin-left:7.5px;">
																		<strong style="color:#96939b;text-transform:uppercase">лей</strong>
																	</p>
																</td>
															</tr>
															<tr>
																<td align="center" style="font-family:'trebuchet ms', 'arial' , 'lucida grande' , 'lucida sans unicode' , 'lucida sans' , 'tahoma' , sans-serif;font-size:16px;line-height:24px;padding-top:8px;vertical-align:top">
																	<p style="color:#96939b;margin-bottom:0px;margin-top:0px;text-decoration:line-through">
																		<strong>
																			<?php if($price['display_price_old'] > 0){?>
																				<?php echo $price['display_price_old'] . ' ' . $price['currency_symbol'];?>
																			<?php }?>
																		</strong>
																	</p>
																</td>
																<td align="left" style="font-family:'trebuchet ms', 'arial' , 'lucida grande' , 'lucida sans unicode' , 'lucida sans' , 'tahoma' , sans-serif;font-size:12px;line-height:19px;vertical-align:bottom">
																</td>
															</tr>
														</tbody>
													</table>
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tbody>
															<tr>
																<td align="center" height="32"></td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
		
										</td>
	
										<?php $_col++;?>
	
										<?php if($_col > 4){?>
											<?php $_col = 1;?>
											<?php $_row_completed = true;?>
											</tr>
										<?php }?>
									<?php }?>
	
									<?php if($_row_completed === false){?>
										<?php for($_col; $_col <= 4; $_col++){?>
											<td style="width:25%;font-size:0 !important;line-height:100%"></td>
										<?php }?>
										</tr>
									<?php }?>
								<?php } else if(!empty($categories)){?>
									<?php foreach($categories as $key_category => $category){?>										
										<tr>
											<td colspan="4" align="center">
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tbody>
														<tr>
															<td align="center" style="vertical-align: middle;background-image:url(<?php echo site_url('theme/accent/css/images/category-line.jpg');?>);background-position: 50% 50%;background-repeat: repeat-x;">
																<a href="<?php echo site_url('promo/'.$promo['promo_url'].'/category/'.encript_uri_hash($key_category));?>" target="_blank" style="background-color: #ffffff;font-size: 24px;line-height: 28px;padding-bottom: 5px;padding-left: 15px;padding-right: 15px;padding-top: 5px;color: #bf0000;display: inline-block;font-family: 'trebuchet ms', 'arial' , 'lucida grande' , 'lucida sans unicode' , 'lucida sans' , 'tahoma' , sans-serif;text-decoration-line: none;vertical-align: middle;">
																	<strong><?php echo $category['name'];?></strong>
																</a>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
										<?php $_col = 1;?>
										<?php $_row_completed = false;?>
										<?php foreach($category['products'] as $item){?>
											<?php if($_col == 1){?>
												<?php $_row_completed = false;?>
												<tr>									
											<?php }?>
											<?php 
												$price = getPriceObject($item['item_price'], [
													'tempAmount' => $item['item_temp_price'],
													'default' => true
												]);
											?>
		
											<td style="width:25%;font-size:0 !important;line-height:100%; vertical-align: top;" valign="top">
			
												<div style="display:inline-block !important;max-width:150px;overflow:hidden;vertical-align:top;width:100%">
													<div style="padding-left:8px;padding-right:8px;text-align:center">
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tbody>
																<tr>
																	<td align="center" height="32"></td>
																</tr>
															</tbody>
														</table>
			
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tbody>
																<tr>
																	<td align="center" height="134" style="vertical-align:middle;" valign="middle">
																		<a href="<?php echo base_url('products/'.$item['item_url']);?>" style="color:#141414;display:inline-block;text-decoration-line:none">
																			<img 
																				src="<?php echo base_url(getImage('files/items/thumb_500x500_'.$item['item_photo']));?>"
																				width="134"
																				height="auto"
																				style="border-bottom-style:none;border-bottom-width:0;border-left-style:none;border-left-width:0;border-right-style:none;border-right-width:0;border-top-style:none;border-top-width:0;text-decoration-line:none;vertical-align:middle" />
																		</a>
																	</td>
																</tr>
															</tbody>
														</table>

														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tbody>
																<tr>
																	<td align="center" height="16"></td>
																</tr>
															</tbody>
														</table>
			
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tbody>
																<tr>
																	<td align="center" height="80" style="color:#141414;font-family:'trebuchet ms', 'arial' , 'lucida grande' , 'lucida sans unicode' , 'lucida sans' , 'tahoma' , sans-serif;font-size:12px;line-height:20px;min-height:100px;vertical-align:top">
																		<p style="margin-bottom:0px;margin-top:0px;font-size:12px;line-height:16px">
																			<a href="<?php echo base_url('products/'.$item['item_url']);?>" style="color:#141414;display:inline-block;text-decoration-line:none">
																				<span style="color:#141414"><?php echo clean_output(text_elipsis($item['item_title'], 60));?></span>
																			</a>
																		</p>
																	</td>
																</tr>
															</tbody>
														</table>
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tbody>
																<tr>
																	<td align="center" height="8"></td>
																</tr>
															</tbody>
														</table>
			
														<table align="center" border="0" cellpadding="0" cellspacing="0" style="margin-bottom:0;margin-left:auto;margin-right:auto;margin-top:0">
															<tbody>
																<tr>
																	<td align="center" style="font-family:'trebuchet ms', 'arial' , 'lucida grande' , 'lucida sans unicode' , 'lucida sans' , 'tahoma' , sans-serif;font-size:16px;line-height:24px;padding-top:8px;vertical-align:top">
			
																		<p style="font-size:36px;line-height:30px;margin-bottom:0px;margin-top:0px;letter-spacing: -2px;">
																			<strong><?php echo $price['display_price'];?></strong>
																		</p>
																	</td>
																	<td align="left" style="font-family:'trebuchet ms', 'arial' , 'lucida grande' , 'lucida sans unicode' , 'lucida sans' , 'tahoma' , sans-serif;font-size:12px;line-height:19px;vertical-align:bottom">
																		<p style="margin-bottom:0px;margin-top:0px;margin-left:7.5px;">
																			<strong style="color:#96939b;text-transform:uppercase">лей</strong>
																		</p>
																	</td>
																</tr>
																<tr>
																	<td align="center" style="font-family:'trebuchet ms', 'arial' , 'lucida grande' , 'lucida sans unicode' , 'lucida sans' , 'tahoma' , sans-serif;font-size:16px;line-height:24px;padding-top:8px;vertical-align:top">
																		<p style="color:#96939b;margin-bottom:0px;margin-top:0px;text-decoration:line-through">
																			<strong>
																				<?php if($price['display_price_old'] > 0){?>
																					<?php echo $price['display_price_old'] . ' ' . $price['currency_symbol'];?>
																				<?php }?>
																			</strong>
																		</p>
																	</td>
																	<td align="left" style="font-family:'trebuchet ms', 'arial' , 'lucida grande' , 'lucida sans unicode' , 'lucida sans' , 'tahoma' , sans-serif;font-size:12px;line-height:19px;vertical-align:bottom">
																	</td>
																</tr>
															</tbody>
														</table>
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tbody>
																<tr>
																	<td align="center" height="32"></td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
			
											</td>
		
											<?php $_col++;?>
		
											<?php if($_col > 4){?>
												<?php $_col = 1;?>
												<?php $_row_completed = true;?>
												</tr>
											<?php }?>
										<?php }?>
		
										<?php if($_row_completed === false){?>
											<?php if($_col == 1){?>
												<tr>
											<?php }?>
											<?php for($_col; $_col <= 4; $_col++){?>
												<td style="font-size:0 !important;line-height:100%"></td>
											<?php }?>
											</tr>
										<?php }?>
									<?php }?>
								<?php }?>
								<tr>
									<td colspan="4" align="center" style="padding-bottom:24px;padding-left:16px;padding-right:16px">
										<table border="0" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td align="center" style="background-color: #bf0000;font-size: 16px;line-height: 24px;padding-bottom: 8px;padding-left: 24px;padding-right: 24px;padding-top: 8px;vertical-align: middle;">
														<a href="<?php echo site_url('promo/'.$promo['promo_url']);?>" target="_blank" style="color:#ffffff;display:block;font-family:'trebuchet ms', 'arial' , 'lucida grande' , 'lucida sans unicode' , 'lucida sans' , 'tahoma' , sans-serif;text-decoration:none;vertical-align:middle">
															<strong style="color:#ffffff">Посмотреть полное предложение</strong>
														</a>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="4" align="center" style="border-bottom-color:#666666;border-bottom-style:solid;border-bottom-width:1px;color:#141414;font-family:'trebuchet ms', 'arial' , 'lucida grande' , 'lucida sans unicode' , 'lucida sans' , 'tahoma' , sans-serif;font-size:12px;line-height:19px;padding-bottom:24px;padding-left:16px;padding-right:16px;padding-top:10px">
										<p style="margin-bottom:0px;margin-top:0px">
											<a href="https://ok.ru/atehno" style="display:inline-block;text-decoration-line:none">
												<img alt="Odnoklassniki Atehno" height="32" src="<?php echo site_url('theme/images/ok.png');?>" width="32" style="border-bottom-style:none;border-bottom-width:0;border-left-style:none;border-left-width:0;border-right-style:none;border-right-width:0;border-top-style:none;border-top-width:0;display:inline-block;height:auto;line-height:100%;max-width:32px;text-decoration-line:none;vertical-align:middle" />
											</a>
											<a href="https://www.facebook.com/atehno.md" style="display:inline-block;text-decoration-line:none">
												<img alt="Facebook Atehno" height="32" src="<?php echo site_url('theme/images/fb.png');?>" width="32" style="border-bottom-style:none;border-bottom-width:0;border-left-style:none;border-left-width:0;border-right-style:none;border-right-width:0;border-top-style:none;border-top-width:0;display:inline-block;height:auto;line-height:100%;max-width:32px;text-decoration-line:none;vertical-align:middle" />
											</a>
											<a href="https://vk.com/club86490904" style="display:inline-block;text-decoration-line:none">
												<img alt="Вконтакте Atehno" height="32" src="<?php echo site_url('theme/images/vk.png');?>" width="32" style="border-bottom-style:none;border-bottom-width:0;border-left-style:none;border-left-width:0;border-right-style:none;border-right-width:0;border-top-style:none;border-top-width:0;display:inline-block;height:auto;line-height:100%;max-width:32px;text-decoration-line:none;vertical-align:middle" />
											</a>
											<a href="https://www.instagram.com/atehno.md" style="display:inline-block;text-decoration-line:none">
												<img alt="Instagram Atehno" height="32" src="<?php echo site_url('theme/images/ig.png');?>" width="32" style="border-bottom-style:none;border-bottom-width:0;border-left-style:none;border-left-width:0;border-right-style:none;border-right-width:0;border-top-style:none;border-top-width:0;display:inline-block;height:auto;line-height:100%;max-width:32px;text-decoration-line:none;vertical-align:middle" />
											</a>
										</p>
									</td>
								</tr>
								<tr>
									<td colspan="4" style="padding: 15px 0; font-family: Arial,sans-serif; vertical-align: moddle; text-align:center; color: #a2a2a2; font-size: 11px; line-height: 20px;">
										&copy; <?php echo date('Y');?> atehno.md &mdash; Balti, Moldova <br>
										<a style="font-family: Arial,sans-serif; vertical-align: moddle; text-align:center; color: #a2a2a2; font-size: 11px; line-height: 20px; text-decoration: underline;" href="<?php echo base_url();?>">www.atehno.md</a> | Магазин - 0(231) 6 30 90 | Сервис Центр - 0(231) 6 31 32
									</td>
								</tr>
							</tbody>
						</table>
	
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div style="margin-bottom:0px;margin-left:0px;margin-right:0px;margin-top:15px;padding-bottom:0px;padding-left:0px;padding-right:0px;padding-top:0px">
	</div>
</body>

</html>