<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                Email Код
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
            <div id="js-code-html" class="custom-height-min-300"><?php if(!empty($email_code)){?><?php echo clean_output($email_code);?><?php }?></div>
        </div>
    </div>
</div>
<script>    
    var editor = ace.edit("js-code-html", {
        theme: "ace/theme/monokai",
        mode: "ace/mode/html",
        showPrintMargin: false,
        maxLines: 30,
        minLines: 20,
        wrap: true,
        autoScrollEditorIntoView: true
    });
</script>
