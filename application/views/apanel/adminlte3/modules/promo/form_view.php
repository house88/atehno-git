<div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($promo) ? 'Редактировать' : 'Добавить';?> промо страницу
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="card card-primary card-outline card-outline-tabs">
                    <div class="card-header p-0 border-bottom-0">
                        <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="custom-tabs-promo-settings-tab" data-toggle="pill" href="#custom-tabs-promo-settings" role="tab" aria-controls="custom-tabs-promo-settings" aria-selected="true">Настройки</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-tabs-promo-email-tab" data-toggle="pill" href="#custom-tabs-promo-email" role="tab" aria-controls="custom-tabs-promo-email" aria-selected="false">Email</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="custom-tabs-promo-tabContent">
                            <div class="tab-pane fade show active" id="custom-tabs-promo-settings" role="tabpanel" aria-labelledby="custom-tabs-promo-settings-tab">
                                <div class="row">
                                    <div class="col-12 col-md-6 col-lg-4">
                                        <div class="form-group">
                                            <label>Название</label>
                                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="title" value="<?php echo !empty($promo) ? $promo['promo_name'] : '';?>">
                                        </div>
                                    </div>
                                    <?php $banners = Modules::run('banners/_get_all');?>
                                    <?php if(!empty($banners)){?>
                                        <div class="col-12 col-md-6 col-lg-4">
                                            <div class="form-group">
                                                <label>Баннер</label>
                                                <select name="promo_banner" class="form-control form-control-sm rounded-0">
                                                    <option value="">Выберите баннер</option>
                                                    <?php foreach($banners as $banner){?>
                                                        <option value="<?php echo $banner['id_banner'];?>" <?php echo set_select('promo_banner', $banner['id_banner'], !empty($promo) && (int) $banner['id_banner'] === (int) $promo['promo_banner']);?>><?php echo $banner['banner_name'];?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php }?>
                                    <div class="col-12 col-lg-4">                        
                                        <div class="form-group">
                                            <label>Дата активности</label>
                                            <div class="input-group input-group-sm">
                                                <div class="input-group-prepend rounded-0">
                                                    <span class="input-group-text"><i class="fad fa-calendar-week"></i></span>
                                                </div>
                                                <input type="text" name="promo_date" class="form-control float-right rounded-0" data-title="Дата активности" placeholder="ex. 21.01.2020 - 31.01.2020" id="js-promo-active-date" value="<?php echo !empty($promo) ? getDateFormat($promo['promo_date_start'], 'Y-m-d', 'd.m.Y') .' - '. getDateFormat($promo['promo_date_end'], 'Y-m-d', 'd.m.Y') : '';?>">
                                            </div>
                                        </div>
                                    </div>                    
                                    <div class="col-12">
                                        <label>Категории</label>
                                        <div class="js-categories-list-container">
                                            <?php if(!empty($promo)){?>
                                                <?php $promo_settings = json_decode($promo['promo_settings'], true);?>
                                                <?php if(!empty($promo_settings)){?>
                                                    <?php foreach($promo_settings as $category_key => $category){?>                                        
                                                        <div class="js-categories-list--item custom-margin-bottom-15">
                                                            <div class="row">
                                                                <div class="col-12 col-md-6 col-xl-4">
                                                                    <div class="custom-margin-bottom-5">
                                                                        <label><small>Название</small></label>
                                                                        <input type="text" class="form-control form-control-sm rounded-0" name="categories[<?php echo $category_key;?>][name]" value="<?php echo $category['name'];?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-md-6 col-xl-4">
                                                                    <div class="custom-margin-bottom-5">
                                                                        <label><small>ИД категорий, разделитель ";"</small></label>
                                                                        <input type="text" class="form-control form-control-sm rounded-0" name="categories[<?php echo $category_key;?>][id_categories]" value="<?php echo $category['id_categories'];?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-xl-4">
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="custom-margin-bottom-5">
                                                                                <label><small>Фото</small></label>
                                                                                <div class="input-group input-group-sm justify-content-between">
                                                                                    <button type="button" class="btn btn-default btn-sm btn-flat flex-fill call-function" title="Добавить подкатегорию" data-callback="add_category_child" data-index="<?php echo $category_key;?>">
                                                                                        <i class="fas fa-folder-tree"></i>
                                                                                    </button>
                                                                                    <input type="hidden" class="form-control form-control-sm rounded-0" name="categories[<?php echo $category_key;?>][photo]" id="photo_<?php echo $category_key;?>" value="<?php echo $category['photo'];?>">
                                                                                    <button type="button" class="btn btn-default btn-sm btn-flat flex-fill call-function" data-callback="clearFilemanagerImage" data-target="#photo_<?php echo $category_key;?>">
                                                                                        <i class="fad fa-times"></i>
                                                                                    </button>
                                                                                    <button type="button" class="btn btn-default btn-sm btn-flat flex-fill call-function" data-callback="previewFilemanagerImage" data-target="#photo_<?php echo $category_key;?>">
                                                                                        <i class="fad fa-eye"></i>
                                                                                    </button>
                                                                                    <button type="button" class="btn btn-default btn-sm btn-flat flex-fill call-function" data-callback="openFilemanager" data-src="<?php echo site_url('tinymanager/init/default');?>/dialog.php?type=1&popup=1&field_id=photo_<?php echo $category_key;?>&relative_url=1">
                                                                                        <i class="fad fa-image"></i>
                                                                                    </button>
                                                                                    <button type="button" class="btn btn-default btn-sm btn-flat flex-fill call-function" data-callback="changeRecordWeight" data-parent-target=".js-categories-list--item" data-order="up">
                                                                                        <i class="fad fa-chevron-up"></i>
                                                                                    </button>
                                                                                    <button type="button" class="btn btn-default btn-sm btn-flat flex-fill call-function" data-callback="changeRecordWeight" data-parent-target=".js-categories-list--item" data-order="down">
                                                                                        <i class="fad fa-chevron-down"></i>
                                                                                    </button>
                                                                                    <button type="button" class="btn btn-danger btn-sm btn-flat flex-fill call-function" data-callback="delete_category" data-photo="{{file_name}}">
                                                                                        <i class="fad fa-trash-alt"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                                                                                
                                                            <div class="js-categories-list--item-childred">
                                                                <?php if(!empty($category['children'])){?>
                                                                    <?php foreach($category['children'] as $child_key => $child){?>
                                                                        <div class="js-categories-list--item-child custom-margin-bottom-10">
                                                                            <div class="row">
                                                                                <div class="col-11 col-md-5 col-lg-5 col-xl-3 offset-1">
                                                                                    <div class="custom-margin-bottom-5">
                                                                                        <label><small>Название</small></label>
                                                                                        <input type="text" class="form-control form-control-sm rounded-0" name="categories[<?php echo $category_key;?>][children][<?php echo $child_key;?>][name]" value="<?php echo $child['name'];?>">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-11 col-md-6 col-lg-4 col-xl-4 offset-1 offset-md-0">
                                                                                    <div class="custom-margin-bottom-5">
                                                                                        <label><small>ИД категорий, разделитель ";"</small></label>
                                                                                        <input type="text" class="form-control form-control-sm rounded-0" name="categories[<?php echo $category_key;?>][children][<?php echo $child_key;?>][id_categories]" value="<?php echo $child['id_categories'];?>">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-11 col-lg-2 col-xl-4 offset-1 offset-lg-0">
                                                                                    <div class="custom-margin-bottom-5">
                                                                                        <label class="d-none d-lg-inline-block"><small style="color:#ffffff">#</small></label>
                                                                                        <div class="input-group input-group-sm justify-content-between">
                                                                                            <button type="button" class="btn btn-default btn-sm btn-flat flex-fill call-function" data-callback="changeRecordWeight" data-parent-target=".js-categories-list--item-child" data-order="up">
                                                                                                <i class="fad fa-chevron-up"></i>
                                                                                            </button>
                                                                                            <button type="button" class="btn btn-default btn-sm btn-flat flex-fill call-function" data-callback="changeRecordWeight" data-parent-target=".js-categories-list--item-child" data-order="down">
                                                                                                <i class="fad fa-chevron-down"></i>
                                                                                            </button>
                                                                                            <button type="button" class="btn btn-danger btn-sm btn-flat flex-fill call-function" data-callback="delete_category_child">
                                                                                                <i class="fad fa-trash-alt"></i>
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    <?php }?>
                                                                <?php }?>
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                                <?php }?>
                                            <?php }?>
                                        </div>
                                        <button type="button" class="btn btn-default btn-flat btn-sm call-function" data-callback="add_category">
                                            <span class="fad fa-plus"></span> Добавить категорию
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="custom-tabs-promo-email" role="tabpanel" aria-labelledby="custom-tabs-promo-email-tab">
								<?php $promo_email = !empty($promo) ? json_decode($promo['promo_email'], true) : null;?>
                                <div class="form-group">
                                    <label>Фото</label>
                                    <div class="input-group input-group-sm justify-content-between">
                                        <input type="text" class="form-control form-control-sm flex-fill rounded-0 bg-white" name="promo_email_image" id="promo_email_image" value="<?php if(!empty($promo_email)){echo $promo_email['image'];}?>" readonly>
                                        <button type="button" class="btn btn-default btn-sm btn-flat call-function" data-callback="clearFilemanagerImage" data-target="#promo_email_image">
                                            <i class="fad fa-times"></i>
                                        </button>
                                        <button type="button" class="btn btn-default btn-sm btn-flat call-function" data-callback="previewFilemanagerImage" data-target="#promo_email_image">
                                            <i class="fad fa-eye"></i>
                                        </button>
                                        <button type="button" class="btn btn-default btn-sm btn-flat call-function" data-callback="openFilemanager" data-src="<?php echo site_url('tinymanager/init/default');?>/dialog.php?type=1&popup=1&field_id=promo_email_image&relative_url=1">
                                            <i class="fad fa-image"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Текст <small>под фото</small></label>
                                    <textarea name="promo_email_text" rows="10" class="form-control rounded-0"><?php if(!empty($promo_email['text'])){echo $promo_email['text'];}?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <div class="icheck-primary">
                    <input name="visible" type="checkbox" id="js-checkbox-promo-active" <?php echo set_checkbox('visible', 1, !empty($promo) ? (int) $promo['promo_active'] === 1 : false);?>>
                    <label for="js-checkbox-promo-active">
                        Активная
                    </label>
                </div>
                <div class="d-flex">
                    <?php if(!empty($promo)){?>
                        <input type="hidden" name="id_promo" value="<?php echo $promo['id_promo'];?>">
                    <?php }?>
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/template" data-template="js-category-item--template">
    <div class="js-categories-list--item custom-margin-bottom-15">
        <div class="form-group custom-margin-bottom-5">
            <div class="row">
                <div class="col-12 col-md-6 col-xl-4">
                    <label><small>Название</small></label>
                    <input type="text" class="form-control form-control-sm rounded-0" name="categories[{index}][name]" value="">
                </div>
                <div class="col-12 col-md-6 col-xl-4">
                    <label><small>ИД категорий, разделитель ";"</small></label>
                    <input type="text" class="form-control form-control-sm rounded-0" name="categories[{index}][id_categories]" value="">
                </div>
                <div class="col-12 col-xl-4">
                    <div class="row">
                        <div class="col-12">
                            <label><small>Фото</small></label>
                            <div class="input-group input-group-sm justify-content-between">
                                <button type="button" class="btn btn-default btn-flat flex-fill call-function" title="Добавить подкатегорию" data-callback="add_category_child" data-index="{index}">
                                    <i class="fas fa-folder-tree"></i>
                                </button>
                                <input type="hidden" class="form-control form-control-sm rounded-0" name="categories[{index}][photo]" id="photo_{index}" value="">
                                <button type="button" class="btn btn-default btn-sm btn-flat flex-fill call-function" data-callback="clearFilemanagerImage" data-clear="#photo_{index}">
                                    <i class="fad fa-times"></i>
                                </button>
                                <button type="button" class="btn btn-default btn-sm btn-flat flex-fill call-function" data-callback="previewFilemanagerImage" data-image="#photo_{index}">
                                    <i class="fad fa-eye"></i>
                                </button>
                                <button type="button" class="btn btn-default btn-sm btn-flat flex-fill call-function" data-callback="openFilemanager" data-src="<?php echo site_url('tinymanager/init/default');?>/dialog.php?type=1&popup=1&field_id=photo_{index}&relative_url=1">
                                    <i class="fad fa-image"></i>
                                </button>
                                <button type="button" class="btn btn-default btn-sm btn-flat flex-fill call-function" data-callback="changeRecordWeight" data-parent-target=".js-categories-list--item" data-order="up">
                                    <i class="fad fa-chevron-up"></i>
                                </button>
                                <button type="button" class="btn btn-default btn-sm btn-flat flex-fill call-function" data-callback="changeRecordWeight" data-parent-target=".js-categories-list--item" data-order="down">
                                    <i class="fad fa-chevron-down"></i>
                                </button>
                                <button type="button" class="btn btn-danger btn-sm btn-flat flex-fill call-function" data-callback="delete_category" data-photo="{{file_name}}">
                                    <i class="fad fa-trash-alt"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="js-categories-list--item-childred"></div>
    </div>
</script>
<script type="text/template" data-template="js-category-item-child--template">
    <div class="js-categories-list--item-child custom-margin-bottom-10">
        <div class="row">
            <div class="col-11 col-md-5 col-lg-5 col-xl-3 offset-1">
                <div class="custom-margin-bottom-5">
                    <label><small>Название</small></label>
                    <input type="text" class="form-control form-control-sm rounded-0" name="categories[{parent_index}][children][{index}][name]">
                </div>
            </div>
            <div class="col-11 col-md-6 col-lg-4 col-xl-4 offset-1 offset-md-0">
                <div class="custom-margin-bottom-5">
                    <label><small>ИД категорий, разделитель ";"</small></label>
                    <input type="text" class="form-control form-control-sm rounded-0" name="categories[{parent_index}][children][{index}][id_categories]">
                </div>
            </div>
            <div class="col-11 col-lg-2 col-xl-4 offset-1 offset-lg-0">
                <div class="custom-margin-bottom-5">
                    <label class="d-none d-lg-inline-block"><small style="color:#ffffff">#</small></label>
                    <div class="input-group input-group-sm justify-content-between">
                        <button type="button" class="btn btn-default btn-sm btn-flat flex-fill call-function" data-callback="changeRecordWeight" data-parent-target=".js-categories-list--item-child" data-order="up">
                            <i class="fad fa-chevron-up"></i>
                        </button>
                        <button type="button" class="btn btn-default btn-sm btn-flat flex-fill call-function" data-callback="changeRecordWeight" data-parent-target=".js-categories-list--item-child" data-order="down">
                            <i class="fad fa-chevron-down"></i>
                        </button>
                        <button type="button" class="btn btn-danger btn-sm btn-flat flex-fill call-function" data-callback="delete_category_child">
                            <i class="fad fa-trash-alt"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script>
    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/promo/ajax_operations/<?php echo !empty($promo) ? 'edit' : 'add';?>',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });

    $('#js-promo-active-date').daterangepicker({
        timePicker: false,
        parentEl: '.modal',
        locale: {
            format: 'DD.MM.YYYY'
        },
        autoUpdateInput: false,
        "autoApply": true
    }).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD.MM.YYYY') + ' - ' + picker.endDate.format('DD.MM.YYYY')).trigger('change');
    }).on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('').trigger('change');
    });
    
	var add_category = function(btn){
		var $this = $(btn);
        var template = $('[data-template="js-category-item--template"]').text();
		var category_index = uniqid('category_');

		template = template.replace(/{index}/g, category_index);
		$('.js-categories-list-container').append(template);
	}

	var delete_category = function(btn){
		var $this = $(btn);

		$this.closest('.js-categories-list--item').remove();
	}

	var add_category_child = function(btn){
		var $this = $(btn);
		var parent_index = $this.data('index');
		var category_index = uniqid('child_');
        var template = $('[data-template="js-category-item-child--template"]').text();

		template = template.replace(/{parent_index}/g, parent_index);
		template = template.replace(/{index}/g, category_index);

		$this.closest('.js-categories-list--item').find('.js-categories-list--item-childred').append(template);
	}

	var delete_category_child = function(btn){
		var $this = $(btn);

		$this.closest('.js-categories-list--item-child').remove();
	}
</script>
