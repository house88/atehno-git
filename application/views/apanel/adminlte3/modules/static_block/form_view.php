<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($static_block) ? 'Редактировать' : 'Добавить';?> блок
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
            <form role="form" id="manage_form">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="title" value="<?php echo !empty($static_block) ? $static_block['block_title'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <label>Алиас</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="ex. menu_categories" name="alias" value="<?php echo !empty($static_block) ? $static_block['block_alias'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <div class="icheck-primary">
                                <input name="is_page" type="checkbox" id="js-checkbox-is_page" <?php echo set_checkbox('is_page', '', !empty($static_block) ? (int) $static_block['is_page'] === 1 : false);?>>
                                <label for="js-checkbox-is_page">
                                    Полноценная страница
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <div class="icheck-primary">
                                <input name="is_landing" type="checkbox" id="js-checkbox-is_landing" <?php echo set_checkbox('is_landing', '', !empty($static_block) ? (int) $static_block['is_landing'] === 1 : false);?>>
                                <label for="js-checkbox-is_landing">
                                    Landing
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group text-right">
                            <?php if(!empty($static_block)){?>
                                <input type="hidden" name="block" value="<?php echo $static_block['id_block'];?>">
                            <?php }?>
                            <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                            <div id="js-breadcrumbs-form" class="d-none"></div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline card-outline-tabs">
                        <div class="card-header p-0 border-bottom-0">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="js-block-code-tab" data-toggle="pill" href="#js-block-code-tab-content" role="tab" aria-controls="js-block-code-tab-content" aria-selected="false">Код страницы</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="js-block-breadcrumb-tab" data-toggle="pill" href="#js-block-breadcrumb-tab-content" role="tab" aria-controls="js-block-breadcrumb-tab-content" aria-selected="false">Breadcrumbs</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane fade active show" id="js-block-code-tab-content" role="tabpanel" aria-labelledby="js-block-code-tab">
                                    <div id="js-code-html" class="custom-height-min-300"><?php if(!empty($static_block)){?><?php echo clean_output($static_block['block_text']);?><?php }?></div>
                                </div>
                                <div class="tab-pane fade" id="js-block-breadcrumb-tab-content" role="tabpanel" aria-labelledby="js-block-breadcrumb-tab">
                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                            <div class="form-group">
                                                <label>Название</label>
                                                <input class="form-control form-control-sm rounded-0" placeholder="ex. Ноутбуки" name="breadcrumb_title" value="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <div class="form-group">
                                                <label>Ссылка</label>
                                                <div class="input-group input-group-sm">
                                                    <input type="text" class="form-control rounded-0" placeholder="ex. catalog/noutbuki" name="breadcrumb_url" value="">
                                                    <span class="input-group-append">
                                                        <button type="button" class="btn btn-primary btn-flat call-function" data-callback="add_breadcrumb_row">
                                                            <i class="fad fa-plus"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="js-breadcrumbs-container">
                                        <?php if(!empty($static_block)){?>
                                            <?php $block_breadcrumbs = json_decode($static_block['block_breadcrumbs'], true);?>
                                            <?php if(!empty($block_breadcrumbs)){?>
                                                <?php foreach($block_breadcrumbs as $key_index => $block_bread){?>
                                                    <div class="row">
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <input class="form-control form-control-sm rounded-0" placeholder="ex. Ноутбуки" name="breadcrumbs[<?php echo $key_index;?>][title]" value="<?php echo $block_bread['link_title'];?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <div class="input-group input-group-sm">
                                                                    <input type="text" class="form-control rounded-0" placeholder="ex. catalog/noutbuki" name="breadcrumbs[<?php echo $key_index;?>][url]" value="<?php echo $block_bread['link_url'];?>">
                                                                    <span class="input-group-append">
                                                                        <button type="button" class="btn btn-default btn-sm btn-flat call-function" data-callback="order_element_row" data-order="up">
                                                                            <i class="fad fa-chevron-up"></i>
                                                                        </button>
                                                                        <button type="button" class="btn btn-default btn-sm btn-flat call-function" data-callback="order_element_row" data-order="down">
                                                                            <i class="fad fa-chevron-down"></i>
                                                                        </button>
                                                                        <button type="button" class="btn btn-danger btn-sm btn-flat call-function" data-callback="delete_breadcrumb">
                                                                            <i class="fad fa-trash-alt"></i>
                                                                        </button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }?>
                                            <?php }?>
                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/template" data-template="breadcrumbs-row-template">
    <div class="row">
        <div class="col-12 col-md-6">
            <div class="form-group">
                <input class="form-control form-control-sm rounded-0" placeholder="ex. Ноутбуки" name="breadcrumbs[{{key_index}}][title]" value="{{title}}">
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="form-group">
                <div class="input-group input-group-sm">
                    <input type="text" class="form-control rounded-0" placeholder="ex. catalog/noutbuki" name="breadcrumbs[{{key_index}}][url]" value="{{url}}">
                    <span class="input-group-append">
                        <button type="button" class="btn btn-default btn-sm btn-flat call-function" data-callback="order_element_row" data-order="up">
                            <i class="fad fa-chevron-up"></i>
                        </button>
                        <button type="button" class="btn btn-default btn-sm btn-flat call-function" data-callback="order_element_row" data-order="down">
                            <i class="fad fa-chevron-down"></i>
                        </button>
                        <button type="button" class="btn btn-danger btn-sm btn-flat call-function" data-callback="delete_breadcrumb">
                            <i class="fad fa-trash-alt"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</script>
<script>
    var editor = ace.edit("js-code-html", {
        theme: "ace/theme/monokai",
        mode: "ace/mode/html",
        showPrintMargin: false,
        maxLines: 30,
        minLines: 20,
        wrap: true,
        autoScrollEditorIntoView: true
    });
    
	var order_element_row = function(btn){
		var $this = $(btn);
		var $element_row = $this.closest('.row');
		switch($this.data('order')){
			case 'up':
				var $prev_element_row = $element_row.prev();
				$element_row.insertBefore($prev_element_row);
			break;
			case 'down':
				var $next_element_row = $element_row.next();
				$element_row.insertAfter($next_element_row);
			break;
		}
	}

    var add_breadcrumb_row = function(btn){
		var $this = $(btn);
		var $this_row = $this.closest('.row');
		var breadcrumb_title = $this_row.find('input[name="breadcrumb_title"]').val();
		var breadcrumb_url = $this_row.find('input[name="breadcrumb_url"]').val();
        var key_index = uniqid('key_');
        var template = $('[data-template="breadcrumbs-row-template"]').text();

		if(breadcrumb_title == '' || breadcrumb_url == ''){
			systemMessages('Поля Название и Ссылка обязательны.', 'error');
			return false;
		}

		$this_row.find('input[name="breadcrumb_title"]').val('');
		$this_row.find('input[name="breadcrumb_url"]').val('');

        var content = {
			key_index: key_index,
			title: breadcrumb_title,
            url: breadcrumb_url
		};

		for (var key in content) {
			if (content.hasOwnProperty(key)) {
				template = template.replace(new RegExp('{{' + key + '}}', 'g'), content[key])
			}
		}

		$('#js-breadcrumbs-container').append(template);
	}

    var delete_breadcrumb = function(btn){
		var $this = $(btn);
        $this.closest('.row').remove();
    }

    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $('#js-breadcrumbs-form').html($('#js-breadcrumbs-container').html());
        var html_text = editor.getValue();        
        var fdata = manage_form.serializeArray();
        fdata.push({name: 'text', value: html_text});
        $.ajax({
            type: 'POST',
            url: base_url+'admin/static_block/ajax_operations/<?php echo !empty($static_block) ? 'edit' : 'add';?>',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });
</script>
