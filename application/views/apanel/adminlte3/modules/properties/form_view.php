<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($property) ? 'Редактировать' : 'Добавить';?> свойство
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="title_property" value="<?php echo !empty($property) ? $property['title_property'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <div class="icheck-primary d-inline">
                                <input type="checkbox" id="js-status_property" name="status_property" value="1" <?php echo set_checkbox('status_property', 1, @$property['status_property'] == 1);?>>
                                <label for="js-status_property">Активный</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <div class="icheck-primary d-inline">
                                <input type="checkbox" id="js-show_faq" name="show_faq" value="1" <?php echo set_checkbox('show_faq', 1, @$property['show_faq'] == 1);?>>
                                <label for="js-show_faq">Показать подсказку</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <div class="icheck-primary d-inline">
                                <input type="checkbox" id="js-on_item" name="on_item" value="1" <?php echo set_checkbox('on_item', 1, @$property['on_item'] == 1);?>>
                                <label for="js-on_item">Показывать на странице товаров</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <div class="icheck-primary d-inline">
                                <input type="checkbox" id="js-in_compare" name="in_compare" value="1" <?php echo set_checkbox('in_compare', 1, @$property['in_compare'] == 1);?>>
                                <label for="js-in_compare">Показывать в сравнение товаров</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <div class="icheck-primary d-inline">
                                <input type="checkbox" id="js-in_filter" name="in_filter" value="1" <?php echo set_checkbox('in_filter', 1, @$property['in_filter'] == 1);?>>
                                <label for="js-in_filter">Показывать в фильтре</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Тип значений</label>
                            <select name="type_property" class="form-control form-control-sm rounded-0">
                                <option value="simple" data-type="simple" <?php echo set_select('type_property', 'simple', @$property['type_property'] == 'simple');?>>Текстовое значение</option>
                                <option value="range" data-type="range" <?php echo set_select('type_property', 'range', @$property['type_property'] == 'range');?>>Числовое значение</option>
                                <option value="select" data-type="select" <?php echo set_select('type_property', 'select', @$property['type_property'] == 'select');?>>Много значений, один выбор</option>
                                <option value="multiselect" data-type="select" <?php echo set_select('type_property', 'multiselect', @$property['type_property'] == 'multiselect');?>>Много значений, множественный выбор</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12">                        
                        <div class="form-group">
                            <label>Значение</label>
                            <div id="property_values">
                                <?php if(!empty($property)){?>
                                    <?php if($property['type_property'] === 'range'){?>
                                        <div class="row">
                                            <?php $value = json_decode($property['property_values'], true);?>
                                            <?php if(!empty($value)){?>
                                                <div class="col-6 col-sm-4">
                                                    <div class="input-group input-group-sm">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text rounded-0">От</span>
                                                        </div>
                                                        <input type="text" class="form-control rounded-0" placeholder="0.00" name="property_value_from" value="<?php echo $value['min_number'];?>">
                                                    </div>
                                                </div>
                                                <div class="col-6 col-sm-4">
                                                    <div class="input-group input-group-sm">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text rounded-0">До</span>
                                                        </div>
                                                        <input type="text" class="form-control rounded-0" placeholder="0.00" name="property_value_to" value="<?php echo $value['max_number'];?>">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-4">
                                                    <div class="input-group input-group-sm">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text rounded-0">Единица</span>
                                                        </div>
                                                        <input type="text" class="form-control rounded-0" placeholder="ex. Mb" name="property_value_unit" value="<?php echo $value['unit'];?>">
                                                    </div>
                                                </div>
                                            <?php } else{?>
                                                <div class="col-6 col-sm-4">
                                                    <div class="input-group input-group-sm">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text rounded-0">От</span>
                                                        </div>
                                                        <input type="text" class="form-control rounded-0" placeholder="0.00" name="property_value_from">
                                                    </div>
                                                </div>
                                                <div class="col-6 col-sm-4">
                                                    <div class="input-group input-group-sm">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text rounded-0">До</span>
                                                        </div>
                                                        <input type="text" class="form-control rounded-0" placeholder="0.00" name="property_value_to">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-4">
                                                    <div class="input-group input-group-sm">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text rounded-0">Единица</span>
                                                        </div>
                                                        <input type="text" class="form-control rounded-0" placeholder="ex. Mb" name="property_value_unit">
                                                    </div>
                                                </div>
                                            <?php }?>
                                        </div>
                                    <?php }?>
    
                                    <?php if(in_array($property['type_property'], array('select', 'multiselect'))){?>
                                        <div class="js-properties-select-container">
                                            <div class="js-properties-select-values">
                                                <?php $values = json_decode($property['property_values'], true);?>
                                                <?php if(!empty($values)){?>
                                                    <?php foreach($values as $value){?>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <div class="input-group input-group-sm">
                                                                        <input type="text" name="property_values[]" class="form-control rounded-0" placeholder="Значение" value="<?php echo $value['value'];?>">
                                                                        <input type="hidden" name="values_list[]" value="<?php echo $value['id_value'];?>">
                                                                        <span class="input-group-append">
                                                                            <input type="hidden" name="properties_weight[]">
                                                                            <button type="button" class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="up"><i class="fad fa-arrow-alt-up"></i></button>
                                                                            <button type="button" class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="down"><i class="fad fa-arrow-alt-down"></i></button>
                                                                            <button type="button" class="btn btn-danger btn-flat confirm-dialog" data-message="Вы уверены что хотите удалить это значение?" data-callback="delete_property_value" data-value="<?php echo $value['id_value'];?>" data-title="<i class='fad fa-trash-alt'></i> Удалить" data-type="bg-danger"><i class="fad fa-trash-alt"></i></button>    
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                                <?php } else{?>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <div class="input-group input-group-sm">
                                                                    <input type="text" name="property_values[]" class="form-control rounded-0" placeholder="Значение">
                                                                    <input type="hidden" name="values_list[]">
                                                                    <span class="input-group-append">
                                                                        <input type="hidden" name="properties_weight[]">
                                                                        <button type="button" class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="up"><i class="fad fa-arrow-alt-up"></i></button>
                                                                        <button type="button" class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="down"><i class="fad fa-arrow-alt-down"></i></button>
                                                                        <button type="button" class="btn btn-danger btn-flat confirm-dialog" data-message="Вы уверены что хотите удалить это значение?" data-callback="remove_row" data-title="<i class='fad fa-trash-alt'></i> Удалить" data-type="bg-danger"><i class="fad fa-trash-alt"></i></button>    
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }?>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <button type="button" class="btn btn-default btn-flat btn-sm call-function" data-callback="add_row">Добавить значение</button>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }?>

                                    <?php if($property['type_property'] == 'simple'){?>
                                        <p>Значания будут добавлены для каждого товара отдельно.</p>
                                    <?php }?>
                                <?php } else{?>
                                    <p>Значания будут добавлены для каждого товара отдельно.</p>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>FAQ</label>
                            <textarea name="faq_text" class="form-control form-control-sm rounded-0" rows="3"><?php echo @$property['faq_text'];?></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-end">
                <div class="d-flex">
                    <?php if(!empty($property)){?>
                        <input type="hidden" name="id_property" value="<?php echo $property['id_property'];?>">
                    <?php }?>
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/template" id="js-formtemplate-type-range">
	<div class="row">
        <?php if(!empty($property) && $property['type_property'] == 'range'){?>
            <?php $value = json_decode($property['property_values'], true);?>
            <?php if(!empty($value)){?>
                <div class="col-6 col-sm-4">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text rounded-0">От</span>
                        </div>
                        <input type="text" class="form-control rounded-0" placeholder="0.00" name="property_value_from" value="<?php echo $value['min_number'];?>">
                    </div>
                </div>
                <div class="col-6 col-sm-4">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text rounded-0">До</span>
                        </div>
                        <input type="text" class="form-control rounded-0" placeholder="0.00" name="property_value_to" value="<?php echo $value['max_number'];?>">
                    </div>
                </div>
                <div class="col-12 col-sm-4">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text rounded-0">Единица</span>
                        </div>
                        <input type="text" class="form-control rounded-0" placeholder="ex. Mb" name="property_value_unit" value="<?php echo $value['unit'];?>">
                    </div>
                </div>
            <?php } else{?>
                <div class="col-6 col-sm-4">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text rounded-0">От</span>
                        </div>
                        <input type="text" class="form-control rounded-0" placeholder="0.00" name="property_value_from">
                    </div>
                </div>
                <div class="col-6 col-sm-4">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text rounded-0">До</span>
                        </div>
                        <input type="text" class="form-control rounded-0" placeholder="0.00" name="property_value_to">
                    </div>
                </div>
                <div class="col-12 col-sm-4">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text rounded-0">Единица</span>
                        </div>
                        <input type="text" class="form-control rounded-0" placeholder="ex. Mb" name="property_value_unit">
                    </div>
                </div>
            <?php }?>
        <?php } else{?>
            <div class="col-6 col-sm-4">
                <div class="input-group input-group-sm">
                    <div class="input-group-prepend">
                        <span class="input-group-text rounded-0">От</span>
                    </div>
                    <input type="text" class="form-control rounded-0" placeholder="0.00" name="property_value_from">
                </div>
            </div>
            <div class="col-6 col-sm-4">
                <div class="input-group input-group-sm">
                    <div class="input-group-prepend">
                        <span class="input-group-text rounded-0">До</span>
                    </div>
                    <input type="text" class="form-control rounded-0" placeholder="0.00" name="property_value_to">
                </div>
            </div>
            <div class="col-12 col-sm-4">
                <div class="input-group input-group-sm">
                    <div class="input-group-prepend">
                        <span class="input-group-text rounded-0">Единица</span>
                    </div>
                    <input type="text" class="form-control rounded-0" placeholder="ex. Mb" name="property_value_unit">
                </div>
            </div>
        <?php }?>
    </div>
</script>

<script type="text/template" id="js-formtemplate-type-select">
    <div class="js-properties-select-container">
        <div class="js-properties-select-values">
            <?php if(!empty($property) && in_array($property['type_property'], array('select', 'multiselect'))){?>
                <?php $values = json_decode($property['property_values'], true);?>
                <?php if(!empty($values)){?>
                    <?php foreach($values as $value){?>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="input-group input-group-sm">
                                        <input type="text" name="property_values[]" class="form-control rounded-0" placeholder="Значение" value="<?php echo $value['value'];?>">
                                        <input type="hidden" name="values_list[]" value="<?php echo $value['id_value'];?>">
                                        <span class="input-group-append">
                                            <input type="hidden" name="properties_weight[]">
                                            <button type="button" class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="up"><i class="fad fa-arrow-alt-up"></i></button>
                                            <button type="button" class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="down"><i class="fad fa-arrow-alt-down"></i></button>
                                            <button type="button" class="btn btn-danger btn-flat confirm-dialog" data-message="Вы уверены что хотите удалить это значение?" data-callback="delete_property_value" data-value="<?php echo $value['id_value'];?>" data-title="<i class='fad fa-trash-alt'></i> Удалить" data-type="bg-danger"><i class="fad fa-trash-alt"></i></button>    
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                <?php } else{?>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <div class="input-group input-group-sm">
                                    <input type="text" name="property_values[]" class="form-control rounded-0" placeholder="Значение">
                                    <input type="hidden" name="values_list[]">
                                    <span class="input-group-append">
                                        <input type="hidden" name="properties_weight[]">
                                        <button type="button" class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="up"><i class="fad fa-arrow-alt-up"></i></button>
                                        <button type="button" class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="down"><i class="fad fa-arrow-alt-down"></i></button>
                                        <button type="button" class="btn btn-danger btn-flat confirm-dialog" data-message="Вы уверены что хотите удалить это значение?" data-callback="remove_row" data-title="<i class='fad fa-trash-alt'></i> Удалить" data-type="bg-danger"><i class="fad fa-trash-alt"></i></button>    
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }?>
            <?php } else{?>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <div class="input-group input-group-sm">
                                <input type="text" name="property_values[]" class="form-control rounded-0" placeholder="Значение">
                                <input type="hidden" name="values_list[]">
                                <span class="input-group-append">
                                    <input type="hidden" name="properties_weight[]">
                                    <button type="button" class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="up"><i class="fad fa-arrow-alt-up"></i></button>
                                    <button type="button" class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="down"><i class="fad fa-arrow-alt-down"></i></button>
                                    <button type="button" class="btn btn-danger btn-flat confirm-dialog" data-message="Вы уверены что хотите удалить это значение?" data-callback="remove_row" data-title="<i class='fad fa-trash-alt'></i> Удалить" data-type="bg-danger"><i class="fad fa-trash-alt"></i></button>    
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }?>
        </div>
        <div class="row">
            <div class="col-12">
                <button type="button" class="btn btn-default btn-flat btn-sm call-function" data-callback="add_row">Добавить значение</button>
            </div>
        </div>
    </div>
</script>

<script type="text/template" id="js-formtemplate-type-select-row">
    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <div class="input-group input-group-sm">
                    <input type="text" name="property_values[]" class="form-control rounded-0" placeholder="Значение">
                    <input type="hidden" name="values_list[]">
                    <span class="input-group-append">
                        <input type="hidden" name="properties_weight[]">
                        <button type="button" class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="up"><i class="fad fa-arrow-alt-up"></i></button>
                        <button type="button" class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="down"><i class="fad fa-arrow-alt-down"></i></button>
                        <button type="button" class="btn btn-danger btn-flat confirm-dialog" data-message="Вы уверены что хотите удалить это значение?" data-callback="remove_row" data-title="<i class='fad fa-trash-alt'></i> Удалить" data-type="bg-danger"><i class="fad fa-trash-alt"></i></button>    
                    </span>
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/template" id="js-formtemplate-type-simple">
    <p>Значания будут добавлены для каждого товара отдельно.</p>
</script>

<script>
    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        tinyMCE.triggerSave();
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/properties/ajax_operations/<?php echo !empty($property) ? 'edit' : 'add';?>',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });

    $('select[name=type_property]').on('change', function(){
        var type_property = $( "select[name=type_property] option:selected" ).data('type');
        $('#property_values').html($('#js-formtemplate-type-'+type_property).html());
    });

	var order_element_row = function(btn){
		var $this = $(btn);
		var $element_row = $this.closest('.row');
		switch($this.data('order')){
			case 'up':
				var $prev_element_row = $element_row.prev();
				$element_row.insertBefore($prev_element_row);
			break;
			case 'down':
				var $next_element_row = $element_row.next();
				$element_row.insertAfter($next_element_row);
			break;
		}
	}
	
	var delete_property_value = function(btn){
		var $this = $(btn);
		var id_value = $this.data('value');
		$('#property_values').append('<input type="hidden" name="delete_values[]" value="'+id_value+'">');
		$this.closest('.row').remove();
	}
	
	var remove_row = function(btn){
		var $this = $(btn);
		$this.closest('.row').remove();
	}
	
	var add_row = function(btn){
		var $this = $(btn);
		var $container = $this.closest('.js-properties-select-container');
		var $container_values = $container.children('.js-properties-select-values');
		var new_row = $('#js-formtemplate-type-select-row').html();
		$container_values.append(new_row);
	}
</script>
