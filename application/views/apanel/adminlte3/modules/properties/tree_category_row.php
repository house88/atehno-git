<?php if(!empty($category)){?>
		<?php 
			$breads = json_decode('['.$category['category_breadcrumbs'].']', true);
			$category_bread = array();
			foreach($breads as $bread){
				$category_bread[] = $bread['category_title'];
			}
		?>
		<tr class="parent_<?php echo $category['category_parent'];?>" data-category="<?php echo $category['category_id'];?>">
			<td class="custom-width-50 text-center">
				<a 
					href="#" 
					class="custom-font-size-34 <?php echo get_choice('fad fa-check-square text-success', (int) $category['property_category_to_children'] === 1, 'fad fa-minus-square text-gray')?> call-function" 
					data-callback="<?php echo get_choice('remove_category_children_from_property', (int) $category['property_category_to_children'] === 1, 'set_category_children_to_property')?>" 
					data-message="<?php echo get_choice('Снять передачу свойства вложеным категориям?', (int) $category['property_category_to_children'] === 1, 'Установить передачу свойства вложеным категориям?')?>"
					data-category="<?php echo $category['category_id'];?>"
					>
				</a>
            </td>
			<td class="<?php if($level > 0){?>pl-<?php echo $level * 15;?>_i<?php }?>">
				<p class="custom-font-size-14 custom-margin-bottom-0">
					<strong>
						<?php if($level > 0){?>
							<span class="simple_tree">&rdsh;</span>					
						<?php }?>
						<?php echo $category['category_title'];?>
					</strong>
				</p>
				<p class="custom-font-size-12 custom-margin-bottom-0"><?php echo implode(' / ', $category_bread);?></p>
			</td>
			<td class="custom-width-50">
				<button type="button" class="btn btn-danger btn-flat btn-sm confirm-dialog" data-title="<i class='fad fa-trash-alt'></i> Удалить" data-callback="remove_category_property_relation" data-category="<?php echo $category['category_id'];?>" data-message="Вы уверены что хотите удалить свойство?"><i class="fad fa-trash-alt"></i></button>
			</td>
		</tr>
<?php }?>
