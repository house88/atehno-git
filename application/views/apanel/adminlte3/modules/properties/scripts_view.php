<?php $this->load->view($this->theme->apanel_view('includes/plugins/datatable'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/tinymce'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/fileupload'));?>

<script>
	var dtTable;
	var dtFilter;
	$(function(){
		'use strict';
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/Russian.json'));?>"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/properties/ajax_operations/list",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "vam text-center custom-width-100", "aTargets": ["dt_id"], "mData": "dt_id"},
				{ "sClass": "vam text-left", "aTargets": ["dt_name"], "mData": "dt_name"},
				{ "sClass": "vam text-center custom-width-100", "aTargets": ["dt_cweight"], "mData": "dt_cweight"},
				{ "sClass": "vam text-center custom-width-100", "aTargets": ["dt_status"], "mData": "dt_status"},
				{ "sClass": "vam text-center custom-width-100", "aTargets": ["dt_on_item"], "mData": "dt_on_item"},
				{ "sClass": "vam text-center custom-width-100", "aTargets": ["dt_in_compare"], "mData": "dt_in_compare"},
				{ "sClass": "vam text-center custom-width-100", "aTargets": ["dt_in_filter"], "mData": "dt_in_filter"},
				{ "sClass": "vam text-center custom-width-100", "aTargets": ["dt_show_faq"], "mData": "dt_show_faq"},
				{ "sClass": "vam text-center custom-width-50", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }
			],
			"aaSorting" : [[0,'desc']],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				if(!dtFilter){
					dtFilter = initDtFilter('properties');
				}

				aoData = aoData.concat(dtFilter.getDTFilter());

				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					beforeSend: function(){
						showLoader('body');
					},
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, data.mess_type);
						}

						$('#total_dtTable-counter').text(data.iTotalDisplayRecords);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                fixDataTablePagination(this);

				mobileDataTable($('.main-data-table'));
				
                hideLoader('body');
			}
		});
	});

	function onDeleteFilters(filter){
		if(filter.name === 'category'){
			$('body').find('select[name="category"] option[value=""]').prop('selected', true);
			$('.bs-select-element').selectpicker('refresh');
		}
	}

	var deteleCategoryFilter = function(element){
		var $this = $(element);
		var $select = $('select[name="category"]');
		var $option = $select.find('option[value=""]');
		$option.prop('selected', true);
		$select.trigger('change');
	}
	
	var change_status = function(btn){
		var $this = $(btn);
		var property = $this.data('property');
		var action = $this.data('action');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/properties/ajax_operations/change_status/'+action,
			data: {property:property},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}

	var delete_action = function(btn){
		var $this = $(btn);
		var property = $this.data('property');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/properties/ajax_operations/delete',
			data: {property:property},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}

	var change_property_category_weight = function(btn){
		var $this = $(btn);
		var property = $this.data('property');
		var category = $this.data('category');
		var property_category_weight = $this.closest('.input-group').find('input[name="property_category_weight"]').val();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/properties/ajax_operations/change_property_category_weight',
			data: {category:category,property:property,property_category_weight:property_category_weight},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>