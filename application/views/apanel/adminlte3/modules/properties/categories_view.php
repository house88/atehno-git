<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                Категории свойства
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label>Добавить категорию</label>
                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <select name="add_category" class="form-control rounded-0 bs-select-categories" data-live-search="true" data-style="btn btn-default btn-sm btn-flat" data-title="Категория" data-show-content="false">
                            <option value="">Выберите категорию</option>
                            <?php if(!empty($categories)){?>
                                <?php echo $categories;?>
                            <?php }?>
                        </select>
                        <span class="input-group-append">
                            <button type="button" class="btn btn-primary btn-flat call-function" data-callback="add_category_to_property" title="Добавить"><i class="fad fa-plus"></i></button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Привязаные категорий</label>						
                <table class="table table-bordered table-hover" id="cTable">
                    <thead>
                        <tr>
                            <th class="custom-width-50"></th>
                            <th>Название</th>
                            <th class="custom-width-50"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($property_categories_table)){?>
                            <?php echo $property_categories_table;?>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/template" id="js-formtemplate-category-row">
    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <div class="input-group input-group-sm">
                    <input type="text" name="property_values[]" class="form-control rounded-0" placeholder="Значение">
                    <input type="hidden" name="values_list[]">
                    <span class="input-group-append">
                        <input type="hidden" name="properties_weight[]">
                        <button type="button" class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="up"><i class="fad fa-arrow-alt-up"></i></button>
                        <button type="button" class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="down"><i class="fad fa-arrow-alt-down"></i></button>
                        <button type="button" class="btn btn-danger btn-flat confirm-dialog" data-message="Вы уверены что хотите удалить это значение?" data-callback="remove_row" data-title="<i class='fad fa-trash-alt'></i> Удалить" data-type="bg-danger"><i class="fad fa-trash-alt"></i></button>    
                    </span>
                </div>
            </div>
        </div>
    </div>
</script>

<script>
    var id_property = intval('<?php echo $property['id_property'];?>');
    $(".bs-select-categories").selectpicker();

    var add_category_to_property = function(btn){
		var $this = $(btn);
		var category = $this.closest('.input-group').find('select[name="add_category"]').val();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/properties/ajax_operations/set_category_property_relation',
			data: {category:category,id_property:id_property},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
				if(resp.mess_type == 'success'){
					$('table#cTable > tbody').html(resp.property_categories_table);
				}
			}
		});
		return false;
	}

	var remove_category_property_relation = function(btn){
		var $this = $(btn);
		var category = $this.data('category');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/properties/ajax_operations/remove_category_property_relation',
			data: {category:category,id_property:id_property},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
				if(resp.mess_type == 'success'){
					$('table#cTable > tbody').html(resp.property_categories_table);
				}
			}
		});
		return false;
	}

	var set_category_children_to_property = function(btn){
		var $this = $(btn);
		var category = $this.data('category');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/properties/ajax_operations/set_category_children_to_property',
			data: {category:category,id_property:id_property},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
				if(resp.mess_type == 'success'){
					$('table#cTable > tbody').html(resp.property_categories_table);
				}
			}
		});
		return false;
	}

	var remove_category_children_from_property = function(btn){
		var $this = $(btn);
		var category = $this.data('category');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/properties/ajax_operations/remove_category_children_from_property',
			data: {category:category,id_property:id_property},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
				if(resp.mess_type == 'success'){
					$('table#cTable > tbody').html(resp.property_categories_table);
				}
			}
		});
		return false;
	}
</script>
