<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-widget custom-margin-top-10">
				<div class="card-header pl-2 pl-sm-3 pr-2 pr-sm-3">
					<h3 class="card-title">
						<?php echo $page_header;?>, <small>найдено: <span id="total_dtTable-counter">0</span></small>
					</h3>
					<div class="card-tools">
						<a class="btn btn-tool call-popup" data-popup="#general_popup_form" href="#" data-href="<?php echo base_url('admin/properties/popup/add');?>" tooltip="Добавить бренд"><i class="fas fa-plus"></i> Добавить</a>
					</div>
				</div>
				<div class="card-body pl-2 pl-sm-3 pr-2 pr-sm-3">
					<div class="form-group">
						<div class="input-group input-group-sm">
							<select name="category" class="form-control rounded-0 dt_filter bs-select-element" data-live-search="true" data-style="btn btn-default btn-sm btn-flat" data-title="Категория" data-show-content="false">
								<option value="0">Корень</option>
								<?php if(!empty($categories)){?>
									<?php echo $categories;?>
								<?php }?>
							</select>
							<span class="input-group-append">
								<button type="button" class="btn btn-danger btn-flat call-function" data-callback="deteleCategoryFilter" title="Почистить фильтры"><i class="fad fa-eraser"></i></button>
							</span>
						</div>
					</div>
					
					<div class="dtfilter-list">
						<div class="filter-plugin-list d-flex justify-content-start flex-wrap"></div>
					</div>

					<div class="table-responsive">
						<table class="table table-bordered table-hover main-data-table" id="dtTable" style="width: 100%;">
							<thead>
								<tr>
									<th class="dt_id"># ID</th>
									<th class="dt_name">Название</th>
									<th class="dt_cweight">Вес</th>
									<th class="dt_status">Статус</th>
									<th class="dt_on_item">В товаре</th>
									<th class="dt_in_compare">В сравнений</th>
									<th class="dt_in_filter">В филтре</th>
									<th class="dt_show_faq">Подсказка</th>
									<th class="dt_actions"></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>