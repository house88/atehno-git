<?php if(!empty($properties)){?>
	<div class="form-group">
		<label class="text-gray custom-font-size-12">Обратная фильтрация</label>
		<div class="btn-toolbar">
			<div class="btn-group btn-group-xs">
				<a href="#" class="btn btn-primary btn-xs btn-flat custom-width-30 dt_filter" data-name="items_by_properties_inverse" title="Включить" data-title="Обратная фильтрация" data-value-text="Да" data-value="1">
					<span class="fad fa-eye"></span>
				</a>
				<a href="#" class="btn btn-warning btn-xs btn-flat custom-width-30 dt_filter" data-name="items_by_properties_inverse" title="Отключить" data-title="Обратная фильтрация" data-value-text="Нет" data-value="0" data-default="true">
					<span class="fad fa-eye-slash"></span>
				</a>
			</div>
		</div>
	</div>
	<?php foreach($properties as $property){?>
		<?php $property_values = json_decode($property['property_values'], true);?>
		<?php if(!empty($property_values)){?>
			<div class="form-group">
				<label class="text-gray custom-font-size-12"><?php echo $property['title_property'];?><?php if($property['type_property'] == 'range'){ echo ', '.$property_values['unit'];}?></label>
				<?php if(in_array($property['type_property'], array('select', 'multiselect'))){?>
					<select name="property_select_<?php echo $property['id_property'];?>" class="form-control form-control-sm rounded-0 dt_filter" <?php echo get_choice('multiple', $property['type_property'] === 'multiselect') ;?> data-title="<?php echo $property['title_property'];?>">
						<?php foreach($property_values as $property_value){?>
							<option value="<?php echo $property_value['id_value'];?>"><?php echo $property_value['value'];?></option>
						<?php }?>
					</select>
				<?php }?>
				<?php if($property['type_property'] == 'range'){?>
					<div class="input-group">
						<input class="form-control dt_filter" type="text" data-title="<?php echo $property['title_property'];?> от" name="property_range_<?php echo $property['id_property'];?>_from" placeholder="От">
						<div class="input-group-addon">-</div>
						<input class="form-control dt_filter" type="text" data-title="<?php echo $property['title_property'];?> до" name="property_range_<?php echo $property['id_property'];?>_to" placeholder="До">
					</div>
					<p class="help-block">Мин. <?php echo $property_values['min_number']. ' ' .$property_values['unit'];?>; Макс. <?php echo $property_values['max_number']. ' ' .$property_values['unit'];?></p>
				<?php }?>
			</div>
		<?php }?>
	<?php }?>
<?php }?>
