<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($menu_item) ? 'Редактировать' : 'Добавить';?> раздел меню
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="title" value="<?php echo !empty($menu_item) ? $menu_item['menu_title'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Елементы меню</label>
                            <div id="js-menu-elements">
                                <?php if(!empty($menu_item)){?>
                                    <?php $menu_elements = json_decode($menu_item['menu_elements'], true);?>
                                    <?php if(!empty($menu_elements)){?>
                                        <?php foreach($menu_elements as $key_index => $menu_element){?>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <div class="input-group input-group-sm photo_colorpicker">
                                                            <input class="form-control custom-width-max-150 rounded-0" placeholder="Название" name="menu_elements[<?php echo $key_index;?>][title]" value="<?php echo $menu_element['title'];?>">
                                                            <span class="input-group-append">
                                                                <span class="input-group-text"><i class="fad fa-link"></i></span>
                                                            </span>
                                                            <input class="form-control rounded-0" placeholder="Ссылка" name="menu_elements[<?php echo $key_index;?>][link]" value="<?php echo $menu_element['link'];?>">
                                                            <div class="input-group-append rounded-0">
                                                                <button type="button" class="btn btn-danger btn-sm btn-flat call-function" data-callback="delete_row">
                                                                    <i class="fad fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }?>
                                    <?php }?>
                                <?php }?>
                            </div>
                            <button class="btn btn-default btn-sm btn-flat custom-margin-top-10 call-function" data-callback="add_row">Добавить елемент</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <div class="icheck-primary">
                    <input name="menu_visible" type="checkbox" id="js-checkbox-menu-active" <?php echo set_checkbox('menu_visible', 1, !empty($menu_item) ? (int) $menu_item['menu_visible'] === 1 : false);?>>
                    <label for="js-checkbox-menu-active">
                        Активный
                    </label>
                </div>
                <div class="d-flex">
                    <?php if(!empty($menu_item)){?>
                        <input type="hidden" name="menu" value="<?php echo $menu_item['id_menu'];?>">
                    <?php }?>
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="templates d-none">
	<div data-template="menu-row-template">        
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <div class="input-group input-group-sm photo_colorpicker">
                        <input class="form-control custom-width-max-150 rounded-0" placeholder="Название" name="menu_elements[{{key_index}}][title]">
                        <span class="input-group-append">
                            <span class="input-group-text"><i class="fad fa-link"></i></span>
                        </span>
                        <input class="form-control rounded-0" placeholder="Ссылка" name="menu_elements[{{key_index}}][link]">
                        <div class="input-group-append rounded-0">
                            <button type="button" class="btn btn-danger btn-sm btn-flat call-function" data-callback="delete_row">
                                <i class="fad fa-trash-alt"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>

<script>
    var add_row = function(element){
        var $this = $(element);
		var template = $('div[data-template="menu-row-template"]').html();
		var setting_type = $this.data('type');
		var key_index = uniqid('key_');

		template = template.replace(/{{key_index}}/g, key_index);
		$('#js-menu-elements').append(template);
    }
    
	var delete_row = function(obj){
		var $this = $(obj);
		$this.closest('.row').remove();
	}

    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/menu/ajax_operations/<?php echo !empty($menu_item) ? 'edit' : 'add';?>',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });
</script>
