<?php $this->load->view($this->theme->apanel_view('includes/plugins/datatable'));?>

<script>
	var dtTable; //obj of datatable
	$(function(){
		'use strict';
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/Russian.json'));?>"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/credit/ajax_operations/list",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "vam text-center mw-100", "aTargets": ["dt_months"], "mData": "dt_months", "bSortable": false},
				{ "sClass": "vam", "aTargets": ["dt_price"], "mData": "dt_price", "bSortable": false},
				{ "sClass": "vam text-center", "aTargets": ["dt_prepayment"], "mData": "dt_prepayment", "bSortable": false},
				{ "sClass": "vam text-center", "aTargets": ["dt_rate"], "mData": "dt_rate", "bSortable": false},
				{ "sClass": "vam text-center", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }
			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					beforeSend: function(){
						showLoader('body');
					},
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, data.mess_type);
						}

						console.log('ajax');
						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                fixDataTablePagination(this);

				mobileDataTable($('.main-data-table'));

				
				hideLoader('body');
			}
		});
	});

	var delete_action = function(btn){
		var $this = $(btn);
		var option = $this.data('option');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/credit/ajax_operations/delete_option',
			data: {option:option},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>