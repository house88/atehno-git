<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($credit_option) ? 'Редактировать' : 'Добавить';?> кредитную опцию
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 col-sm-5">
                        <div class="form-group">
                            <label>Кол. месяцев</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="ex. 6" name="months" value="<?php echo !empty($credit_option) ? $credit_option['months'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label>Аванс</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="0.00" name="pre_pay" value="<?php echo !empty($credit_option) ? $credit_option['pre_pay'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12 col-sm-3">
                        <div class="form-group">
                            <label>%</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="7.5" name="percents" value="<?php echo !empty($credit_option) ? $credit_option['percents'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Сумма, лей</label>
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">от</span>
                                </div>
                                <input class="form-control form-control-sm rounded-0" placeholder="0.00" name="price_from" value="<?php echo !empty($credit_option) ? $credit_option['price_from'] : '';?>">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">до</span>
                                </div>
                                <input class="form-control form-control-sm rounded-0" placeholder="0.00" name="price_to" value="<?php echo !empty($credit_option) ? $credit_option['price_to'] : '';?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-end">
                <?php if(!empty($credit_option)){?>
                    <input type="hidden" name="option" value="<?php echo $credit_option['id_option'];?>">
                <?php }?>
                <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
            </div>
        </form>
    </div>
</div>

<script>
    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/credit/ajax_operations/<?php echo !empty($credit_option) ? 'edit_option' : 'add_option';?>',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });
</script>
