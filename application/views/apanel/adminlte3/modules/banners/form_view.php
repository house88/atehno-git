<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($banner) ? 'Редактировать' : 'Добавить';?> баннер
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="banner_name" value="<?php echo !empty($banner) ? $banner['banner_name'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12" id="js-banners-container">
                        <?php if(!empty($banner)){?>
                            <?php $banner_images = json_decode($banner['banner_data'], true);?>
                            <?php if(!empty($banner_images)){?>
                                <?php foreach($banner_images as $key_image => $banner_image ){?>
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <img src="<?php echo base_url('files/banners/'. $banner['banner_tocken'] .'/'. $banner_image['photo']);?>" class="custom-width-100pr custom-margin-bottom-10">
                                            <input type="hidden" name="banner[<?php echo $key_image;?>][photo]" value="<?php echo $banner_image['photo'];?>">
                                            <input type="hidden" name="banner[<?php echo $key_image;?>][is_old]" value="1">
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label>Название</label>
                                                        <input class="form-control form-control-sm rounded-0" placeholder="Название" name="banner[<?php echo $key_image;?>][title]" value="<?php echo $banner_image['title'];?>">
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label>Ссылка</label>
                                                        <input class="form-control form-control-sm rounded-0" placeholder="Ссылка" name="banner[<?php echo $key_image;?>][link]" value="<?php echo $banner_image['link'];?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group input-group-sm photo_colorpicker">
                                                    <button type="button" class="btn btn-default btn-sm btn-flat" title="Текст" data-index="<?php echo $key_image;?>" data-toggle="modal" data-target="#modal-banner-text">
                                                        <i class="fas fa-pencil"></i>
                                                    </button>
                                                    <div class="input-group-append">
                                                        <input type="hidden" class="form-control colorpicker-input" name="banner[<?php echo $key_image;?>][bg_color]" value="<?php echo $banner_image['bg_color'];?>"/>
                                                        <span class="input-group-text colorpicker-input-addon"><i></i></span>
                                                        <span class="input-group-text">
                                                            <div class="icheck-primary custom-margin-auto_i">
                                                                <input name="banner[<?php echo $key_image;?>][active]" <?php echo get_choice('checked', (int) $banner_image['active'] === 1);?> type="checkbox" id="js-checkbox-banner-<?php echo $key_image;?>-active">
                                                                <label for="js-checkbox-banner-<?php echo $key_image;?>-active" class="no-content"></label>
                                                            </div>
                                                        </span>
                                                    </div>
                                                    <button type="button" class="btn btn-default btn-sm btn-flat call-function" data-callback="order_element_row" data-order="up">
                                                        <i class="fad fa-chevron-up"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default btn-sm btn-flat call-function" data-callback="order_element_row" data-order="down">
                                                        <i class="fad fa-chevron-down"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-sm btn-flat call-function" data-callback="delete_image" data-photo="<?php echo $banner_image['photo'];?>">
                                                        <i class="fad fa-trash-alt"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <textarea name="banner[<?php echo $key_image;?>][text]" id="js-banner-text-<?php echo $key_image;?>" class="d-none"><?php echo $banner_image['text'];?></textarea>
                                        </div>
                                    </div>
                                <?php }?>
                            <?php }?>
                        <?php }?>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <bitton type="button" class="btn btn-primary btn-flat btn-sm btn-file pull-left">
                    <i class="fad fa-picture-o"></i>
                    Добавить фото <input id="select_photo" type="file" name="userfile">
                </bitton>
                <div class="d-flex">
                    <?php if(!empty($banner)){?>
                        <input type="hidden" name="id_banner" value="<?php echo $banner['id_banner'];?>">
                    <?php }?>
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/template" data-template="banner-row-template">
    <div class="row">
        <div class="col-12 col-sm-6">
            <img src="<?php echo base_url('files/temp')?>/{{file_name}}" class="custom-width-100pr custom-margin-bottom-10">
            <input type="hidden" name="banner[{{key_index}}][photo]" value="{{file_name}}">
        </div>
        <div class="col-12 col-sm-6">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label>Название</label>
                        <input class="form-control form-control-sm rounded-0" placeholder="Название" name="banner[{{key_index}}][title]" value="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Ссылка</label>
                        <input class="form-control form-control-sm rounded-0" placeholder="Ссылка" name="banner[{{key_index}}][link]" value="">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group input-group-sm photo_colorpicker">
                    <button type="button" class="btn btn-default btn-sm btn-flat" title="Текст" data-index="{{key_index}}" data-toggle="modal" data-target="#modal-banner-text">
                        <i class="fas fa-pencil"></i>
                    </button>
                    <div class="input-group-append">
                        <input type="hidden" class="form-control colorpicker-input" name="banner[{{key_index}}][bg_color]"/>
                        <span class="input-group-text colorpicker-input-addon"><i></i></span>
                        <span class="input-group-text">
                            <div class="icheck-primary custom-margin-auto_i">
                                <input name="banner[{{key_index}}][active]" type="checkbox" id="js-checkbox-banner-{{key_index}}-active">
                                <label for="js-checkbox-banner-{{key_index}}-active" class="no-content"></label>
                            </div>
                        </span>
                    </div>
                    <button type="button" class="btn btn-default btn-sm btn-flat call-function" data-callback="order_element_row" data-order="up">
                        <i class="fad fa-chevron-up"></i>
                    </button>
                    <button type="button" class="btn btn-default btn-sm btn-flat call-function" data-callback="order_element_row" data-order="down">
                        <i class="fad fa-chevron-down"></i>
                    </button>
                    <button type="button" class="btn btn-danger btn-sm btn-flat call-function" data-callback="delete_new_image" data-photo="{{file_name}}">
                        <i class="fad fa-trash-alt"></i>
                    </button>
                </div>
            </div>
            <textarea name="banner[{{key_index}}][text]" id="js-banner-text-{{key_index}}" class="d-none"></textarea>
        </div>
    </div>
</script>

<script>
	function init_colorpicker(){
		$('.photo_colorpicker').colorpicker({
            input: '.colorpicker-input',
            format: 'hex'
        });
    }
    
	var order_element_row = function(btn){
		var $this = $(btn);
		var $element_row = $this.closest('.row');
		switch($this.data('order')){
			case 'up':
				var $prev_element_row = $element_row.prev();
				$element_row.insertBefore($prev_element_row);
			break;
			case 'down':
				var $next_element_row = $element_row.next();
				$element_row.insertAfter($next_element_row);
			break;
		}
	}

    var delete_new_image = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_temp_photos[]" value="'+photo+'"/>');
        $this.closest('.row').remove();
    }

    var delete_image = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
        $this.closest('.row').remove();
    }

    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/banners/ajax_operations/<?php echo !empty($banner) ? 'edit' : 'add';?>',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });
    
    init_colorpicker();
    
    $('#select_photo').fileupload({
        url: base_url+'admin/banners/ajax_operations/upload',
        dataType: 'json',
        done: function (e, data) {
            if(data.result.mess_type == 'error'){
                systemMessages( data.result.message, data.result.mess_class );
            } else{
                var uploaded_file = data.result.file;
                var key_index = uniqid('key_');
                var template = $('[data-template="banner-row-template"]').text();
                
                template = template.replace(/{{file_name}}/g, uploaded_file.filename);
                template = template.replace(/{{key_index}}/g, key_index);

                $('#js-banners-container').append(template);

                init_colorpicker();
            }
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
</script>
