<?php $this->load->view($this->theme->apanel_view('includes/plugins/datatable'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/tinymce'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/fileupload'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/colorpicker'));?>

<script>
	var dtTable;
	var banner_key_index = null;
	$(function(){
		'use strict';
    	
		initTinyMce();
		
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/Russian.json'));?>"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/banners/ajax_operations/list",
			"sServerMethod": "POST",
			"iDisplayLength": 50,
			"aoColumnDefs": [
				{ "sClass": "vam text-center custom-width-50", "aTargets": ["dt_id"], "mData": "dt_id"},
				{ "sClass": "vam text-left", "aTargets": ["dt_name"], "mData": "dt_name", "bSortable": false},
				{ "sClass": "vam text-left custom-width-200", "aTargets": ["dt_folder"], "mData": "dt_folder", "bSortable": false},
				{ "sClass": "vam text-center custom-width-100", "aTargets": ["dt_active"], "mData": "dt_active", "bSortable": false},
				{ "sClass": "vam text-center custom-width-50", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }
			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					beforeSend: function(){
						showLoader('body');
					},
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, data.mess_type);
						}

						$('#total_dtTable-counter').text(data.iTotalDisplayRecords);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                fixDataTablePagination(this);

				mobileDataTable($('.main-data-table'));
				
                hideLoader('body');
			}
		});

		$('#modal-banner-text').on('show.bs.modal', function (e) {
			var $opener = $(e.relatedTarget);
			banner_key_index = $opener.data('index');
			
			var $textarea = $('body').find('textarea#js-banner-text-'+ banner_key_index);
			var text = $textarea.val();

			tinyMCE.get('js-banner-text-editor').setContent(text);
		}).on('hide.bs.modal', function (e) {
			banner_key_index = null;
			tinyMCE.get('js-banner-text-editor').setContent('');
		});
	});

	var set_banner_text = function(element){
		var $this = $(element);
		var $textarea = $('body').find('textarea#js-banner-text-'+ banner_key_index);
		$textarea.val(tinyMCE.get('js-banner-text-editor').getContent());
		$('#modal-banner-text').modal('hide');
	}

	var change_status = function(btn){
		var $this = $(btn);
		var banner = $this.data('banner');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/banners/ajax_operations/change_status',
			data: {banner:banner},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
	}

	var delete_action = function(btn){
		var $this = $(btn);
		var banner = $this.data('banner');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/banners/ajax_operations/delete',
			data: {banner:banner},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>