<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-widget custom-margin-top-10">
				<div class="card-header pl-2 pl-sm-3 pr-2 pr-sm-3">
					<h3 class="card-title">
						<?php echo $page_header;?>, <small>найдено: <span id="total_dtTable-counter">0</span></small>
					</h3>
					<div class="card-tools">
						<a class="btn btn-tool call-popup" data-popup="#general_popup_form" href="#" data-href="<?php echo base_url('admin/banners/popup/add');?>" tooltip="Добавить баннер"><i class="fas fa-plus"></i> Добавить</a>
					</div>
				</div>
				<div class="card-body pl-2 pl-sm-3 pr-2 pr-sm-3">
					<div class="table-responsive">
						<table class="table table-bordered table-hover main-data-table" id="dtTable" style="width: 100%;">
							<thead>
								<tr>
									<th class="dt_id">#</th>
									<th class="dt_name">Название</th>
									<th class="dt_folder">Папка</th>
									<th class="dt_active">Активен</th>
									<th class="dt_actions"></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="modal fade" data-backdrop="static" id="modal-banner-text">
    <div class="modal-dialog  modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Текст баннера</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <textarea class="description" name="description" id="js-banner-text-editor"></textarea>
                </div>
            </div>
            <div class="modal-footer">
				<button type="button" class="btn btn-default btn-flat btn-sm" data-dismiss="modal">Отмена</button>
				<input type="hidden" name="banner_key_index">
                <button type="button" class="btn btn-success btn-flat btn-sm call-function" data-callback="set_banner_text">Сохранить</button>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->