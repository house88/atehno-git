<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($city) ? 'Редактировать' : 'Добавить';?> город
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="city_name" value="<?php echo !empty($city) ? $city['city_name'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Срок доставки</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="ex. 3" name="city_delivery_days" value="<?php echo !empty($city) ? $city['city_delivery_days'] : '';?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <div class="icheck-primary">
                    <input name="active" type="checkbox" id="js-checkbox-city-active" <?php echo set_checkbox('active', 1, !empty($city) ? (int) $city['city_visible'] === 1 : false);?>>
                    <label for="js-checkbox-city-active">
                        Активный
                    </label>
                </div>
                <div class="d-flex">
                    <?php if(!empty($city)){?>
                        <input type="hidden" name="city" value="<?php echo $city['id_city'];?>">
                    <?php }?>
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/cities/ajax_operations/<?php echo !empty($city) ? 'edit' : 'add';?>',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });
</script>
