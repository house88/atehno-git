<div class="row" style="display:none;" id="import_reports-wr">
	<div class="col-lg-3 col-md-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-3">
						<i class="fa fa-th-list fa-5x"></i>
					</div>
					<div class="col-xs-9 text-right">
						<div class="huge" id="import_report-new_items">0</div>
						<div>Добавлено товаров</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-6">
		<div class="panel panel-green">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-3">
						<i class="fa fa-refresh fa-5x"></i>
					</div>
					<div class="col-xs-9 text-right">
						<div class="huge" id="import_report-updated_items">0</div>
						<div>Обновлено товаров</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-6">
		<div class="panel panel-yellow">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-3">
						<i class="ca-icon ca-icon_tree-view fa-5x"></i>
					</div>
					<div class="col-xs-9 text-right">
						<div class="huge" id="import_report-new_categories">0</div>
						<div>Добавлено категорий</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-6">
		<div class="panel panel-red">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-3">
						<i class="fa fa-refresh fa-5x"></i>
					</div>
					<div class="col-xs-9 text-right">
						<div class="huge" id="import_report-updated_categories">0</div>
						<div>Обновлено категорий</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-widget custom-margin-top-10">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_header;?></h3>
					<div class="card-tools">
						<a class="btn btn-tool call-popup" data-popup="#general_popup_form" href="#" data-href="<?php echo base_url('admin/import/popup/add');?>" tooltip="Добавить вариант импорта"><i class="fas fa-plus"></i> Добавить</a>
					</div>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-hover main-data-table" id="dtTable" style="width: 100%;">
							<thead>
								<tr>
									<th class="dt_id text-center">#</th>
									<th class="dt_name">Название</th>
									<th class="dt_columns">Поля</th>
									<th class="dt_active">Активная</th>
									<th class="dt_cron">Cron</th>
									<th class="text-center dt_actions"></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->
