<?php $this->load->view($this->theme->apanel_view('includes/plugins/datatable'));?>

<script>
	var dtTable; //obj of datatable
	$(function(){
		'use strict';
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/Russian.json'));?>"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/import/ajax_operations/list",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "text-center vam", "aTargets": ["dt_id"], "mData": "dt_id", "bSortable": false},
				{ "sClass": "vam mnw-150", "aTargets": ["dt_name"], "mData": "dt_name", "bSortable": false},
				{ "sClass": "vam", "aTargets": ["dt_columns"], "mData": "dt_columns", "bSortable": false},
				{ "sClass": "text-center vam", "aTargets": ["dt_active"], "mData": "dt_active", "bSortable": false},
				{ "sClass": "text-center vam", "aTargets": ["dt_cron"], "mData": "dt_cron", "bSortable": false},
				{ "sClass": "text-center vam", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }

			],
			"aaSorting" : [[0,'asc']],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, data.mess_type);
						}

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                fixDataTablePagination(this);

            	mobileDataTable($('.main-data-table'));
			}
		});
	});

	var change_status = function(element){
		var $this = $(element);
		var id_import = $this.data('import');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/import/ajax_operations/change_status',
			data: {id_import:id_import},
			dataType: 'JSON',
			beforeSend: function(){
				$this.addClass('disabled');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}

	var change_cron = function(element){
		var $this = $(element);
		var id_import = $this.data('import');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/import/ajax_operations/change_cron',
			data: {id_import:id_import},
			dataType: 'JSON',
			beforeSend: function(){
				$this.addClass('disabled');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}

	var delete_action = function(btn){
		var $this = $(btn);
		var id_import = $this.data('import');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/import/ajax_operations/delete',
			data: {id_import:id_import},
			dataType: 'JSON',
			beforeSend: function(){
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>