<div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($import) ? 'Редактировать' : 'Добавить';?> вариант импорта
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="title" value="<?php echo !empty($import) ? $import['import_title'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="form-group clearfix">
                            <label>Формат данных</label>
                            <select name="import_format" class="form-control form-control-sm rounded-0">
                                <option value="">Выберите фотмат</option>
                                <option value="xml" <?php echo  set_select('import_format', 'xml', (!empty($import) ? $import['import_format'] === 'xml' : false));?>>XML</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="form-group clearfix">
                            <label>Перед импортом</label>
                            <select name="before_action" class="form-control form-control-sm rounded-0">
                                <option value="nothing"  <?php echo  set_select('before_action', 'nothing', (!empty($import) ? $import['import_before_action'] == 'nothing' : false));?>>Ничего не трогать</option>
                                <option value="reset_quantity"  <?php echo  set_select('before_action', 'reset_quantity', (!empty($import) ? $import['import_before_action'] == 'reset_quantity' : false));?>>Обнулить у всех товаров их количество на складе</option>
                                <option value="delete_all_items"  <?php echo  set_select('before_action', 'delete_all_items', (!empty($import) ? $import['import_before_action'] == 'delete_all_items' : false));?>>Удалить все товары из каталога</option>
                                <option value="hide_all"  <?php echo  set_select('before_action', 'hide_all', (!empty($import) ? $import['import_before_action'] == 'hide_all' : false));?>>Скрыть все товары от показа на сайте</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <p>Поля отмеченные <span class="text-danger">*</span> обязательны.</p>
                    <?php 
                        $import_columns = !empty($import) ? json_decode($import['import_columns'], true) : array();
                        $import_columns_groups = array(
                            'items' => array(
                                'name' => 'Поля для товаров',
                                'columns' => array(
                                    'item_prog_id' => array(
                                        'name' => 'item_prog_id',
                                        'label' => 'Программный ИД товара',
                                        'required' => true
                                    ),
                                    'category_prog_id' => array(
                                        'name' => 'category_prog_id',
                                        'label' => 'Программный ИД категорий',
                                        'required' => true
                                    ),
                                    'item_title' => array(
                                        'name' => 'item_title',
                                        'label' => 'Название товара',
                                        'required' => true
                                    ),
                                    'item_price' => array(
                                        'name' => 'item_price',
                                        'label' => 'Цена товара',
                                        'required' => true,
                                        'additional' => array_merge(
                                            array(array(
                                                'name' => 'item_temp_price',
                                                'label' => 'Акционная цена товара',
                                                'required' => false
                                            )),
                                            array_map(function($price_variant){
                                                return array(
                                                    'name' => "item_price_{$price_variant['id_price_variant']}",
                                                    'label' => "Цена товара - Групы {$price_variant['price_variant_title']}",
                                                    'required' => false
                                                );
                                            }, $price_variants)
                                        )
                                    ),
                                    'item_cashback_price' => array(
                                        'name' => 'item_cashback_price',
                                        'label' => 'Бонусные баллы',
                                        'required' => false
                                    ),
                                    'item_zero_credit_months' => array(
                                        'name' => 'item_zero_credit_months',
                                        'label' => 'Кредит 0%, кол. месяцев',
                                        'required' => false
                                    ),
                                    'item_discount' => array(
                                        'name' => 'item_discount',
                                        'label' => 'Скидка для главной цены',
                                        'required' => false
                                    ),
                                    'item_quantity' => array(
                                        'name' => 'item_quantity',
                                        'label' => 'Количество',
                                        'required' => false
                                    ),
                                    'suppliers' => array(
                                        'name' => 'suppliers',
                                        'label' => 'Остатки по складам',
                                        'required' => false
                                    ),
                                    'stocks' => array(
                                        'name' => 'stocks',
                                        'label' => 'Остатки по поставщикам',
                                        'required' => false
                                    ),
                                    'item_code' => array(
                                        'name' => 'item_code',
                                        'label' => 'Артикул',
                                        'required' => false
                                    ),
                                    'item_guaranty' => array(
                                        'name' => 'item_guaranty',
                                        'label' => 'Гарантия',
                                        'required' => false
                                    ),
                                    'item_small_description' => array(
                                        'name' => 'item_small_description',
                                        'label' => 'Краткое описание',
                                        'required' => false
                                    ),
                                    'item_description' => array(
                                        'name' => 'item_description',
                                        'label' => 'Полное описание',
                                        'required' => false
                                    ),
                                    'item_visible' => array(
                                        'name' => 'item_visible',
                                        'label' => 'Признак <span class="badge badge-warning">Активный</span>',
                                        'required' => false
                                    ),
                                    'item_hit' => array(
                                        'name' => 'item_hit',
                                        'label' => 'Признак <span class="badge badge-warning">Хит продаж</span>',
                                        'required' => false
                                    ),
                                    'item_newest' => array(
                                        'name' => 'item_newest',
                                        'label' => 'Признак <span class="badge badge-warning">Новинка</span>',
                                        'required' => false
                                    ),
                                    'item_action' => array(
                                        'name' => 'item_action',
                                        'label' => 'Признак <span class="badge badge-warning">Акция</span>',
                                        'required' => false
                                    ),
                                    'mk' => array(
                                        'name' => 'mk',
                                        'label' => 'Meta keywords',
                                        'required' => false
                                    ),
                                    'md' => array(
                                        'name' => 'md',
                                        'label' => 'Meta description',
                                        'required' => false
                                    )
                                )
                            ),
                            'categories' => array(
                                'name' => 'Поля для категорий', 
                                'columns' => array(
                                    'category_prog_id' => array(
                                        'name' => 'category_prog_id',
                                        'label' => 'Программный ИД категорий',
                                        'required' => true
                                    ),
                                    'category_prog_parent_id' => array(
                                        'name' => 'category_prog_parent_id',
                                        'label' => 'Программный ИД родителя категорий',
                                        'required' => true
                                    ),
                                    'category_title' => array(
                                        'name' => 'category_title',
                                        'label' => 'Название категорий',
                                        'required' => true
                                    ),
                                    'category_seo' => array(
                                        'name' => 'category_seo',
                                        'label' => 'Полное описание',
                                        'required' => false
                                    ),
                                    'mk' => array(
                                        'name' => 'mk',
                                        'label' => 'Meta keywords',
                                        'required' => false
                                    ),
                                    'md' => array(
                                        'name' => 'md',
                                        'label' => 'Meta description',
                                        'required' => false
                                    )
                                )
                            ),
                            'stocks' => array(
                                'name' => 'Поля для складов', 
                                'columns' => array(
                                    'stock_prog_id' => array(
                                        'name' => 'stock_prog_id',
                                        'label' => 'Программный ИД склада',
                                        'required' => true
                                    ),
                                    'stock_name' => array(
                                        'name' => 'stock_name',
                                        'label' => 'Название склада',
                                        'required' => true
                                    )
                                )
                            ),
                            'suppliers' => array(
                                'name' => 'Поля для поставщиков', 
                                'columns' => array(
                                    'supplier_prog_id' => array(
                                        'name' => 'supplier_prog_id',
                                        'label' => 'Программный ИД поставщика',
                                        'required' => true
                                    ),
                                    'supplier_name' => array(
                                        'name' => 'supplier_name',
                                        'label' => 'Название поставщика',
                                        'required' => true
                                    )
                                )
                            )
                        );
                    ?>
                    <?php foreach($import_columns_groups as $group_key => $import_columns_group){?>
                        <div class="card card-default">
                            <div class="card-header">
                                <h6 class="mb-0"><?php echo $import_columns_group['name'];?></h6>
                            </div>
                            <div class="card-body" style="zoom:0.825;">
                                <div class="row">
                                    <?php foreach($import_columns_group['columns'] as $column){?>
                                        <div class="col-12 col-sm-6 col-md-4 col-xl-3">
                                            <div class="icheck-primary">
                                                <input type="checkbox" id="input-<?php echo $group_key . '_' . $column['name'];?>" name="import_columns[<?php echo $group_key;?>][]" value="<?php echo $column['name'];?>" <?php if(isset($import_columns[$group_key])){echo set_checkbox("import_columns[{$group_key}]", $column['name'], in_array($column['name'], $import_columns[$group_key]));}?>>
                                                <label for="input-<?php echo $group_key . '_' . $column['name'];?>">
                                                    <?php if($column['required'] === true){?>
                                                        <span class="text-danger">*</span>
                                                    <?php }?>
                                                    <?php echo $column['label'];?>
                                                </label>
                                            </div>
                                        </div>
                                        
                                        <?php if(!empty($column['additional'])){?>
                                            <?php foreach($column['additional'] as $additional){?>
                                                <div class="col-12 col-sm-6 col-md-4 col-xl-3">
                                                    <div class="icheck-primary">
                                                        <input type="checkbox" id="input-<?php echo $group_key . '_' . $column['name'] . '_' . $additional['name'];?>" name="import_columns[<?php echo $group_key;?>][]" value="<?php echo $additional['name'];?>" <?php if(isset($import_columns[$group_key])){echo set_checkbox("import_columns[{$group_key}]", $additional['name'], in_array($additional['name'], $import_columns[$group_key]));}?>>
                                                        <label for="input-<?php echo $group_key . '_' . $column['name'] . '_' . $additional['name'];?>">
                                                            <?php if($additional['required'] === true){?>
                                                                <span class="text-danger">*</span>
                                                            <?php }?>
                                                            <?php echo $additional['label'];?>
                                                        </label>
                                                    </div>
                                                </div>                                        
                                            <?php }?>
                                        <?php }?>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-right">
                <?php if(!empty($import)){?>
                    <input type="hidden" name="id_import" value="<?php echo $import['id_import'];?>">
                <?php }?>
                <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Закрыть</button>
            </div>
        </form>
    </div>
</div>

<script>
    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/import/ajax_operations/<?php echo !empty($import) ? 'edit' : 'add';?>',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });
</script>
