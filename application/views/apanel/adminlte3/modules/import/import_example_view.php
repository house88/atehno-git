<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Пример файла для импорта</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
<pre lang="xml">
<?php 
	$columns = json_decode($import['import_columns'], true);
	$xw = xmlwriter_open_memory();
	xmlwriter_set_indent($xw, 1);
	$res = xmlwriter_set_indent_string($xw, '  ');
	xmlwriter_start_document($xw, '1.0', 'UTF-8');
	xmlwriter_start_element($xw, 'imports');

	if(!empty($columns['items'])){
		xmlwriter_start_element($xw, 'items');
		foreach( $columns['items'] as $column ){
			xmlwriter_start_element($xw, $column);
			xmlwriter_text($xw, '{{VALUE}}');
			xmlwriter_end_element($xw);
		}
		xmlwriter_end_element($xw);
	}

	if(!empty($columns['categories'])){
		xmlwriter_start_element($xw, 'categories');
		foreach( $columns['categories'] as $column ){
			xmlwriter_start_element($xw, $column);
			xmlwriter_text($xw, '{{VALUE}}');
			xmlwriter_end_element($xw);
		}
		xmlwriter_end_element($xw);
	}

	if(!empty($columns['stocks'])){
		xmlwriter_start_element($xw, 'stocks');
		foreach( $columns['stocks'] as $column ){
			xmlwriter_start_element($xw, $column);
			xmlwriter_text($xw, '{{VALUE}}');
			xmlwriter_end_element($xw);
		}
		xmlwriter_end_element($xw);
	}

	if(!empty($columns['suppliers'])){
		xmlwriter_start_element($xw, 'suppliers');
		foreach( $columns['suppliers'] as $column ){
			xmlwriter_start_element($xw, $column);
			xmlwriter_text($xw, '{{VALUE}}');
			xmlwriter_end_element($xw);
		}
		xmlwriter_end_element($xw);
	}

	xmlwriter_end_element($xw);
	xmlwriter_end_document($xw);

	echo htmlspecialchars(trim(xmlwriter_output_memory($xw)));
?>
</pre>
        </div>
        <div class="modal-footer justify-content-right">
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>