<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-xs-12">
				<h1 class="m-0 text-dark"><?php echo $page_header;?></h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-lg">
				<div class="card shadow-lg bg-white card-primary card-outline card-outline-tabs">
					<div class="card-header p-0 border-0">
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" data-toggle="pill" href="#custom-tabs-home_page" role="tab" aria-controls="custom-tabs-home_page" aria-selected="true">Главная страница</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="pill" href="#custom-tabs-sms" role="tab" aria-controls="custom-tabs-sms" aria-selected="false">СМС уведомления</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="pill" href="#custom-tabs-settings" role="tab" aria-controls="custom-tabs-settings" aria-selected="false">Другие настройки</a>
							</li>
						</ul>
					</div>
					<div class="card-body">
						<div class="tab-content" id="custom-tabs-three-tabContent">
							<div class="tab-pane fade show active" id="custom-tabs-home_page" role="tabpanel" aria-labelledby="home_page-tab">
								<?php $recommended_blocks = json_decode($home_settings['recomended_products']['setting_data']);?>
								<div class="row">
									<div class="col-12 col-md-6">
										<form id="recomended_blocks">
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Блок Рекомендуем</h3>
												</div>
												<!-- /.card-header -->
												<div class="card-body" style="display: block;">
													<div class="form-group">
														<label>Количество товаров в блоке</label>
														<input class="form-control form-control-sm rounded-0" placeholder="ex. 4" name="recomended_blocks_limit" value="<?php echo $recommended_blocks->limit;?>">
													</div>
													<div class="form-group">
														<div class="icheck-primary">
															<input name="recomended_blocks_active" type="checkbox" id="js-checkbox-recomended-block-active" <?php echo set_checkbox('recomended_blocks_active', 1, $home_settings['recomended_products']['setting_active'] == 1);?>>
															<label for="js-checkbox-recomended-block-active">
																Активный
															</label>
														</div>
													</div>
													
													<div class="panel_blocks">
														<?php if(!empty($recommended_blocks->blocks)){?>
															<?php foreach($recommended_blocks->blocks as $recommended_block_key => $recommended_block){?>														
																<div class="input-group input-group-sm custom-margin-top-3 js-recomended-block-<?php echo $recommended_block_key;?>">
																	<input type="text" class="form-control rounded-0" name="recomended_blocks[<?php echo $recommended_block_key;?>][block_name]" value="<?php echo $recommended_block->block_name;?>">
																	<input type="hidden" name="recomended_blocks[<?php echo $recommended_block_key;?>][block_categories]" value="<?php echo $recommended_block->block_categories;?>">
																	<span class="input-group-append">
																		<button type="button" class="btn btn-danger btn-flat call-function" data-callback="delete_recomended_block" title="Удалить">
																			<i class="fas fa-trash-alt"></i>
																		</button>
																		<button type="button" class="btn btn-default btn-flat" title="Категорий" data-index="<?php echo $recommended_block_key;?>" data-toggle="modal" data-target="#modal-recomended-block-categories">
																			<i class="fas fa-folder-tree"></i>
																		</button>
																	</span>
																</div>
															<?php }?>
														<?php }?>
													</div>
												</div>
												<!-- /.card-body -->
												<div class="card-footer d-flex justify-content-between">
													<button class="btn btn-default btn-flat btn-sm call-function" data-callback="add_recomended_block"><i class="ca-icon ca-icon_plus"></i> Добавить блок</button>
													<button class="btn btn-success btn-flat btn-sm call-function" data-callback="save_recomended_block"><i class="ca-icon ca-icon_ok"></i> Сохранить</button>
												</div>
											</div>
										</form>
										<div class="modal fade" data-backdrop="static" id="modal-recomended-block-categories">
											<div class="modal-dialog  modal-dialog-centered">
												<div class="modal-content">
													<div class="modal-header">
														<h4 class="modal-title">Категории</h4>
													</div>
													<div class="modal-body">
														<div class="form-group">
															<label>Выберите категории</label>
															<select name="block_categories" class="form-control bs-select-element" data-style="btn btn-default btn-sm btn-flat" data-live-search="true" data-show-content="false" multiple>
																<option value="0">Выберите категорию</option>
																<?php if(!empty($categories)){?>
																	<?php echo $categories;?>
																<?php }?>
															</select>
														</div>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default btn-flat btn-sm" data-dismiss="modal">Отмена</button>
														<button type="button" class="btn btn-primary btn-flat btn-sm call-function" data-callback="set_recomended_block_categories">Сохранить</button>
													</div>
												</div>
											<!-- /.modal-content -->
											</div>
											<!-- /.modal-dialog -->
										</div>
										<!-- /.modal -->
									</div>
									<div class="col-12 col-md-6">
										<?php $popular_block = json_decode($home_settings['popular_products']['setting_data']);?>
										<form id="popular_block">
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Блок Популярные товары</h3>
												</div>
												<!-- /.card-header -->
												<div class="card-body" style="display: block;">
													<div class="form-group">
														<label>Название блока</label>
														<input class="form-control form-control-sm rounded-0" placeholder="ex. Популярные товары" name="block_name" value="<?php echo $popular_block->block_name;?>">
													</div>
													<div class="form-group">
														<label>Количество товаров в блоке</label>
														<input class="form-control form-control-sm rounded-0" placeholder="ex. 4" name="popular_block_limit" value="<?php echo $popular_block->limit;?>">
													</div>
												</div>
												<!-- /.card-body -->
												<div class="card-footer d-flex justify-content-between">
													<div class="icheck-primary">
														<input name="popular_block_active" type="checkbox" id="js-checkbox-popular-block-active" <?php echo set_checkbox('popular_block_active', 1, $home_settings['popular_products']['setting_active'] == 1);?>>
														<label for="js-checkbox-popular-block-active">
															Активный
														</label>
													</div>
													<button class="btn btn-success btn-flat btn-sm float-right call-function" data-callback="save_popular_block"><i class="fad fa-check"></i> Сохранить</button>
												</div>
											</div>											
										</form>

										<?php $newest_block = json_decode($home_settings['newest_products']['setting_data']);?>
										<form id="newest_block">
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Блок Новинки</h3>
												</div>
												<!-- /.card-header -->
												<div class="card-body" style="display: block;">
													<div class="form-group">
														<label>Название блока</label>
														<input class="form-control form-control-sm rounded-0" placeholder="ex. Популярные товары" name="block_name" value="<?php echo $newest_block->block_name;?>">
													</div>
													<div class="form-group">
														<label>Количество товаров в блоке</label>
														<input class="form-control form-control-sm rounded-0" placeholder="ex. 4" name="newest_block_limit" value="<?php echo $newest_block->limit;?>">
													</div>
												</div>
												<!-- /.card-body -->
												<div class="card-footer d-flex justify-content-between">
													<div class="icheck-primary">
														<input name="newest_block_active" type="checkbox" id="js-checkbox-newest-block-active" <?php echo set_checkbox('newest_block_active', 1, $home_settings['newest_products']['setting_active'] == 1);?>>
														<label for="js-checkbox-newest-block-active">
															Активный
														</label>
													</div>
													<button class="btn btn-success btn-flat btn-sm float-right call-function" data-callback="save_newest_block"><i class="fad fa-check"></i> Сохранить</button>
												</div>
											</div>
										</form>	
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="custom-tabs-sms" role="tabpanel" aria-labelledby="sms-tab">
								<?php $smsNotificationsSettings = Modules::run('sms/_getConfigs');?>
								<div class="row">
									<div class="col-12 col-md-6">
										<form id="js-sms-settings-form">
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Настройки СМС уведомлений</h3>
												</div>
												<div class="card-body">
													<div class="form-group">
														<div class="icheck-primary">
															<input name="is_active" type="checkbox" id="js-checkbox-sms-notification-settings-enabled" <?php echo get_choice('checked', 1 === @$smsNotificationsSettings['is_active']);?>>
															<label for="js-checkbox-sms-notification-settings-enabled">
																Отправлять Смс уведомлении
															</label>
														</div>
													</div>
													<div class="form-group">
														<label>Группы пользователей</label>
														<?php $smsUsersGroups = Modules::run('users/_getGroups');?>
														<select name="sms_groups_of_users[]" class="form-control form-control-sm rounded-0 js-multiple-select-sms-users-groups" data-style="btn btn-default btn-sm btn-flat" multiple>
															<option value="">Выберите группы</option>
															<option value="-1" <?php echo get_choice('selected', in_array(-1, $smsNotificationsSettings['groups_of_users']??[]));?>>Не авторизованные пользователи</option>
															<?php foreach($smsUsersGroups as $smsUserGroup){?>
																<option value="<?php echo $smsUserGroup['id_group'];?>" <?php echo get_choice('selected', in_array($smsUserGroup['id_group'], $smsNotificationsSettings['groups_of_users']??[]));?>><?php echo $smsUserGroup['group_name'];?></option>
															<?php }?>
														</select>
													</div>
												</div>
												<div class="card-footer d-flex justify-content-end">
													<button class="btn btn-success btn-flat btn-sm float-right call-function" data-callback="sms_settings_update"><i class="fad fa-check"></i> Сохранить</button>
												</div>
											</div>											
										</form>
									</div>
									<div class="col-12 col-md-6">
										<form id="js-sms-bulk-send-form">
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Отправка СМС уведомлений</h3>
												</div>
												<div class="card-body">
													<div class="form-group">
														<label>Получатели</label>
														<textarea name="phone_numbers" rows="10" class="form-control form-control-sm rounded-0"></textarea>
													</div>
													<div class="form-group">
														<label>Шаблон сообшения</label>
														<select name="id_template" id="js-select-sms-send-bulk-template" class="form-control form-control-sm rounded-0">
															<option value="">Обшее сообшение</option>
															<?php $smsTemplates = Modules::run('sms/_getAll');?>
															<?php foreach($smsTemplates as $smsTemplate){?>
																<option value="<?php echo $smsTemplate['id'];?>"><?php echo $smsTemplate['title'];?></option>
															<?php }?>
														</select>
													</div>
													<div class="form-group">
														<label>Текст сообшения</label>
														<textarea name="message" rows="3" id="js-sms-send-bulk-message" class="form-control form-control-sm rounded-0"></textarea>
													</div>
												</div>
												<div class="card-footer d-flex justify-content-end">
													<button class="btn btn-success btn-flat btn-sm float-right call-function" data-callback="smsSendBulk"><i class="fad fa-check"></i> Отправить</button>
												</div>
											</div>											
										</form>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="custom-tabs-settings" role="tabpanel" aria-labelledby="settings-tab">
								<div class="row">
									<div class="col-12">
										<form class="form-horizontal" id="settings_form">
											<div class="card card-primary">
												<div class="card-body">
													<?php foreach($settings as $setting){?>
														<div class="form-group row">
															<label class="col-sm-4 col-form-label"><?php echo $setting['setting_title'];?></label>
															<div class="col-sm-8">
																<input type="text" class="form-control form-control-sm rounded-0" name="settings[<?php echo $setting['setting_alias'];?>]" value="<?php echo $setting['setting_value'];?>">
															</div>
														</div>
													<?php }?>
												</div>
												<!-- /.card-body -->
												<div class="card-footer d-flex justify-content-end">
													<button class="btn btn-success btn-flat btn-sm" type="submit"><i class="fad fa-check"></i> Сохранить</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.card -->
				</div>
			</div>
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</section>