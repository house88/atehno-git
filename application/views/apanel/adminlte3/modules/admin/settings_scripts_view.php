
<script type="text/template" id="js-template-add-recomended-block">    
    <div class="input-group input-group-sm custom-margin-top-3 js-recomended-block-{{index}}">
        <input type="text" class="form-control rounded-0" name="recomended_blocks[{{index}}][block_name]">
        <input type="hidden" name="recomended_blocks[{{index}}][block_categories]">
        <span class="input-group-append">
            <button type="button" class="btn btn-danger btn-flat call-function" data-callback="delete_recomended_block" title="Удалить">
                <i class="fas fa-trash-alt"></i>
            </button>
            <button type="button" class="btn btn-default btn-flat" title="Категорий" data-index="{{index}}" data-toggle="modal" data-target="#modal-recomended-block-categories">
                <i class="fas fa-folder-tree"></i>
            </button>
        </span>
    </div>
</script>
<script>
    var recomended_block_index = null;
    $(function(){
        $('#modal-recomended-block-categories').on('show.bs.modal', function (e) {
            var $this = $(e.relatedTarget);
            recomended_block_index = $this.data('index');

            var $categories = $('select[name="block_categories"]');
            var $categories_list = $this.closest('.input-group').find('input[name="recomended_blocks[' + recomended_block_index + '][block_categories]"]').val().split(',');
            $categories.find("option:selected").prop("selected", false);

            if($categories_list !== null && $categories_list.length){
                var total_selected = 0;
                $.each($categories_list, function( intIndex, objValue ){
                    $categories.find('option[value="' + objValue + '"]').prop('selected', true);
                    total_selected++;

                    if(total_selected === $categories_list.length){
                        $(".bs-select-element").selectpicker('refresh');
                    }
                    
                });
            }
        }).on('hide.bs.modal', function (e) {
            recomended_block_index = null;
            $(".bs-select-element").selectpicker('deselectAll');
        });

		$(".js-multiple-select-sms-users-groups").selectpicker();
		$("#js-select-sms-send-bulk-template").on('change', function(){
			var $this = $(this);
			var option = $this.val();
			console.log(option);
			if (option === '') {
				$("#js-sms-send-bulk-message").prop('disabled', false);
			} else{
				$("#js-sms-send-bulk-message").prop('disabled', true);
			}
		});
    });

	var add_recomended_block = function(target){
		var $this = $(target);
        var template = $('#js-template-add-recomended-block').text();
        
        var content = {
			index: uniqid()
		};

		for (var key in content) {
			if (content.hasOwnProperty(key)) {
				template = template.replace(new RegExp('{{' + key + '}}', 'g'), content[key])
			}
        }
        
		$this.closest('#recomended_blocks').find('.panel_blocks').append(template);
	}

    var set_recomended_block_categories = function(target){
        var $this = $(target);
        var $categories = $('select[name="block_categories"]');
        var categories_list = $categories.val();
        if(recomended_block_index !== null && categories_list !== null && categories_list.length){
            $('body').find('.js-recomended-block-' + recomended_block_index + ' input[name="recomended_blocks[' + recomended_block_index + '][block_categories]"]').val(categories_list.join());
            $categories.find("option:selected").prop("selected", false);
            $(".bs-select-element").selectpicker('refresh');

            systemMessages('Выбранные категории добавлены!', 'success');
            $('#modal-recomended-block-categories').modal('hide');
        } else{
            systemMessages('Не выбраны категории!', 'error');
        }
    }

	var delete_recomended_block = function(target){
		var $this = $(target);
		$this.closest('.input-group').remove();
    }

	var save_recomended_block = function(btn){
		var $this = $(btn);
		var $form = $this.closest('form#recomended_blocks');
		var fdata = $form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/ajax_operations/save_recomended_block',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	}

	var save_popular_block = function(btn){
		var $this = $(btn);
		var $form = $this.closest('form#popular_block');
		var fdata = $form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/ajax_operations/save_popular_block',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	}

	var save_newest_block = function(btn){
		var $this = $(btn);
		var $form = $this.closest('form#newest_block');
		var fdata = $form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/ajax_operations/save_newest_block',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	}

	var sms_settings_update = function(btn){
		var $this = $(btn);
		var $form = $this.closest('form#js-sms-settings-form');
		var fdata = $form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/sms/ajax_operations/settings',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	}

	var smsSendBulk = function(btn){
		var $this = $(btn);
		var $form = $this.closest('form#js-sms-bulk-send-form');
		var fdata = $form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/sms/ajax_operations/smsSendBulk',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				$form[0].reset();
			}
		});
		return false;
	}

	var settings_form = $('#settings_form');
	settings_form.submit(function () {
		var fdata = settings_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/ajax_operations/save_settings',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	});
</script>