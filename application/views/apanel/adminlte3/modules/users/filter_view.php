<div class="form-group">
    <div class="input-group input-group-sm">
        <span class="input-group-prepend">
            <button type="button" class="btn btn-default btn-flat"><i class="fad fa-search"></i></button>
        </span>
        <input class="form-control rounded-0 dt_filter" type="text" name="keywords" data-title="Поиск" placeholder="Поиск">
    </div>
</div>
<div class="form-group">
    <label class="text-gray custom-font-size-12">Дата регистраций</label>
    <div class="input-group input-group-sm">
        <div class="input-group-prepend rounded-0">
            <span class="input-group-text"><i class="fad fa-calendar-week"></i></span>
        </div>
        <input type="text" name="registered_on" class="form-control float-right rounded-0 dt_filter" data-title="Дата регистраций" placeholder="ex. 21/01/2020 - 31/01/2020" id="js-user-registered-date">
    </div>
</div>