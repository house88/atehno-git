<?php $this->load->view($this->theme->apanel_view('includes/plugins/datatable'));?>

<script>
	var dtTable;
	$(function(){
		'use strict';
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/Russian.json'));?>"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/group_rights/ajax_operations/list",
			"sServerMethod": "POST",
			"iDisplayLength": 1000,
			"aoColumnDefs": [
				{ "sClass": "vam custom-width-200", "aTargets": ["dt_name"], "mData": "dt_name", "bSortable": false},
				<?php foreach($groups as $group){?>
					{ "sClass": "vam text-center", "aTargets": ["dt_<?php echo $group['id_group'];?>"], "mData": "dt_<?php echo $group['id_group'];?>", "bSortable": false},
				<?php }?>
			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					beforeSend: function(){
						showLoader('body');
					},
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, data.mess_type);
							
						}
						
						$('#total_dtTable-counter').text(data.iTotalDisplayRecords);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                fixDataTablePagination(this);

				mobileDataTable($('.main-data-table'));
				
                hideLoader('body');
			}
		});
	});

	var change_status = function(element){
		var $this = $(element);
		var group = $this.data('group');
		var right = $this.data('right');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/group_rights/ajax_operations/change_status',
			data: {group:group, right:right},
			dataType: 'JSON',
			beforeSend: function(){
				showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
	}
</script>