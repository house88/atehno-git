<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-widget custom-margin-top-10">
				<div class="card-header">
					<h3 class="card-title">
						<?php echo $page_header;?>, <small>найдено: <span id="total_dtTable-counter">0</span></small>
					</h3>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-hover main-data-table" id="dtTable" style="width: 100%;">
							<thead>
								<tr>
									<th class="dt_name"></th>									
									<?php foreach($groups as $group){?>
										<th class="dt_<?php echo $group['id_group'];?>"><?php echo $group['group_name'];?></th>
									<?php }?>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
