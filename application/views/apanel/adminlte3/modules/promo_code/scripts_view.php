<?php $this->load->view($this->theme->apanel_view('includes/plugins/datatable'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/daterangepicker'));?>

<script>
	var dtTable;
	$(function(){
		'use strict';
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/Russian.json'));?>"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/promo_code/ajax_operations/list",
			"sServerMethod": "POST",
			"iDisplayLength": 50,
			"aoColumnDefs": [
				{ "sClass": "vam text-left", "aTargets": ["dt_name"], "mData": "dt_name", "bSortable": false},
				{ "sClass": "vam text-center custom-width-150", "aTargets": ["dt_start"], "mData": "dt_start"},
				{ "sClass": "vam text-center custom-width-150", "aTargets": ["dt_end"], "mData": "dt_end"},
				{ "sClass": "vam text-center custom-width-50", "aTargets": ["dt_active"], "mData": "dt_active", "bSortable": false},
				{ "sClass": "vam text-center custom-width-50", "aTargets": ["dt_actions"], "mData": "dt_actions", "bSortable": false}
			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					beforeSend: function(){
						showLoader('body');
					},
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, data.mess_type);
							
						}
						
						$('#total_dtTable-counter').text(data.iTotalDisplayRecords);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                fixDataTablePagination(this);

				mobileDataTable($('.main-data-table'));
				
                hideLoader('body');
			}
		});
	});
	
	var change_status = function(btn){
		var $this = $(btn);
		var pcode = $this.data('pcode');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/promo_code/ajax_operations/change_status',
			data: {pcode:pcode},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}

	var delete_action = function(btn){
		var $this = $(btn);
		var pcode = $this.data('pcode');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/promo_code/ajax_operations/delete',
			data: {pcode:pcode},
			dataType: 'JSON',
			beforeSend: function(){
				showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>