<div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php if(!empty($pcode)){?>Редактировать<?php } else{?>Добавить<?php }?> промо код
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="title" value="<?php echo !empty($pcode) ? $pcode['pcode_name'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <label>Товары</label>
                        <select name="items_type" class="form-control form-control-sm rounded-0">
                            <option value="all" <?php echo set_select('items_type', 'all', !empty($pcode) && $pcode['pcode_items_type'] == 'all');?>>Все</option>
                            <option value="not_actional" <?php echo set_select('items_type', 'not_actional', !empty($pcode) && $pcode['pcode_items_type'] == 'not_actional');?>>Исключить Акционные</option>
                            <option value="not_newest" <?php echo set_select('items_type', 'not_newest', !empty($pcode) && $pcode['pcode_items_type'] == 'not_newest');?>>Исключить Новинки</option>
                        </select>
                    </div>                    
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="form-group">
                            <label>Тип</label>
                            <select name="pcode_type" class="form-control form-control-sm rounded-0">
                                <option value="multiple" <?php echo set_select('pcode_type', 'multiple', !empty($pcode) && $pcode['pcode_type'] == 'multiple');?>>Многоразовый</option>
                                <option value="once" <?php echo set_select('pcode_type', 'once', !empty($pcode) && $pcode['pcode_type'] == 'once');?>>Одноразовый</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="form-group">
                            <label>Дата</label>
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend rounded-0">
                                    <span class="input-group-text"><i class="fad fa-calendar-week"></i></span>
                                </div>
                                <input type="text" name="pcode_date" class="form-control float-right rounded-0 dt_filter" placeholder="ex. 21/01/2020 - 31/01/2020" id="js-pcode-active-date" value="<?php echo !empty($pcode) ? getDateFormat($pcode['pcode_date_start'], 'Y-m-d', 'd.m.Y') . ' - ' . getDateFormat($pcode['pcode_date_end'], 'Y-m-d', 'd.m.Y') : '';?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Коды товаров</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="ex. 123,321,234 ..." name="items" value="<?php echo !empty($pcode) ? $pcode['pcode_items'] : '';?>">
                            <p class="custom-margin-bottom-0 custom-font-size-12 text-info">Если оставить пустым то применится ко всем товарам. Разделитель ",".</p>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Коды категорий</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="ex. 123,321,234 ..." name="categories" value="<?php echo !empty($pcode) ? $pcode['pcode_categories'] : '';?>">
                            <p class="custom-margin-bottom-0 custom-font-size-12 text-info">Если оставить пустым то применится ко всем категориям. Разделитель ",".</p>
                        </div>
                    </div>                    
                    <div class="col-12" id="pcode-hashes">
                        <label>Коды</label>
                        <?php if(!empty($pcode_hashes)){?>
                            <?php foreach($pcode_hashes as $hash_info){?>
                                <?php $hash_key = "hash_{$hash_info['id_hash']}";?>
                                <div class="form-group">
                                    <div class="input-group input-group-sm">
                                        <div class="input-group-prepend rounded-0" title="Код">
                                            <span class="input-group-text">Код</span>
                                        </div>
                                        <input class="form-control float-right rounded-0" placeholder="ex. PROMO" name="pcode_hashes[<?php echo $hash_key;?>][hash]" value="<?php echo $hash_info['pcode_hash'];?>">
                                        <div class="input-group-append rounded-0" title="Скидка">
                                            <span class="input-group-text">%</span>
                                        </div>
                                        <input class="form-control float-right rounded-0" placeholder="0" name="pcode_hashes[<?php echo $hash_key;?>][discount]" value="<?php echo $hash_info['pcode_discount'];?>">
                                        <div class="input-group-append rounded-0" title="Количество">
                                            <span class="input-group-text">Кол.</span>
                                        </div>
                                        <input class="form-control float-right rounded-0" placeholder="ex. 10" name="pcode_hashes[<?php echo $hash_key;?>][quantity]" value="<?php echo $hash_info['pcode_once_quantity'];?>">
                                        <div class="input-group-append rounded-0">
                                            <button type="button" class="btn btn-danger btn-flat btn-sm confirm-dialog" data-callback="delete_hash" data-message="Вы уверенны что хотите удалить этот Код?" data-title="<i class='fad fa-trash-alt'></i> Удалить" data-type="bg-danger">
                                                <span class="fad fa-trash"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            <?php }?>
                        <?php }?>
                    </div>
                    <div class="col-12 text-right">
                        <button type="button" class="btn btn-default btn-flat btn-sm mb-15 call-function" data-callback="add_hash">
                            <span class="fad fa-plus"></span> 
                            Добавить код
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <div class="icheck-primary">
                    <input name="visible" type="checkbox" id="js-checkbox-pcode-active" <?php echo set_checkbox('visible', 1, !empty($pcode) && (int) $pcode['pcode_active'] === 1);?>>
                    <label for="js-checkbox-pcode-active">
                        Активный
                    </label>
                </div>
                <div class="d-flex">
                    <?php if(!empty($pcode)){?>
                        <input type="hidden" name="id_pcode" value="<?php echo $pcode['id_pcode'];?>">
                    <?php }?>
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="templates d-none">
	<div data-template="pcode_hash">        
        <div class="form-group">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend rounded-0" title="Код">
                    <span class="input-group-text">Код</span>
                </div>
                <input class="form-control float-right rounded-0" placeholder="ex. PROMO" name="pcode_hashes[{index}][hash]" value="">
                <div class="input-group-append rounded-0" title="Скидка">
                    <span class="input-group-text">%</span>
                </div>
                <input class="form-control float-right rounded-0" placeholder="0" name="pcode_hashes[{index}][discount]" value="">
                <div class="input-group-append rounded-0" title="Количество">
                    <span class="input-group-text">Кол.</span>
                </div>
                <input class="form-control float-right rounded-0" placeholder="ex. 10" name="pcode_hashes[{index}][quantity]" value="">
                <div class="input-group-append rounded-0">
                    <button type="button" class="btn btn-danger btn-flat btn-sm confirm-dialog" data-callback="delete_hash" data-message="Вы уверенны что хотите удалить этот Код?" data-title="<i class='fad fa-trash-alt'></i> Удалить" data-type="bg-danger">
                        <span class="fad fa-trash"></span>
                    </button>
                </div>
            </div>
        </div>
	</div>
</div>

<script>
    $('#js-pcode-active-date').daterangepicker({
        timePicker: false,
        parentEl: '.modal',
        locale: {
            format: 'DD.MM.YYYY'
        },
        autoUpdateInput: false,
        "autoApply": true,
        "opens": "left"
    }).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD.MM.YYYY') + ' - ' + picker.endDate.format('DD.MM.YYYY')).trigger('change');
    }).on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('').trigger('change');
    });

	var delete_hash = function(obj){
		var $this = $(obj);
		$this.closest('.form-group').fadeOut('slow', function(){$(this).remove()});
	}

	var add_hash = function(obj){
		var template = $('div[data-template="pcode_hash"]').html();
		var hash_index = uniqid('hash_');

		template = template.replace(/{index}/g, hash_index);
		$('#pcode-hashes').append(template);
	}

    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/promo_code/ajax_operations/<?php if(!empty($pcode)){?>edit<?php } else{?>add<?php }?>',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });
</script>
