<div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title">Заказ <?php echo orderNumber($order['id_order']);?></h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <?php $order_statuses = get_order_statuses();?>
                <fieldset>
                    <legend>Клиент</legend>
                    <div class="row">
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label class="text-gray custom-font-size-12">Имя</label>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-prepend">
                                        <button type="button" class="btn btn-default btn-flat"><i class="fad fa-user"></i></button>
                                    </span>
                                    <input class="form-control rounded-0" type="text" name="order[user_name]" value="<?php echo $order['order_user_name'];?>">
                                </div>
                            </div>          
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label class="text-gray custom-font-size-12">Еmail</label>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-prepend">
                                        <button type="button" class="btn btn-default btn-flat"><i class="fad fa-envelope"></i></button>
                                    </span>
                                    <input class="form-control rounded-0" type="text" name="order[user_email]" value="<?php echo $order['order_user_email'];?>">
                                </div>
                            </div>          
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label class="text-gray custom-font-size-12">Телефон</label>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-prepend">
                                        <button type="button" class="btn btn-default btn-flat call-function" data-callback="callClient" data-phone="<?php echo $order['order_user_phone'];?>"><i class="fad fa-phone"></i></button>
                                    </span>
                                    <input class="form-control rounded-0" type="text" name="order[user_phone]" value="<?php echo $order['order_user_phone'];?>">
                                </div>
                            </div>          
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label class="text-gray custom-font-size-12">Адрес доставки</label>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-prepend">
                                        <button type="button" class="btn btn-default btn-flat"><i class="fad fa-map-marker-alt"></i></button>
                                    </span>
                                    <input class="form-control rounded-0" type="text" name="order[user_address]" value="<?php echo $order['order_user_address'];?>">
                                </div>
                            </div>          
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <legend>Доставка</legend>

                    <div class="row">
                        <div class="col-12 col-sm-6 col-lg-9">
                            <div class="form-group">
                                <label class="text-gray custom-font-size-12">Метод доставки</label>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-prepend">
                                        <button type="button" class="btn btn-default btn-flat"><i class="fad fa-truck"></i></button>
                                    </span>
                                    <select name="delivery_method" class="form-control rounded-0">
                                        <?php foreach($delivery_options as $delivery_key => $delivery_option){?>
                                            <?php
                                                $delivery_price_options = json_decode($delivery_option['delivery_price_options'], true);

                                                if(!isset($delivery_price_options[$order['order_currency_code']])){
                                                    continue;
                                                }

                                                if(empty($payment_delivery_methods[$order['order_payment_option']]) || !in_array($delivery_option['id_delivery'], $payment_delivery_methods[$order['order_payment_option']])){
                                                    continue;
                                                }

                                                $delivery_price_option = $delivery_price_options[$order['order_currency_code']];
                                                $delivery_price = $order['order_price_final'] >= $delivery_price_option['price_free'] ? 0 : $delivery_price_option['price'];
                                                if(!empty($delivery_price_option['price_for_oversize'])){
                                                    $delivery_price += $delivery_price_option['price_for_oversize'] * $count_oversize_items;
                                                }
                                            ?>
                                            <option 
                                                value="<?php echo $delivery_option['id_delivery'];?>" 
                                                data-price="<?php echo $delivery_price;?>" 
                                                data-payment-available="<?php echo !empty($delivery_payment_methods[$delivery_option['id_delivery']]) ? implode(',', $delivery_payment_methods[$delivery_option['id_delivery']]) : '';?>" 
                                                <?php echo set_select('delivery_method', $delivery_option['id_delivery'], $order['order_delivery_option'] == $delivery_option['id_delivery']);?>>
                                                    <?php echo $delivery_option['delivery_title'];?>  <?php if($delivery_price > 0){?>( +<?php echo numberFormat($delivery_price);?> <?php echo $order['order_currency'];?> )<?php }?>
                                            </option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>          
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label class="text-gray custom-font-size-12">Цена доставки, <?php echo $order['order_currency'];?></label>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-prepend">
                                        <button type="button" class="btn btn-default btn-flat"><i class="fad fa-money-bill-alt"></i></button>
                                    </span>
                                    <input class="form-control rounded-0" type="text" name="delivery_price" value="<?php echo numberFormat($order['order_delivery_price']);?>" placeholder="0.00">
                                </div>
                            </div>          
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <legend>Оплата</legend>
                    <div class="row">
                        <div class="col-12 col-sm-6 col-lg-9">
                            <div class="form-group">
                                <label class="text-gray custom-font-size-12">Метод оплаты</label>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-prepend">
                                        <button type="button" class="btn btn-default btn-flat"><i class="fad fa-credit-card-front"></i></button>
                                    </span>
                                    <select name="payment_method" class="form-control rounded-0">
                                        <?php foreach($payment_options as $payment_option){?>
                                            <?php
                                                if(empty($delivery_payment_methods[$order['order_delivery_option']]) || !in_array($payment_option['id_payment'], $delivery_payment_methods[$order['order_delivery_option']])){
                                                    continue;
                                                }

                                                $payment_add_options = json_decode($payment_option['payment_add_options'], true);
                                                $payment_add_options_percent = (isset($payment_add_options[$order['order_currency']]))?$payment_add_options[$order['order_currency']]:0;
                                            ?>
                                            <option 
                                                value="<?php echo $payment_option['id_payment'];?>" 
                                                data-credit="<?php echo $payment_option['payment_by_credit'];?>" 
                                                data-delivery-available="<?php echo !empty($payment_delivery_methods[$payment_option['id_payment']]) ? implode(',', $payment_delivery_methods[$payment_option['id_payment']]) : '';?>" 
                                                data-payment-add="<?php echo $payment_add_options_percent;?>" 
                                                <?php echo set_select('payment_method', $payment_option['id_payment'], $order['order_payment_option'] == $payment_option['id_payment']);?>>
                                                <?php echo $payment_option['payment_title'];?>
                                                <?php if($payment_add_options_percent > 0){?>( +<?php echo numberFormat($total_price*$payment_add_options_percent/100);?> <?php echo $price['currency_symbol'];?> )<?php }?>
                                            </option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>          
                        </div>
                    </div>
                </fieldset>
                
                <div class="orders_items-wr order_<?php echo $order['id_order'];?>_detail">
                    <?php foreach($ordered_items as $ordered_item){?>
                        <div class="row ordered_item-b custom-margin-bottom-10">
                            <div class="col-12">
                                <div class="ordered-item-container">
                                    <div class="ordered-item-image">
                                        <img src="<?php echo site_url(getImage('files/items/' . $ordered_item['item_photo']))?>" class="img-thumbnail">
                                    </div>
                                    <a class="ordered-item-link" href="<?php echo base_url('products/'.$ordered_item['item_url']);?>"><?php echo clean_output($ordered_item['item_title']);?></a>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row order_<?php echo $order['id_order'];?>_item_<?php echo $ordered_item['id_order_item'];?>">
                                    <div class="col-6 col-md-4 col-lg-2 col-xl-2">
                                        <div class="form-group">
                                            <label class="custom-font-size-10 text-gray custom-margin-bottom-0">ИД / Артикул</label>
                                            <div class="d-flex flex-column custom-font-size-10">
                                                <div class="">
                                                    <?php echo $ordered_item['item_prog_id'];?>
                                                </div>
                                                <div class="">
                                                    <?php echo $ordered_item['item_code'];?> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-4 col-lg-2 col-xl-1">
                                        <div class="form-group">
                                            <label class="custom-font-size-10 text-gray custom-margin-bottom-0">Кол.</label>
                                            <input class="form-control form-control-sm rounded-0 quantity" onkeyup="javascript: change_ordered_item_price(event, <?php echo $order['id_order'];?>,<?php echo $ordered_item['id_order_item'];?>,'quantity');" type="text" name="order[ordered_items][<?php echo $ordered_item['id_order_item'];?>][quantity]" value="<?php echo $ordered_item['item_quantity'];?>" placeholder="1">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-4 col-lg-3 col-xl-1">
                                        <div class="form-group">
                                            <label class="custom-font-size-10 text-gray custom-margin-bottom-0">Цена, <?php echo $ordered_item['item_currency'];?></label>
                                            <div class="form-control form-control-sm rounded-0">
                                                <span class="price" data-default-price="<?php echo $ordered_item['item_price'];?>"><?php echo $ordered_item['item_price'];?></span><?php if($ordered_item['item_has_promo_discount'] == true){?> - <span class="custom-font-size-10"><span class="badge bg-danger">Промо</span><?php }?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-4 col-lg-2 col-xl-1">
                                        <div class="form-group">
                                            <label class="custom-font-size-10 text-gray custom-margin-bottom-0">Скидка, %</label>
                                            <input class="form-control form-control-sm rounded-0 discount" data-default-discount="<?php echo $ordered_item['item_discount'];?>" onkeyup="javascript: change_ordered_item_price(event, <?php echo $order['id_order'];?>,<?php echo $ordered_item['id_order_item'];?>,'discount');" type="text" name="order[ordered_items][<?php echo $ordered_item['id_order_item'];?>][discount]" value="<?php echo $ordered_item['item_discount'];?>" placeholder="0" title="Скидка на товар, выраженная в процентах">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-4 col-lg-2 col-xl-2">
                                        <div class="form-group">
                                            <label class="custom-font-size-10 text-gray custom-margin-bottom-0">Цена со скидкой</label>
                                            <input class="form-control form-control-sm rounded-0 final_price" data-default-final-price="<?php echo $ordered_item['item_price_final'];?>" onkeyup="javascript: change_ordered_item_price(event, <?php echo $order['id_order'];?>,<?php echo $ordered_item['id_order_item'];?>,'final_price');" type="text" name="order[ordered_items][<?php echo $ordered_item['id_order_item'];?>][price_final]" value="<?php echo $ordered_item['item_price_final'];?>" placeholder="0.00">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-4 col-lg-2 col-xl-1">
                                        <div class="form-group">
                                            <label class="custom-font-size-10 text-gray custom-margin-bottom-0">Подарок</label>
                                            <div class="custom-padding-5">
                                                <?php if(isset($ordered_item['item_cashback_price']) && $ordered_item['item_cashback_price'] > 0){?>
                                                    <span class="badge bg-danger"><?php echo $ordered_item['item_cashback_price'];?> <?php echo $ordered_item['item_currency'];?></span>
                                                <?php } else{?>
                                                    &mdash;
                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                                        <div class="form-group">
                                            <label class="custom-font-size-10 text-gray custom-margin-bottom-0">Кол. на складе</label>
                                            <div class="custom-font-size-10 custom-height-min-30 d-flex flex-column justify-content-center">
                                                <?php echo get_choice(implode('', $ordered_item['stocks']), !empty($ordered_item['stocks']), '&mdash;');?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                                        <div class="form-group">
                                            <label class="custom-font-size-10 text-gray custom-margin-bottom-0">Поставшики</label>
                                            <div class="custom-font-size-10 custom-height-min-30 d-flex flex-column justify-content-center">
                                                <?php echo get_choice(implode('', $ordered_item['suppliers']), !empty($ordered_item['suppliers']), '&mdash;');?>
                                            </div>
                                        </div>
                                    </div>                
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <div class="row">
                        <div class="col-12">
                            <hr class="row-delimiter">
                        </div>
                    </div>
                    <div class="ordered_item">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label class="text-gray custom-font-size-12">Скидка к заказу</label>
                                    <div class="input-group input-group-sm">
                                        <input class="form-control rounded-0 order_discount_price"  onkeyup="javascript: calc_order_price(event, <?php echo $order['id_order'];?>, 'by_discount_price');" title="Сумма дополнительной скидки" type="text" name="order[discount_by_currecy]" value="<?php echo $order['order_discount_by_currecy'];?>" placeholder="0.00">
                                        <span class="input-group-append">
                                            <button type="button" class="btn btn-default btn-flat"><?php echo $order['order_currency'];?></button>
                                        </span>
                                        <input class="form-control rounded-0 order_discount_percent"  onkeyup="javascript: calc_order_price(event, <?php echo $order['id_order'];?>, 'by_discount_percent');" title="Сумма дополнительной скидки, выраженная в процентах" type="text" name="order[discount_by_percent]" value="<?php echo $order['order_discount_by_percent'];?>" placeholder="0">
                                        <span class="input-group-append">
                                            <button type="button" class="btn btn-default btn-flat rounded-0">%</button>
                                        </span>
                                    </div>
                                </div>          
                            </div>
                            <div class="col-12 col-sm-6 col-lg-2">
                                <div class="form-group">
                                    <label class="text-gray custom-font-size-12">Товары на сумму</label>
                                    <div class="input-group input-group-sm">
                                        <input class="order_price" type="hidden" name="order[price]" value="<?php echo $order['order_price'];?>" placeholder="">
                                        <input class="form-control rounded-0 order_final_price" onkeyup="javascript: calc_order_price(event, <?php echo $order['id_order'];?>, 'by_order_final_price');" type="text" name="order[price_final]" value="<?php echo $order['order_price_final'];?>" placeholder="">
                                        <span class="input-group-append">
                                            <button type="button" class="btn btn-default btn-flat rounded-0"><?php echo $order['order_currency'];?></button>
                                        </span>
                                    </div>
                                </div>          
                            </div>
                            <div class="col-12 col-sm-6 col-lg-2">
                                <div class="form-group">
                                    <label class="text-gray custom-font-size-12">Доставка</label>
                                    <div class="text-left custom-line-height-31">+ <strong class="delivery_price"><?php echo $order['order_delivery_price'];?></strong> <?php echo $order['order_currency'];?></div>
                                </div>          
                            </div>
                            <div class="col-12 col-sm-6 col-lg-2">
                                <div class="form-group">
                                    <label class="text-gray custom-font-size-12">Сумма заказа</label>
                                    <div class="text-left custom-height-30 d-flex <?php echo get_choice('flex-column', $order['order_cashback'] > 0, 'align-items-center');?> flex-fill">
                                        <div class="custom-line-height-16">
                                            <strong class="order_final_price_for_payment"><?php echo numberFormat($order['order_price_final'] + $order['order_delivery_price'], $order['order_price_decimals']);?></strong> <?php echo $order['order_currency'];?>
                                        </div>
                                        <?php if($order['order_cashback'] > 0){?>
                                            <strong class="text-danger custom-line-height-14 custom-font-size-10">- <?php echo numberFormat($order['order_cashback'], $order['order_price_decimals']);?> <?php echo $order['order_currency'];?> Бонус</strong>
                                        <?php }?>
                                    </div>
                                </div>          
                            </div>
                            <div class="col-12 col-sm-6 col-lg-2">
                                <div class="form-group">
                                    <label class="text-gray">Итого к оплате</label>
                                    <div class="text-left custom-line-height-24 custom-height-30 d-flex align-items-center text-success">
                                        <strong class="custom-font-size-20"><span class="order_final_price_for_payment_bonus"><?php echo numberFormat($order['order_price_final'] + $order['order_delivery_price'] - $order['order_cashback'], $order['order_price_decimals']);?></span> <?php echo $order['order_currency'];?></strong>
                                    </div>
                                </div>          
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <label class="text-gray custom-font-size-12">Коментарий клиента</label>
                            <textarea name="order[user_comment]" rows="3" class="form-control rounded-0 custom-font-size-12"><?php echo $order['order_user_comment'];?></textarea>
                        </div>          
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <label class="text-gray custom-font-size-12">Коментарий менеджера</label>
                            <textarea name="order[admin_comment]" rows="3" class="form-control rounded-0 custom-font-size-12"><?php echo $order['order_admin_comment'];?></textarea>
                        </div>          
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <div class="icheck-primary">
                    <input name="notify_user" type="checkbox" id="js-checkbox-notify-buyer">
                    <label for="js-checkbox-notify-buyer" class="custom-font-size-12">Уведомить клиента<span class="d-none d-sm-inline"> об изминениях</span></label>
                </div>
                <div class="d-flex">
                    <input type="hidden" name="order[id_order]" value="<?php echo $order['id_order'];?>">
                    <button type="button" data-callback="updateOrder" data-close-popup="0" class="btn btn-primary btn-flat call-function custom-margin-right-10"><i class="fad fa-check"></i> Сохранить</button>
                    <button type="button" data-callback="updateOrder" data-close-popup="1" class="btn btn-success btn-flat call-function"><i class="fad fa-check"></i> Сохранить и закрыть</button>
                </div>
            </div>
        </form>
    </div>
</div>