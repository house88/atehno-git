<div class="form-group">
    <div class="input-group input-group-sm">
        <span class="input-group-prepend">
            <button type="button" class="btn btn-default btn-flat"><i class="fad fa-search"></i></button>
        </span>
        <input class="form-control rounded-0 dt_filter" type="text" name="keywords" data-title="Поиск" placeholder="Поиск">
    </div>
</div>
<div class="form-group">
    <div class="input-group input-group-sm">
        <span class="input-group-prepend">
            <button type="button" class="btn btn-default btn-flat"><i class="fad fa-search"></i></button>
        </span>
        <input class="form-control rounded-0 dt_filter" type="text" name="prog_order_id" data-title="Номер реализации в 1С" placeholder="Номер реализации в 1С">
    </div>
</div>
<div class="form-group">
    <div class="input-group input-group-sm">
        <span class="input-group-prepend">
            <button type="button" class="btn btn-default btn-flat"><i class="fad fa-user-shield"></i></button>
        </span>						
        <select name="id_manager" class="form-control rounded-0 dt_filter" data-title="Менеджер">
            <option value="">Выберите менеджера</option>
            <?php $managers = Modules::run('users/_get_all', array('group_type' => array('admin','moderator','content_menedger')));?>
            <?php foreach($managers as $manager){?>
                <option value="<?php echo $manager['id'];?>"><?php echo $manager['user_login'];?></option>
            <?php }?>
        </select>
    </div>
</div>
<div class="form-group">
    <div class="input-group input-group-sm">
        <span class="input-group-prepend">
            <button type="button" class="btn btn-default btn-flat"><i class="fad fa-user"></i></button>
        </span>						
        <select name="user" class="form-control rounded-0 dt_filter" data-title="Покупатель">
            <option value="">Выберите покупателя</option>
            <?php foreach($users as $order_user){?>
                <option value="<?php echo $order_user['id'];?>"><?php echo $order_user['user_nicename'];?></option>
            <?php }?>
        </select>
    </div>
</div>
<div class="form-group">
    <div class="input-group input-group-sm">
        <span class="input-group-prepend">
            <button type="button" class="btn btn-default btn-flat"><i class="fad fa-user-headset"></i></button>
        </span>						
        <select name="status_client" class="form-control rounded-0 dt_filter" data-title="Статус клиента">
            <option value="">Статус клиента</option>
            <option value="1">Не отвечает</option>
        </select>
    </div>
</div>
<div class="form-group">
    <div class="input-group input-group-sm">
        <span class="input-group-prepend">
            <button type="button" class="btn btn-default btn-flat"><i class="fad fa-bring-forward"></i></button>
        </span>						
        <select name="status" class="form-control rounded-0 dt_filter" data-title="Статус заказа">
            <option value="">Статус заказа</option>
            <?php $statuses = get_order_statuses();?>
            <?php foreach($statuses as $status_key => $status){?>
                <option value="<?php echo $status_key;?>"><?php echo $status['title'];?></option>
            <?php }?>
        </select>
    </div>
</div>
<div class="form-group">
    <div class="input-group input-group-sm">
        <span class="input-group-prepend">
            <button type="button" class="btn btn-default btn-flat"><i class="fad fa-truck"></i></button>
        </span>						
        <select name="delivery" class="form-control rounded-0 dt_filter" data-title="Доставка">
            <option value="">Выберите метод доставки</option>
            <?php foreach($delivery_options as $delivery_option){?>
                <option value="<?php echo $delivery_option['id_delivery'];?>"><?php echo $delivery_option['delivery_title'];?></option>
            <?php }?>
        </select>
    </div>
</div>
<div class="form-group">
    <div class="input-group input-group-sm">
        <span class="input-group-prepend">
            <button type="button" class="btn btn-default btn-flat"><i class="fad fa-money-bill-alt"></i></button>
        </span>						
        <select name="payment" class="form-control rounded-0 dt_filter" data-title="Оплата">
            <option value="">Выберите метод оплаты</option>
            <?php foreach($payment_options as $payment_option){?>
                <option value="<?php echo $payment_option['id_payment'];?>"><?php echo $payment_option['payment_title'];?></option>
            <?php }?>
        </select>
    </div>
</div>
<div class="form-group">
    <label class="text-gray custom-font-size-12">Дата получения заказа</label>
    <div class="input-group input-group-sm">
        <div class="input-group-prepend rounded-0">
            <span class="input-group-text"><i class="fad fa-calendar-week"></i></span>
        </div>
        <input type="text" name="order_pickup" class="form-control float-right rounded-0 dt_filter" data-title="Дата получения заказа" placeholder="ex. 21/01/2020 - 31/01/2020" id="js-order-pickup-date">
    </div>
</div>
<div class="form-group">
    <label class="text-gray custom-font-size-12">Создан</label>
    <div class="input-group input-group-sm">
        <div class="input-group-prepend rounded-0">
            <span class="input-group-text"><i class="fad fa-calendar-week"></i></span>
        </div>
        <input type="text" name="order_created" class="form-control float-right rounded-0 dt_filter" data-title="Создан" placeholder="ex. 21/01/2020 - 31/01/2020" id="js-order-created-date">
    </div>
</div>
<div class="form-group">
    <label class="text-gray custom-font-size-12">Обновлен</label>
    <div class="input-group input-group-sm">
        <div class="input-group-prepend rounded-0">
            <span class="input-group-text"><i class="fad fa-calendar-week"></i></span>
        </div>
        <input type="text" name="order_updated" class="form-control float-right rounded-0 dt_filter" data-title="Обновлен" placeholder="ex. 21/01/2020 - 31/01/2020" id="js-order-updated-date">
    </div>
</div>
<div class="form-group">
    <label class="text-gray custom-font-size-12">Цена заказа</label>
    <div class="input-group input-group-sm">
        <div class="input-group-prepend rounded-0">
            <span class="input-group-text">от</span>
        </div>
        <input type="text" class="form-control float-right rounded-0 dt_filter" data-title="Цена заказа, от" name="order_price_final_from" placeholder="0.00">
        <div class="input-group-append rounded-0">
            <span class="input-group-text">до</span>
        </div>
        <input type="text" class="form-control float-right rounded-0 dt_filter" data-title="Цена заказа, до" name="order_price_final_to" placeholder="0.00">
    </div>
</div>