<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Заказ на осмотр товаров</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label>ИД</label>
                            <p class="mb-0"><?php echo $showroom['item_prog_id'];?></p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label>Артикул</label>
                            <p class="mb-0"><?php echo $showroom['item_code'];?></p>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Название товара</label>
                            <p class="mb-0">
                                <a href="<?php echo site_url('products/' . $showroom['item_url']);?>" target="_blank">
                                    <?php echo $showroom['item_title'];?>
                                </a>    
                            </p>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Коментарий администратора</label>
                            <textarea name="admin_comment" rows="10" class="form-control rounded-0"><?php echo $showroom['showroom_comment_admin'];?></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-end">
                <div class="d-flex">
                    <input type="hidden" name="showroom" value="<?php echo $showroom['id_showroom'];?>">
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/showroom/ajax_operations/edit',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });
</script>
