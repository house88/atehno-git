<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-widget custom-margin-top-10">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_header;?></h3>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-hover main-data-table" id="dtTable" style="width: 100%;">
							<thead>
								<tr>
									<th class="dt_toggle custom-width-50">#</th>
									<th class="dt_photo custom-width-100">Фото</th>
									<th class="dt_item">Товар</th>
									<th class="dt_client custom-width-200">Клиент</th>
									<th class="dt_date custom-width-100">Дата осмотра</th>
									<th class="dt_status custom-width-150">Статус</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
