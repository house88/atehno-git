<?php $this->load->view($this->theme->apanel_view('includes/plugins/datatable'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/fancybox'));?>

<script>
	var dtTable; //obj of datatable
	$(function(){
		'use strict';
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/Russian.json'));?>"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/showroom/ajax_operations/list",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "vam text-center custom-width-50", "aTargets": ["dt_toggle"], "mData": "dt_toggle", "bSortable": false},
				{ "sClass": "vam text-center custom-width-100 d-block d-md-none d-xl-table-cell", "aTargets": ["dt_photo"], "mData": "dt_photo", "bSortable": false},
				{ "sClass": "vam table-grid-text text-left custom-font-size-12", "aTargets": ["dt_item"], "mData": "dt_item", "bSortable": false},
				{ "sClass": "text-left custom-width-200 d-block d-md-none d-xl-table-cell", "aTargets": ["dt_client"], "mData": "dt_client", "bSortable": false},
				{ "sClass": "vam text-center custom-width-100", "aTargets": ["dt_date"], "mData": "dt_date", "bSortable": false},
				{ "sClass": "text-center custom-width-150", "aTargets": ["dt_status"], "mData": "dt_status", "bSortable": false}
			],
			"aaSorting" : [[0,'asc']],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					beforeSend: function(){
						showLoader('body');
					},
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, data.mess_type);
						}

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                fixDataTablePagination(this);

				mobileDataTable($('.main-data-table'));
				
                hideLoader('body');
			}
		});
	});

	var change_status = function(btn){
		var $this = $(btn);
		var showroom = $this.closest('.dropdown').data('showroom');
		var showroom_status = $this.data('status');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/showroom/ajax_operations/change_status',
			data: {showroom:showroom,showroom_status:showroom_status},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>