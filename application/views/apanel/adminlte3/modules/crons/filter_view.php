<div class="form-group">
    <label class="text-gray custom-font-size-12">Дата</label>
    <div class="input-group input-group-sm">
        <div class="input-group-prepend rounded-0">
            <span class="input-group-text"><i class="fad fa-calendar-week"></i></span>
        </div>
        <input type="text" name="log_date" class="form-control float-right rounded-0 dt_filter" data-title="Дата" placeholder="ex. 21/01/2020 - 31/01/2020" id="js-cron-created-date">
    </div>
</div>