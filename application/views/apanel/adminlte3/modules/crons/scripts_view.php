<?php $this->load->view($this->theme->apanel_view('includes/plugins/datatable'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/daterangepicker'));?>

<script>
	var dtTable; //obj of datatable
	var dtFilter;
	$(function(){
		'use strict';
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/Russian.json'));?>"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/crons/ajax_operations/list",
			"sServerMethod": "POST",
			"iDisplayLength": 50,
			"aoColumnDefs": [
				{ "sClass": "vam text-left custom-width-200", "aTargets": ["dt_name"], "mData": "dt_name", "bSortable": false},
				{ "sClass": "vam text-left", "aTargets": ["dt_text"], "mData": "dt_text", "bSortable": false},
				{ "sClass": "vam text-center custom-width-150", "aTargets": ["dt_date"], "mData": "dt_date", "bSortable": false},
				{ "sClass": "vam text-center custom-width-50", "aTargets": ["dt_status"], "mData": "dt_status", "bSortable": false}
			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				if(!dtFilter){
					dtFilter = initDtFilter('comments');
				}

				aoData = aoData.concat(dtFilter.getDTFilter());

				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					beforeSend: function(){
						showLoader('body');
					},
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, data.mess_type);
							
						}
						
						$('#total_dtTable-counter').text(data.iTotalDisplayRecords);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                fixDataTablePagination(this);

				mobileDataTable($('.main-data-table'));
				
                hideLoader('body');
			}
		});

		$('#js-cron-created-date').daterangepicker({
			timePicker: false,
			parentEl: '.modal',
			locale: {
				format: 'DD.MM.YYYY'
			},
			autoUpdateInput: false,
			"autoApply": true,
			"opens": "left"
		}).on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD.MM.YYYY') + ' - ' + picker.endDate.format('DD.MM.YYYY')).trigger('change');
		}).on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('').trigger('change');
		});
	});
</script>