<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-widget custom-margin-top-10">
				<div class="card-header">
					<h3 class="card-title">
						<?php echo $page_header;?>, <small>найдено: <span id="total_dtTable-counter">0</span></small>
					</h3>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-toggle="modal" data-target="#dtFilter" data-toggle-class="modal-open-aside">
							<i class="fas fa-filter"></i>
						</button>
					</div>
				</div>
				<div class="card-body">
					<div class="dtfilter-list d-flex justify-content-start flex-wrap">
						<div class="filter-plugin-list d-flex justify-content-start flex-wrap"></div>
						<div class="filter-control-btns d-flex justify-content-start flex-wrap"></div>
					</div>
					
					<?php if(!empty($dt_filter)){?>
						<?php $this->load->view($this->theme->apanel_view('includes/filter'));?>
					<?php }?>

					<div class="table-responsive">
						<table class="table table-bordered table-hover main-data-table" id="dtTable" style="width: 100%;">
							<thead>
								<tr>
									<th class="dt_status"></th>
									<th class="dt_name">Название</th>
									<th class="dt_text">Текст</th>
									<th class="dt_date">Дата</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
