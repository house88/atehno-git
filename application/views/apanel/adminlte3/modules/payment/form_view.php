<div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($payment) ? 'Редактировать' : 'Добавить';?> метод оплаты
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="title" value="<?php echo !empty($payment) ? $payment['payment_title'] : '';?>">
                        </div>
                    </div>
                    <?php $payment_add_options = !empty($payment) ? json_decode($payment['payment_add_options'], true) : array();?>
                    <?php foreach($currencies as $currency){?>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label>Дополнительный %, для <?php echo $currency['currency_code'];?></label>
                                <input class="form-control form-control-sm rounded-0" placeholder="Дополнительный %" name="price_add[<?php echo $currency['currency_code'];?>]" value="<?php echo (!empty($payment_add_options[$currency['currency_code']])) ? $payment_add_options[$currency['currency_code']] : 0;?>">
                            </div>
                        </div>
                    <?php }?>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Доставка</label>
                            <select class="form-control form-control-sm rounded-0 bs-select-element" data-style="btn btn-default btn-sm btn-flat" data-title="Привязанные мотоды доставки" multiple>
                                <?php $delivery_options = arrayByKey(Modules::run('delivery/_get_all'), 'id_delivery');?>
                                <?php foreach($delivery_options as $delivery_option){?>
                                    <option value="<?php echo $delivery_option['id_delivery'];?>" <?php echo get_choice('selected', !empty($delivery_selected) && in_array($delivery_option['id_delivery'], $delivery_selected))?>><?php echo $delivery_option['delivery_title'];?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div id="js-relation-text">
                            <?php if(!empty($delivery_relations)){?>
                                <?php foreach($delivery_relations as $delivery_relation){?>
                                    <?php if(!isset($delivery_options[$delivery_relation['id_delivery']])){ continue; }?>

                                    <fieldset id="js-relation-delivery-<?php echo $delivery_relation['id_delivery'];?>">
                                        <legend>
                                            <span><?php echo $delivery_options[$delivery_relation['id_delivery']]['delivery_title'];?></span>
                                            <div class="fieldset-tools">
                                                <a href="#" class="btn btn-tool call-function" data-callback="delete_relation_delivery" data-delivery="<?php echo $delivery_relation['id_delivery'];?>" tooltip="Удалить">
                                                    <i class="fad fa-times"></i> 
                                                    <span class="d-none d-md-inline">Удалить</span>
                                                </a>
                                            </div>
                                        </legend>
                                        <div class="form-group">
                                            <textarea class="form-control form-control-sm rounded-0" placeholder="Текст для заказа" name="delivery[<?php echo $delivery_relation['id_delivery'];?>][text]"><?php echo $delivery_relation['relation_text'];?></textarea>
                                        </div>
                                    </fieldset>
                                <?php }?>
                            <?php }?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Краткое описание</label>
                            <textarea class="form-control form-control-sm rounded-0" name="stext"><?php echo !empty($payment) ? $payment['payment_sdescription'] : '';?></textarea>
                            <p class="help-block">Не должно содержать более 250 символов.</p>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Текст</label>
                            <textarea class="description" name="description"><?php echo !empty($payment) ? $payment['payment_description'] : '';?></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <div class="icheck-primary">
                    <input name="active" type="checkbox" id="js-checkbox-payment-active" <?php echo set_checkbox('active', 1, !empty($payment) ? (int) $payment['payment_active'] === 1 : false);?>>
                    <label for="js-checkbox-payment-active">
                        Активный
                    </label>
                </div>
                <div class="d-flex">
                    <?php if(!empty($payment)){?>
                        <input type="hidden" name="payment" value="<?php echo $payment['id_payment'];?>">
                    <?php }?>
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/template" id="js-relation-delivery-template">
    <fieldset id="js-relation-delivery-{{id_delivery}}">
        <legend>
            <span>{{delivery_title}}</span>
            <div class="fieldset-tools">
                <a href="#" class="btn btn-tool call-function" data-callback="delete_relation_delivery" data-delivery="{{id_delivery}}" tooltip="Удалить">
                    <i class="fad fa-times"></i> 
                    <span class="d-none d-md-inline">Удалить</span>
                </a>
            </div>
        </legend>
        <div class="form-group">
            <textarea class="form-control form-control-sm rounded-0" placeholder="Текст для заказа" name="delivery[{{id_delivery}}][text]"></textarea>
        </div>
    </fieldset>
</script>

<script>
    $(".bs-select-element").selectpicker()
    .on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
        var option = $(e.target[clickedIndex]);
        if(isSelected){
            var template = $('#js-relation-delivery-template').text();
            var context = {
                'delivery_title': option.text(),
                'id_delivery': option.val()
            };

			for (var key in context) {
				template = template.replace(new RegExp('{{' + key + '}}', 'g'), context[key]);
            }
            
            $('#js-relation-text').append(template);
        } else{
            $('fieldset#js-relation-delivery-' + option.val()).remove();
        }
    });

    var delete_relation_delivery = function(element){        
        $('.bs-select-element').find('[value="'+ $(element).data('delivery') +'"]').prop('selected', false);
        $('.bs-select-element').selectpicker('refresh');
        $(element).closest('fieldset').remove();
    }


    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        tinyMCE.triggerSave();
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/payment/ajax_operations/<?php echo !empty($payment) ? 'edit' : 'add';?>',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });

    initTinyMce();
</script>
