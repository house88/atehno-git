<?php $this->load->view($this->theme->apanel_view('includes/plugins/datatable'));?>

<script>
	var dtTable; //obj of datatable
	$(function(){
		'use strict';
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/Russian.json'));?>"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/currency/ajax_operations/list",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "vam text-center", "aTargets": ["dt_id"], "mData": "dt_id", "bSortable": false},
				{ "sClass": "vam", "aTargets": ["dt_name"], "mData": "dt_name", "bSortable": false},
				{ "sClass": "vam", "aTargets": ["dt_code"], "mData": "dt_code", "bSortable": false},
				{ "sClass": "vam", "aTargets": ["dt_symbol"], "mData": "dt_symbol", "bSortable": false},
				{ "sClass": "vam", "aTargets": ["dt_rate"], "mData": "dt_rate"},
				{ "sClass": "vam text-center", "aTargets": ["dt_default"], "mData": "dt_default", "bSortable": false},
				{ "sClass": "vam text-center", "aTargets": ["dt_adefault"], "mData": "dt_adefault", "bSortable": false},
				{ "sClass": "vam text-center", "aTargets": ["dt_udefault"], "mData": "dt_udefault", "bSortable": false},
				{ "sClass": "vam text-center", "aTargets": ["dt_active"], "mData": "dt_active" , "bSortable": false },
				{ "sClass": "vam text-center", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }
			],
			"aaSorting" : [[0,'asc']],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					beforeSend: function(){
						showLoader('body');
					},
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, data.mess_type);
						}

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                fixDataTablePagination(this);

				mobileDataTable($('.main-data-table'));
				
                hideLoader('body');
			}
		});
	});

	var change_status = function(btn){
		var $this = $(btn);
		var currency = $this.data('currency');
		var option = $this.data('option');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/currency/ajax_operations/change_status/'+option,
			data: {currency:currency},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
	}

	var delete_action = function(btn){
		var $this = $(btn);
		var currency = $this.data('currency');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/currency/ajax_operations/delete',
			data: {currency:currency},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>