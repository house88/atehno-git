<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                Редактировать склад
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Название в 1C</label>
                            <div class="form-control form-control-sm rounded-0"><?php echo $stock['stock_name'];?></div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Название на сайте</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название на сайте" name="name_display" value="<?php echo $stock['stock_name_display'];?>">
                        </div>
                    </div>
                    <div class="col-12">
						<?php $_cities = Modules::run('cities/_get_cities');?>
						<div class="form-group">
							<label>Регион</label>
							<select name="stock_city" class="form-control form-control-sm rounded-0">
								<option value="">Выберите регион</option>
								<?php foreach($_cities as $_city){?>
									<option value="<?php echo $_city['id_city'];?>" <?php echo set_select('stock_city', $_city['id_city'], $stock['stock_city'] == $_city['id_city']);?>><?php echo $_city['city_name'];?></option>
								<?php }?>
							</select>
						</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-end">
                <div class="d-flex">
					<input type="hidden" name="stock" value="<?php echo $stock['id_stock'];?>">
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/stocks/ajax_operations/edit',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });
</script>
