<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($page) ? 'Редактировать' : 'Добавить';?> страницу
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="title" value="<?php echo !empty($page) ? $page['page_title'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>URL</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="URL" name="url" value="<?php echo !empty($page) ? $page['url'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Meta keywords</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Meta keywords" name="mk" value="<?php echo !empty($page) ? $page['mk'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Meta description</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Meta description" name="md" value="<?php echo !empty($page) ? $page['md'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Текст</label>
                            <textarea class="description" name="description"><?php echo !empty($page) ? $page['page_description'] : '';?></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <div class="icheck-primary">
                    <input name="visible" type="checkbox" id="js-checkbox-brand-active" <?php echo set_checkbox('visible', 1, !empty($page) ? (int) $page['page_visible'] === 1 : false);?>>
                    <label for="js-checkbox-brand-active">
                        Активная
                    </label>
                </div>
                <div class="d-flex">
                    <?php if(!empty($page)){?>
                        <input type="hidden" name="page" value="<?php echo $page['id_page'];?>">
                    <?php }?>
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        tinyMCE.triggerSave();
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/pages/ajax_operations/<?php echo !empty($page) ? 'edit' : 'add';?>',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });

    initTinyMce();
</script>
