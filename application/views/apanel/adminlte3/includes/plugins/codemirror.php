<!-- CodeMirror -->
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/codemirror/lib/codemirror.js'));?>"></script>
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/codemirror/mode/xml/xml.js'));?>"></script>
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('plugins/codemirror/lib/codemirror.css'));?>">
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('plugins/codemirror/theme/eclipse.css'));?>">