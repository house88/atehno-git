<!-- DataTables -->
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/css/dataTables.bootstrap4.css'));?>">
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/jquery.dataTables.js'));?>"></script>
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/dataTables.bootstrap.js'));?>"></script>
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/dataTables.bootstrap4.js'));?>"></script>
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/jquery.dtFilters.js'));?>"></script>
<script>
    $.fn.DataTable.ext.pager.numbers_length = 6;
    $.fn.dataTable.ext.classes.sPageButton = 'pagination-sm';
</script>