<!-- jQuery -->
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/jquery/jquery.min.js'));?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/jquery-ui/jquery-ui.min.js'));?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script src="<?php echo file_modification_time($this->theme->apanel_assets('js/jquery.cookie.js'));?>"></script>
<script>
	$.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/bootstrap/js/bootstrap.bundle.min.js'));?>"></script>

<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/bootstrap-dialog/bootstrap-dialog.js'));?>"></script>

<!-- Resizestop -->
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/resizestop/jquery.resizestop.min.js'));?>"></script>
<!-- overlayScrollbars -->
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js'));?>"></script>

<!--BOOTSTRAP SELECT-->
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/bootstrap-select/js/bootstrap-select.js'));?>"></script>
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/bootstrap-select/js/i18n/defaults-ru_RU.js'));?>"></script>
<!--BOOTSTRAP SELECT END-->
<!--BOOTSTRAP Dialog-->
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/bootstrap-modal-wrapper/bootstrap-modal-wrapper-factory.js'));?>"></script>

<!-- Toastr -->
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/toastr/toastr.min.js'));?>"></script>

<!-- AdminLTE App -->
<script src="<?php echo file_modification_time($this->theme->apanel_assets('js/adminlte.js'));?>"></script>
<script src="<?php echo file_modification_time($this->theme->apanel_assets('js/scripts.js'));?>"></script>

<?php if(!empty($main_scripts)){?>
	<?php $this->load->view($main_scripts);?>
<?php }?>

<?php if(!empty($flash_message)){?>
	<script>
		var flash_message = JSON.parse('<?php echo $flash_message;?>');
		systemMessages(flash_message.message, flash_message.type);
	</script>
<?php }?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/filemanager'));?>

<div class="modal fade" id="general_popup_form" data-keyboard="false" data-backdrop="static"></div>