<!DOCTYPE html>
<html lang="ru">
<head>
    <?php $this->load->view($this->theme->public_view('includes/head')); ?>
</head>
<body>	
	<?php $this->load->view($this->theme->public_view('includes/snowflake')); ?>
	
	<div class="page-wrap">
		<?php $this->load->view($this->theme->public_view('includes/header')); ?>
		
		<?php $home_banner = Modules::run('banners/_get_by', array('banner_tocken' => '95c20a4ef6b25c9a03107282c9cb901d'));?>
		<?php if(!empty($home_banner)){?>
			<?php $banner_photos = !empty($home_banner['banner_data']) ? json_decode($home_banner['banner_data'], true) : array();?>
			<?php if(!empty($banner_photos)){?>
				<section id="wr-banner">
					<div class="wr-banner-home">	
						<ul class="bxslider home_top-slider">
							<?php foreach($banner_photos as $banner_photo){?>
								<?php if((int)@$banner_photo['active'] == 1){?>
									<li style="background-color:<?php echo !empty($banner_photo['bg_color']) ? $banner_photo['bg_color'] : '#ffffff';?>">
										<?php if(!empty($banner_photo['link'])){?>
											<a href="<?php echo $banner_photo['link'];?>">
												<img src="<?php echo site_url('files/banners/' . $home_banner['banner_tocken'] . '/' . $banner_photo['photo']);?>"/>
											</a>
										<?php } else{?>
											<img src="<?php echo site_url('files/banners/' . $home_banner['banner_tocken'] . '/' . $banner_photo['photo']);?>"/>
										<?php }?>
									</li>
								<?php }?>
							<?php }?>
						</ul>
					</div>
				</section>
			<?php }?>
		<?php }?>

		<section id="wr-body">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php if($home_settings['newest_products']['setting_active']){?>
							<div class="home_products mb-50">
								<?php $newest_block = json_decode($home_settings['newest_products']['setting_data']);?>
								<h2 class="heading-title"><span><?php echo $newest_block->block_name;?></span></h2>
								<div class="mt-15 home_products-slider newest_products-slider grid products_block">
									<?php 
										$params_newest_products = array(
											'item_newest' => 1,
											'item_visible' => 1,
											'moderation' => true,
											'is_filled' => 1,
											'order_by' => 'RAND()',
											'limit' => $newest_block->limit,
											'start' => 0
										);
										$products = Modules::run('items/_get_all', $params_newest_products);
									?>

									<?php if(!empty($products)){?>
										<?php foreach($products as $item){?>
											<!-- PRODUCT - START -->
											<div class="slide">
												<?php $this->load->view($this->theme->public_view('shop/catalog/product_cart_view'), array('item' => $item));?>
											</div>
											<!-- PRODUCT - END -->
										<?php }?>
									<?php }?>
								</div>
							</div>
						<?php }?>
						<?php if($home_settings['popular_products']['setting_active']){?>
							<div class="home_products mb-50">
								<?php $popular_block = json_decode($home_settings['popular_products']['setting_data']);?>
								<h2 class="heading-title"><span><?php echo $popular_block->block_name;?></span></h2>
								<div class="mt-15 pt-10 pb-10 home_products-slider popular_products-slider grid products_block">
									<?php 
										$params_popular_products = array(
											'item_popular' => 1,
											'item_visible' => 1,
											'moderation' => true,
											'is_filled' => 1,
											'order_by' => 'RAND()',
											'limit' => $popular_block->limit,
											'start' => 0
										);
										$products = Modules::run('items/_get_all', $params_popular_products);
									?>

									<?php if(!empty($products)){?>
										<?php foreach($products as $item){?>
											<!-- PRODUCT - START -->
											<div class="slide">
												<?php $this->load->view($this->theme->public_view('shop/catalog/product_cart_view'), array('item' => $item));?>
											</div>
											<!-- PRODUCT - END -->
										<?php }?>
									<?php }?>
								</div>
							</div>
						<?php }?>
						<?php if($home_settings['recomended_products']['setting_active']){?>
							<div class="hot-categories mb-50">
								<h2 class="heading-title"><span>Рекомендуем</span></h2>
								<?php $recommended_blocks = json_decode($home_settings['recomended_products']['setting_data']);?>
								<?php if(!empty($recommended_blocks->blocks)){?>
									<!-- Nav tabs -->
									<ul class="nav nav-tabs" role="tablist">
										<?php foreach($recommended_blocks->blocks as $recommended_block_key => $recommended_block){?>
											<li role="presentation" class=" <?php echo (($recommended_block_key == 0)?'active':'');?>">
												<a href="#recommended_<?php echo $recommended_block_key;?>" aria-controls="recommended_<?php echo $recommended_block_key;?>" role="tab" data-toggle="tab"><?php echo $recommended_block->block_name;?></a>
											</li>
										<?php }?>
									</ul>
									<!-- Tab panes -->
									<div class="tab-content">
										<?php foreach($recommended_blocks->blocks as $recommended_block_key => $recommended_block){?>
											<?php
												$params = array(
													'item_on_home' => 1, 
													'item_visible' => 1,
													'moderation' => true,
													'is_filled' => 1,
													'order_by' => 'RAND()',
													'limit' => $recommended_blocks->limit, 
													'start' => 0
												);
												$categories = explode(',', $recommended_block->block_categories);
												if(!empty($categories)){
													$params['id_category'] = $categories;
												}	
											?>
											<div role="tabpanel" class="tab-pane fade <?php echo (($recommended_block_key == 0)?'in active':'');?>" id="recommended_<?php echo $recommended_block_key;?>">
												<div class="row mt-15 grid products_block">
													<?php $products = Modules::run('items/_get_all', $params);?>
													<?php if(!empty($products)){?>
														<?php foreach($products as $item){?>
															<!-- PRODUCT - START -->
															<div class="col-md-3 col-sm-4 col-xs-6">
																<?php $this->load->view($this->theme->public_view('shop/catalog/product_cart_view'), array('item' => $item));?>
															</div>
															<!-- PRODUCT - END -->
														<?php }?>
													<?php } else{?>
														<div class="col-xs-12">
															<div class="alert alert-info">Нет товаров.</div>
														</div>
													<?php }?>
												</div>
											</div>
										<?php }?>
									</div>
								<?php }?>
							</div>
						<?php }?>
					</div>
				</div>
			</div>
		</section>
    	<?php $this->load->view($this->theme->public_view('includes/footer')); ?>
	</div>
</body>
</html>
