<meta charset="utf-8">
<base href="<?php echo base_url();?>">
<title><?php echo ((isset($stitle))?clean_output($stitle):clean_output($settings['default_title']['setting_value']));?></title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="<?php echo ((isset($skeywords))?clean_output($skeywords):clean_output($settings['mk']['setting_value']));?>">
<meta name="description" content="<?php echo ((isset($sdescription))?clean_output($sdescription):clean_output($settings['md']['setting_value']));?>">
<meta name="author" content="Cravciuc Andrei, cravciucandy@gmail.com">
<meta name="yandex-verification" content="efa2d3bfc2fa36d7" />

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-141548608-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-141548608-1');
</script>

<?php if(!empty($product_jsonld)){?>
    <script type="application/ld+json">
        <?php echo $product_jsonld;?>
    </script>
<?php }?>

<?php if(isset($og_meta)){?>
    <!-- og_meta -->
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="<?php echo $og_meta['site_name'];?>">
    <meta property="og:title" content="<?php echo $og_meta['title'];?>" />
    <meta property="og:description" content="<?php echo $og_meta['description'];?>" />
    <meta property="og:url" content="<?php echo $og_meta['url'];?>" />
    <meta property="og:image" content="<?php echo $og_meta['image'];?>" />

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="<?php echo $og_meta['title'];?>">
    <meta name="twitter:description" content="<?php echo $og_meta['description'];?>">
    <meta name="twitter:url" content="<?php echo $og_meta['url'];?>">
    <meta name="twitter:domain" content="<?php echo base_url();?>">
    <meta name="twitter:site" content="@<?php echo @$settings['default_title']['setting_value'];?>">
    <meta name="twitter:image" content="<?php echo $og_meta['image'];?>">
    <!-- og_meta END -->
<?php }?>

<link href="<?php echo file_modification_time('theme/css/ca-icons/style.css', $settings['update_css']['setting_value'] == 1);?>" rel="stylesheet">
<link href="<?php echo file_modification_time('theme/css/font-awesome.css', $settings['update_css']['setting_value'] == 1);?>" rel="stylesheet">
<link href="<?php echo file_modification_time('theme/accent/css/style.css', $settings['update_css']['setting_value'] == 1);?>" rel="stylesheet">

<?php if($settings['black_friday_active']['setting_value'] == 1){?>
    <link href="<?php echo file_modification_time('theme/accent/css/style_black.css', $settings['update_css']['setting_value'] == 1);?>" rel="stylesheet">
<?php }?>


<script>
    Object.defineProperty(window, 'base_url', { writable: false, value: "<?php echo base_url();?>" });
    Object.defineProperty(window, 'filter_url', { writable: false, value: "<?php echo ((!empty($filter_url))?$filter_url:current_url());?>" });
    Object.defineProperty(window, 'filter_title', { writable: false, value: "<?php if(isset($ftitle)) echo clean_output($ftitle);?>" });
    Object.defineProperty(window, 'filter_get_url', { writable: false, value: "<?php if(!empty($get_params)) echo "?".implode('&', $get_params);?>" });
    Object.defineProperty(window, 'view_decimals', { writable: false, value: Boolean(~~'<?php echo $this->lauth->group_view_decimals() ? 1 : 0;?>') });
</script> 
<script src="<?php echo file_modification_time('theme/accent/js/all-scripts.js');?>"></script>
<script src="<?php echo file_modification_time('theme/accent/plugins/inputmask/jquery.inputmask.min.js');?>"></script>

<?php if($this->lauth->is_admin()){?>
    <script src="<?php echo file_modification_time('theme/admin/js/admin_scripts.js');?>"></script>
<?php }?>

<link rel="apple-touch-icon" sizes="57x57" href="<?php echo file_modification_time('theme/css/favicon/apple-icon-57x57.png'); ?>">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo file_modification_time('theme/css/favicon/apple-icon-60x60.png'); ?>">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo file_modification_time('theme/css/favicon/apple-icon-72x72.png'); ?>">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo file_modification_time('theme/css/favicon/apple-icon-76x76.png'); ?>">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo file_modification_time('theme/css/favicon/apple-icon-114x114.png'); ?>">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo file_modification_time('theme/css/favicon/apple-icon-120x120.png'); ?>">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo file_modification_time('theme/css/favicon/apple-icon-144x144.png'); ?>">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo file_modification_time('theme/css/favicon/apple-icon-152x152.png'); ?>">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo file_modification_time('theme/css/favicon/apple-icon-180x180.png'); ?>">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo file_modification_time('theme/css/favicon/android-icon-192x192.png'); ?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo file_modification_time('theme/css/favicon/favicon-32x32.png'); ?>">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo file_modification_time('theme/css/favicon/favicon-96x96.png'); ?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo file_modification_time('theme/css/favicon/favicon-16x16.png'); ?>">
<link rel="manifest" crossorigin="use-credentials" href="<?php echo site_url('theme/css/favicon/manifest.json'); ?>">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo file_modification_time('theme/css/favicon/ms-icon-144x144.png'); ?>">
<meta name="theme-color" content="#ffffff">