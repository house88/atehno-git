<section id="header">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="header-info">
					<?php echo Modules::run('static_block/_view', array('menu' => 'header_links'));?>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="header-info_user">
					<?php if(!$this->lauth->logged_in()){?>
						<a href="#" class="cabinet-b header-info_link" data-toggle="modal" data-target="#auth-modal">
							<i class="ca-icon ca-icon_user"></i>
							<span class="link-title">
								войдите в личный кабинет
							</span>
						</a>
					<?php } else{?>
						<span class="dropdown header-info_link">
							<a href="#" class="cabinet-b dropdown-toggle header-info_link" type="link" id="dropdownCabinet" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								<i class="ca-icon ca-icon_user pull-left fs-20 mr-5"></i>
								<span class="link-title">
									<?php echo $this->lauth->user_data()->user_nicename;?>
								</span>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" aria-labelledby="dropdownCabinet">
								<?php if($this->lauth->is_admin()){?>
									<li><a href="<?php echo base_url('admin');?>">Перейти в админ панель</a></li>							
								<?php }?>
								<li><a href="<?php echo base_url('account');?>">Перейти в кабинет</a></li>
								<li><a href="<?php echo base_url('logout');?>">Выход</a></li>
							</ul>
						</span>
					<?php }?>
					<span class="pull-right">Здравствуйте,</span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-8">
								<a href="<?php echo base_url();?>" class="header-logo <?php if($settings['snowflake_active']['setting_value'] == 1){?>logo-winter<?php }?>"></a>
				<div class="form-group mb-0 header-search_form">
					<div class="search-wr">
						<div class="search-input">
							<input type="text" class="form-control" name="search" autocomplete="off">
							<a href="" class="search-icon ca-icon ca-icon_search"></a>
						</div>
						<div class="search-results">
							<div class="row">
								<div class="col-xs-7" id="search_result_products"></div>
								<div class="col-delimiter"></div>
								<div class="col-xs-5" id="search_result_categories"></div>
							</div>
						</div>
					</div>
					<div class="header-search_form-shadow"></div>
				</div>
			</div>
			<div class="col-xs-4 pl-0">
				<span class="header-info_phone">
					<span class="phone">
						<i class="ca-icon ca-icon_smartphone"></i>
						(0231) 6 30 90
					</span>
					<a href="#" class="city-select" data-toggle="modal" data-target="#cities-modal" title="Выберите ваш город">
						<i class="ca-icon ca-icon_marker"></i>
						<span class="city_name">Ваш город</span> <span class="ca-icon ca-icon_arrow-down-slim"></span>
					</a>
				</span>
			</div>
		</div>
	</div>
</section>

<section id="header-navigation" class="animated nav-static-top">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 nav-col">
				<a href="<?php echo base_url();?>" class="nav-header-logo <?php if($settings['snowflake_active']['setting_value'] == 1){?>logo-winter<?php }?>"></a>
				<div class="nav-catalog-wr">
					<div class="catalog-menu-link">
						<i class="ca-icon ca-icon_menu"></i>
						Каталог товаров
						<div class="nav-catalog_categories animated">
							<?php echo Modules::run('static_block/_view', array('menu' => 'catalog_menu'));?>
						</div>
					</div>
				</div>
				<?php if($settings['black_friday_active']['setting_value'] == 1){?>
					<div class="nav-black-friday"></div>
				<?php }?>
				<ul class="nav-menu nav-menu-right">
					<li><a href="<?php echo base_url('spage/credit-menu');?>">Кредит</a></li>
					<li><a href="<?php echo base_url('spage/contacts-menu');?>">Контакты</a></li>
					<li>
						<span class="cart-b" role="button" data-toggle="collapse" data-target="#collapse-cart" aria-expanded="false">
							<i class="ca-icon ca-icon_shopping-cart"></i>
							<div class="cart-description"></div>
						</span>
						<div class="collapse collapse-cart" id="collapse-cart">
							<div class="collapse-cart-inner">
								<div class="wr-cart-items" style="max-height:200px;"></div>
								<div class="row cart-btn">
									<div class="col-sm-12">
										<a href="<?php echo base_url('cart');?>" class="btn btn-danger btn-ssm pull-left">Перейти в корзину</a>
										<span class="close-cart" title="Закрыть" role="button" data-toggle="collapse" data-target="#collapse-cart" aria-expanded="false">
											<i class="ca-icon ca-icon_remove"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section id="header-nav-mobile">
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="navbar-header">
			<div class="navbar-logo">
				<a href="<?php echo base_url();?>" class="header-logo <?php if($settings['snowflake_active']['setting_value'] == 1){?>logo-winter<?php }?>"></a>
			</div>
			<div class="navbar-items">
				<div class="row">
					<div class="col-xs-3 navbar-header-item" data-toggle="collapse" data-target=".navbar-collapse-menu" aria-expanded="false">
						<span class="ca-icon ca-icon_list"></span><span class="title"> Меню</span>
					</div>
					<div class="col-xs-3 navbar-header-item call-function" data-callback="show_search_dimmer">
						<span class="ca-icon ca-icon_search"></span><span class="title"> Пойск</span>
					</div>
					<?php if(!$this->lauth->logged_in()){?>
						<div class="col-xs-3 navbar-header-item" data-toggle="modal" data-target="#auth-modal">
							<span class="ca-icon ca-icon_user"></span><span class="title"> Кабинет</span>
						</div>			
					<?php } else{?>
						<div class="col-xs-3 navbar-header-item" data-toggle="collapse" data-target=".navbar-collapse-user" aria-expanded="false">
							<span class="ca-icon ca-icon_user"></span><span class="title"> Кабинет</span>
						</div>
					<?php }?>
					<a class="col-xs-3 navbar-header-item" href="<?php echo base_url('cart');?>">
						<span class="ca-icon ca-icon_shopping-cart"></span>
						<span class="title">Корзина</span>
						<span class="badge cart_count">0</span>
					</a>
				</div>
			</div>
		</div>
		<div class="collapse navbar-collapse navbar-collapse-menu">
			<ul class="nav navbar-nav">
				<li>
					<a href="#">
						Категории товаров
						<span class="ca-icon ca-icon_arrow-down"></span>
					</a>
					<?php echo Modules::run('static_block/_view', array('menu' => 'catalog_menu'));?>
				</li>
				<li><a href="<?php echo base_url('spage/contacts-menu');?>">Контакты и график работы</a></li>
				<li><a href="<?php echo base_url('spage/credit-menu');?>">Покупка в кредит</a></li>
				<li><a href="<?php echo base_url('spage/delivery-menu');?>">Доставка и оплата</a></li>
				<li><a href="http://service.atehno.md/">Сервис центр</a></li>
				<li><a href="<?php echo base_url('spage/service_centres');?>">Авторизированные сервис-центры</a></li>
				<li><a href="<?php echo base_url('trade-in');?>">Trade-In</a></li>
				<li><a href="<?php echo base_url('spage/return');?>">Обмен и возврат</a></li>
				<li><a href="callto:+37323163090">Позвонить нам на <b>Тел. : 0231 6-30-90</b></a></li>
			</ul>
		</div>

		<?php if($this->lauth->logged_in()){?>
			<div class="collapse navbar-collapse navbar-collapse-user">
				<ul class="nav navbar-nav">
					<?php if($this->lauth->have_right('manage_news')){?>
						<li><a href="<?php echo base_url('admin');?>">Перейти в админ панель</a></li>							
					<?php }?>
					<li><a href="<?php echo base_url('account');?>">Перейти в кабинет</a></li>
					<li><a href="<?php echo base_url('logout');?>">Выход</a></li>
				</ul>
			</div>
		<?php }?>
	</nav>
</section>

<section class="mobile-search-wr">	
	<div class="mheader-search_form">
		<div class="form-group">
			<div class="input-group">
				<input type="text" class="form-control" name="search">
				<span class="input-group-btn">
					<a href="" class="btn btn-default msearch-direct" type="button">
						<i class="ca-icon ca-icon_search"></i>
					</a>
					<span class="btn btn-default call-function" data-callback="close_search_dimmer" type="button">
						<i class="ca-icon ca-icon_remove"></i>
					</span>
				</span>
			</div>
		</div>			
		<div class="msearch-results">
			<div class="row">
				<div class="col-xs-12 col-sm-6" id="msearch_result_products"></div>
				<div class="col-xs-12 col-sm-6" id="msearch_result_categories"></div>
			</div>
		</div>
	</div>
</section>