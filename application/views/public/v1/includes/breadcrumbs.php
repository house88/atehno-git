<?php if(!empty($breadcrumbs)){?>
    <div class="breadcrumbs ">
		<ul class="breadcrumbs__list">
			<li class="breadcrumbs__item">
				<a class="breadcrumbs__lbl" href="<?php echo base_url();?>">
					<span class="ca-icon ca-icon_home"></span>
				</a>
			</li>
			<?php foreach($breadcrumbs as $breadcrumb){?>
				<li class="breadcrumbs__item">
					<a class="breadcrumbs__lbl" href="<?php echo $breadcrumb['link'];?>">
						<span><?php echo $breadcrumb['title'];?></span>
					</a>
				</li>
			<?php }?>
		</ul>
	</div>
<?php }?>
