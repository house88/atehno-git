<footer>
	<div class="container">
		<div class="row">
			<div class="col-sm-4 col-xs-12">
				<div class="footer-widget footer-widget-contacts">
					<h4>Контакты</h4>
					<ul class="list-unstyled">
						<li>
							<i class="fa fa-map-marker"></i>
							<a href="https://g.page/atehno-md?gm" target="_blank">Молдова, г. Бельцы, Штефан чел Маре 76а</a>
						</li>
						<li><i class="fa fa-phone"></i> <a href="callto:+37323163090">(0231) 6 30 90</a></li>
						
						<li><i class="fa fa-envelope"></i> <a href="mailto:info@atehno.md">info@atehno.md</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4 col-xs-12">
				<div class="footer-widget footer-widget-links">
					<?php $footer_menu = Modules::run("menu/_get_by_key", 'footer_menu'); ?>
					<?php if(!empty($footer_menu)){?>
						<h4>Информация</h4>
						<ul class="list-unstyled">
							<?php foreach($footer_menu as $menu_element){?>
								<li>
									<a href="<?php echo $menu_element['link'];?>"><?php echo $menu_element['title'];?></a>
								</li>
							<?php }?>
						</ul>
					<?php }?>
				</div>
			</div>
			<div class="col-sm-4 col-xs-12">
				<div class="footer-widget footer-widget-social">
					<h4>Следите за нами</h4>
					<ul class="list-unstyled">
						<li class="social">
							<a href="https://www.facebook.com/atehno.md" target="_blank"><i class="fa fa-facebook"></i></a>
							<a href="http://ok.ru/atehno" target="_blank"><i class="fa fa-odnoklassniki"></i></a>
							<a href="http://vk.com/club86490904" target="_blank"><i class="fa fa-vk"></i></a>
							<a href="https://www.instagram.com/atehno.md" target="_blank"><i class="fa fa-instagram"></i></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="row">
				<div class="col-sm-6">
					<p class="copyright">© Atehno 2016 - <?php echo date('Y');?> All right reserved.</p>
					<p class="copyright">"BASM" SRL, c/f: 1003602003322, Cod TVA:12000009</p>
				</div>
			</div>
		</div>
	</div>
</footer>
<div id="scrolltop" class="hidden-xs"><i class="fa fa-angle-up"></i></div>
<div class="modal fade" id="cities-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog w-700">
		<div class="modal-cities">
			<div class="row">
				<div class="col-xs-12">
					<h1>Выберите свой регион <span class="modal-close" data-dismiss="modal"><i class="ca-icon ca-icon_remove"></i></span></h1>
					<p class="delivery-wr"><i class="ca-icon ca-icon_truck"></i> доставляем заказы по всей Молдове!</p>
				</div>
				<?php $cities = Modules::run('cities/_get_cities');?>
				<?php foreach($cities as $city){?>
					<div class="col-xs-3">
						<span class="city-wr" data-city="<?php echo $city['id_city'];?>"><?php echo $city['city_name'];?> <i class="ca-icon ca-icon_remove"></i></span>	
					</div>
				<?php }?>
			</div>
		</div>
	</div>
</div>
<?php if(!$this->lauth->logged_in()){?>
	<div class="modal fade" id="auth-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog mw-350">
			<div class="modal-form-container">
				<div class="collapsed in" id="login-form">
					<h1>Авторизация</h1><br>
					<form id="modal_login_form">
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">
									<span class="ca-icon ca-icon_envelope"></span>
								</div>
								<input type="text" name="user_email" class="form-control mb-0" placeholder="Email">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">
									<span class="ca-icon ca-icon_password"></span>
								</div>
								<input type="password" name="password" class="form-control mb-0" placeholder="Пароль">
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-danger btn-lg btn-block mb-15">Войти</button>
						</div>
						<div class="form-group text-center">
							<label class="label-line"><span>или войти через</span></label>
						</div>
						<div class="form-group">
							<div class="btn-group btn-group-justified">
								<div class="btn btn-primary call-function" title="Вход через Facebook" data-callback="fb_login"><i class="fa fa-facebook"></i> facebook</div>
								<a href="#" class="btn btn-warning call-function" title="Вход через Одноклассники" data-callback="ok_login" data-link="<?php echo $this->socials_auth->get_ok_callback_url();?>"><i class="fa fa-odnoklassniki"></i> ok.ru</a>
								<a href="#" class="btn btn-primary call-function" title="Вход через Вконтакте" data-callback="vk_login" data-link="<?php echo $this->socials_auth->get_vk_callback_url();?>"><i class="fa fa-vk"></i> vk.ru</a>
							</div>
						</div>
						<div class="wr-help">
							<a href="#" class="register-form-link pull-left">Регистрация</a>
							<a href="#" class="forgot-form-link pull-right">Забыли пароль?</a>
						</div>
					</form>
				</div>
				<div class="collapse" id="register-form">
					<h1>Регистрация</h1><br>
					<form id="modal_register_form">
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">
									<span class="ca-icon ca-icon_user"></span>
								</div>
								<input type="text"name="username" class="form-control mb-0" placeholder="Имя пользователя">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">
									<span class="ca-icon ca-icon_envelope"></span>
								</div>
								<input type="text" name="email" class="form-control mb-0" placeholder="Email">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">
									<span class="ca-icon ca-icon_password"></span>
								</div>
								<input type="password" name="password" class="form-control mb-0" placeholder="Пароль">
							</div>
						</div>
						
						<?php $_cities = Modules::run('cities/_get_cities');?>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">
									<span class="ca-icon ca-icon_marker"></span>
								</div>
								<select name="user_city" class="form-control">
									<option value="">Выберите регион</option>
									<?php foreach($_cities as $_city){?>
										<option value="<?php echo $_city['id_city'];?>"><?php echo $_city['city_name'];?></option>
									<?php }?>
								</select>
							</div>
						</div>

						<div class="form-group">
							<button type="submit" class="btn btn-danger btn-lg btn-block mb-15">Регистрация</span>
						</div>
						
						<div class="wr-help">
							<a href="#" class="login-form-link pull-left">Авторизация</a>
							<a href="#" class="forgot-form-link pull-right">Востановить пароль</a>
						</div>
					</form>
				</div>
				<div class="collapse" id="forgot-form">
					<h1>Востановить пароль</h1><br>
					<form id="modal_forgot_password_form">
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">
									<span class="ca-icon ca-icon_envelope"></span>
								</div>
								<input type="text" name="email" class="form-control mb-0" placeholder="Email">
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-danger btn-lg btn-block mb-15">Востановить</button>
						</div>
						<div class="wr-help">
							<a href="#" class="login-form-link pull-left">Авторизация</a>
							<a href="#" class="register-form-link pull-right">Регистрация</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(function(){
			$('#modal_login_form').on('submit', function(e){
				var $form = $(this);
				var fdata = $form.serialize();

				$.ajax({
					type: 'POST',
					url: base_url+'auth/ajax_operations/signin',
					data: fdata,
					dataType: 'JSON',
					beforeSend: function(){
						showLoader('body');
					},
					success: function(resp){
						if(resp.mess_type == 'success'){
							location.reload(true);
						} else{
							systemMessages(resp.message, resp.mess_type);
							hideLoader('body');
						}
					}
				});
				return false;
			});

			$('#modal_register_form').on('submit', function(e){
				var $form = $(this);
				var fdata = $form.serialize();

				$.ajax({
					type: 'POST',
					url: base_url+'auth/ajax_operations/signup',
					data: fdata,
					dataType: 'JSON',
					beforeSend: function(){
						showLoader('body');
					},
					success: function(resp){
						systemMessages(resp.message, resp.mess_type);
						hideLoader('body');
						if(resp.mess_type == 'success'){
							$("#auth-modal").modal('hide');
						}
					}
				});
				return false;
			});

			$('#modal_forgot_password_form').on('submit', function(e){
				var $form = $(this);
				var fdata = $form.serialize();
				
				$.ajax({
					type: 'POST',
					url: base_url+'auth/ajax_operations/forgot_password',
					data: fdata,
					dataType: 'JSON',
					beforeSend: function(){
						showLoader('body');
					},
					success: function(resp){
						if(resp.mess_type == 'success'){
							window.location.href = base_url;
						} else{
							systemMessages(resp.message, resp.mess_type);
							hideLoader('body');
						}
					}
				});
				return false;
			});
		});
	</script>
<?php }?>

<div class="system-text-messages-b" style="<?php if(!empty($system_messages)){echo 'display: block;';}?>">	
	<div class="text-b">
		<ul>
			<?php if(!empty($system_messages)){echo $system_messages;}?>
		</ul>
	</div>
</div>
<div  class="modal fade" id="general-popup" data-keyboard="false" data-backdrop="static"></div>
<!-- BEGIN JIVOSITE CODE -->
<script type='text/javascript'>
(function(){ var widget_id = 'oxTf2oXoXM';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- END JIVOSITE CODE -->
<style>
	div#jivo-iframe-container.jivo-iframe-container-bottom{
		right:50px;
	}
</style>

<?php 
	$socials_scripts = Modules::run('static_block/_get', 'socials_scripts');
	if(!empty($socials_scripts)){
		echo $socials_scripts['block_text'];
	}	
?>

<script src="<?php echo file_modification_time('theme/accent/js/plugins_init.js');?>"></script>