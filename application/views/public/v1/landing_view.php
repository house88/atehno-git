<!DOCTYPE html>
<html lang="ru">
<head>
    <?php $this->load->view($this->theme->public_view('includes/head')); ?>
</head>
<body class="bg-white">	
    <?php $this->load->view($this->theme->public_view('includes/snowflake')); ?>
	<div class="page-wrap">
		<?php $this->load->view($this->theme->public_view('includes/header')); ?>
		<div class="landing-container">
			<?php $this->load->view($this->theme->public_view($main_content));?>
		</div>

    	<?php $this->load->view($this->theme->public_view('includes/footer')); ?>
	</div>
</body>
</html>
