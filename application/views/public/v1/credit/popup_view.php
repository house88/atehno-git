<div class="modal-dialog" role="document">
    <div class="modal-content">
        <form id="credit_form-popup">			
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="credit-header">Оформление кредита возможно по всей Молдове!</h3>
                <div class="credit-header-subtext">Кредит оформляется на безналичную стоимость <?php if($credit_type == 'product'){?>товара<?php } else{?>заказа<?php }?> &mdash; <?php echo $price;?> <?php echo $currency;?></div>
                <div class="form-group">
                    <div class="text-center">
                        <div class="credit-price-per-month">
                            <?php foreach($credit['options'] as $key_credit => $credit_option){?>
                                <span class="credit_payments" data-option="<?php echo $credit_option['months'];?>" <?php if($credit_option['months'] != $credit['default']['months']){echo 'style="display:none;"';}?>>
                                    <span class="credit_payments-months"><?php echo $credit_option['months'];?></span> платежей на <span class="credit_payments-months"><?php echo $credit_option['months'];?></span>  месяцев
                                </span>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="credit_range_slider-wr pl-15 pr-15">
                        <input id="credit-range-slider" name="credit_months" type="text" data-slider-tooltip="show" data-slider-ticks="[<?php echo $credit_months;?>]" data-slider-ticks-snap-bounds="30" ticks_positions="[<?php echo $credit_months;?>]" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <?php foreach($credit['options'] as $key_credit => $credit_option){?>
                            <span class="credit-price-per-month" data-option="<?php echo $credit_option['months'];?>" <?php if($credit_option['months'] != $credit['default']['months']){echo 'style="display:none;"';}?>>
                                <span class="credit_price"><?php echo $credit_option['credit_price_month'];?></span>
                                <span class="credit_currency"><?php echo $credit_option['credit_currency'];?>/мес.</span>
                            </span>
                        <?php }?>
                        <input type="hidden" name="item" value="<?php echo $product['id_item'];?>">
                        <span class="btn btn-danger btn-lg btn-cart ml-10 call-function" data-callback="add_to_cart_credit">
                            <i class="ca-icon ca-icon_shopping-cart"></i> <span class="hidden-xs">Купить</span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-left">
                Для оформления кредита необходимо нажать кнопку «Купить», перейти к оформлению заказа, либо продолжить покупки и добавить дополнительный товар в «Корзину». В «Корзине» выбрать способ оплаты покупок в «Кредит», заполнить анкету и отправить на рассмотрение. После этого с Вами свяжется сотрудник для уточнения данных.
            </div>
        </form>
    </div>
</div>

<script>
    var selected_slider_value = intval('<?php echo $credit['default']['months'];?>');
    var slider_values = '<?php echo $credit_months;?>';
    slider_values = slider_values.split(',');
    $.each(slider_values, function(index, value){
        slider_values[index] = intval(value);
    });

    $(function(){
        var $credit_range_slider = $('#credit-range-slider');
        $credit_range_slider.bootstrapSlider({
            value: selected_slider_value
        }).on('change', function(ev){
            var months = ev.value.newValue;
            if($.inArray(months, slider_values) > -1){
                $('.credit_payments[data-option="'+months+'"]').show().siblings().hide();
                $('.credit-price-per-month[data-option="'+months+'"]').show().siblings('.credit-price-per-month').hide();
            }
        });
    });
</script>