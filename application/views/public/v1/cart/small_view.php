<?php if(!empty($items)){?>
    <?php $priceColumn = getPriceColumn();?>
    <?php foreach($items as $item){?>
        <!-- CART ITEM - START -->
        <div class="cart-item row">
            <div class="col-sm-3">
                <img src="<?php echo base_url(getImage('files/items/thumb_500x500_'.$item['item_photo']));?>" class="img-responsive" alt="">
            </div>
            <div class="col-sm-9">
                <h4><a href="<?php echo base_url('products/'.$item['item_url']);?>"><?php echo clean_output($item['item_title']);?></a></h4>
                <?php 
                    $price = getPriceObject($item[$priceColumn['db_price_column']], [
                        'tempAmount' => $item['item_temp_price'],
                        'pcode' => $pcode
                    ]);
                ?>
                <p><?php echo $item['item_cart_qty'];?>x - <?php echo $price['display_price'] . ' <span class="currency">' . $price['currency_symbol'] . ' </span>';?></p>
                <a href="#" class="remove call-function" data-callback="remove_from_cart" data-item="<?php echo $item['id_item'];?>"><i class="fa fa-times-circle"></i></a>
            </div>
        </div>
        <!-- CART ITEM - END -->
    <?php }?>
<?php } else{?>
    <div class="empty_cart-text">Корзина пуста :(</div>
<?php }?>