<article class="cart-content">
    <?php if(!empty($basket['products'])){?>
        <form class="basket_form">
            <div class="products-order shopping-cart">
                <?php foreach($basket['products'] as $product){?>
                    <div class="row row-eq-height cart_product-wr">
                        <div class="col-xs-12 col-sm-2 col-md-1">
                            <img src="<?php echo base_url(getImage('files/items/thumb_500x500_'.$product['item_photo']));?>" class="img-responsive">
                        </div>
                        <div class="col-xs-12 col-sm-10 col-md-7">
                            <a class="fs-13" href="<?php echo base_url('products/'.$product['item_url']);?>"><?php echo clean_output($product['item_title']);?></a>
                            <p class="fs-10 mb-0">Артикул: <?php echo $product['item_code'];?>, Ид товара: <?php echo $product['item_prog_id'];?></p>
                        </div>
                        <div class="col-sm-12 visible-sm hidden-md mb-15"></div>
                        <div class="col-xs-10 col-sm-9 col-sm-offset-2 col-md-3 col-md-offset-0 text-center">
                            <?php if($product['priceObject']['has_promo_discount']){?>
                                <div class="w-45pr pull-left h-15"></div>
                                <div class="w-10pr pull-left h-15"></div>
                                <div class="w-45pr pull-left h-15 fs-12 text-left">
                                    <div class="label label-danger">Промо цена</div>
                                </div>
                            <?php }?>
                            <div class="w-45pr pull-left">
                                <div class="form-group product-quantity">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-sm pl-5 pr-5 call-function" data-callback="change_qty" data-action="minus" data-type="basket" data-item="<?php echo $product['id_item'];?>">
                                                <span class="ca-icon ca-icon_minus fs-10_i"></span>
                                            </button>
                                        </span>
                                        <input class="form-control input-sm text-center item_quantity" onchange="change_qty_input(this);" data-item="<?php echo $product['id_item'];?>" type="text" name="item_<?php echo $product['id_item'];?>_quantity" value="<?php echo $product['item_quantity'];?>"/>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-sm pl-5 pr-5 call-function" data-callback="change_qty" data-action="plus" data-type="basket" data-item="<?php echo $product['id_item'];?>">
                                                <span class="ca-icon ca-icon_plus fs-10_i"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="w-10pr pull-left h-30 lh-30">x</div>
                            <div class="w-45pr pull-left h-30 lh-30 text-left">
                                <strong><span class="item_price"><?php echo $product['priceObject']['display_price'];?></span> <span class="currency"><?php echo $product['priceObject']['currency_symbol'];?></span></strong>
                                <input type="hidden" name="item_price" value="<?php echo $product['priceObject']['display_price'];?>">
                            </div>
                        </div>
                        <div class="col-xs-2 col-sm-1 col-md-1 text-right h-30 <?php if($product['priceObject']['has_promo_discount']){?>pt-15<?php }?>">
                            <a href="#" class="btn btn-danger btn-xs mt-4 call-function" data-callback="remove_from_big_cart" data-item="<?php echo $product['id_item'];?>">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                        <div class="col-xs-12"><hr class="mt-15 mb-15"></div>
                    </div>
                    <!-- CART ITEM - END -->
                <?php }?>
            </div>
            <div class="row cart-blocks-order">
                <div class="order-2 col-sm-7">
                    <div class="row">
                        <?php if(true === $basket['existPromoCode']){?>
                            <fieldset>
                                <legend class="scheduler-border">Промо код</legend>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <?php if(empty($basket['promoCode'])){?>
                                                <input name="pcode" type="text" class="form-control" id="inputPcode" placeholder="ex. PROMO" value="">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-danger call-function" data-callback="cart_apply_pcode" type="button">Применить</button>
                                                </span>
                                            <?php } else{?>
                                                <div class="form-control"><?php echo $basket['promoCode']['pcode_hash'];?></div>
                                                <span class="input-group-addon success">
                                                    <span class="ca-icon ca-icon_ok"></span>
                                                </span>
                                            <?php }?>
                                        </div>
                                        <span class="fs-12"><span class="fw-bold text-danger">Внимининание!</span> Все кредиты 0% и бонсные баллы при применении промокода не приминяються.</span>
                                    </div>
                                </div>
                            </fieldset>
                        <?php }?>
                        <fieldset>
                            <legend class="scheduler-border">Контакты</legend>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="inputName">Имя</label>
                                    <input name="user_name" type="text" class="form-control" id="inputName" placeholder="Имя" value="<?php echo $basket['userInfo']['user_nicename'] ?? '';?>">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="inputEmail">Email</label>
                                    <input name="user_email" type="text" class="form-control" id="inputEmail" placeholder="Email" value="<?php echo $basket['userInfo']['user_email'] ?? '';?>">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="inputPhone">Телефон</label>
                                    <input name="user_phone" type="text" class="form-control" id="inputPhone" placeholder="Телефон" value="<?php echo $basket['userInfo']['user_phone_display'] ?? '';?>">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="inputAddress">Адрес</label>
                                    <input name="user_address" type="text" class="form-control" id="inputAddress" placeholder="Адрес" value="<?php echo $basket['userInfo']['user_address'] ?? '';?>">
                                </div>
                            </div>                    
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="inputComment">Комментарий к заказу</label>
                                    <textarea name="user_comment" style="resize:none;" rows="5" class="form-control" id="inputComment" placeholder="Коментарий"></textarea>
                                </div>
                            </div>                    
                        </fieldset>
                        <fieldset>
                            <legend class="scheduler-border">Способ доставки</legend>
                            <div class="col-xs-12">
                                <?php if(!empty($basket['deliveryOptions'])){?>
                                    <?php foreach($basket['deliveryOptions'] as $deliveryOption){?>
                                        <div class="form-group mt-10">
                                            <label class="cur-pointer">
                                                <input 
                                                    type="radio" 
                                                    name="delivery_method" 
                                                    value="<?php echo $deliveryOption['id_delivery'];?>" 
                                                    data-payment-available="<?php echo !empty($basket['methodsDeliveryPayment'][$deliveryOption['id_delivery']]) ? implode(',', $basket['methodsDeliveryPayment'][$deliveryOption['id_delivery']]) : '';?>"
                                                    <?php echo get_choice('checked', $basket['defaultDeliveryMethodId'] == $deliveryOption['id_delivery']);?>> 
                                                <span class="lh-22 vam">
                                                    <?php echo $deliveryOption['delivery_title'];?> <?php if($deliveryOption['amount'] > 0){?><small>( +<?php echo numberFormat($deliveryOption['amount']);?> <?php echo $basket['currency']['symbol'];?> )</small><?php }?>
                                                </span>
                                            </label>
                                        </div>
                                    <?php }?>
                                <?php }?>
                                <p class="text-right fs-12"><a href="<?php echo base_url('spage/delivery-menu');?>">Подробнее о доставке <span class="ca-icon ca-icon_arrow-right fs-8"></span></a></p>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend class="scheduler-border">Способ оплаты</legend>
                            <div class="col-xs-12">
                                <?php if(!empty($basket['paymentOptions'])){?>
                                    <?php foreach($basket['paymentOptions'] as $paymentOption){?>
                                        <div class="form-group mt-10">
                                            <?php if(0 === (int) $paymentOption['payment_by_credit']){?>
                                                <label class="cur-pointer">
                                                    <input 
                                                        type="radio" 
                                                        class="input_payment_option" 
                                                        name="payment_method" 
                                                        value="<?php echo $paymentOption['id_payment'];?>" 
                                                        data-credit="<?php echo $paymentOption['payment_by_credit'];?>" 
                                                        data-delivery-available="<?php echo !empty($basket['methodsPaymentDelivery'][$paymentOption['id_payment']]) ? implode(',', $basket['methodsPaymentDelivery'][$paymentOption['id_payment']]) : '';?>" 
                                                        data-payment-add="<?php echo $paymentOption['paymentAddPercent'];?>" 
                                                        data-apply-bonus="<?php echo $basket['applyBonusAmount'];?>"> 
                                                    <span>
                                                        <?php echo $paymentOption['payment_title'];?>
                                                        <?php if($paymentOption['paymentAddPercent'] > 0){?><small>( +<?php echo numberFormat($paymentOption['paymentAddAmount']);?> <?php echo $basket['currency']['symbol'];?> )</small><?php }?>
                                                    </span>
                                                </label>
                                            <?php } else{?>
                                                <?php if(false !== $basket['credit'] && !empty($basket['credit'])){?>
                                                    <?php $basket['defaultPaymentMethodId'] = (int) $paymentOption['id_payment'];?>
                                                    <label class="cur-pointer">
                                                        <input 
                                                            type="radio" 
                                                            class="input_payment_option" 
                                                            name="payment_method" 
                                                            value="<?php echo $paymentOption['id_payment'];?>" 
                                                            data-credit="<?php echo $paymentOption['payment_by_credit'];?>" 
                                                            data-delivery-available="<?php echo !empty($basket['methodsPaymentDelivery'][$paymentOption['id_payment']]) ? implode(',', $basket['methodsPaymentDelivery'][$paymentOption['id_payment']]) : '';?>" 
                                                            data-payment-add="<?php echo $paymentOption['paymentAddPercent'];?>" 
                                                            data-apply-bonus="<?php echo $basket['credit']['default']['appliedBonusAmount'];?>"
                                                            checked> 
                                                        <span class="lh-22 vam">
                                                            <?php echo $paymentOption['payment_title'];?>
                                                        </span>
                                                    </label>
                                                    <div class="panel panel-default ml-25" id="cart_credit-wr" style="display:block;">
                                                        <div class="panel-body">
                                                            <h3 class="credit-header mt-0">Оформление кредита возможно по всей Молдове!</h3>
                                                            <div class="credit-header-subtext">Кредит оформляется на безналичную стоимость заказа &mdash; <span id="js-basket-credit-final-amount"><?php echo $basket['credit']['default']['credit_price'];?></span> <?php echo $basket['credit']['default']['currency'];?></div>
                                                            <div class="form-group">
                                                                <div class="text-center">
                                                                    <div class="credit-price-per-month">
                                                                        <?php foreach($basket['credit']['options'] as $creditOption){?>
                                                                            <span class="credit_payments" data-option="<?php echo $creditOption['months'];?>" <?php if($creditOption['months'] != $basket['credit']['default']['months']){echo 'style="display:none;"';}?>>
                                                                                <span class="credit_payments-months"><?php echo $creditOption['months'];?></span> платежей на <span class="credit_payments-months"><?php echo $creditOption['months'];?></span>  месяцев
                                                                            </span>
                                                                        <?php }?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="credit_range_slider-wr pl-15 pr-15">
                                                                    <input id="credit-range-slider" name="credit_months" type="text" data-slider-tooltip="show" data-slider-ticks="[<?php echo implode(',', @$basket['credit']['months_list']);?>]" data-slider-ticks-snap-bounds="30" ticks_positions="[<?php echo implode(',', @$basket['credit']['months_list']);?>]" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group mb-0">
                                                                <div class="text-center">
                                                                    <span class="credit-price-per-month">
                                                                        <span class="credit_price" id="js-credit-option-monthly-amount">0.00</span>
                                                                        <span class="credit_currency"><?php echo $creditOption['currency'];?>/мес.</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                                
                                                <?php }?>
                                            <?php }?>
                                        </div>
                                    <?php }?>
                                <?php }?>
                                <div id="js-payment-delivery-relation-text"></div>
                                <p class="text-right fs-12"><a href="<?php echo base_url();?>payment">Подробнее о способах оплаты <span class="ca-icon ca-icon_arrow-right fs-8"></span></a></p>
                            </div>
                        </fieldset>
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-danger pull-right call-function" data-callback="create_order">Оформить заказ</button>
                        </div>
                    </div>
                </div>
                <div class="order-1 col-sm-5">
                    <div class="row">
                        <div class="col-xs-12 text-right fs-12">
                            <div class="cart-footer">
                                <span class="cart-footer__text">Сумма заказа:</span>
                                <span class="cart-footer__value">
                                    <span class="value">
                                        <strong>
                                            <span class="final_order_price" id="js-basket-total-amount">0</span>                                
                                            <span class="currency"><?php echo $basket['currency']['symbol'];?></span>
                                        </strong>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 text-right fs-12" id="js-delivery-container" style="display:none;">
                            <div class="cart-footer">
                                <span class="cart-footer__text">Доставка:</span>
                                <span class="cart-footer__value">
                                    <span class="value">
                                        <strong>
                                            + <span class="price" id="js-delivery-price">0</span>
                                            <span class="currency"><?php echo $basket['currency']['symbol'];?></span>
                                        </strong>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 text-right fs-12" id="js-payment-percent-container" style="display:none;">
                            <div class="cart-footer">
                                <span class="cart-footer__text">Оплата, %:</span>
                                <span class="cart-footer__value">
                                    <span class="value">
                                        <strong>
                                            + <span class="price" id="js-payment-percent-price">0</span>
                                            <span class="currency"><?php echo $basket['currency']['symbol'];?></span>
                                        </strong>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 text-right fs-12" id="js-basket-apply-bonus-container" style="display:none;">
                            <div class="cart-footer">
                                <span class="cart-footer__text">Бонусные баллы:</span>
                                <span class="cart-footer__value">
                                    <span class="value">
                                        <strong>
                                            - <span class="final_order_bonus" id="js-basket-apply-bonus-amount">0</span>
                                            <span class="currency"><?php echo $basket['currency']['symbol'];?></span>
                                        </strong>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 text-right">
                            <div class="cart-footer cart-footer__border-top">
                                <span class="cart-footer__text">К оплате:</span>
                                <span class="cart-footer__value">
                                    <span class="value">
                                        <strong>
                                            <span class="final_order_price_bonus" id="js-basket-final-amount">0</span>
                                            <span class="currency"><?php echo $basket['currency']['symbol'];?></span>
                                        </strong>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <?php if(!$this->lauth->logged_in()){?>
                            <div class="col-xs-12 text-right fs-12" id="js-basket-apply-bonus-disclaimer-container">
                                <span class="cart-footer__info text-danger"><sup>*</sup>Чтобы воспользоваться бонусными балами или остатком бонусных балов при следуюших покупках просим вас <a href="#" class="call-function" data-callback="show_auth_popup" data-target="#auth-modal" data-action=".login-form-link">авторизоваться</a> или <a href="#" class="call-function" data-callback="show_auth_popup" data-target="#auth-modal" data-action=".register-form-link">зарегистрироваться</a>!</span>
                            </div>                            
                        <?php } else{?>
                            <div class="col-xs-12 text-right fs-12" id="js-next-order-cashback-container" style="display:none;">
                                <div class="cart-footer text-danger">
                                    <span class="cart-footer__text cashback-text"><sup>*</sup>Остаток бонусных балов на следующие покупки:</span>
                                    <span class="cart-footer__value">
                                        <span class="value cashback-text">
                                            <span id="js-next-order-cashback-price">0</span> лей
                                        </span>
                                    </span>
                                </div>                      
                            </div>
                            <div class="col-xs-12 text-right fs-12" id="js-basket-apply-bonus-disclaimer-container" style="display:none;">
                                <span class="cart-footer__info text-primary"><sup>*</sup>Остаток бонусных балов попадет в личный кабинет и будет использован при следующих заказах.</span>
                            </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </form>
        <div id="js-payment-form-component" style="display:none;"></div>

        <script>
            update_big_cart = true;
            var creditAvailable = Boolean(~~intval("<?php echo (!empty($basket['credit'])) ? 1 : 0;?>"));
            var basketRelationSettings = <?php echo json_encode($basket['basketRelationSettings']);?>;
            var relationPaymentDelivery = <?php echo json_encode($basket['relationPaymentDelivery']);?>;
            var settingsDefaultKey = creditAvailable === true ? 'defaultCredit' : 'default';
            var updateSettingsKey = creditAvailable === true ? 'defaultCredit' : null;
            var paymentByCredit = creditAvailable;
            var selectedCreditMonths = 0;
            var creditMonths = [];

            var deliveryMethodId = intval("<?php echo $basket['defaultDeliveryMethodId'];?>");
            var paymentMethodId = intval("<?php echo $basket['defaultPaymentMethodId'];?>");

			var inputPaymentOption;
            var inputDeliveryOption;
            
            <?php if(!empty($basket['credit'])){?>
                if(true === creditAvailable){
                    selectedCreditMonths = intval('<?php echo $basket['credit']['default']['months'] ?? 0;?>');
                    var creditMonths = '<?php echo implode(',', @$basket['credit']['months_list']);?>';
                    creditMonths = creditMonths.split(',');
                    $.each(creditMonths, function(index, value){
                        creditMonths[index] = intval(value);
                    });
                }
            <?php }?>


            $(function(){
				inputDeliveryOption = $('input[name="delivery_method"]');
				inputPaymentOption = $('.input_payment_option');;

                $('#inputPhone').inputmask({"mask": "099999999"});

                if(creditAvailable){
                    var creditRangeSlider = $('#credit-range-slider');
                    creditRangeSlider.bootstrapSlider({
                        value: selectedCreditMonths
                    }).on('change', function(ev){
                        selectedCreditMonths = 0;
                        paymentByCredit = false;
                        var months = ev.value.newValue;
                        if($.inArray(months, creditMonths) > -1){
                            selectedCreditMonths = months;
                            paymentByCredit = true;
                            $('.credit_payments-months').text(selectedCreditMonths);
                            // $('.credit_payments[data-option="'+months+'"]').show().siblings().hide();
                            // $('.credit-price-per-month[data-option="'+months+'"]').show().siblings('.credit-price-per-month').hide();
                        }

                        updateBasketBySettings();
                    });
				}
                
                inputDeliveryOption.on('ifChecked', function(event){
                    deliveryMethodId = intval($(this).val());
					
					var paymentsAvailableIds = $(this).data('payment-available').toString();
					var paymentsMethodsAvailable = paymentsAvailableIds.split(',');
					inputPaymentOption.each(function(){
						var paymentId = $(this).val();						
						
						if($.inArray( paymentId, paymentsMethodsAvailable ) === -1){
							$(this).prop('checked', false).closest('.form-group').hide();
						} else{
							$(this).closest('.form-group').show();
						}
					});

                    updateBasketBySettings();
                });

                inputPaymentOption.on('ifChecked', function(event){
                    paymentByCredit = ($(this).data('credit') === 1) ? true : false;
                    paymentMethodId = intval($(this).val());
					
					var deliveryAvailableIds = $(this).data('delivery-available').toString();
                    var deliveryMethodsAvailable = deliveryAvailableIds.split(',');
                    deliveryMethodsAvailable = deliveryMethodsAvailable.map(x => intval(x));
                    var defaultDeliveryIsSelected = $.inArray(deliveryMethodId, deliveryMethodsAvailable ) !== -1;
					inputDeliveryOption.each(function(){
						var deliveryId = intval($(this).val());	
						if($.inArray( deliveryId, deliveryMethodsAvailable ) === -1){
							$(this).prop('checked', false).closest('.form-group').hide();
						} else{
                            if(false === defaultDeliveryIsSelected){
                                $(this).prop('checked', true);
                                $(this).trigger('change');
                                defaultDeliveryIsSelected = true;
                            } 
                            $(this).closest('.form-group').show();
                        }
                    });

                    updateBasketBySettings();
                });

                updateBasketBySettings();
            });

            function updateBasketBySettings(){
                updateSettingsKey = paymentMethodId + '_' + deliveryMethodId;
                
                if(true === paymentByCredit){
                    updateSettingsKey = selectedCreditMonths > 0 ? updateSettingsKey + '-' + selectedCreditMonths + '-months' : 'defaultCredit';
                }

                if(undefined === basketRelationSettings[updateSettingsKey]){
                    updateSettingsKey = true === paymentByCredit ? settingsDefaultKey : 'default';
                }

                var settings = basketRelationSettings[updateSettingsKey];
                console.log(settings);

                if(settings.credit === true){
                    $('#cart_credit-wr').show();
                } else{
                    $('#cart_credit-wr').hide();
                }

                $('#js-next-order-cashback-container').hide();
                $('#js-next-order-cashback-price').text('0');
                if(settings.cashbackAmount.amount > 0){
                    $('#js-next-order-cashback-container').show();
                    $('#js-next-order-cashback-price').text(settings.cashbackAmount.text);
                }

                $('#js-delivery-container').hide();
                $('#js-delivery-price').text('0');
                if(settings.deliveryPrice.amount > 0){
                    $('#js-delivery-container').show();
                    $('#js-delivery-price').text(settings.deliveryPrice.text);
                }

                $('#js-payment-percent-container').hide();
                $('#js-payment-percent-price').text('0');
                if(settings.paymentAddPrice.amount > 0){
                    $('#js-payment-percent-container').show();
                    $('#js-payment-percent-price').text(settings.paymentAddPrice.text);
                }

                $('#js-basket-total-amount').text('0');
                if(settings.price.amount > 0){
                    $('#js-basket-total-amount').text(settings.price.text);
                }

                $('#js-basket-apply-bonus-container').hide();
                $('#js-basket-apply-bonus-amount').text('0');
                if(settings.applyBonus.amount > 0){
                    $('#js-basket-apply-bonus-container').show();
                    $('#js-basket-apply-bonus-amount').text(settings.applyBonus.text);
                }

                $('#js-basket-final-amount').text('0');
                $('#js-basket-credit-final-amount').text('0');
                if(settings.priceFinal.amount > 0){
                    $('#js-basket-final-amount').text(settings.priceFinal.text);
                    $('#js-basket-credit-final-amount').text(settings.priceFinal.text);
                }

                $('#js-credit-option-monthly-amount').text('0');
                if(undefined !== settings.creditMonthlyAmount){
                    $('#js-credit-option-monthly-amount').text(settings.creditMonthlyAmount.text);
                }

                var relationPaymentDeliveryTextKey = paymentMethodId + '_' + deliveryMethodId;
                $('#js-payment-delivery-relation-text').html('');
                if(relationPaymentDelivery.hasOwnProperty(relationPaymentDeliveryTextKey)){
                    $('#js-payment-delivery-relation-text').html(relationPaymentDelivery[relationPaymentDeliveryTextKey]);
                }
            }
        </script>
    <?php } else{?>
        Ваша корзина пуста. Помогите ей наполниться, вернитесь к покупкам!
    <?php }?>
</article>