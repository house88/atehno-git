<div class="modal-dialog" role="document">
    <div class="modal-content">
        <form id="credit_form-popup">			
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="credit-header text-left">Заказать осмотр товара</h3>
                <div class="credit-header-subtext text-left">Вы можете заказать осмотр этого товара и после принять решение о его покупке. После вашего запроса наши менеджеры согласуют с вами время, когда с ним можно будет ознакомится в нашем шоу-руме.</div>
                <div class="row mt-15">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="ca-icon ca-icon_user"></i></span>
                                <input type="text" class="form-control" name="showroom_username" placeholder="Имя">                            
                            </div>
                        </div>                    
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <div class="input-group date" id="order_showroom_datepicker">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control" readonly name="showroom_date" placeholder="Дата">
                            </div>
                        </div>                    
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="ca-icon ca-icon_envelope"></i></span>
                                <input type="text" class="form-control" name="showroom_user_email" placeholder="Email">
                            </div>
                        </div>                    
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="ca-icon ca-icon_phone"></i></span>
                                <input type="text" class="form-control" name="showroom_user_phone" placeholder="Телефон">
                            </div>
                        </div>                    
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <?php echo $recaptcha_widget;?>
                        </div>               
                    </div>
                </div>
            </div>
            <div class="modal-footer text-left">
                <input type="hidden" name="item" value="<?php echo $id_item;?>">
                <span class="btn btn-danger call-function" data-callback="order_showroom_callback">Заказать осмотр</span>
            </div>
        </form>
    </div>
</div>
<script src='https://www.google.com/recaptcha/api.js'></script>

<script>
    var current_date = new Date();
    $(function(){
        $('#order_showroom_datepicker').datetimepicker({
            sideBySide: true,
            locale: 'ru',
            tooltips: i18n_bootstrap_datetimepisker.ru,
            minDate: current_date,
            defaultDate: current_date,
            ignoreReadonly: true
        });
    });

    var order_showroom_callback = function(btn){
        var $this = $(btn);
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: base_url+'showroom/ajax_operations/order_showroom',
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general-popup').modal('hide').html("");
                }
            }
        });
        return false;
    }
</script>