<div class="row pb-30">
    <div class="col-xs-12">
        <div class="p-15">
            <h2 class="heading-title"><span><?php echo $page['page_title'];?></span></h2>
            <?php echo $page['page_description'];?>
        </div>
    </div>
</div>