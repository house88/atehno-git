<?php if(!empty($brands)){?>
    <?php foreach($brands as $brand_info){?>
        <li data-property-value="<?php echo $brand_info['id_brand'];?>">
            <label>
                <input class="js_filter" type="checkbox" <?php echo set_checkbox('fb', $brand_info['id_brand'], isset($properties_selected['fb']) && in_array($brand_info['id_brand'], $properties_selected['fb']));?> name="fb" value="<?php echo $brand_info['id_brand'];?>" data-title="Брэнд" data-value-text="<?php echo $brand_info['brand_title'];?>"> 
                <?php echo $brand_info['brand_title'];?>
                <span class="filter-counter">(<?php echo (isset($available_properties_values_counters['fb'][$brand_info['id_brand']]))?$available_properties_values_counters['fb'][$brand_info['id_brand']]:$brand_info['items_count'];?>)</span>
            </label>
        </li>
    <?php }?>
<?php }?>