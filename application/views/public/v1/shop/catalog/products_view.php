<div class="row products-list products_block <?php echo (($product_view = get_cookie('_product_view'))?$product_view:'grid');?>" id="products">
	<?php if(!empty($products)){?>
		<?php $this->load->view($this->theme->public_view('shop/catalog/products_list_view'));?>
	<?php } else{?>
		<div class="col-xs-12">
			<div class="alert alert-info mb-0" role="alert">Товары не найдены!</div>
		</div>
	<?php }?>
</div>
<div class="row">
	<div class="col-xs-12 pl-0 pr-0">
		<div class="pagination-wrapper">
			<?php if($pagination != ''){?>
					<?php echo $pagination;?>
			<?php }?>	
		</div>
	</div>
</div>