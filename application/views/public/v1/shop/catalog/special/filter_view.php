<?php if(!empty($search_categories)){?>
    <ul class="catalog_mode_categories-list">
        <?php foreach($search_categories as $search_category){?>
            <li class="<?php if(!empty($category) && $category['category_id'] == $search_category['category']->category_id){echo 'active';}?>">
                <a class="parent-title" href="<?php echo base_url('catalog_mode/'.$catalog_mode.'?category='.$search_category['category']->url);?>">
                    <?php echo $search_category['category']->category_title;?>             
                </a>
                <span class="products-count">(<?php echo $search_category['search_count'];?>)</span>
            </li>
        <?php }?>
    </ul>
<?php }?>
<ul class="filters">    
    <li class="clearfix">
        <div class="jsfilters-container pt-15"></div>
    </li>
    <li class="filter">
        <h3 class="filter-heading">Цена</h3>
        <div class="pl-10_i jsfilter-range">
            <input class="range-slider display-n" type="text" name="bprice" value="" data-min="<?php echo $min_price;?>" data-max="<?php echo $max_price;?>" <?php echo ((!empty($properties_selected['fp']))?'data-fvalue="['.implode(',', $properties_selected['fp']).']"':'');?>/>
            <span class="display-n">
                <input class="js_filter" data-jsfilter-type="range" data-title="Цена" type="text" name="fp" value="<?php echo ((!empty($properties_selected['fp']))?implode('-', $properties_selected['fp']):'');?>"/>
            </span>
        </div>
    </li>
    <?php if($this->lauth->group_view_stocks()){?>
        <li class="filter" data-property-type="stock" data-property="fstock">
            <h3 class="filter-heading">Наличие</h3>
            <ul class="filter-list">
                <li data-property-value="yes">
                    <label>
                        <input class="js_filter" type="checkbox" <?php echo set_checkbox('fstock', 'yes', isset($properties_selected['fstock']) && in_array('yes', $properties_selected['fstock']));?> name="fstock" value="yes" data-title="Наличие" data-value-text="Есть"> 
                        Есть
                        <span class="filter-counter">(<?php echo (isset($available_properties_values_counters['fstock']['yes']))?$available_properties_values_counters['fstock']['yes']:0;?>)</span>
                    </label>
                </li>
                <li data-property-value="no">
                    <label>
                        <input class="js_filter" type="checkbox" <?php echo set_checkbox('fstock', 'no', isset($properties_selected['fstock']) && in_array('no', $properties_selected['fstock']));?> name="fstock" value="no" data-title="Наличие" data-value-text="Нет"> 
                        Нет
                        <span class="filter-counter">(<?php echo (isset($available_properties_values_counters['fstock']['no']))?$available_properties_values_counters['fstock']['no']:0;?>)</span>
                    </label>
                </li>
                <li data-property-value="ordering">
                    <label>
                        <input class="js_filter" type="checkbox" <?php echo set_checkbox('fstock', 'ordering', isset($properties_selected['fstock']) && in_array('ordering', $properties_selected['fstock']));?> name="fstock" value="ordering" data-title="Наличие" data-value-text="Под заказ"> 
                        Под заказ
                        <span class="filter-counter">(<?php echo (isset($available_properties_values_counters['fstock']['ordering']))?$available_properties_values_counters['fstock']['ordering']:0;?>)</span>
                    </label>
                </li>
            </ul>
        </li>
    <?php }?>
</ul>