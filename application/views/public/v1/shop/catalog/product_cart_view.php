<?php
    $item_is_visible = true;
    $item_suppliers = json_decode($item['item_suppliers'], true);
    if($this->lauth->group_view_stocks()){
        if($item['item_quantity'] <= 0 && empty($item_suppliers) || $item['item_visible'] == 0){
            $item_is_visible = false;
        }
    } else{
        $item_is_visible = $item['item_visible'] == 1;
    }
?>
<article class="product-item <?php if(!$item_is_visible){?>product-invisible<?php }?>">
    <?php 
        $priceColumn = getPriceColumn();
        $price = getPriceObject($item[$priceColumn['db_price_column']], [
            'tempAmount' => $item['item_temp_price']
        ]);
    ?>
    <div class="product-inside">
        <?php if($this->lauth->is_admin()){?>
            <div class="admin-tools_wr">
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                        <i class="ca-icon ca-icon_more "></i>
                    </button>
                    <ul class="dropdown-menu  dropdown-menu-right">
                        <?php if($this->lauth->have_right_or('edit_items,edit_items_simple,delete_items')){?>
                            <li class="text-center">
                                <div class="btn-group">
                                    <?php if($this->lauth->have_right_or('edit_items,edit_items_simple')){?>
                                        <a href="<?php echo base_url('admin/items/edit/'.$item['id_item'])?>" class="btn btn-primary" title="Редактировать">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    <?php }?>
                                    <?php if($this->lauth->have_right('delete_items')){?>
                                        <a href="#" class="btn btn-danger confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить товар?" data-callback="delete_item" data-item="<?php echo $item['id_item'];?>">
                                            <i class="ca-icon ca-icon_remove"></i>
                                        </a>
                                    <?php }?>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                        <?php }?>                        
                        <?php if($this->lauth->have_right('manage_items_marketing_fields')){?>
                            <li class="text-center w-170">
                                <div class="btn-group">
                                    <?php if($item['item_visible'] == 1){?>
                                        <a href="#" class="btn btn-primary btn-xs confirm-dialog" title="Запретить показ на сайте" data-message="Вы уверены что хотите запретить показ на сайте?" data-callback="change_marketing_status" data-marketing="visible" data-item="<?php echo $item['id_item'];?>">
                                            <i class="ca-icon ca-icon_eye"></i>
                                        </a>
                                    <?php } else{?>
                                        <a href="#" class="btn btn-default btn-xs confirm-dialog" title="Установить показ на сайте" data-message="Вы уверены что хотите установить показ на сайте?" data-callback="change_marketing_status" data-marketing="visible" data-item="<?php echo $item['id_item'];?>">
                                            <i class="ca-icon ca-icon_eye"></i>
                                        </a>
                                    <?php }?>
                                    <?php if($item['item_hit'] == 1){?>
                                        <a href="#" class="btn btn-primary btn-xs confirm-dialog" title="Снять признак Хит продаж" data-message="Вы уверены что хотите снять признак Хит продаж?" data-callback="change_marketing_status" data-marketing="hit" data-item="<?php echo $item['id_item'];?>">
                                            <i class="ca-icon ca-icon_fire"></i>
                                        </a>
                                    <?php } else{?>
                                        <a href="#" class="btn btn-default btn-xs confirm-dialog" title="Установить признак Хит продаж" data-message="Вы уверены что хотите установить признак Хит продаж?" data-callback="change_marketing_status" data-marketing="hit" data-item="<?php echo $item['id_item'];?>">
                                            <i class="ca-icon ca-icon_fire"></i>
                                        </a>
                                    <?php }?>
                                    <?php if($item['item_newest'] == 1){?>
                                        <a href="#" class="btn btn-primary btn-xs confirm-dialog" title="Снять признак Новинка" data-message="Вы уверены что хотите снять признак Новинка?" data-callback="change_marketing_status" data-marketing="newest" data-item="<?php echo $item['id_item'];?>">
                                            <i class="ca-icon ca-icon_gift"></i>
                                        </a>
                                    <?php } else{?>
                                        <a href="#" class="btn btn-default btn-xs confirm-dialog" title="Установить признак Новинка" data-message="Вы уверены что хотите установить признак Новинка?" data-callback="change_marketing_status" data-marketing="newest" data-item="<?php echo $item['id_item'];?>">
                                            <i class="ca-icon ca-icon_gift"></i>
                                        </a>
                                    <?php }?>
                                    <?php if($item['item_action'] == 1){?>
                                        <a href="#" class="btn btn-primary btn-xs confirm-dialog" title="Снять признак Акция" data-message="Вы уверены что хотите снять признак Акция?" data-callback="change_marketing_status" data-marketing="action" data-item="<?php echo $item['id_item'];?>">
                                            <i class="ca-icon ca-icon_star-full"></i>
                                        </a>
                                    <?php } else{?>
                                        <a href="#" class="btn btn-default btn-xs confirm-dialog" title="Установить признак Акция" data-message="Вы уверены что хотите установить признак Акция?" data-callback="change_marketing_status" data-marketing="action" data-item="<?php echo $item['id_item'];?>">
                                            <i class="ca-icon ca-icon_star-full"></i>
                                        </a>
                                    <?php }?>
                                    <?php if($item['item_popular'] == 1){?>
                                        <a href="#" class="btn btn-primary btn-xs confirm-dialog" title="Снять признак Популярный" data-message="Вы уверены что хотите снять признак Популярный?" data-callback="change_marketing_status" data-marketing="popular" data-item="<?php echo $item['id_item'];?>">
                                            <i class="ca-icon ca-icon_like"></i>
                                        </a>
                                    <?php } else{?>
                                        <a href="#" class="btn btn-default btn-xs confirm-dialog" title="Установить признак Популярный" data-message="Вы уверены что хотите установить признак Популярный?" data-callback="change_marketing_status" data-marketing="popular" data-item="<?php echo $item['id_item'];?>">
                                            <i class="ca-icon ca-icon_like"></i>
                                        </a>
                                    <?php }?>
                                    <?php if($item['item_commented'] == 1){?>
                                        <a href="#" class="btn btn-primary btn-xs confirm-dialog" title="Снять признак Обсуждаемый" data-message="Вы уверены что хотите снять признак Обсуждаемый?" data-callback="change_marketing_status" data-marketing="commented" data-item="<?php echo $item['id_item'];?>">
                                            <i class="ca-icon ca-icon_papers"></i>
                                        </a>
                                    <?php } else{?>
                                        <a href="#" class="btn btn-default btn-xs confirm-dialog" title="Установить признак Обсуждаемый" data-message="Вы уверены что хотите установить признак Обсуждаемый?" data-callback="change_marketing_status" data-marketing="commented" data-item="<?php echo $item['id_item'];?>">
                                            <i class="ca-icon ca-icon_papers"></i>
                                        </a>
                                    <?php }?>
                                    <?php if($item['item_on_home'] == 1){?>
                                        <a href="#" class="btn btn-primary btn-xs confirm-dialog" title="Снять признак Показать на главной" data-message="Вы уверены что хотите снять признак Показать на главной?" data-callback="change_marketing_status" data-marketing="on_home" data-item="<?php echo $item['id_item'];?>">
                                            <i class="ca-icon ca-icon_home"></i>
                                        </a>
                                    <?php } else{?>
                                        <a href="#" class="btn btn-default btn-xs confirm-dialog" title="Установить признак Показать на главной" data-message="Вы уверены что хотите установить признак Показать на главной?" data-callback="change_marketing_status" data-marketing="on_home" data-item="<?php echo $item['id_item'];?>">
                                            <i class="ca-icon ca-icon_home"></i>
                                        </a>
                                    <?php }?>
                                </div>
                            </li>
                        <?php }?>
                    </ul>
                </div>
            </div>
        <?php }?>

        <?php if($item_is_visible){?>
            <?php if($item['item_quantity'] > 0){?>
                <div class="product-available-label item-available" title="В наличии"></div>
            <?php } elseif(!empty($item_suppliers)){?>
                <div class="product-available-label item-only-order" title="Под заказ"></div>
            <?php }?>
        <?php }?>

        <div class="product-labels">
            <?php if($item['item_zero_credit'] == 1){?>
                <span class="product_label label-bonus">Кредит 0%<i class="ca-icon ca-icon_label-end"></i></span>
            <?php }?>
            <?php if($item['item_cashback_price'] > 0 && $this->lauth->group_view_cashback()){?>
                <span class="product_label label-bonus">+ Подарок<br/><?php echo numberFormat($item['item_cashback_price'], false);?> <span class="currency"><?php echo $price['currency_symbol'];?></span> <i class="ca-icon ca-icon_label-end"></i></span>            
            <?php }?>
            <?php if($item['item_action'] == 1){?>
                <span class="product_label label-actional">Акция <i class="ca-icon ca-icon_label-end"></i></span>            
            <?php }?>
            <?php if($item['item_newest'] == 1){?>
                <span class="product_label label-newest">Новинка <i class="ca-icon ca-icon_label-end"></i></span>            
            <?php }?>
            <?php if($item['item_hit'] == 1){?>
                <span class="product_label label-hit">Хит <i class="ca-icon ca-icon_label-end"></i></span>
            <?php }?>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <div class="product-overlay">
                    <a class="img-wr" href="<?php echo base_url('products/'.$item['item_url']);?>">
                        <img src="<?php echo base_url(getImage('files/items/thumb_500x500_'.$item['item_photo']));?>" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="product-body clearfix">
                    <?php if($item['item_hide_price'] == 0){?>
                        <span class="price clearfix">
                            <?php if($price['display_price_old'] > 0){?>
                                <del>
                                    <span class="amount"><?php echo $price['display_price_old'] . '<span class="currency">' . $price['currency_symbol'] . ' </span>';?></span>
                                </del>                        
                            <?php }?>
                            <ins><span class="amount"><?php echo $price['display_price'] . '<span class="currency">' . $price['currency_symbol'] . ' </span>';?></span></ins>
                        </span>
                        <?php if($item['item_visible'] == 1){?>
                            <div class="btn-group">
                                <span class="btn btn-link btn-cart call-function" data-callback="add_to_cart" data-item="<?php echo $item['id_item'];?>" title="Добавить в корзину">
                                    <i class="ca-icon ca-icon_shopping-cart text-danger"></i>
                                    <span>В корзину</span>
                                </span>
                            </div>
                        <?php } else{?>
                            <div class="btn-group">
                                <span class="btn btn-link btn-cart call-systmess" data-type="info" data-message="Нет в наличий" title="Нет в наличий">
                                    <i class="ca-icon ca-icon_shopping-cart"></i>
                                </span>
                            </div>
                        <?php }?>
                    <?php } else{?>
                        <span class="btn btn-danger btn-cart btn-block h-35 mb-15 call-popup" data-popup="#general-popup" data-href="<?php echo base_url('items/popup_forms/ask_price/'.$item['id_item']);?>">
                            Связаться с менеджером
                        </span>
                    <?php }?>
                </div>
                <h3><a href="<?php echo base_url('products/'.$item['item_url']);?>"><?php echo clean_output($item['item_title']);?></a></h3>
            </div>
        </div>
    </div>
</article>