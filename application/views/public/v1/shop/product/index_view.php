
    <?php 
        $priceColumn = getPriceColumn();
        $price = getPriceObject($product[$priceColumn['db_price_column']], [
            'tempAmount' => $product['item_temp_price']
        ]);
        $credit = getCredit($price['display_price'], $price['currency_symbol'], $product['item_zero_credit'], $product['item_zero_credit_months']);
    ?>
    <article class="product-single">
        <div class="row">
            <div class="product-header-wr col-xs-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <h2 class="product-title"><?php echo clean_output($product['item_title']);?></h2>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="product-code_wr">
                            <span class="product-code text-left-sm">Артикул: <?php echo $product['item_code'];?></span>
                            <span class="product-prog-id text-left-sm">ИД: <?php echo $product['item_prog_id'];?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-6">
                <?php if($this->lauth->is_admin()){?>
                    <div class="admin-tools_wr">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                <i class="ca-icon ca-icon_more "></i>
                            </button>
                            <ul class="dropdown-menu">
                                <?php if($this->lauth->have_right_or('edit_items,edit_items_simple,delete_items')){?>
                                    <li class="text-center">
                                        <div class="btn-group">
                                            <?php if($this->lauth->have_right_or('edit_items,edit_items_simple')){?>
                                                <a href="<?php echo base_url('admin/items/edit/'.$product['id_item'])?>" class="btn btn-primary" title="Редактировать">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            <?php }?>
                                            <?php if($this->lauth->have_right('delete_items')){?>
                                                <a href="#" class="btn btn-danger confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить товар?" data-callback="delete_item" data-item="<?php echo $product['id_item'];?>">
                                                    <i class="ca-icon ca-icon_remove"></i>
                                                </a>
                                            <?php }?>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                <?php }?>                        
                                <?php if($this->lauth->have_right('manage_items_marketing_fields')){?>
                                    <li class="text-center w-170">
                                        <div class="btn-group">
                                            <?php if($product['item_visible'] == 1){?>
                                                <a href="#" class="btn btn-primary btn-xs confirm-dialog" title="Запретить показ на сайте" data-message="Вы уверены что хотите запретить показ на сайте?" data-callback="change_marketing_status" data-marketing="visible" data-item="<?php echo $product['id_item'];?>">
                                                    <i class="ca-icon ca-icon_eye"></i>
                                                </a>
                                            <?php } else{?>
                                                <a href="#" class="btn btn-default btn-xs confirm-dialog" title="Установить показ на сайте" data-message="Вы уверены что хотите установить показ на сайте?" data-callback="change_marketing_status" data-marketing="visible" data-item="<?php echo $product['id_item'];?>">
                                                    <i class="ca-icon ca-icon_eye"></i>
                                                </a>
                                            <?php }?>
                                            <?php if($product['item_hit'] == 1){?>
                                                <a href="#" class="btn btn-primary btn-xs confirm-dialog" title="Снять признак Хит продаж" data-message="Вы уверены что хотите снять признак Хит продаж?" data-callback="change_marketing_status" data-marketing="hit" data-item="<?php echo $product['id_item'];?>">
                                                    <i class="ca-icon ca-icon_fire"></i>
                                                </a>
                                            <?php } else{?>
                                                <a href="#" class="btn btn-default btn-xs confirm-dialog" title="Установить признак Хит продаж" data-message="Вы уверены что хотите установить признак Хит продаж?" data-callback="change_marketing_status" data-marketing="hit" data-item="<?php echo $product['id_item'];?>">
                                                    <i class="ca-icon ca-icon_fire"></i>
                                                </a>
                                            <?php }?>
                                            <?php if($product['item_newest'] == 1){?>
                                                <a href="#" class="btn btn-primary btn-xs confirm-dialog" title="Снять признак Новинка" data-message="Вы уверены что хотите снять признак Новинка?" data-callback="change_marketing_status" data-marketing="newest" data-item="<?php echo $product['id_item'];?>">
                                                    <i class="ca-icon ca-icon_gift"></i>
                                                </a>
                                            <?php } else{?>
                                                <a href="#" class="btn btn-default btn-xs confirm-dialog" title="Установить признак Новинка" data-message="Вы уверены что хотите установить признак Новинка?" data-callback="change_marketing_status" data-marketing="newest" data-item="<?php echo $product['id_item'];?>">
                                                    <i class="ca-icon ca-icon_gift"></i>
                                                </a>
                                            <?php }?>
                                            <?php if($product['item_action'] == 1){?>
                                                <a href="#" class="btn btn-primary btn-xs confirm-dialog" title="Снять признак Акция" data-message="Вы уверены что хотите снять признак Акция?" data-callback="change_marketing_status" data-marketing="action" data-item="<?php echo $product['id_item'];?>">
                                                    <i class="ca-icon ca-icon_star-full"></i>
                                                </a>
                                            <?php } else{?>
                                                <a href="#" class="btn btn-default btn-xs confirm-dialog" title="Установить признак Акция" data-message="Вы уверены что хотите установить признак Акция?" data-callback="change_marketing_status" data-marketing="action" data-item="<?php echo $product['id_item'];?>">
                                                    <i class="ca-icon ca-icon_star-full"></i>
                                                </a>
                                            <?php }?>
                                            <?php if($product['item_popular'] == 1){?>
                                                <a href="#" class="btn btn-primary btn-xs confirm-dialog" title="Снять признак Популярный" data-message="Вы уверены что хотите снять признак Популярный?" data-callback="change_marketing_status" data-marketing="popular" data-item="<?php echo $product['id_item'];?>">
                                                    <i class="ca-icon ca-icon_like"></i>
                                                </a>
                                            <?php } else{?>
                                                <a href="#" class="btn btn-default btn-xs confirm-dialog" title="Установить признак Популярный" data-message="Вы уверены что хотите установить признак Популярный?" data-callback="change_marketing_status" data-marketing="popular" data-item="<?php echo $product['id_item'];?>">
                                                    <i class="ca-icon ca-icon_like"></i>
                                                </a>
                                            <?php }?>
                                            <?php if($product['item_commented'] == 1){?>
                                                <a href="#" class="btn btn-primary btn-xs confirm-dialog" title="Снять признак Обсуждаемый" data-message="Вы уверены что хотите снять признак Обсуждаемый?" data-callback="change_marketing_status" data-marketing="commented" data-item="<?php echo $product['id_item'];?>">
                                                    <i class="ca-icon ca-icon_papers"></i>
                                                </a>
                                            <?php } else{?>
                                                <a href="#" class="btn btn-default btn-xs confirm-dialog" title="Установить признак Обсуждаемый" data-message="Вы уверены что хотите установить признак Обсуждаемый?" data-callback="change_marketing_status" data-marketing="commented" data-item="<?php echo $product['id_item'];?>">
                                                    <i class="ca-icon ca-icon_papers"></i>
                                                </a>
                                            <?php }?>
                                            <?php if($product['item_on_home'] == 1){?>
                                                <a href="#" class="btn btn-primary btn-xs confirm-dialog" title="Снять признак Показать на главной" data-message="Вы уверены что хотите снять признак Показать на главной?" data-callback="change_marketing_status" data-marketing="on_home" data-item="<?php echo $product['id_item'];?>">
                                                    <i class="ca-icon ca-icon_home"></i>
                                                </a>
                                            <?php } else{?>
                                                <a href="#" class="btn btn-default btn-xs confirm-dialog" title="Установить признак Показать на главной" data-message="Вы уверены что хотите установить признак Показать на главной?" data-callback="change_marketing_status" data-marketing="on_home" data-item="<?php echo $product['id_item'];?>">
                                                    <i class="ca-icon ca-icon_home"></i>
                                                </a>
                                            <?php }?>
                                        </div>
                                    </li>
                                <?php }?>
                            </ul>
                        </div>
                    </div>
                <?php }?>
                <div class="product-labels">
                    <?php if($product['item_zero_credit'] == 1){?>
                        <span class="product_label label-bonus">Кредит 0%<i class="ca-icon ca-icon_label-end"></i></span>
                    <?php }?>
                    <?php if($product['item_cashback_price'] > 0 && $this->lauth->group_view_cashback()){?>
                        <span class="product_label label-bonus">+ Подарок<br/><?php echo numberFormat($product['item_cashback_price'], false);?> <span class="currency"><?php echo $price['currency_symbol'];?></span> <i class="ca-icon ca-icon_label-end"></i></span>            
                    <?php }?>
                    <?php if($product['item_action'] == 1){?>
                        <span class="product_label label-actional">Акция <i class="ca-icon ca-icon_label-end"></i></span>            
                    <?php }?>
                    <?php if($product['item_newest'] == 1){?>
                        <span class="product_label label-newest">Новинка <i class="ca-icon ca-icon_label-end"></i></span>            
                    <?php }?>
                    <?php if($product['item_hit'] == 1){?>
                        <span class="product_label label-hit">Хит <i class="ca-icon ca-icon_label-end"></i></span>
                    <?php }?>
                </div>
                <div class="product-carousel-wrapper">
                    <?php $slider_pager = '';?>
                    <ul class="bxslider product-carousel">
                        <li>
                            <a href="<?php echo base_url(getImage('files/items/'.$product['item_photo']));?>" data-fancybox="images" data-caption="<?php echo clean_output($product['item_title']);?>">
                                <img src="<?php echo base_url(getImage('files/items/thumb_500x500_'.$product['item_photo']));?>" alt="" />
                            </a>
                            <?php $slider_pager .= '<a class="page" data-slide-index="0" href=""><div class="img-container"><img src="'.base_url(getImage('files/items/thumb_500x500_'.$product['item_photo'])).'" /></div></a>';?>
                        </li>
                        <?php $other_protos = json_decode($product['item_photos']);?>
                        <?php if(!empty($other_protos)){?>
                            <?php foreach($other_protos as $key_photo => $other_photo){?>
                                <li>
                                    <a href="<?php echo base_url(getImage('files/items/'.$other_photo));?>" data-fancybox="images" data-caption="<?php echo clean_output($product['item_title']);?>">
                                        <img src="<?php echo base_url(getImage('files/items/thumb_500x500_'.$other_photo));?>" alt="" />
                                    </a>
                                    <?php $slider_pager .= '<a class="page" data-slide-index="'.($key_photo+1).'" href=""><div class="img-container"><img src="'.base_url(getImage('files/items/thumb_500x500_'.$other_photo)).'" /></div></a>';?>
                                </li>
                            <?php }?>
                        <?php }?>
                    </ul>
                    <div class="product-carousel-pages">
                        <?php echo $slider_pager;?>
                    </div>
                    <script>
                        $(function(){
                            $('.product-carousel').bxSlider({
                                adaptiveHeight: true,
                                pagerCustom: '.product-carousel-pages',
                                mode: 'fade',
                                touchEnabled:false,
                                controls: false,
                                onSliderLoad: function(){
                                    $(".product-carousel > li").css("visibility", "visible");
                                }
                            });
                        });
                    </script>
                </div>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-6">
                <div class="product-body">
                    <div class="showroom-wr">
                        <a class="showroom-order call-popup" href="#" data-popup="#general-popup" data-href="<?php echo base_url('showroom/popup_forms/order_showroom/'.$product['id_item']);?>">Заказать осмотр товара</a>
                    </div>
                    <div class="row row-xs">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <?php if($product['item_hide_price'] == 0){?>
                                <span class="price">
                                    <?php if($price['display_price_old'] > 0){?>
                                        <del>
                                            <span class="amount"><?php echo $price['display_price_old'].'<span class="currency">' . $price['currency_symbol'] . ' </span>';?></span>
                                        </del>                        
                                    <?php }?>
                                    <ins><span class="amount"><?php echo $price['display_price'].'<span class="currency">' . $price['currency_symbol'] . ' </span>';?></span></ins>

                                    <?php if($credit){?>
                                        <span class="credit"><?php echo $credit['credit_price_month'];?><span class="currency"><?php echo $credit['credit_currency'];?>/мес.</span></span>
                                        <div class="text-left">
                                            <?php if($product['item_zero_credit'] == 1){?>
                                                <strong class="text-danger">Кредит 0%</strong> / 
                                            <?php }?>
                                            <?php echo $credit['months'];?> мес. / <?php echo $credit['pre_pay'];?> лей аванс
                                        </div>
                                    <?php }?>
                                </span> 
                            <?php } else{?>
                                <span class="btn btn-danger btn-lg btn-cart mt-10 call-popup" data-popup="#general-popup" data-href="<?php echo base_url('items/popup_forms/ask_price/'.$product['id_item']);?>">
                                    Связаться с менеджером
                                </span>
                            <?php }?>                           
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="pt-15 hidden-xs"></div>
                            <?php if($product['item_guaranty'] > 0){?>
                                <div class="text-right text-left-xs">Гарантия: <?php echo $product['item_guaranty'];?> мес.</div>
                            <?php }?>
                            <div class="useful_links text-right text-left-xs mt-10">
                                <span class="ca-icon ca-icon_compare"></span>
                                <span class="ca-icon ca-icon_play-button"></span>
                                <span class="ca-icon ca-icon_like"></span>
                                <span class="product-share-btn ca-icon ca-icon_share" title="Поделиться">
                                    <div class="product-share">
                                        <ul class="share-list">
                                            <li class="share-list_item fb">
                                                <a href="#" class="share-list_item-link call-function" data-callback="popup_share" data-title="<?php echo clean_output($product['item_title']);?>" data-url="<?php echo current_url();?>" data-social="facebook">
                                                    <span class="fa fa-facebook share-list_item-link-icon"></span>
                                                </a>
                                            </li>
                                            <li class="share-list_item ok">
                                                <a href="#" class="share-list_item-link call-function" data-callback="popup_share" data-title="<?php echo clean_output($product['item_title']);?>" data-url="<?php echo current_url();?>" data-social="ok">
                                                    <span class="fa fa-odnoklassniki share-list_item-link-icon"></span>
                                                </a>
                                            </li>
                                            <li class="share-list_item vk">
                                                <a href="#" class="share-list_item-link call-function" data-callback="popup_share" data-title="<?php echo clean_output($product['item_title']);?>" data-url="<?php echo current_url();?>" data-social="vk">
                                                    <span class="fa fa-vk share-list_item-link-icon"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>                                
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php if($category['category_hide_price'] == 0){?>
                            <?php if($product['item_visible'] == 1){?>
                                <div class="col-xs-5 col-sm-6 mt-15 pr-8">
                                    <span class="btn btn-danger btn-lg btn-block btn-cart call-function" data-callback="add_to_cart" data-item="<?php echo $product['id_item'];?>">
                                        <i class="ca-icon ca-icon_shopping-cart"></i> <span class="hidden-xs">Купить</span>
                                    </span>
                                </div>
                                <div class="col-xs-7 col-sm-6 mt-15 pl-7">
                                    <?php if($credit){?>
                                        <span class="btn btn-default btn-lg btn-block btn-credit call-popup" data-popup="#credit_info-popup" data-href="<?php echo base_url('credit/popup_forms/credit_form/'.$product['id_item'].'?type=product');?>">
                                            Купить в кредит
                                        </span>                                
                                        <div  class="modal fade" id="credit_info-popup" data-keyboard="false" data-backdrop="static"></div>
                                    <?php }?>
                                </div>

                                <?php if($price['display_price'] >= 500 && 'MDL' === $price['currency_code']){?>
                                    <div class="col-xs-12">
                                        <a href="#" class="virtula-payment-btn" data-toggle="modal" data-target="#js-payment-virtula-modal">
                                            <span class="ca-icon ca-icon_piggy-bank text-danger"></span> 
                                            <span class="virtula-payment-btn--text">Оплата 3-мя частями без процента</span>
                                        </a>
                                        <div class="modal fade" id="js-payment-virtula-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog w-100pr mw-500">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                        <div class="modal-virtula-payment-details">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <div class="virtula-logo">
                                                                        <img src="<?php echo base_url(getImage($this->theme->public_assets('css/images/virtula-logo.png')));?>" alt="Virtula">
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <h1>Купить в 3 бесплатных платежа</h1>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <ul>
                                                                        <li>Добавить товар в корзину;</li>
                                                                        <li>Выберите метод оплаты <strong>"VIRTULA"</strong>;</li>
                                                                        <li>Создать учетную запись и подать заявку;</li> 
                                                                        <li>Оплати первый взнос непосредственно "ATEHNO" при получение товара , а оставшийся 2 платежа должны будут быть выплачены ежемесячно в <strong>"VIRTULA"</strong>.</li>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }?>
                            <?php }?>
                        <?php }?>
                    </div>
                    <?php if($product['item_cashback_price'] > 0 && $this->lauth->group_view_cashback()){?>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="cashback__item">
                                    <div class="image">
                                        <div class="img"></div>
                                    </div>
                                    <div class="cashback__body">
                                        <div class="amount">
                                            + <?php echo numberFormat($product['item_cashback_price'], false);?> <?php echo $price['currency_symbol'];?>
                                        </div>
                                        <div class="text">
                                            Подарок на следующую покупку, 
                                            <a href="<?php echo site_url('spage/order_gift');?>"> подробнее</a>...
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php $item_stocks = json_decode($product['item_stocks']);?>
                    <?php $item_suppliers = json_decode($product['item_suppliers'], true);?>
                    <?php if($this->lauth->group_view_stocks()){?>
                        <div class="row product-stocks-wr">
                            <div class="col-xs-12">
                                <div class="product-available_wr">
                                    <?php if($product['item_quantity'] > 0){?>
                                        <span class="product-available">В наличии</span>
                                    <?php } elseif(!empty($item_suppliers)){?>
                                        <span class="product-only-order">Под заказ</span>
                                    <?php } else{?>
                                        <span class="product-not-available">Нет в наличии</span>
                                    <?php }?>
                                </div>
                            </div>
                            <?php if(!empty($item_stocks) || !empty($item_suppliers)){?>
                                <div class="col-xs-12">
                                    <?php if(!empty($item_stocks)){?>
                                        <?php foreach($item_stocks as $item_stock){?>
                                            <div class="product-stocks_item">
                                                <span class="product-stocks_item-name">
                                                    <?php echo (!empty($stocks[$item_stock->stock_prog_id]['stock_name_display']))?$stocks[$item_stock->stock_prog_id]['stock_name_display']:$stocks[$item_stock->stock_prog_id]['stock_name'];?>
                                                </span>
                                                <div class="product-stocks_item-quantity">
                                                    <?php $stock_options = $this->lauth->group_get_stock_setting($item_stock->quantity);?>
                                                    <?php for($i=1;$i<=10;$i++){?>
                                                        <?php if(!empty($stock_options) && $stock_options['lines'] >= $i){?>
                                                            <span style="background-color:<?php echo $stock_options['color'];?>"></span>                                            
                                                        <?php } else{?>
                                                            <span></span>                                            
                                                        <?php }?>
                                                    <?php }?>
                                                </div>
                                            </div>                                
                                        <?php }?>
                                    <?php } else{?>
                                        <div class="product_supplier">Срок доставки: <?php echo $item_suppliers[0]['from'];?> - <?php echo $item_suppliers[0]['to'];?> дня.</div>
                                    <?php }?>
                                </div>
                                <div class="col-xs-12">
                                    <div class="product-stocks-hr"></div>
                                </div>
                            <?php }?>
                        </div>
                    <?php } else{?>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="product-available_wr">
                                    <?php if($product['item_visible'] == 1){?>
                                        <span class="product-available">В наличии</span>
                                    <?php } else{?>
                                        <span class="product-not-available">Нет в наличии</span>
                                    <?php }?>
                                </div>
                            </div>
                            <?php if($product['item_visible'] == 1){?>
                                <div class="col-xs-12">
                                    <div class="product-delivery-block">
                                        <div class="product-delivery-block--header">Доставка по адресу:</div>
                                        <div class="product-delivery-block--info">
                                            Срок доставки до
                                            <?php $_at_city = get_cookie('_at_city');?>
                                            <?php $_city = Modules::run('cities/_get_city', (int) $_at_city);?>
                                            <?php if(empty($_city)){?>
                                                <a href="#" class="city-select" data-toggle="modal" data-target="#cities-modal">
                                                    <span class="city_name">Выберите ваш город</span>
                                                </a>
                                                ... дней
                                            <?php } else{?>
                                                <a href="#" class="city-select" data-toggle="modal" data-target="#cities-modal">
                                                    <span class="city_name"><?php echo $_city['city_name'];?></span>
                                                </a>
    
                                                <?php 
                                                    $_hour = date('G');
                                                    $_in_city_stock = false;
                                                    if(!empty($item_stocks)){
                                                        foreach ($item_stocks as $_item_stock) {
                                                            if($stocks[$_item_stock->stock_prog_id]['stock_city'] == $_at_city){
                                                                $_in_city_stock = true;
                                                            }
                                                        }
                                                    }
                                                ?>
                                                <?php if(!empty($item_stocks)){?>
                                                    <?php if($_in_city_stock){?>
                                                        <?php if($_hour <= 17){?>
                                                            &mdash; В день заказа.
                                                        <?php } else{?>
                                                            &mdash; На следующий день.
                                                        <?php }?>
                                                    <?php } else{?>
                                                        <?php echo "{$_city['city_delivery_days']}" . ' ' .plural_numword($_city['city_delivery_days'], 'день', array('2_4' => 'дня', '5_' => 'дней'));?>.
                                                    <?php }?>
                                                <?php } elseif(!empty($item_suppliers)){?>
                                                    <?php $_delivery_days_from =  $item_suppliers[0]['from'] + $_city['city_delivery_days'];?>
                                                    <?php $_delivery_days_to =  $item_suppliers[0]['to'] + $_city['city_delivery_days'];?>
                                                    
                                                    <?php if($_delivery_days_from == $_delivery_days_to){?>
                                                        <?php echo "{$_delivery_days_to}" . ' ' .plural_numword($_delivery_days_to, 'день', array('2_4' => 'дня', '5_' => 'дней'));?>.
                                                    <?php } else{?>
                                                        <?php echo "{$_delivery_days_from} &mdash; {$_delivery_days_to}" . ' ' .plural_numword($_delivery_days_to, 'день', array('2_4' => 'дня', '5_' => 'дней'));?>.
                                                    <?php }?>
                                                <?php }?>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                                <?php if(!empty($item_suppliers) || !empty($item_stocks)){?>
                                    <div class="col-xs-12">
                                        <div class="product-taking-block">
                                            <div class="product-taking-block--header">Самовывоз из магазина:</div>
                                            <ul class="product-taking-block--list">
                                                <?php if(!empty($item_suppliers)){?>
                                                    <?php $_suppliers_available = false;?>
                                                    <?php foreach($item_suppliers as $item_supplier){?>
                                                        <?php if($item_supplier['quantity'] > 0){$_suppliers_available = true;}?>
                                                    <?php }?>
                                                    <li class="<?php if($_suppliers_available){echo 'available';}?>">
                                                        <span>Склады компании</span>
                                                    </li>
                                                <?php }?>
    
                                                <?php if(!empty($item_stocks)){?>
                                                    <?php foreach($item_stocks as $item_stock){?>
                                                        <li class="<?php if($item_stock->quantity > 0){echo 'available';}?>">
                                                            <span><?php echo $stocks[$item_stock->stock_prog_id]['stock_name_display'];?></span>
                                                        </li>                             
                                                    <?php }?>
                                                <?php }?>
                                            </ul>
                                        </div>
                                    </div>
                                <?php }?>
                            <?php }?>
                        </div>
                    <?php }?>
                    <div class="row row-xs">
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="product-delivery_wr">
                                <span class="title-b">
                                    <span class="ca-icon ca-icon_shopping-bag"></span>
                                    Самовывоз<br>из магазина
                                </span>
                                <p class="description-b">Закажи на сайте заберите сегодня</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="product-delivery_wr">
                                <span class="title-b">
                                    <span class="ca-icon ca-icon_small-truck"></span>
                                    Доставка<br>по адресу
                                </span>
                                <p class="description-b">Доставляем в любую точку Молдовы</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="product-delivery_wr">
                                <span class="title-b">
                                    <span class="icon nova-poshta"></span>
                                    Новая<br>Почта
                                </span>
                                <p class="description-b">Без комиссии за наложенный платеж</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php echo Modules::run("properties/_get_product_properties",array('id_item' => $product['id_item'])); ?>

            <?php if(!empty($product['item_description'])){?>
                <?php $item_description_column = empty($settings['show_cleaned_description']['setting_value']) || empty($product['item_description_cleaned']) ? 'item_description' : 'item_description_cleaned';?>
                <div class="col-xs-12">
                    <h2 class="heading-title"><span>Описание</span></h2>
                    <div class="item_description-wr">
                        <?php echo $product[$item_description_column];?>
                    </div>
                    <script>
                        $(function(){
                            $('.item_description-wr table').addClass('table table-striped table-bordered');
                        });
                    </script>
                </div>                   
            <?php }?>

            <?php if(!empty($product['item_video_code'])){?>
                <div class="col-xs-12">
                    <h2 class="heading-title"><span>Видео</span></h2>
                    <div class="video-wr">
                        <?php echo generate_video_html($product['item_video_code'], $product['item_video_source'],940,529);?>
                    </div>
                </div>                   
            <?php }?>
            
            <div class="col-xs-12">
                <p><i>*Сопровождающие товар фотографии и описание может отличаться от оригинала. Производитель может изменять дизайн, технические характеристики и комплектацию изделия без уведомления об этом продавца. Уточняйте важные для Вас параметры при заказе.</i></p>
            </div>

            <div class="col-xs-12">
                <h2 class="heading-title">
                    <span>Отзывы</span>
                    <span class="absolute-b_i r-0 pr-0_i">
                        <button class="btn btn-danger pull-right" data-fancybox data-type="ajax" data-src="<?php echo base_url();?>items/popup_forms/add_comment/<?php echo $product['id_item'];?>" href="javascript:;"><i class="ca-icon ca-icon_reviews"></i><b class="hidden-xs"> Написать отзыв</b></button>
                    </span>
                </h2>
                <div class="row">
                    <div class="col-xs-12">
                        <div id="reviews">
                            <?php $comments = Modules::run('items/_get_comments', array('id_item' => $product['id_item']))?>
                            <div class="comments">
                                <?php if(!empty($comments)){?>
                                    <?php foreach($comments as $comment){?>
                                        <?php $this->load->view($this->theme->public_view('shop/product/comments/item_view'), array('comment' => $comment));?>
                                    <?php }?>
                                <?php } else{?>
                                    <p class="empty-comments fs-14">Будьте первым &mdash; напишите ваш отзыв об зтом товаре!</p>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
    <article class="pl-15 pr-15 pb-85">
        <?php if(!empty($same_products)){?>
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="heading-title"><span>Похожие товары</span></h2>
                    <div class="row grid products_block pl-15 pr-15">
                        <?php foreach($same_products as $same_product){?>
                            <!-- PRODUCT - START -->
                            <div class="col-md-3 col-sm-4 col-xs-6 pl-0 pr-0">
			                    <?php $this->load->view($this->theme->public_view('shop/catalog/product_cart_view'), array('item' => $same_product));?>
                            </div>
                            <!-- PRODUCT - END -->               
                        <?php }?>
                    </div>
                </div>
            </div>
        <?php }?>
    </article>