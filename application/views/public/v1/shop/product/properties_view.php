<?php if(!empty($properties)){?>
    <div class="col-xs-12">
        <h2 class="heading-title"><span>Характеристики</span></h2>
        <div class="item_properties-wr">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Свойство</th>
                        <th>Значение</th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach($properties as $property){?>                
                        <?php $property_values = json_decode($property['property_values'], true);?>
                        <tr>
                            <td scope="row">
                                <?php echo $property['title_property']; if($property['type_property'] == 'range'){ echo ', '.$property_values['unit'];}?>
                            </td>
                            <td><?php echo implode(', ', $property['item_values']);?></td>
                        </tr>
                    <?php }?>
                </tbody>
            </table>
        </div>
    </div>
<?php }?>