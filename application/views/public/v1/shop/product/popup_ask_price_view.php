<div class="modal-dialog mw-335" role="document">
    <div class="modal-content">
        <form id="credit_form-popup">			
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="credit-header text-left">Связаться с менеджером</h3>
                <div class="credit-header-subtext text-left">для покупки или консультации по цене</div>
                <div class="row mt-15">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="ca-icon ca-icon_user"></i></span>
                                <input type="text" class="form-control" name="username" placeholder="Имя">                            
                            </div>
                        </div>                    
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="ca-icon ca-icon_phone"></i></span>
                                <input type="text" class="form-control" name="phone" placeholder="Телефон">
                            </div>
                        </div>                    
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="ca-icon ca-icon_envelope"></i></span>
                                <input type="text" class="form-control" name="email" placeholder="Email">
                            </div>
                        </div>                    
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <?php echo $recaptcha_widget;?>
                        </div>               
                    </div>
                </div>
            </div>
            <div class="modal-footer text-left">
                <input type="hidden" name="item" value="<?php echo $id_item;?>">
                <span class="btn btn-danger call-function" data-callback="ask_price_callback">Отправить запрос</span>
            </div>
        </form>
    </div>
</div>
<script src='https://www.google.com/recaptcha/api.js'></script>

<script>
    var ask_price_callback = function(btn){
        var $this = $(btn);
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: base_url+'items/ajax_operations/ask_price',
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general-popup').modal('hide').html("");
                }
            }
        });
        return false;
    }
</script>