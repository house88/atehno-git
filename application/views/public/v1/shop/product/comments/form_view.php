<div class="row mw-600">
    <div class="col-xs-12">
        <h4 class="modal-title">Написать отзыв</h4>
        <form class="comment-form">
            <p class="comment-notes"><span id="email-notes">Ваш email не будет опубликован.</span> Поля помеченые <span class="required">*</span> обязательны.</p>
            <div class="row">
                <div class="form-group comment-form-author col-sm-6">
                    <label for="author">Имя <span class="required">*</span></label> 
                    <input class="form-control" name="author" type="text" value="" placeholder="Имя">
                </div>
                <div class="form-group comment-form-email col-sm-6">
                    <label for="email">Email<span class="required">*</span></label> 
                    <input class="form-control" name="email" type="email" value="" placeholder="Email">
                </div>
                <div class="form-group comment-form-comment col-xs-12">
                    <label for="comment">Коментарий<span class="required">*</span></label>
                    <textarea class="form-control" name="comment" placeholder="Коментарий"></textarea>
                </div>
            </div>
            <div class="form-group comment-form-comment">
                <label for="comment">Подтвердите что вы не робот<span class="required">*</span></label>                            
                <?php echo $recaptcha_widget;?>
            </div>    
            <div class="form-group comment-form-comment">
                <input type="hidden" name="item" value="<?php echo $product['id_item'];?>">
                <button class="btn btn-danger call-function" data-callback="add_comment" type="button"><i class="fa fa-check"></i>Отправить</button>
            </div>    
        </form>
    </div>
</div>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
    var add_comment = function(btn){
        var $this = $(btn);
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: base_url+'items/ajax_operations/add_comment',
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                showLoader($form);
            },
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader($form);
                if(resp.mess_type == 'success'){
                    $('.tab-pane#reviews .comments').append(resp.comment);
                    if($('.tab-pane#reviews .comments > .empty-comments').length > 0){
                        $('.tab-pane#reviews .comments > .empty-comments').remove();
                    }
                    $.fancybox.close();
                }
            }
        });
        return false;
    }
</script>