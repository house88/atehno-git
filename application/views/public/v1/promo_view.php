<!DOCTYPE html>
<html lang="ru">
<head>
    <?php $this->load->view($this->theme->public_view('includes/head')); ?>
</head>
<body class="bg-grey">	
    <?php $this->load->view($this->theme->public_view('includes/snowflake')); ?>
	<div class="page-wrap">
		<?php $this->load->view($this->theme->public_view('includes/header')); ?>

		<?php $id_banner = (int) $promo['promo_banner'];?>
		<?php if($id_banner > 0){?>
			<?php $promo_banner = Modules::run('banners/_get', $id_banner);?>
			<?php if(!empty($promo_banner)){?>
				<?php $banner_photos = !empty($promo_banner['banner_data']) ? json_decode($promo_banner['banner_data'], true) : array();?>
				<?php if(!empty($banner_photos)){?>
					<section id="wr-banner" class="pb-0_i">
						<div class="wr-banner-home">	
							<ul class="bxslider home_top-slider">
								<?php foreach($banner_photos as $banner_photo){?>
									<?php if((int)@$banner_photo['active'] == 1){?>
										<li style="background-color:<?php echo !empty($banner_photo['bg_color']) ? $banner_photo['bg_color'] : '#ffffff';?>">
											<?php if(!empty($banner_photo['link'])){?>
												<a href="<?php echo $banner_photo['link'];?>">
													<img src="<?php echo site_url('files/banners/' . $promo_banner['banner_tocken'] . '/' . $banner_photo['photo']);?>"/>
												</a>
											<?php } else{?>
												<img src="<?php echo site_url('files/banners/' . $promo_banner['banner_tocken'] . '/' . $banner_photo['photo']);?>"/>
											<?php }?>
										</li>
									<?php }?>
								<?php }?>
							</ul>
						</div>
					</section>
				<?php }?>
			<?php }?>
		<?php }?>

		<section id="wr-body">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php $promo_settings = json_decode($promo['promo_settings'], true);?>
						<div class="row">
							<?php if(!empty($promo_settings)){?>
								<div class="col-xs-12 bg-white">
									<div class="promo-categories__slider">
										<?php $categories_links = array();?>
										<?php foreach($promo_settings as $category_key => $category){?>
											<!-- SLIDE - START -->
											<a class="slide <?php echo (isset($selected_category) && $category_key == $selected_category)?'active':'';?>" href="<?php echo site_url('promo/'.$promo['promo_url'].'/category/'.encript_uri_hash($category_key));?>">
												<div class="promo-categories__slider-image">
													<img src="<?php echo site_url('files/uploads/'.$category['photo']);?>" alt="<?php echo $category['name'];?>">                        
												</div>
												<div class="promo-categories__slider-title"><?php echo $category['name'];?></div>
											</a>
											<!-- SLIDE - END -->
											<?php if(!empty($category['children'])){?>
												<?php foreach($category['children'] as $sub_category_key => $sub_category){?>
													<?php $categories_links[] = '<a href="'.site_url('promo/'.$promo['promo_url'].'/category/'.encript_uri_hash($category_key.':'.$sub_category_key)).'" class="promo-categories__link '.((isset($selected_category) && $category_key == $selected_category) ? 'current' : '').' '.((isset($selected_category_child) && $sub_category_key == $selected_category_child)?'active':'').'" data-parent="'.$category_key.'">'.$sub_category['name'].'</a>';?>
												<?php }?>
											<?php }?>
										<?php }?>
									</div>
									
									<script>
										var slideIndex = 1;
										$(function(){
											slideIndex = $(".promo-categories__slider a.slide.active").index();

											$('.promo-categories__slider').slick({
												infinite: false,
												speed: 300,
												slidesToShow: 8,
												slidesToScroll: 1,
												prevArrow:'<a class="slick-slide-prev" href=""><i class="ca-icon ca-icon_arrow-left "></i></a>',
												nextArrow:'<a class="slick-slide-next" href=""><i class="ca-icon ca-icon_arrow-right "></i></a>',
												responsive: [
													{
														breakpoint: 1024,
														settings: {
															slidesToShow: 6,
															slidesToScroll: 1,
															infinite: true
														}
													},
													{
														breakpoint: 768,
														settings: {
															slidesToShow: 4,
															slidesToScroll: 1
														}
													},
													{
														breakpoint: 480,
														settings: {
															slidesToShow: 3,
															slidesToScroll: 1
														}
													},
													{
														breakpoint: 425,
														settings: {
															slidesToShow: 2,
															slidesToScroll: 1
														}
													}
												]
											});
											
											$('.promo-categories__slider').slick('slickGoTo', intval(slideIndex), true);
										});
									</script>

									<div class="promo-categories__links">
										<?php echo implode('', $categories_links);?>
									</div>
								</div>
							<?php }?>
							<?php if(!empty($selected_category_breadcrumb)){?>
								<div class="col-xs-12">
									<div class="breadcrumbs ">
										<ul class="breadcrumbs__list">
											<li class="breadcrumbs__item">
												<a class="breadcrumbs__lbl" href="<?php echo site_url('promo/'.$promo['promo_url']);?>">
													<span class="ca-icon ca-icon_home"></span>
												</a>
											</li>
											<?php foreach($selected_category_breadcrumb as $promo_breadcrumb){?>
												<li class="breadcrumbs__item">
													<a class="breadcrumbs__lbl" href="<?php echo $promo_breadcrumb['link'];?>">
														<span><?php echo $promo_breadcrumb['name'];?></span>
													</a>
												</li>
											<?php }?>
										</ul>
									</div>
								</div>
							<?php }?>
						</div>

						<div class="row products-page-wr bg-white pt-15">
							<div class="col-xs-12">
								<div id="catalog-wr" class="pl-15 pr-15">
									<div class="row products-list products_block grid" id="products">
										<?php if(!empty($products)){?>
											<?php foreach($products as $item){?>
												<!-- PRODUCT - START -->
												<div class="col-xs-6 col-sm-4 col-md-3 pl-0 pr-0">
													<?php $this->load->view($this->theme->public_view('shop/catalog/product_cart_view'), array('item' => $item));?>
												</div>
												<!-- PRODUCT - END -->
											<?php }?>
										<?php } else{?>
											<div class="col-xs-12">
												<div class="alert alert-info mb-0" role="alert">Товары не найдены!</div>
											</div>
										<?php }?>
									</div>
									<div class="row">
										<div class="col-xs-12 pl-0 pr-0">
											<div class="pagination-wrapper">
												<?php if($pagination != ''){?>
														<?php echo $pagination;?>
												<?php }?>	
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

    	<?php $this->load->view($this->theme->public_view('includes/footer')); ?>
	</div>
</body>
</html>
