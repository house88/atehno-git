<?php if(!empty($concurs)){?>
    <div class="col-xs-12">
        <!-- <h2 class="heading-title"><span><?php echo $concurs['concurs_title'];?></span></h2> -->
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#concurs_participants" aria-controls="concurs_participants" role="tab" data-toggle="tab">Участники</a>
            </li>
            <li role="presentation">
                <a href="#concurs_text" aria-controls="concurs_text" role="tab" data-toggle="tab">Описание</a>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="concurs_participants">
                <div class="mt-15 mb-15">
                    <div class="row">
                        <?php if($this->lauth->logged_in()){?>
                            <?php if(!$i_uploaded_photo){?>
                                <div class="col-xs-4 col-sm-3 col-md-2">
                                    <div class="square-b">
                                        <div class="square_content">
                                            <div class="concurs_btn_upload">
                                                <div class="btn-content">
                                                    <div class="icon">
                                                        <span class="ca-icon ca-icon_photo-camera"></span>
                                                    </div>
                                                    <span class="btn-text">Добавить фото</span>
                                                </div>
                                                <input id="select_concurs_photo" type="file" name="userfile">
                                            </div>
                                            <div class="concurs_uploaded_photo-wr"></div>
                                        </div>
                                    </div>
                                </div>
                            <?php }?>
                        <?php } else{?>
                            <div class="col-xs-4 col-sm-3 col-md-2">
                                <div class="square-b">
                                    <div class="square_content">
                                        <div class="concurs_btn_upload call-systmess" data-message="Вы не авторизированы." data-type="info">
                                            <div class="btn-content">
                                                <div class="icon">
                                                    <span class="ca-icon ca-icon_photo-camera"></span>
                                                </div>
                                                <span class="btn-text">Добавить фото</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                        <?php if(!empty($concurs_users)){?>
                            <?php foreach($concurs_users as $concurs_user){?>
                                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <div class="square-b">
                                        <div class="square_content">
                                            <div class="concurs_user_photo-wr">
                                                <div class="concurs_user_photo-item" style="background-image:url('<?php echo site_url('files/concurs/'.$concurs_user['id_concurs'].'/thumb_500x500_'.$concurs_user['concurs_photo']);?>');"></div>
                                                <div class="concurs_user_photo-vote-wr">
                                                    <a href="<?php echo site_url('files/concurs/'.$concurs_user['id_concurs'].'/'.$concurs_user['concurs_photo']);?>" class="concurs_user_photo-btn" data-fancybox="images" data-title="Полный размер" title="Полный размер">
                                                        <span class="ca-icon ca-icon_image-zoom"></span>
                                                    </a>
                                                    <?php if(!empty($user_vote) && $user_vote['id_relation'] != $concurs_user['id_relation']){?>
                                                        <a href="#" class="concurs_user_photo-btn confirm-dialog" data-message="Вы уже голосовали за другого участника. Вы хотите отменить предыдуший голос и проголосовать за этого участника?" data-dtype="danger" data-callback="concurs_vote" title="Проголосовать" data-title="Проголосовать" data-vote="<?php echo $concurs_user['id_relation'];?>">
                                                            <span class="ca-icon ca-icon_hand-up"></span> <?php echo $concurs_user['votes'];?>
                                                        </a>                                                    
                                                    <?php } elseif(!empty($user_vote) && $user_vote['id_relation'] == $concurs_user['id_relation']){?>
                                                        <div class="concurs_user_photo-btn" title="Проголосовано" data-title="Проголосовано">
                                                            <span class="ca-icon ca-icon_ok"></span> <?php echo $concurs_user['votes'];?>
                                                        </div>
                                                    <?php } elseif($concurs_user['id_user'] == $this->lauth->id_user()){?>
                                                        <div class="concurs_user_photo-btn" title="Это ваша фотография" data-title="Это ваша фотография">
                                                            <span class="ca-icon ca-icon_user"></span> <?php echo $concurs_user['votes'];?>
                                                        </div>
                                                    <?php } else{?>
                                                        <a href="#" class="concurs_user_photo-btn call-function" data-callback="concurs_vote" title="Проголосовать" data-title="Проголосовать" data-vote="<?php echo $concurs_user['id_relation'];?>">
                                                            <span class="ca-icon ca-icon_hand-up"></span> <?php echo $concurs_user['votes'];?>
                                                        </a>
                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            
                            <?php }?>
                        <?php }?>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="concurs_text">
                <div class="mt-15 mb-15">
                    <?php echo $concurs['concurs_text'];?>
                </div>
            </div>
        </div>
    </div>
    <script>
        var id_concurs = intval('<?php echo $concurs['id_concurs'];?>');
    </script>
    <script src="<?php echo file_modification_time('theme/accent/js/concurs.scripts.js');?>"></script>
<?php } else{?>
    <div class="alert alert-info mb-0" role="alert">
        Нет данных!
    </div>
<?php }?>